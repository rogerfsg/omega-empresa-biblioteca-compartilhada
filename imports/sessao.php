<?php
//print_r($_COOKIE);
Helper::sessionStart();

$nomeDoScript = Helper::getNomeDoScriptAtual();

Registry::add(new Seguranca());
Registry::add(new HelperLog());

$objSeguranca = Registry::get('Seguranca');
$autenticado = $objSeguranca->isAutenticado();

//$autenticado = true;

if ($nomeDoScript == "actions.php")
{
    $class = Helper::POSTGET("class");
    $action = Helper::POSTGET("action");

    if ($class == null || $action == null)
    {
        throw new InvalidArgumentException("Parametro action deve ser preenchido!");
    }
    else
    {
        if ($objSeguranca->isAcaoRestrita($class, $action) && !$autenticado)
        {
            Helper::imprimirCabecalhoParaFormatarAction();
            Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO_ACAO, MENSAGEM_ERRO);
            exit();
        }
    }
}
else
{
    if ($nomeDoScript == "webservice.php")
    {
        $class = Helper::POSTGET("class");
        $action = Helper::POSTGET("action");

        if ($class == null || $action == null)
        {
            throw new InvalidArgumentException("Parametro action deve ser preenchido!");
        }
        else
        {
            $objSeguranca = Registry::get('Seguranca');
            if ($objSeguranca->isAcaoRestrita($class, $action) && !$autenticado)
            {
                echo "{\"mCodRetorno\": -71, \"mMensagem\": \"A��o restrita\"  }";
                exit();
            }
        }
    }
    else
    {
        $pagina = Helper::POSTGET("page");
        $pagina = $pagina == null ? "" : $pagina;

        $objSeguranca = Registry::get('Seguranca');
        if ($objSeguranca->isPaginaRestrita($pagina))
        {
            if (!$autenticado)
            {
                $objSeguranca->__actionLogout("Fa�a o Login para continuar.");
                return;
            }
        }
    }
}

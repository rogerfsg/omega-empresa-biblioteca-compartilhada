<?php

    header("Content-type: text/html; charset=iso-8859-1", true);

    include_once '../languages/pt-br.php';
    include_once 'constants.php';
    include_once 'database_config.php';
    include_once 'funcoes.php';

    class PagSeguroNpi
    {

        private $timeout = 20; // Timeout em segundos

        public function notificationPost()
        {
            $postdata = 'Comando=validar&Token=' . TOKEN_PAGSEGURO;
            foreach ($_POST as $key => $value)
            {
                $valued = $this->clearStr($value);
                $postdata .= "&$key=$valued";
            }

            return $this->verify($postdata);
        }

        private function clearStr($str)
        {
            if (!get_magic_quotes_gpc())
            {
                $str = addslashes($str);
            }

            return $str;
        }

        private function verify($data)
        {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = trim(curl_exec($curl));
            curl_close($curl);

            return $result;
        }

    }

    $objBanco = new Database();

    if (count($_POST) > 0)
    {
        $npi = new PagSeguroNpi();
        $result = $npi->notificationPost();

        if ($result == "VERIFICADO")
        {
            if (strlen($_POST["TransacaoID"]) && is_numeric($_POST["Referencia"]))
            {
                $objCrypt = new Crypt();

                $idCobrancaNoSistema = $_POST["Referencia"];
                $idTransacao = $_POST["TransacaoID"];
                $statusAlfanumerico = $_POST["StatusTransacao"];
                $formaDePagamento = $_POST["TipoPagamento"];
                $numeroDeParcelas = $_POST["Parcelas"];
                $dadosPagSeguro = addslashes(serialize($_POST));

                $objCobranca = new EXTDAO_Cobranca();

                $objCobranca->setId_transacao_pag_seguro($idTransacao);
                $objCobranca->setStatus_pag_seguro($statusAlfanumerico);
                $objCobranca->setTipo_pagamento_pag_seguro($formaDePagamento);
                $objCobranca->setParcelas_pag_seguro($numeroDeParcelas);
                $objCobranca->setDados_retorno_pag_seguro($dadosPagSeguro);
                $objCobranca->formatarParaSQL();

                $arrUpdate["id_transacao_pag_seguro"] = true;
                $arrUpdate["status_pag_seguro"] = true;
                $arrUpdate["tipo_pagamento_pag_seguro"] = true;
                $arrUpdate["parcelas_pag_seguro"] = true;
                $arrUpdate["dados_retorno_pag_seguro"] = true;

                $objCobranca->update($idCobrancaNoSistema, $arrUpdate, "");

                if ($statusAlfanumerico === 'Aguardando Pagamento')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "1");
                }
                elseif ($statusAlfanumerico === 'Em Análise')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "2");
                }
                elseif ($statusAlfanumerico === 'Aprovado')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "3");
                }
                elseif ($statusAlfanumerico === 'Em Disputa')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "4");
                }
                elseif ($statusAlfanumerico === 'Devolvido')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "5");
                }
                elseif ($statusAlfanumerico === 'Cancelado')
                {
                    $objCobranca->alterarStatus($idCobrancaNoSistema, "6");
                }
            }
        }
    }

?>

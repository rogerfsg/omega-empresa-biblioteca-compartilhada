<?php



    class ConfiguracaoCorporacaoDinamica
    {

        public static function getParametroCorporacao()
        {
            $corporacao = null;
            if (isset($_GET["corporacao"]))
            {
                $corporacao = $_GET["corporacao"];
                setcookie("corporacao", $corporacao, time() + (86400 * 30), "/");
            }
            else
            {
                if (isset($_POST["corporacao"]))
                {
                    $corporacao = $_POST["corporacao"];
                    //30 dias
                    setcookie("corporacao", $corporacao, time() + (86400 * 30), "/");
                }
                else
                {
                    if (isset($_COOKIE["corporacao"]))
                    {
                        $corporacao = $_COOKIE["corporacao"];
                    }
                }
            }

            return $corporacao;
        }

        public static function init($funcoesSemCache = null)
        {
            $pathAbsoluto = str_replace("\\", "/", __DIR__);
            $pathRelativo = str_replace($_SERVER["DOCUMENT_ROOT"], "", $pathAbsoluto);

            $pathRelativoRaiz = substr($pathRelativo, 0, strrpos($pathRelativo, "recursos"));

            $corporacao = ConfiguracaoCorporacaoDinamica::getParametroCorporacao();

            if (empty($corporacao))
            {
                //    Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB);
                if (Helper::isWebserviceFile())
                {
                    echo "{\"mCodRetorno\":1,\"mMensagem\":\"Parametro invalido corporacao indefinda.\"}";
                    exit();
                }
                else
                {


                    header("Location: ".__DOMINIO_DE_ACESSO_SICOB."/client_area/login.php");
                    exit();

                }
            }
            else
            {
                if (!defined('REDIS_PORT'))
                {
                    $nomeMaquina = strtoupper(php_uname("n"));

                    if ($nomeMaquina == "LI1047-176.MEMBERS.LINODE.COM")
                    {
                        define('REDIS_SCHEME', "tcp");
                        define('REDIS_HOST', "database1.workoffline.com.br");
                        define('REDIS_PORT', 7333);
                    } else {
                        define('REDIS_SCHEME', "tcp");
                        define('REDIS_HOST', "127.0.0.1");
                        define('REDIS_PORT', 6379);
                    }

                }

                $msg = ConfiguracaoCorporacaoDinamica::factoryConfiguracaoSite($corporacao, true, $funcoesSemCache);
                if ($msg == null)
                {
                    if (Helper::isWebserviceFile())
                    {
                        echo "{\"mCodRetorno\":1,\"mMensagem\":\"Falha ao recuperar os dados de acesso da corporacao '{$corporacao}'.\"}";
                        exit();
                    }
                    else
                    {
                        throw new Exception("Falha ao recuperar os dados de acesso da corporacao $corporacao");
                    }
                }
                else
                {
                    if (!Interface_mensagem::checkOk($msg))
                    {
                        if (Helper::isWebserviceFile())
                        {
                            echo json_encode($msg);
                            exit();
                        }
                        else
                        {
                            throw new Exception("Falha ao recuperar os dados de acesso da corporacao $corporacao");
                        }
                    }
                }

                ConfiguracaoCorporacaoDinamica::daCordaSincronizadorSeNecessario();
            }
        }

        public static function daCordaSincronizadorSeNecessario()
        {
            if (Helper::POSTGET("da_corda_sincronizador") == "true")
            {
                $idCorporacao = Helper::POSTGET("id_corporacao");
                $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

                if (!empty($idCorporacao) && !empty($idSistemaSihop))
                {
                    $helperProcesso = new HelperProcesso($idSistemaSihop);
                    $helperProcesso->addItemProc0($idCorporacao);
                }
            }
        }

        public static function clearConfiguracaoSiteFromCache($corporacao)
        {
            try
            {
                $keyRedisConfiguracaoSite = ConfiguracaoCorporacaoDinamica::getChaveRedis($corporacao);
                $redis = ConfiguracaoCorporacaoDinamica::factoryRedis();
                $redis->del($keyRedisConfiguracaoSite);

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            catch (Exception $ex)
            {
                return new Mensagem(null, null, $ex);
            }
        }

        private static function getChaveRedis($corporacao)
        {
            $keyRedisConfiguracaoSite = PROJETO . "Conf{$corporacao}";

            return $keyRedisConfiguracaoSite;
        }

        private static function factoryRedis()
        {
            $redis = new HelperRedis(HelperRedis::PREFIXO_REDIS_COMUM);

            return $redis;
        }

        public static function factoryConfiguracaoSite($corporacao, $usarCache = true, $funcoesSemCache = null)
        {
            $keyRedisConfiguracaoSite = ConfiguracaoCorporacaoDinamica::getChaveRedis($corporacao);

            if ($usarCache)
            {
                if ($funcoesSemCache != null)
                {
                    $action = Helper::POSTGET("action");
                    if ($action != null
                        && array_search($action, $funcoesSemCache) != false)
                    {
                        $usarCache = false;
                    }
                }
            }

            $jsonDados = null;
            $redis = ConfiguracaoCorporacaoDinamica::factoryRedis();

            if ($usarCache)
            {
                $jsonDados = $redis->get($keyRedisConfiguracaoSite);
            }

            $configuracaoSite = null;
            if (empty($jsonDados))
            {
                $msg = BOSihopComum::getConfiguracaoSite($corporacao);
                if ($msg == null)
                {
                    return null;
                }
                else
                {
                    if ($msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                    {
                        return $msg;
                    }
                    else
                    {
                        $configuracaoSite = $msg->mObj;

                        $redis->set($keyRedisConfiguracaoSite, json_encode($configuracaoSite));
                    }
                }
            }
            else
            {
                $configuracaoSite = json_decode($jsonDados);
            }

            if ($configuracaoSite != null)
            {
                $configDatabase = new ConfiguracaoDatabase();
                $configDatabase->DBName = $configuracaoSite->NOME_BANCO_DE_DADOS_PRINCIPAL;
                $configDatabase->host = $configuracaoSite->BANCO_DE_DADOS_HOST;
                $configDatabase->porta = $configuracaoSite->BANCO_DE_DADOS_PORTA;
                $configDatabase->senha = $configuracaoSite->BANCO_DE_DADOS_SENHA;
                $configDatabase->usuario = $configuracaoSite->BANCO_DE_DADOS_USUARIO;

                $configDatabaseSecundario = new ConfiguracaoDatabase();
                $configDatabaseSecundario->DBName = $configuracaoSite->NOME_BANCO_DE_DADOS_SECUNDARIO;
                $configDatabaseSecundario->host = $configuracaoSite->BANCO_DE_DADOS_SECUNDARIO_HOST;
                $configDatabaseSecundario->porta = $configuracaoSite->BANCO_DE_DADOS_SECUNDARIO_PORTA;
                $configDatabaseSecundario->senha = $configuracaoSite->BANCO_DE_DADOS_SECUNDARIO_SENHA;;
                $configDatabaseSecundario->usuario = $configuracaoSite->BANCO_DE_DADOS_SECUNDARIO_USUARIO;
                Registry::remove('ConfiguracaoDatabasePrincipal');
                Registry::remove('ConfiguracaoDatabaseSecundario');
                Registry::remove('ConfiguracaoSite');

                Registry::add($configDatabase, 'ConfiguracaoDatabasePrincipal');
                Registry::add($configDatabaseSecundario, 'ConfiguracaoDatabaseSecundario');
                Registry::add($configuracaoSite, 'ConfiguracaoSite');

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            else
            {
                return null;
            }
        }

    }
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/ConfiguracaoDatabase.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Registry.php';

    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Helper.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperRedis.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperLog.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Interface_mensagem.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Mensagem.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/PROTOCOLO_SISTEMA.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/BOSihopComum.php';
    include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/pipeline_sync/HelperProcesso.php';







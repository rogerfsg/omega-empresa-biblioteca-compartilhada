<?php

    include("headerXML.php");
    include("funcoes.php");
    include("constants.php");

    $objAjax = new Ajax();
    $objBanco = new Database();

    $query = $_GET["query"];
    $valor = $_GET["valor"];

    if ($valor != "")
    {
        $objCriptografia = new Crypt();
        $query = $objCriptografia->decrypt($query);

        if (strpos(strtolower($query), "select") === 0)
        {
            $query = str_replace("?", $valor, $query);

            $objBanco->query($query);

            $xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
            $xml .= "\t<valores>\n";

            while ($dados = $objBanco->fetchArray())
            {
                if ($dados[2])
                {
                    $css = "options_1";
                }
                elseif ($dados[3])
                {
                    $css = "options_2";
                }
                else
                {
                    $css = "nenhum";
                }

                $xml .= "\t\t<valor>\n";
                $xml .= "\t\t\t<codigo>" . $dados[0] . "</codigo>\n";
                $xml .= "\t\t\t<descricao>" . $dados[1] . "</descricao>\n";
                $xml .= "\t\t\t<css>" . $css . "</css>\n";
                $xml .= "\t\t</valor>\n";
            }

            $xml .= "\t</valores>\n";
        }
    }

    print $xml;

?>

<?php

    header("Content-type: text/html; charset=iso-8859-1", true);

    include_once 'constants.php';
    include_once 'database_config.php';
    include_once 'funcoes.php';

    $objCrypt = new Crypt();
    $email = $objCrypt->decrypt(Helper::GET("email"));

    if (Helper::verificarEmail($email))
    {
        $objBanco = new Database();
        $objBanco->query("SELECT id, teste_BOOLEAN, desinscreveu_BOOLEAN FROM cliente WHERE email='{$email}'");

        if ($objBanco->rows == 1)
        {
            $idCliente = $objBanco->getPrimeiraTuplaDoResultSet(0);
            $teste = $objBanco->getPrimeiraTuplaDoResultSet(1);
            $desinscreveu = $objBanco->getPrimeiraTuplaDoResultSet(2);

            if ($teste)
            {
                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem("Seu email é um email de teste do sistema, não pode ser descadastrado.");
            }
            elseif ($desinscreveu)
            {
                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem("Seu email já foi desativado em nossa mala direta.", MENSAGEM_INFO);
            }
            else
            {
                $objBanco->query("UPDATE cliente SET desinscreveu_BOOLEAN=1 AND data_desinscricao_DATETIME=NOW() WHERE id={$idCliente}");
                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem("Email <font color='#FF0000'>&lt;$email>&gt;</font> desinscrito com sucesso!", MENSAGEM_OK);
            }
        }
        else
        {
            Helper::imprimirCabecalhoParaFormatarAction();
            Helper::imprimirMensagem("Email não cadastrado em nosso banco de dados.", MENSAGEM_WARNING);
        }

        Helper::imprimirComandoJavascriptComTimer("window.close()", 5);
    }

?>

<?php

    include("funcoes.php");
    include("constants.php");

    $objAjax = new Ajax();

    $valor = $_GET["valorComboBox1"];
    $idCmb1 = $_GET["comboBox1"];
    $idCmb2 = $_GET["comboBox2"];

    $classe1 = ucfirst(substr($idCmb1, 0, strpos($idCmb1, "_")));
    $classe2 = ucfirst(substr($idCmb2, 0, strpos($idCmb2, "_")));

    $obj1 = call_user_func_array(array("$classe1", "factory"), "");
    $obj1->id = $valor;

    $obj2 = call_user_func_array(array("$classe2", "factory"), "");

    print $objAjax->carregarXMLComboBox($obj1, $obj2);

?>

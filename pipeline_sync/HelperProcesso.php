<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 14/08/2017
     * Time: 21:16
     */
    class HelperProcesso
    {
        const MAX_TEMPO_ESPERA_SEGUNDOS_QUEUE_EMPTY = 45;
        const MAX_TEMPO_ESPERA_SEGUNDOS = 5;
        const TEMPO_ESPERA_ROTINA_SEGURANCA_SEGUNDOS = 60;
        const MAX_TEMPO_ADD_ORDEM_SINCRONIZACAO_DE_UMA_CORPORACAO_CONSECUTIVA_SEGUNDOS = 60;

        const TEMPO_ITERACAO_P0 = 1;
        const TEMPO_ITERACAO_P1 = 5;
        const TEMPO_ITERACAO_P2 = 10;
        const TEMPO_ITERACAO_P3 = 10;
        const TEMPO_ITERACAO_P4 = 15;

        const TEMPO_MAXIMO_EXECUCAO_PROCESSO_MINUTOS = 5;

        const HASHSET_SINCRONIZACAO_P0 = "HSSP0";

        const QUEUE_SINCRONIZACAO_P0 = "QSP0";
        const QUEUE_SINCRONIZACAO_P1 = "QSP1";
        const QUEUE_SINCRONIZACAO_P2 = "QSP2";
        const QUEUE_SINCRONIZACAO_P3 = "QSP3";
        const QUEUE_GERAR_BANCO_SQLITE_ERRO = "QGBSE";

        ///Processo 0 nao tem lock nem reader nem writer
        ///Processo 1 2 e 3 readers
        /// Processo 4 � somente escrita
        const TOTAL_READERS = 3;

        public $helperRedis;
        public $prefixo = null;

        public function __construct($idSistemaSihop)
        {
            $this->prefixo = "SIHOP_{$idSistemaSihop}";
            $this->helperRedis = new HelperRedis($this->prefixo);
        }

        //Chamado por Web, Mobile, Rotina Seguranca e pelo proprio Processo1
        public function addItemProc0($idCorporacao, $force = false)
        {
            $this->helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P0, $idCorporacao);
        }

        public function factory()
        {
            return new HelperProcesso();
        }
    }
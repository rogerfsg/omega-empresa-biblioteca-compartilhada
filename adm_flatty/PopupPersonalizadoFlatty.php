<?php
//Pagina Wrapper para Popups

    class PopupPersonalizadoFlatty extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }
        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
            Helper::sessionStart();
            I18N::loadLanguage();
        }
        public function render()
        {
            try
            {

                $singleton = SessionRedis::getSingleton();
                $singleton->loadSession();

                $pathImgCarregando = Helper::getUrlRaizDaBibliotecaCompartilhada("/css/flatty/assets/images/ajax-loaders/17.gif");
                $pathLogo = Helper::getUrlRaizDoProjeto("/imgs/omega_empresa.png");
                ?>

                <html>
                <head>
                    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                    <?= Helper::getTagDoFavicon(); ?>

                    <? Helper::includePHPDaBibliotecaCompartilhada('imports/header.php'); ?>
                    <?= Helper::carregarCssPagamentoFlatty(); ?>
                    <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>

                </head>
                <body class='contrast-orange login contrast-background'>

                <?

                    $mf = new  MensagensFlatty();
                    $mf->render();

                    $ref = new ReportarErroFlatty();
                    $ref->render();
                ?>
                <div class='middle-container'>
                    <div class='middle-row'>
                        <div class='middle-wrapper'>
                            <div class='login-container-header'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-sm-12'>
                                            <div class='text-center'>
                                                <img width="150" height="150" src="<?= $pathLogo; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='login-container'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-sm-12'>


                                            <?

                                                $mf = new MensagensPopupFlatty();
                                                $mf->render();
                                                $tipo = Helper::GET("tipo");
                                                $pagina = Helper::GET("page");
                                                if ($tipo && $pagina)
                                                {
                                                    if (Helper::GET("dialog"))
                                                    {
                                                        Helper::imprimirComandoJavascript("$(document).ready(function(){
                                            setTimeout(\"autoRedimensionarDialog()\", 1000);
                                        })");
                                                    }
                                                    Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                                }

                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='login-container-footer'>
                                <div class='container'>
                                    <div class='row'>
                                        <div class='col-sm-12'>
                                            <div class='text-center'>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                </body>

                </html>
                <?

                Database::closeAll();
                HelperRedis::closeAll();
                HelperLog::verbose();
            }
            catch (Exception $exc)
            {
                InterfaceReportarErro::reportarExcecao($exc);
                Database::closeAll();
                HelperRedis::closeAll();
                HelperLog::verbose(null, $exc);
            }
        }
    }

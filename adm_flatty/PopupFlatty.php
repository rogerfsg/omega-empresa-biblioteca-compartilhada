<?php
//Pagina Wrapper para Popups

    class PopupFlatty extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }
public function beforeHeader()
{
    Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
    Helper::sessionStart();
    I18N::loadLanguage();
}
        public function render()
        {
            try
            {

                $singleton = SessionRedis::getSingleton();
                $singleton->loadSession();

                $tipo = Helper::GET("tipo");
                $pagina = Helper::GET("page");

                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>

                    <head>
                        <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                        <?= Helper::getTagDoFavicon(); ?>
                        <?= Helper::carregarCssFlatty(); ?>
                        <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>

                    </head>

                    <body leftmargin="0" topmargin="0" id="body_identificator">

                    <?

                        Helper::includePHP(1, "client_area/imports/mensagens_popup.php");

                        $passouSeguranca = true;

                        if ($passouSeguranca)
                        {
                            if ($tipo && $pagina)
                            {
                                if (Helper::GET("dialog"))
                                {
                                    Helper::imprimirComandoJavascript("$(document).ready(function(){
                                                                            setTimeout(\"autoRedimensionarDialog()\", 1000);
                                                                        })");
                                }

                                Helper::includePHP(false, PATH_RELATIVO_PROJETO . "/" . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                            }
                        }

                    ?>

                    </body>

                    <?

                Database::closeAll();
                HelperRedis::closeAll();

                HelperLog::verbose();
            }
            catch (Exception $exc)
            {
                InterfaceReportarErro::reportarExcecao($exc);
                Database::closeAll();
                HelperRedis::closeAll();

                HelperLog::verbose(null, $exc);
            }

        }

    }

?>

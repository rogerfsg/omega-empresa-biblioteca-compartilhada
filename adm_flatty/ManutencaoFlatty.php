<?php

class ManutencaoFlatty extends InterfaceScript
{
    private $parametros = null;

    public function __construct($parametros = null)
    {
        parent::__construct(true);
        $this->parametros = $parametros;
    }

    public function render()
    {
        try
        {
            ?>
            <html>
            <head>
                <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                <? Helper::includePHPDaBibliotecaCompartilhada('imports/header.php'); ?>
                <?= Helper::getTagDoFavicon(); ?>

                <?= Helper::carregarCssPagamentoFlatty(); ?>
                <?= Helper::carregarCssLoginFlatty(); ?>
                <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>


            </head>
            <body class='contrast-dark login contrast-background'>

            <?
            $mf = new  MensagensFlatty();
            $mf->render();
            $ref = new ReportarErroFlatty();
            $ref->render();

            $pathLogo = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/omega_empresa.png');
            ?>


            <div class='middle-container'>
                <div class='middle-row'>
                    <div class='middle-wrapper'>
                        <div class='login-container-header'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-12'>
                                        <div class='text-center'>
                                            <img src="<?= $pathLogo; ?>" style="width: 150px;height: 150px;"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='login-container'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-4 col-sm-offset-4'>
                                        <h1 class='text-center title'>
                                            <?= I18N::getExpression("Em manutencao volte em alguns minutos...") ?>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='login-container-footer'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-12'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            </body>

            </html>
            <?
            //            $singletonHtml = SingletonCacheHtml::getSingleton();
            //            $singletonHtml->stopCaching();
            HelperLog::verbose("login");
        } catch (Exception $ex) {
            InterfaceReportarErro::reportarExcecao($ex);
            HelperLog::verbose("login", null, $ex);
        }
    }
}


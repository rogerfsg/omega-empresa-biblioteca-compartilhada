<?php

    //Pagina Wrapper para requisições Ajax
    class AjaxModalFlatty extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

            I18N::loadLanguage();
        }
        public function render()
        {
            try
            {


                $tituloModal = Helper::POSTGET("titulo_modal");
                $idModal = Helper::POSTGET("id_modal");
                $botaoFechar = Helper::POSTGET('botao_fechar')
                ?>

                <a class='btn btn-info btn-lg' id="<?= $idModal ?>Botao" data-toggle='modal' href='#<?= $idModal; ?>'
                   style="display: none;" role='button'>&nbsp;</a>

                <div class='modal fade in' id='<?= $idModal; ?>' tabindex='-1'>
                    <div class='modal-dialog'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <?
                                    if ($botaoFechar)
                                    {
                                        ?>

                                        <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>X
                                        </button>
                                        <?
                                    }
                                    if (strlen($tituloModal))
                                    {
                                        ?>
                                        <h4 class='modal-title' id='myModalLabel'><?= $tituloModal; ?></h4>
                                        <?
                                    }
                                ?>
                            </div>
                            <div class='modal-body'>
                                <?
                                    $ajaxPadrao = new AjaxPadrao();
                                    $ajaxPadrao->renderConteudoSeAutorizado();
                                ?>
                            </div>
                            <?
                                if ($botaoFechar == 'true')
                                {
                                    ?>
                                    <div class='modal-footer'>
                                        <button class='btn btn-default' data-dismiss='modal' type='button'>Fechar
                                        </button>
                                    </div>
                                    <?
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <?
                Database::closeAll();

                HelperLog::verbose("ajax_modal", "OK!");
            }
            catch (Exception $exc)
            {
                $h = new HelperLog();
                $h->gravarLogEmCasoErro(new Mensagem(null, null, $exc));
                Database::closeAll();

                HelperLog::verbose(null, $exc);
            }
        }
    }

?>
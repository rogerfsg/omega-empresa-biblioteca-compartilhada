<?php

    class IndexAngularFlatty extends InterfaceScript
    {
        public function __construct()
        {
            parent::__construct(true);
        }

        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
            I18N::loadLanguage();
        }

        public function render()
        {
            try
            {
                Helper::includePHP(false, PATH_RELATIVO_PROJETO . "/imports/header_html.php");
                $objSeguranca = Registry::get('Seguranca');

                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>
                <head>
                    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                    <?= Helper::getTagDoFavicon(); ?>

                    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
                          name='viewport'>

                    <script type="text/javascript">

                        var baseURL = "<?=Helper::acharRaiz(); ?>";
                        var baseURLAdm = "<?=Helper::acharRaiz(); ?>adm/";
                        <?=Helper::gerarObjetoJSDeVariaveisGET() ?>

                    </script>

                    <?= Javascript::importarBibliotecasFlattyCabecalho(); ?>

                    <?= Javascript::importarBibliotecaJqueryResizable() ?>

                    <?= Javascript::importarBibliotecaBootstrapSwitch() ?>

                    <?= Javascript::importarBibliotecaAngularJS(); ?>

                    <?= Javascript::importarModulosDoAngularJS(Javascript::$MODULOS_ANGUJARJS_TODOS); ?>

                    <?= Javascript::importarPluginsDoAngularJS(); ?>

                    <!-- JSs da aplicação Angular.js -->
                    <?= Javascript::importarBibliotecaComumOmegasoftware(); ?>

                    <?= Javascript::importarAngularApp(); ?>

                    <?=Javascript::importarBibliotecaJavascriptLogger(); ?>

                    <?= Javascript::importarBibliotecaOmegaAngularJS(Javascript::OMEGA_ANGULARJS_THEME_NEON); ?>

                    <script src="<?= Javascript::getPathAteRaizDoProjeto() ?>recursos/js/AngularApp/Constants.js"
                            type="text/javascript"></script>
                    <script src="<?= Javascript::getPathAteRaizDoProjeto() ?>recursos/js/AngularApp/Utils.js"
                            type="text/javascript"></script>
                    <script src="<?= Javascript::getPathAteRaizDoProjeto() ?>recursos/js/AngularApp/AngularRootScope.js"
                            type="text/javascript"></script>

                </head>

                <? Helper::includePHP(false, PATH_RELATIVO_PROJETO . "/imports/topo_pagina.php"); ?>

                <body class='contrast-orange omega-app-body'>

                <div id='wrapper'>

                    <? Helper::includePHP(false, PATH_RELATIVO_PROJETO . "/imports/menu_principal.php", false); ?>

                    <section id="content">

                        <div class="container internal-page-container" ng-app="omegaApp">

                            <?php

                                $mf = new  MensagensFlatty();
                                $mf->render();

                                $ref = new ReportarErroFlatty();
                                $ref->render();

                                $mcf = new ModalCarregandoFlatty();
                                $mcf->render();

                                $autenticado = false;
                                $objSeguranca = Registry::get('Seguranca');

                                //!$objSeguranca->verificaPermissao(Helper::GET("tipo"), Helper::GET("page"))
                                if (!$objSeguranca)
                                {
                                    Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                                    $autenticado = false;
                                    Helper::mudarLocation("login.php");
                                }
                                else
                                {
                                    $autenticado = true;
                                }

                                if ($autenticado)
                                {
                                    if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                    {
                                        Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO);
                                    }
                                    else
                                    {
                                        if(Helper::GET("tipo") == "pages")
                                        {
                                            Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                        }
                                        elseif(Helper::GET("tipo") == "nextGen")
                                        {
                                            Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/view/" . Helper::GET("page") . ".php");
                                        }
                                        else
                                        {
                                            Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . "/view.php");
                                        }
                                    }
                                }

                            ?>

                        </div>

                    </section>

                </div>

                <script type="text/javascript">

                    ApplicationUtil.detectDeviceTypeAndAddCssClass();
                    ApplicationUtil.autoExpandMenuItens();

                </script>

                </body>

                </html>

                <?php

                Database::closeAll();
                HelperRedis::closeAll();
                HelperLog::verbose("OK!");
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $json = InterfaceReportarErro::reportarExcecao($exc);
                HelperLog::verbose(null, $json);
            }

            echo Javascript::importarBibliotecasFlattyRodape();
        }

    }
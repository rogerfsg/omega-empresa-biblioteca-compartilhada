<?php

    class ModalCarregandoFlatty extends InterfaceScript
    {
        public function render()
        {
            $pathImgCarregando = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/17.gif');
            $pathLogo = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/omega_empresa.png');
            ?>

            <a class='btn btn-info btn-lg' style="display: none;" data-toggle='modal' id='botaoModalCarregando'
               href='#modalCarregando' role='button'>&nbsp;</a>

            <div class='modal fade' id='modalCarregando' tabindex='-1'>
                <div class='modal-dialog'>
                    <div class='modal-content'>
                        <button aria-hidden='true' id='botaoFecharModalCarregando' class='close' data-dismiss='modal'
                                type='button'
                                style='visibility: hidden;'></button>
                        <div class='modal-body'>

                            <div class="box-content">
                                <img src="<?= $pathImgCarregando; ?>">

                                <h3 class="subtotal contrast">Carregando</h3>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?
        }
    }
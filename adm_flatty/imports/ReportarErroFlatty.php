<?php

    class ReportarErroFlatty extends InterfaceReportarErro
    {
        public function render()
        {
            ?>
            <div class="modal fade" id="<?= InterfaceReportarErro::ID_DIV_ERRO ?>" tabindex="-1" aria-hidden="false"
                 style="display: none;">
                <a style="display: none;" class="botao-acao" data-toggle="modal"
                   href="#<?= InterfaceReportarErro::ID_DIV_ERRO ?>" role="button">Launch big modal</a>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button aria-hidden="true" class="close" data-dismiss="modal" type="button">�</button>
                            <h4 class="modal-title" id="myModalLabel">Reportar erro</h4>
                        </div>
                        <div class="modal-body">

                            <form class="form" style="margin-bottom: 0;" method="post" action="#"
                                  accept-charset="UTF-8">
                                <input name="authenticity_token" type="hidden">

                                <div class="alert alert-warning alert-dismissable erro-tempo" style="display: none;">
                                    <h4>
                                        <i class="icon-exclamation"></i>
                                        Servidor ocupado
                                    </h4>
                                    <p class="mensagem-usuario">O servidor est� muito ocupado, tente novamente mais
                                        tarde</p>

                                </div>
                                <div class="alert alert-danger alert-dismissable erro-excecao" style="display: none;">
                                    <h4>
                                        <i class="icon-remove-sign"></i>
                                        Erro
                                    </h4>
                                    <p class="mensagem-usuario">Ocorreu um erro durante o processamento, estamos
                                        trabalhando para corrigir o problema.</p>
                                </div>

                                <div class="form-group">
                                    <label for="inEmail">Email</label>
                                    <input class="form-control" id="inEmail" placeholder="email" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="inDescricao">Descri��o</label>
                                    <textarea class="form-control inDescricao" placeholder="descri��o"
                                              rows="2"></textarea>
                                </div>
                                <input class="form-control inErro" type="hidden">
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal" type="button">Fechar</button>
                            <button class="btn btn-primary" type="button">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <?
        }

    }



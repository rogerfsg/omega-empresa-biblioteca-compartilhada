<?php

    class IndexFlatty extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
            I18N::loadLanguage();
        }
        public function render()
        {
            try
            {
                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>
                <head>
                    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                    <?= Helper::getTagDoFavicon(); ?>

                    <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>

                    <?= Helper::carregarCssPagamentoFlatty(); ?>


                </head>

                <body class='contrast-red without-footer'>

                <?php

                    $mf = new  MensagensFlatty();
                    $mf->render();

                    $ref = new ReportarErroFlatty();
                    $ref->render();

                    Ajax::imprimirCorpoDaDivDeRetornoAjax();

                    include("imports/topo_pagina.php");

                    $mcf = new ModalCarregandoFlatty();
                    $mcf->render();

                ?>
                <div id="divAlterarSenha">
                </div>
                <div id='wrapper'>
                    <? include("imports/menu_esquerdo.php"); ?>
                    <section id='content'>


                        <div class='container'>
                            <div class='row' id='content-wrapper'>
                                <div class='col-xs-12'>
                                    <?php

                                        $autenticado = false;
                                        $objSeguranca = Registry::get('Seguranca');

                                        if (!$objSeguranca || !$objSeguranca->verificaPermissao(Helper::GET("tipo"), Helper::GET("page")))
                                        {
                                            Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                                            $autenticado = false;
                                            Helper::mudarLocation("login.php");
                                        }
                                        else
                                        {
                                            $autenticado = true;
                                        }

                                        if ($autenticado)
                                        {
                                            if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                            {
                                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO);
                                            }
                                            else
                                            {
                                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                            }
                                        }
                                    ?>
                                </div>
                            </div>

                            <?

                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . "imports/rodape.php"); ?>
                        </div>
                    </section>
                </div>


                </body>
                </html>
                <?
                Database::closeAll();
                HelperRedis::closeAll();

                HelperLog::verbose("OK!");
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $json = InterfaceReportarErro::reportarExcecao($exc);
                HelperLog::verbose(null, $json);
            }
        }
    }
<?php

class LoginFlatty extends InterfaceScript
{
    public function __construct()
    {
        parent::__construct(true);
    }

    public function beforeHeader()
    {
        Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
        Helper::sessionStart();
        I18N::loadLanguage();
    }

    public function render()
    {
        try
        {
            $singletonHtml = SingletonCacheHtml::getSingleton();
            $singleton = SessionRedis::getSingleton();
            $singleton->loadSession();
            ?>
            <html>
            <head>
                <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                <? Helper::includePHPDaBibliotecaCompartilhada('imports/header.php'); ?>
                <?= Helper::getTagDoFavicon(); ?>

                <?= Helper::carregarCssPagamentoFlatty(); ?>
                <?= Helper::carregarCssLoginFlatty(); ?>
                <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>
                <?= Javascript::importarBibliotecaDaTela("login"); ?>


            </head>
            <body class='contrast-dark login contrast-background'>

            <?
            $mf = new  MensagensFlatty();
            $mf->render();
            $ref = new ReportarErroFlatty();
            $ref->render();
            $pathImgCarregando = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/17.gif');
            $pathLogo = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/omega_empresa.png');
            ?>


            <div class='middle-container'>
                <div class='middle-row'>
                    <div class='middle-wrapper'>
                        <div class='login-container-header'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-12'>
                                        <div class='text-center'>
                                            <img src="<?= $pathLogo; ?>" style="width: 150px;height: 150px;"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='login-container'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-4 col-sm-offset-4'>
                                        <h1 class='text-center title'>
                                            <?= I18N::getExpression("Login") ?>
                                        </h1>
                                        <script type="text/javascript">

                                            var loginValidado = false;

                                            function login(formularioLogin)
                                            {
                                                try
                                                {
                                                    if (!loginValidado
                                                        && formularioLogin != null
                                                        && !validaFormularioLogin(formularioLogin))
                                                    {
                                                        return false;
                                                    }
                                                    if (loginValidado)
                                                    {
                                                        $('#imgLogando').css('display', 'inline-block');
                                                        $('#aLogin').css('display', 'none');
                                                        return true;
                                                    }
                                                    var post = {
                                                        txtLogin: $('#txtLogin').val(),
                                                        txtSenha: $('#txtSenha').val()
                                                    };
                                                    $('#imgLogando').css('display', 'inline-block');
                                                    $('#aLogin').css('display', 'none');
                                                    var funcaoASerChamada = function ()
                                                    {
                                                        try
                                                        {
                                                            var retorno = arguments[0];
                                                            var codRetorno = retorno.mCodRetorno;
                                                            if (codRetorno == OPERACAO_REALIZADA_COM_SUCESSO)
                                                            {
                                                                loginValidado = true;
                                                                $('#formulario_login').submit();
                                                                //document.location.href = "index.php";
                                                                return true;
                                                            }
                                                            else
                                                            {
                                                                if (codRetorno == EMAIL_DO_USUARIO_INEXISTENTE)
                                                                {
                                                                    $('#imgLogando').css('display', 'none');
                                                                    $('#aLogin').css('display', 'inline-block');
                                                                    bootbox.confirm("O email " + $('#txtLogin').val() + "  n�o est� cadastrado. Deseja cadastra-lo?", function (result)
                                                                    {
                                                                        if (result)
                                                                        {
                                                                            document.location.href = 'popup_personalizado.php?tipo=formularios&page=cliente_simplificado&email=' + encodeURIComponent($('#txtLogin').val());
                                                                        }
                                                                        return;
                                                                    });
                                                                    return false;
                                                                }
                                                                else
                                                                {
                                                                    $('#imgLogando').css('display', 'none');
                                                                    $('#aLogin').css('display', 'inline-block');
                                                                    bootbox.alert(retorno.mMensagem);
                                                                    return false;
                                                                }
                                                            }
                                                        }
                                                        catch (err)
                                                        {
                                                            $('#imgLogando').css('display', 'none');
                                                            $('#aLogin').css('display', 'inline-block');
                                                            bootbox.alert({
                                                                message: err.stack
                                                            });
                                                            return false;
                                                        }
                                                        finally
                                                        {
                                                        }
                                                    }
                                                    carregarValorRemotoAsyncPost(
                                                        'webservice.php?class=Seguranca&action=loginWebservice',
                                                        null,
                                                        funcaoASerChamada,
                                                        post);
                                                }
                                                catch (err)
                                                {
                                                    $('#imgLogando').css('display', 'none');
                                                    $('#aLogin').css('display', 'inline-block');
                                                    bootbox.alert({
                                                        message: err.stack
                                                    });
                                                    return false;
                                                }
                                                return loginValidado;
                                            }

                                        </script>
                                        <form class="form form-horizontal validate-form"
                                              style="margin-bottom: 0;"
                                              action="actions.php?class=Seguranca&action=login"
                                              method="post"
                                              name="formulario_login"
                                              id="formulario_login"
                                              onsubmit="return login(this);">

                                            <?
                                            $objGeneric = new Generic_DAO(Generic_DAO::NAO_INICIALIZAR_BANCO);
                                            $objArg = new Generic_Argument();
                                            $objArg->label = I18N::getExpression("Email");
                                            $objArg->nome = "txtLogin";
                                            $objArg->id = "txtLogin";
                                            $objArg->classeCss = "form-control";
                                            $objArg->placeHolder = "E-mail";
                                            $objArg->obrigatorio = true;
                                            $objArg->lowerCase = true;
                                            $objArg->email = true;
                                            $objArg->dataRuleRequired = true;
                                            $objArg->mensagemValidacao = I18N::getExpression("Email inv�lido");
                                            ?>

                                            <input type="hidden" id='next_action' name='next_action'
                                                   value='pagina_inicial'/>

                                            <div class='form-group'>
                                                <div class='controls with-icon-over-input'>
                                                    <?= $objGeneric->campoTexto($objArg); ?>
                                                    <br/>

                                                    <i class='icon-user text-muted'></i>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <div class='controls with-icon-over-input'>
                                                    <?
                                                    $objArg = new Generic_Argument();
                                                    $objArg->numeroDoRegistro = null;
                                                    $objArg->label = I18N::getExpression("Senha");
                                                    $objArg->nome = "txtSenha";
                                                    $objArg->id = "txtSenha";
                                                    $objArg->classeCss = "form-control";
                                                    $objArg->obrigatorio = true;
                                                    $objArg->campoSenha = true;
                                                    $objArg->dataRuleRequired = true;
                                                    $objArg->placeHolder = "senha";
                                                    ?>
                                                    <?= $objGeneric->campoTexto($objArg); ?>
                                                    <i class='icon-lock text-muted'></i>
                                                </div>
                                            </div>
                                            <div class='checkbox' style='display: none;'>
                                                <label for='remember_me'>
                                                    <input id='remember_me' name='remember_me' type='checkbox'
                                                           value='1'>
                                                    <?= I18N::getExpression("Permanecer conectado") ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <button id='aLogin' class='btn btn-block btn-success' type='submit'>
                                                    <?= I18N::getExpression("Logar") ?>
                                                </button>
                                                <img id='imgLogando' src="<?= $pathImgCarregando; ?>"
                                                     style="display: none;">
                                            </div>
                                            <div class="col-sm-4">
                                                <a class='btn btn-block btn-info'
                                                   href="popup_personalizado.php?tipo=formularios&page=cliente_simplificado">
                                                    <?= I18N::getExpression("Cadastrar") ?>
                                                </a>
                                            </div>
                                            <div class="col-sm-4">
                                                <a class='btn btn-block btn-info'
                                                   onclick='abrirModalRelembrarSenha()'>
                                                    <?= I18N::getExpression("Relembrar senha") ?>
                                                </a>
                                                <a class='btn btn-block btn-info' id='aModalRelembrarSenha'
                                                   href='#modal-example2'
                                                   style="display: none;" data-toggle='modal'>
                                                    <?= I18N::getExpression("Relembrar senha") ?>
                                                </a>
                                            </div>

                                        </form>
                                        <script type='text/javascript'>

                                            function abrirModalRelembrarSenha()
                                            {
                                                var email = $('#txtLogin').val();
                                                $('#emailRelembrarSenha').val(email);
                                                $('#aModalRelembrarSenha').click();
                                            }

                                            $(document).ready(function ()
                                            {
                                                var txtLogin = $.cookie("txtLogin");
                                                $('#txtLogin').val(txtLogin);
                                            });

                                        </script>

                                        <?
                                        include('paginas/lembrar_senha.php');
                                        ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='login-container-footer'>
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-12'>
                                        <div class='text-center'>
                                            <a href='popup_personalizado.php?tipo=formularios&page=cliente_simplificado'>
                                                <i class='icon-user'></i>
                                                <?= I18N::getExpression("Voc� � novo na {0}?", Helper::getNomeDaEmpresa()) ?>
                                                <strong><?= I18N::getExpression("Cadastre-se") ?></strong>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            </body>

            </html>
            <?
            Database::closeAll();
            HelperRedis::closeAll();
            //            $singletonHtml = SingletonCacheHtml::getSingleton();
            //            $singletonHtml->stopCaching();
            HelperLog::verbose("login");
        } catch (Exception $exc)
        {
            Database::closeAll();
            HelperRedis::closeAll();
            InterfaceReportarErro::reportarExcecao($exc);
            HelperLog::verbose("login", null, $exc);
        }
    }
}

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of HelperAPC
     *
     * @author home
     */
    class HelperAPC
    {
        const TURN_ON = false;

        private $prefixo;
        static $singletonR = null;

        public static function getSingleton()
        {
            if (HelperAPC::$singletonR == null)
            {
                HelperAPC::$singletonR = new HelperAPC();
            }

            return HelperAPC::$singletonR;
        }

        public function __construct($prefixo = null)
        {
            // since we connect to default setting localhost
            // and 6379 port there is no need for extra
            // configuration. If not then you can specify the
            // scheme, host and port to connect as an array
            // to the constructor.
            if ($prefixo == null)
            {
                $prefixo = IDENTIFICADOR_SESSAO;
            }

            $this->prefixo = $prefixo;
        }

        //put your code here
        public function add($key, $value, $ttl = 0)
        {
            if (!HelperAPC::TURN_ON)
            {
                return false;
            }

            return apc_store($this->prefixo . $key, $value, $ttl);
        }

        public function exists($key)
        {
            if (!HelperAPC::TURN_ON)
            {
                return false;
            }

            return apc_exists($this->prefixo . $key);
        }

        public function get($key)
        {
            if (!HelperAPC::TURN_ON)
            {
                return null;
            }

            return apc_fetch($this->prefixo . $key);
        }
    }

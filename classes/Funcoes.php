<?php

    //O vetor abaixo deve ser inicializado no site filho
    class Funcoes
    {
        private $vVetorDiretoriosPercorridos = null;
        static $singleton = null;

        public static function getSingleton()
        {
            if (static::$singleton == null)
            {
                static::$singleton = new Funcoes();
            }

            return static::$singleton;
        }

        public function setDiretorios($vetor)
        {
            $this->vVetorDiretoriosPercorridos = $vetor;

//        print_r($this->vVetorDiretoriosPercorridos);
            foreach ($this->vVetorDiretoriosPercorridos as $vetorParametro)
            {
                $vRetorno = $this->__loadFilesInPath($vetorParametro[0], $vetorParametro[1], $vetorParametro[2]);
                //        print_r($vetorParametro);
                //        print_r($vRetorno);
//            if ($vRetorno != false) {

//                echo $vRetorno;
//            }
            }
        }

        private function __construct()
        {
            $this->setAutoregister();

            $workspaceRaiz = acharRaizWorkspace();

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/constants.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Helper.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperLog.php';
            $helperLog = new HelperLog ();
            $helperLog->setMyErrorHandler();

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Registry.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/AES.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Database.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/PROTOCOLO_SISTEMA.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Interface_mensagem.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Mensagem.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/InterfaceSeguranca.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/InterfaceSegurancaUsuario.php';

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Crypt.php';

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperAPC.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperRedis.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperAPCRedis.php';
        }

        function setAutoregister()
        {
            spl_autoload_register(array($this, 'carregarClasses'));
        }

        function carregarClasses($className)
        {//Fun��o que ir� carregas as classes do projeto automatica e dinamicamente - s� funciona PHP 5

            $class = $this->getClass($className);

            if (file_exists($class))
            {
                $retorno = require_once($class);
                //        echo "$class: $retorno<br/>";
            }
        }

        public function getClass($classe)
        {
            if (!strlen($classe))
            {
                return;
            }
//        echo "$classe<br/>";
//print_r($this);
//        print_r($this->vVetorDiretoriosPercorridos);
            if (is_array($this->vVetorDiretoriosPercorridos))
            {
                foreach ($this->vVetorDiretoriosPercorridos as $vetorParametro)
                {
//            print_r($vetorParametro);
//            echo "<br/>";
                    $vRetorno = $this->__loadFilesInPath($classe, $vetorParametro[1], $vetorParametro[2]);

                    //        print_r($vRetorno);
                    if ($vRetorno != false)
                    {
//                echo "retorno: ";
//                print_r($vRetorno);
                        return $vRetorno;
                    }
                }
            }
//        echo "SAIU";

        }

        function __loadFilesInPath($classe, $classPath, $pVetorDiretorio)
        {
//        print_r($classPath);
//        echo "<br/><br/>";
//        print_r($pVetorDiretorio);
//        echo "<br/><br/>";

            if (is_dir($classPath))
            {
                $pathFinal = $classPath . $classe . ".php";

                if (file_exists($pathFinal))
                {
                    return $pathFinal;
                }
                foreach ($pVetorDiretorio as $dir)
                {
                    $pathFinal = $classPath . $dir . $classe . ".php"; // Classes espec�ficas - criptografia, banco...
//                echo $pathFinal;
//                echo "<br/>";
                    if (file_exists($pathFinal))
                    {
                        return $pathFinal;
                    }
                }
            }

            return false;
        }

    }




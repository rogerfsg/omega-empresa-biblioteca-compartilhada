<?php

    class Ajax
    {

        public $database;

        public function __construct()
        {
            $this->database = new Database();
        }

        public static function getLinkParaCarregamentoDeNovoBlocoEmLista($tipoPagina, $paginaSemExtensao, $selectorJQueryContainer, $label, $cssClass = "link_padrao")
        {
            $strRetorno = "<input type=\"button\" class=\"botoes_form\" onclick=\"javascript:carregarDivAjaxEmLista('{$tipoPagina}', '{$paginaSemExtensao}', '{$selectorJQueryContainer}');\" class=\"{$cssClass}\" value=\"{$label}\"></input>";

            return $strRetorno;
        }

        public static function getContainerParaCarregamentoDeNovoBlocoEmLista($idContainer, $contadorInicial, $invisivel = true)
        {
            if ($invisivel)
            {
                $strStyle = "style=\"display: none;\"";
            }

            $strRetorno = "<div id=\"{$idContainer}\" {$strStyle} contador=\"{$contadorInicial}\"></div>";

            return $strRetorno;
        }

        /**
         *
         * Persiste a entidade contida em um bloco
         *
         */

        public static function persistirEntidadesPresentesNosBlocosEmLista($objetoExtDao, $campoContendoId, $opcaoDeExclusao = true, $arrCamposNaoNulos = false)
        {
            $nomeDaTabela = $objetoExtDao->nomeTabela;

            $chavesPOST = array_keys($_POST);

            $numerosDosBlocos = array();

            foreach ($chavesPOST as $chavePOST)
            {
                if (substr_count($chavePOST, "{$campoContendoId}_") > 0)
                {
                    $numeroBloco = str_replace("{$campoContendoId}_", "", $chavePOST);

                    if (is_numeric($numeroBloco))
                    {
                        $numerosDosBlocos[] = $numeroBloco;
                    }
                }
            }

            foreach ($numerosDosBlocos as $i)
            {
                $obj = clone $objetoExtDao;

                $obj->setValorDosAtributosAtravesDeArray($_POST, $i);
                $arrUpdate = $obj->getArrayDeAtualizacaoDoComandoUpdate($_POST, $i);

                $obj->formatarParaSQL();

                $fazerPersistencia = true;

                if (is_array($arrCamposNaoNulos))
                {
                    for ($j = 0; $j < count($arrCamposNaoNulos); $j++)
                    {
                        if (!trim($_POST["{$nomeDaTabela}_{$arrCamposNaoNulos[$j]}{$i}"]))
                        {
                            $fazerPersistencia = false;
                        }
                    }
                }

                //update
                if (is_numeric($_POST["{$campoContendoId}_{$i}"]))
                {
                    $idInterno = $_POST["{$campoContendoId}_{$i}"];

                    if ($fazerPersistencia)
                    {
                        $obj->update($idInterno, $arrUpdate, "_{$i}");
                    }
                }
                elseif (substr_count($_POST["{$campoContendoId}_{$i}"], "_remover") > 0)
                {
                    $idInterno = str_replace("_remover", "", $_POST["{$campoContendoId}_{$i}"]);

                    if (is_numeric($idInterno) && $opcaoDeExclusao)
                    {
                        $obj->delete($idInterno);
                    }
                }
                //insert
                else
                {
                    if ($fazerPersistencia)
                    {
                        $obj->insert();
                    }
                }
            }
        }

        public static function imprimirCorpoDaDivDeRetornoAjax()
        {
            print "<div id=\"div_carregamento_ajax\"></div>";
        }

        public static function getCorpoDoContainerAjax($idContainer)
        {
            return "<div id=\"{$idContainer}\"></div>";
        }

        public static function getChamadaDeFuncaoCarregamentoAjax($tipoPagina, $pagina, $idContainer, $funcaoJSPreenchimentoCamposGET, $funcaoJSPosExecucao)
        {
            $niveisRaiz = Helper::acharRaiz(true);

            return "carregarListAjax('{$tipoPagina}', '{$pagina}', '{$idContainer}', {$funcaoJSPreenchimentoCamposGET}, {$funcaoJSPosExecucao}, {$niveisRaiz})";
        }

        public static function getValorRemoto($classe, $metodoComParametros)
        {
            $strRaiz = Helper::getStringNiveisAteRaiz();

            $retorno = "carregarValorRemoto('{$classe}', '{$metodoComParametros}', '{$strRaiz}')";

            return $retorno;
        }

        public function getComboBoxAjax($objArgumento, $arrayFilhos, $valoresSelecionadosFilhos = false)
        {
            $nomeComboBox = $objArgumento->nome;
            $idComboBox = $objArgumento->id;
            $valorSelecionado = $objArgumento->valor;
            $label = $objArgumento->label;
            $largura = $objArgumento->largura;
            $obrigatorio = $objArgumento->obrigatorio;
            $classeCss = $objArgumento->classeCss;
            $classeCssFocus = $objArgumento->classeCssFocus;
            $strFiltro = $objArgumento->strFiltro;
            $valueReplaceId = $objArgumento->valueReplaceId;
            $query1 = $objArgumento->query1;
            $arrQueriesFilhos = $objArgumento->arrQueriesFilhos;
            $disabled = $objArgumento->disabled;
            $readonly = $objArgumento->readonly;
            $onChangeUser = $objArgumento->onChange;
            $autoCarregarFilhos = $objArgumento->carregarFilhosAutomaticamente;

            if (!is_array($valoresSelecionadosFilhos))
            {
                $valoresSelecionadosFilhos = array($valoresSelecionadosFilhos);
            }

            $objCriptografia = new Crypt();

            for ($i = 0; $i < count($arrQueriesFilhos); $i++)
            {
                $arrQueriesFilhos[ $i ] = $objCriptografia->crypt(Helper::removerQuebrasDeLinha($arrQueriesFilhos[ $i ], true));
            }

            if (count($arrayFilhos) > 0)
            {
                $onChange .= "javascript: ";

                if (is_array($arrayFilhos))
                {
                    for ($i = 0; $i < count($arrayFilhos); $i++)
                    {
                        $valorSelecionadoFilho = $valoresSelecionadosFilhos[ $i ];

                        if (Helper::isNull($valorSelecionadoFilho))
                        {
                            $valorSelecionadoFilho = '0';
                        }

                        $onChange .= "carregarComboBoxSQL('$idComboBox','$arrayFilhos[$i]','$label','$arrQueriesFilhos[$i]','$valorSelecionadoFilho', this.value, 1);";
                    }
                }
            }

            if (strlen($onChange) > 12)
            {
                $strOnChange = "onchange=\"{$onChange}{$onChangeUser}\"";
            }
            else
            {
                $strOnChange = "";
            }

            if ($disabled)
            {
                $strDisabled = "disabled=\"disabled\"";
            }

            if ($readonly)
            {
                $strReadOnly = "readonly=\"true\"";
            }

            $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

            $strRetorno = "<select name=\"$nomeComboBox\" id=\"$idComboBox\" class=\"$classeCss\" $strFocus $strOnChange $strDisabled $strReadOnly style=\"width: {$largura}px;\">\n";

            if ($query1)
            {
                $strRetorno .= $this->gerarOptionsAjax($query1, $valorSelecionado, $idComboBox);
            }
            else
            {
                $strRetorno .= "<option id=\"options_{$idComboBox}\">";
            }

            $strRetorno .= "\t\t\t\t</select>\n";

            if ($autoCarregarFilhos && $valorSelecionado && count($arrayFilhos))
            {
                $strRetorno .= "
                
            <script language=\"javascript\">";

                for ($i = 0; $i < count($arrayFilhos); $i++)
                {
                    $valorSelecionadoFilho = $valoresSelecionadosFilhos[ $i ];

                    if (Helper::isNull($valorSelecionadoFilho))
                    {
                        $valorSelecionadoFilho = '0';
                    }

                    $strRetorno .= "carregarComboBoxSQL('$idComboBox','$arrayFilhos[$i]','$label','$arrQueriesFilhos[$i]','$valorSelecionadoFilho', '{$valorSelecionado}', 1);";
                }

                $strRetorno .= "
                
            </script>";
            }

            return $strRetorno;
        }

        public function gerarOptionsAjax($stringSQL, $valorSelecionado, $idComboBox = false)
        {
            if ($idComboBox)
            {
                $strId = "id=\"options_$idComboBox\"";
            }

            if (!$stringSQL)
            {
                $strRetorno .= "<option $strId value=''></option>";
            }
            else
            {
                if ($valorSelecionado)
                {
                    $select = true;
                }

                $this->database->query($stringSQL);

                if ($this->database->rows < 1)
                {
                    $strRetorno .= "<option $strId value=''><b>Nenhuma opção encontrada</b></option>";
                }
                else
                {
                    $strRetorno .= "<option $strId value=''><b>Selecione uma opção</b></option>";
                }

                for ($i = 0; $dados = $this->database->fetchArray(); $i++)
                {
                    if ($i != 0)
                    {
                        $strId = "";
                    }

                    $strRetorno .= "<option $strId value=\"{$dados[0]}\"";

                    if ($select && $dados[0] == $valorSelecionado)
                    {
                        $strRetorno .= " selected=\"selected\" ";
                    }

                    $strRetorno .= ">{$dados[1]}</option>";
                }
            }

            return $strRetorno;
        }

        public function carregarXMLComboBox($objPai, $objFilho, $strRestricoes)
        {
            $tabela = $objFilho->nomeTabela;
            $campoChave = $objFilho->campoId;
            $campoLabel = $objFilho->campoLabel;

            $campoCss1 = $objFilho->campoCSS1;
            $campoCss2 = $objFilho->campoCSS2;

            if ($campoCss1 && $campoCss2)
            {
                $strCamposCSS .= ",{$campoCss1},{$campoCss2}";
            }
            elseif ($campoCss1)
            {
                $strCamposCSS .= ",{$campoCss1} ";
            }
            elseif ($campoCss2)
            {
                $strCamposCSS .= ",{$campoCss2} ";
            }

            $campoRestricao = $objPai->nomeTabela . "_" . $objPai->campoId . "_INT";
            $chaveRestricao = $objPai->id;

            if (strlen($strRestricoes) > 0)
            {
                $strRestricoes = "AND " . $strRestricoes;
            }

            $query = "SELECT $campoChave, $campoLabel {$strCamposCSS} FROM $tabela WHERE $campoRestricao=$chaveRestricao $strRestricoes";

            $this->database->query($query);

            $xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
            $xml .= "\t<valores>\n";

            while (($dados = $this->database->fetchArray()) != false)
            {
                if ($dados[2])
                {
                    $css = "options_1";
                }
                elseif ($dados[3])
                {
                    $css = "options_2";
                }
                else
                {
                    $css = "";
                }

                $xml .= "\t\t<valor>\n";
                $xml .= "\t\t\t<codigo>" . $dados[0] . "</codigo>\n";
                $xml .= "\t\t\t<descricao>" . $dados[1] . "</descricao>\n";
                $xml .= "\t\t\t<css>" . $css . "</css>\n";
                $xml .= "\t\t</valor>\n";
            }

            $xml .= "\t</valores>\n";

            return $xml;
        }

        public function carregarPagina($objPai, $objFilho, $strRestricoes)
        {
            $tabela = $objFilho->nomeTabela;
            $campoChave = $objFilho->campoId;
            $campoLabel = $objFilho->campoLabel;

            $campoCss1 = $objFilho->campoCSS1;
            $campoCss2 = $objFilho->campoCSS2;

            if ($campoCss1 && $campoCss2)
            {
                $strCamposCSS .= ",{$campoCss1},{$campoCss2}";
            }
            elseif ($campoCss1)
            {
                $strCamposCSS .= ",{$campoCss1} ";
            }
            elseif ($campoCss2)
            {
                $strCamposCSS .= ",{$campoCss2} ";
            }

            $campoRestricao = $objPai->nomeTabela . "_" . $objPai->campoId . "_INT";
            $chaveRestricao = $objPai->id;

            if (strlen($strRestricoes) > 0)
            {
                $strRestricoes = "AND " . $strRestricoes;
            }

            $query = "SELECT $campoChave, $campoLabel {$strCamposCSS} FROM $tabela WHERE $campoRestricao=$chaveRestricao $strRestricoes";

            $this->database->query($query);

            $xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
            $xml .= "\t<valores>\n";

            while (($dados = $this->database->fetchArray()) != false)
            {
                if ($dados[2])
                {
                    $css = "options_1";
                }
                elseif ($dados[3])
                {
                    $css = "options_2";
                }
                else
                {
                    $css = "";
                }

                $xml .= "\t\t<valor>\n";
                $xml .= "\t\t\t<codigo>" . $dados[0] . "</codigo>\n";
                $xml .= "\t\t\t<descricao>" . $dados[1] . "</descricao>\n";
                $xml .= "\t\t\t<css>" . $css . "</css>\n";
                $xml .= "\t\t</valor>\n";
            }

            $xml .= "\t</valores>\n";

            return $xml;
        }

    }

<?php

    class PROTOCOLO_SISTEMA
    {
        const OPERACAO_REALIZADA_COM_SUCESSO = -1;
        const FALHA_DURANTE_CHAMADA_CURL = 0;
        const ERRO_COM_SERVIDOR = 1;
        const ERRO_VERSAO_DE_SISTEMA_ANTIGA = 2;

        const EMAIL_DO_USUARIO_INEXISTENTE = 4;
        const SENHA_DO_USUARIO_INCORRETA = 5;
        const USUARIO_NAO_PERTENCE_A_CORPORACAO = 6;
        const ERRO_SEM_SER_EXCECAO = 7;
        const USUARIO_INATIVO_NA_CORPORACAO = 8;
        const SISTEMA_EM_MANUTENCAO = 9;
        const MOBILE_DESCONECTADO = 10;
        const RESULTADO_VAZIO = 11;
        //const ERRO_PARAMETRO_INVALIDO = 12;
        //O erro com parametro invalido passou as ser considerado como erro com o servidor
        const ERRO_PARAMETRO_INVALIDO = ERRO_COM_SERVIDOR;
        const ARQUIVO_INEXISTENTE = 13;
        const ARQUIVO_INVALIDO = 14;
        const REINSTALAR_VERSAO = 15;
        const ATUALIZACAO_DE_VERSAO_BLOQUEADA = 16;

        const BLOQUEADA = 17;
        const NAO_BLOQUEADA = 18;

        const ACESSO_NEGADO = 19;

        const SIM = 20;
        const NAO = 21;
        const USUARIO_JA_EXISTE_NO_SICOB_E_NO_SISTEMA = 22;
        const ERRO_EXECUCAO_CONSULTA_SQL = 23;
        const ERRO_SINCRONIZACAO = 24;
        const DUPLICADA_SOLUCIONADA = 25;
        const PROCESSO_EM_ABERTO = 28;
        const ERRO_PROCESSAMENTO = 2998;
        const SEM_DADOS_PARA_SEREM_SINCRONIZADOS = 32156;

        const BANCO_SQLITE_DEFAZADO = 235411;

        const SENHA_INCORRETA = 455;
        const REMOVIDO_ON_CASCADE = 2145436;

        const CORRIGIDO = 9142343456;
        const SEM_ALTERACAO = 9343456;
        const CRUD_PENALIZADO = 5654698432;
        const BANCO_FORA_DO_AR = 13216549;

        const CHAVE_TEMPO_EXPIRADA = -53;
        const CHAVE_INVALIDA = -54;

        const SERVIDOR_OCUPADO = -56;
        const EM_MANUTENCAO = -57;
        const ESTRUTURA_MIGRADA = -58;
        const CORPORACAO_DUPLICADA = -59;
        const CONFIGURACAO_DA_CORPORACAO_INDEFINIDO = -60;

        const OPERACAO_INCOMPLETA = -61;
        const ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE = -62;
        const ERRO_POR_FALTA_DE_HISTORICO_SINCRONIZACAO = -67;

        const AUTENTICACAO_INVALIDA = -71;

        const NUMERO_USUARIOS_EXCEDEU = -5448;
        const USUARIO_SEM_CORPORACAO = -5559;
        const USUARIO_NAO_CADASTRADO = -5560;
		const ERRO_UPLOAD = -5571;
		const SESSAO_EXPIRADA = 50;

        const FORA_HORARIO_FUNCIONAMENTO = 52830;

        const SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE = 273648;
    }


<?php

    class CSV_Reader
    {

        public static $listaModelos = array(

            "estrutura_organizacional" => "estrutura_organizacional.csv",
            "padrao_nivel_similaridade" => "padrao_nivel_similaridade.csv",
            "curriculo" => "curriculo.csv",
            "dados_pessoais" => "dados_pessoais.csv",
            "associacao_nome_vaga" => "associacao_nome_vaga.csv",
            "vaga" => "vaga.csv",
            "cargo" => "cargo.csv",
            "spv" => "spv.csv",
            "geral" => "geral.csv",
            "folha_pagamentos" => "folha_pagamentos.csv",
            "planejamento_treinamentos" => "planejamento_treinamentos.csv",
            "treinamentos_realizados" => "treinamentos_realizados.csv",

        );

        public static function getNomeArquivoModelo($tipo)
        {
            return self::$listaModelos[ $tipo ];
        }

        public $verificacaoCabecalho;
        public $cabecalho = array();
        public $tipoColunas;
        public $arquivo;
        public $entidade;
        public $contador;
        public $banco;
        public $dao;

        public function __construct($pathArquivo, $entidade = "")
        {
            $this->banco = new Database();
            $this->dao = new Generic_DAO();
            $this->contador = 0;
            $this->verificacaoCabecalho = false;

            $pathArquivo = Helper::acharRaiz() . "temp/" . $pathArquivo;

            if (file_exists($pathArquivo))
            {
                $this->arquivo = fopen($pathArquivo, "r+");

                if ($entidade != "")
                {
                    $cabecalho = fgets($this->arquivo);
                    $this->cabecalho = explode(";", $cabecalho);

                    $this->entidade = $entidade;
                    $this->verificarCabecalho();
                }
            }
            else
            {
                print "Erro ao abrir o arquivo CSV";
                exit(0);
            }
        }

        public function formatarCaractere($char)
        {
            if ($char == ";")
            {
                return "\n";
            }
            else
            {
                return $char;
            }
        }

        public function fgets($arquivo)
        {
            $linha = "";
            $char = "";

            while (($char = fgetc($arquivo)) != "\n")
            {
                if ($char === false)
                {
                    $linha = false;
                    break;
                }

                $pegouAspas = false;

                if ($char == "\"")
                {
                    while (($char = fgetc($arquivo)) != "\"")
                    {
                        $linha .= $this->formatarCaractere($char);
                    }

                    $pegouAspas = true;
                }

                if (!$pegouAspas)
                {
                    $linha .= $char;
                }
            }

            return $linha;
        }

        public function lerLinha()
        {
            $linha = $this->fgets($this->arquivo);

            if ($linha === false)
            {
                return false;
            }
            else
            {
                $dados = explode(";", $linha);

                $dadosRetorno;

                for ($i = 0; $i < count($dados); $i++)
                {
                    $dadosRetorno[ trim($this->cabecalho[ $i ]) ] = trim($dados[ $i ]);
                }

                $this->contador++;

                return $dadosRetorno;
            }
        }

        public function inserirLinha($linha)
        {
            if (!is_array($linha))
            {
                print "Erro no inserção da linha {$this->contador}!";
            }

            $insert = "INSERT INTO {$this->entidade}(";

            foreach ($linhas as $coluna => $valor)
            {
                $insert .= "{$coluna},";
            }

            $insert = Helper::removerOsUltimosCaracteresDaString($insert, 1);
            $insert .= ") VALUES (";

            foreach ($linhas as $coluna => $valor)
            {
                if ($this->tipoColunas[ $coluna ] == "varchar")
                {
                    $insert .= "{'$valor'},";
                }
                elseif ($this->tipoColunas[ $coluna ] == "int")
                {
                    if (is_int($valor))
                    {
                        $insert .= "{$valor},";
                    }
                    else
                    {
                        $insert .= "null,";
                    }
                }
                elseif ($this->tipoColunas[ $coluna ] == "float")
                {
                    $valor = $this->dao->formatarFloatParaComandoSQL($valor);

                    if (is_float($valor))
                    {
                        $insert .= "{$valor},";
                    }
                    else
                    {
                        $insert .= "null,";
                    }
                }
                elseif ($this->tipoColunas[ $coluna ] == "date")
                {
                    $insert .= "{$this->dao->formatarDataParaComandoSQL($valor)},";
                }
                elseif ($this->tipoColunas[ $coluna ] == "datetime")
                {
                    $insert .= "{$this->dao->formatarDataTimeParaComandoSQL(valor)},";
                }
                else
                {
                    $insert .= "'$valor',";
                }
            }

            $insert = Helper::removerOsUltimosCaracteresDaString($insert, 1);
            $insert .= ");";

            $this->banco->query($insert);
        }

        public function getCabecalho()
        {
            $linha = fgets($this->arquivo);

            if ($linha === false)
            {
                return false;
            }
            else
            {
                $dados = explode(";", $linha);

                $i = 0;

                foreach ($dados as $valor)
                {
                    $this->cabecalho[ $i ] = $valor;
                    $i++;
                }

                return true;
            }
        }

        public function verificarCabecalho()
        {
            $this->banco->query("SHOW COLUMNS FROM {$this->entidade}");

            if ($this->banco->rows == 0)
            {
                print "Erro no cabecalho, favor conferir o nome da entidade!";
                exit();
            }

            $colunas = array();

            for ($i = 0; $dados = $this->banco->fetchArray(); $i++)
            {
                $colunas[ $i ] = $dados["Field"];

                if (($chave = array_search($dados["Field"], $this->cabecalho)))
                {
                    $this->tipoColunas[ $this->cabecalho[ $chave ] ] = substr($dados["Type"], 0, strpos($dados["Type"], "("));
                }
            }

            if ($this->verificacaoCabecalho)
            {
                for ($i = 0; $i < count($this->cabecalho); $i++)
                {
                    if (!in_array($this->cabecalho[ $i ], $colunas))
                    {
                        print "<br /><br />";
                        print "Erro no cabecalho, favor conferir o nome das colunas!<br />";
                        print "Coluna Inválida: " . $this->cabecalho[ $i ] . " (coluna " . $i . ")<br />";
                        print "Colunas Válidas: ";
                        print_r($colunas);
                        exit();
                    }
                }
            }
        }

        public function __destruct()
        {
            fclose($this->arquivo);
        }

    }

?>

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of HelperAPCRedis
     *
     * @author home
     */
    class HelperStaticAPCRedis
    {
        static $singletonR = null;

        public static function getSingleton()
        {
            if (HelperAPCRedis::$singletonR == null)
            {
                HelperAPCRedis::$singletonR = new HelperAPCRedis();
            }

            return HelperAPCRedis::$singletonR;
        }

        private $helperRedis = null;
        private $helperAPC = null;
        private $hash = null;
        private $prefixo = null;

        public function __construct($prefixo = null)
        {
            // since we connect to default setting localhost
            // and 6379 port there is no need for extra
            // configuration. If not then you can specify the
            // scheme, host and port to connect as an array
            // to the constructor.
            if ($prefixo == null)
            {
                $prefixo = IDENTIFICADOR_SESSAO;
            }

            $this->prefixo = $prefixo;
            $this->helperRedis = new HelperRedis($prefixo);
            $this->helperAPC = new HelperAPC($prefixo);
            $this->hash = array();
        }

        //put your code here
        public function add($key, $value, $ttlApc = 0)
        {
            $this->helperAPC->add($key, $value, $ttlApc);
            $this->helperRedis->set($key, $value);
            $this->hash[ $key ] = $value;
        }

        public function exists($key)
        {
            if (!$this->hash[ $key ])
            {
                if (!$this->helperAPC->exists($key))
                {
                    return $this->helperRedis->exists($key);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public function getIfExists($key, $default = null, $ttlApc = 0)
        {
            if (isset($this->hash[ $key ]))
            {
                return $this->hash[ $key ];
            }

            if (!$this->helperAPC->exists($key))
            {
                if ($this->helperRedis->exists($key))
                {
                    $value = $this->helperRedis->get($key);
                    $this->helperAPC->add($key, $value, $ttlApc);
                    $this->hash[ $key ] = $value;

                    return $value;
                }
                else
                {
                    return $default;
                }
            }
            else
            {
                $v = $this->helperAPC->get($key);
                $this->hash[ $key ] = $v;

                return $v;
            }
        }

        public function hgetIfExists($hash, $key, $default = null, $ttlApc = 0)
        {
            if (isset($this->hash[ $hash ][ $key ]))
            {
                return $this->hash[ $hash ][ $key ];
            }

            if (!$this->helperAPC->exists($hash . $key))
            {
                if ($this->helperRedis->exists($hash . $key))
                {
                    $value = $this->helperRedis->hget($hash, $key);
                    $this->helperAPC->add($hash . $key, $value, $ttlApc);

                    if (!isset($this->hash[ $hash ]))
                    {
                        $this->hash[ $hash ] = array();
                    }
                    $this->hash[ $hash ][ $key ] = $value;

                    return $value;
                }
                else
                {
                    return $default;
                }
            }
            else
            {
                $value = $this->helperAPC->get($hash . $key);
                if (!isset($this->hash[ $hash ]))
                {
                    $this->hash[ $hash ] = array();
                }
                $this->hash[ $hash ][ $key ] = $value;

                return $value;
            }
        }

        //put your code here
        public function hset($hash, $key, $value, $ttlApc = 0)
        {
            $this->helperAPC->add($hash . $key, $value, $ttlApc);
            $this->helperRedis->hset($key, $value);
            if (!isset($this->hash[ $hash ]))
            {
                $this->hash[ $hash ] = array();
            }
            $this->hash[ $hash ][ $key ] = $value;
        }

        public function hmsetAPCRedis($hash, $matrixHash, $ttlApc = 0)
        {
            foreach ($matrixHash as $h => $v)
            {
                $this->helperAPC->add($hash . $h, $v, $ttlApc);
            }
            if (!isset($this->hash[ $hash ]))
            {
                $this->hash[ $hash ] = array();
            }
            $this->hash[ $hash ] = $matrixHash;

            $this->helperRedis->hmset($hash, $matrixHash);
        }

        public function hmsetOnlyRedis($hash, $matrixHash, $ttlApc = 0)
        {
            $this->helperRedis->hmset($hash, $matrixHash);
        }

    }

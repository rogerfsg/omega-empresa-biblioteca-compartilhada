<?php

    class JavascriptLogger
    {
        const __JAVASCRIPT_REMOTE_DEBUG_DEFAULT_SERVER = "http://www.omegasoftware.com.br:1337";
        const __JAVASCRIPT_REMOTE_DEBUG_DEFAULT_TIMEOUT_SECONDS = 3;
        const __JAVASCRIPT_LOGGER_FRAMEWORK_VERSION = "1.4.13";

        const __SCRIPT_START_TAG = "<script type=\"text/javascript\">";
        const __SCRIPT_END_TAG = "</script>";
        const __DEFAULT_OMEGALOG_INSTANCE_NAME = "omegaLog";
        const __DEFAULT_OMEGALOG_POPUP_APPENDER_INSTANCE_NAME = "popUpAppender";
        const __DEFAULT_AJAX_APPENDER_BATCHSIZE = 10;
        const __DEFAULT_AJAX_APPENDER_TIMERINTERVAL_IN_SECONDS = 20;

        const LOGLEVEL_DEBUG = 1;
        const LOGLEVEL_INFO = 2;
        const LOGLEVEL_WARNING = 3;
        const LOGLEVEL_ERROR = 4;
        const LOGLEVEL_FATAL = 5;

        private $omegaLogInstanceName;
        private $omegaLogPopupAppenderInstanceName;
        private $angularAppModuleName;
        private $remoteDebugDetectionTimeoutInSeconds;

        public function __construct($angularAppModuleName = null)
        {
            $this->omegaLogInstanceName = null;
            $this->omegaLogPopupAppenderInstanceName = null;
            $this->angularAppModuleName = $angularAppModuleName;
        }

        public static function getCurrentEnvironment()
        {
            return Helper::isDebugModeActive() ? 'development' : 'production';
        }

        public function importByEnvironment()
        {
            $enrironment = self::getCurrentEnvironment();
            switch ($enrironment)
            {
                case 'production':
                    return $this->importForProductionEnvironment();
                    break;

                case 'development':
                    return $this->importForDevelopmentEnvironment();
                    break;
            }
        }

        public function importForProductionEnvironment()
        {
            $this->setOmegaLogInstanceName('omegaLog');
            $this->setOmegaLogPopupAppenderInstanceName('logPopupAppender');
            $this->setRemoteDebugDetectionTimeoutInSeconds(2);

            $strRetorno = "";
            $strRetorno .= self::importLogLibary();
            $strRetorno .= self::importRemoteDebugLibrary();

            $strRetorno .= $this->defineLogInstanceWithDefaultOptions();
            $strRetorno .= $this->setAjaxAppender(self::LOGLEVEL_WARNING);
            $strRetorno .= $this->setGoogleAnalyticsAppender(self::LOGLEVEL_WARNING);
            $strRetorno .= $this->setBrowserConsoleAppender(self::LOGLEVEL_DEBUG);

            if (!is_null($this->angularAppModuleName))
            {
                $strRetorno .= $this->overrideAngularLogger();
            }

            $strRetorno .= $this->setListenterToLogAllUncatchErrors();

            return $strRetorno;
        }

        public function importForDevelopmentEnvironment()
        {
            $this->setOmegaLogInstanceName('omegaLog');
            $this->setOmegaLogPopupAppenderInstanceName('logPopupAppender');
            $this->setRemoteDebugDetectionTimeoutInSeconds(5);

            $strRetorno = "";
            $strRetorno .= self::importLogLibary();
            $strRetorno .= self::importRemoteDebugLibrary();

            $strRetorno .= $this->defineLogInstanceWithDefaultOptions();
            $strRetorno .= $this->setPopupAppender(self::LOGLEVEL_DEBUG);
            $strRetorno .= $this->setAjaxAppender(self::LOGLEVEL_DEBUG);
            $strRetorno .= $this->setBrowserConsoleAppender(self::LOGLEVEL_DEBUG);
            $strRetorno .= $this->setGoogleAnalyticsAppender(self::LOGLEVEL_DEBUG);

            if (!is_null($this->angularAppModuleName))
            {
                $strRetorno .= $this->overrideAngularLogger();
            }

            $strRetorno .= $this->setHotkeyCombinationToOpenLoggerPopup();
            $strRetorno .= $this->setListenterToLogAllUncatchErrors();

            return $strRetorno;
        }

        //recupera o endere�o para que o log javascript envie os dados
        //para que sejam gravados no servidor - varia de acordo com a aplica��o e servidor em quest�o
        public function getAjaxDefaultRemoteEndpoint()
        {
            return "webservice.php?class=JavascriptLogger&action=serverSideReceiver";
        }

        //tanto para desenvolvimento quanto para produ��o
        //caso receba parametro por GET 'true', ativa parametro na sess�o para persistir para as demais requisi��es da sess�o
        //caso receba parametro por GET 'false', deleta na sess�o para n�o persistir nas demais requisi��es da sess�o
        public function isRemoteDebugActive()
        {
            $isSessionStarted = Helper::isPhpSessionStarted();
            if ($isSessionStarted && Helper::SESSION('remoteDebug') == 'true')
            {
                return true;
            }
            elseif (Helper::GET('remoteDebug') != null && Helper::GET('remoteDebug') == 'false')
            {
                if ($isSessionStarted)
                {
                    unset($_SESSION['remoteDebug']);
                }

                return false;
            }
            elseif (Helper::GET('remoteDebug') != null && Helper::GET('remoteDebug') == 'true')
            {
                if ($isSessionStarted)
                {
                    $_SESSION['remoteDebug'] = 'true';
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public function detectIfRemoteDebugServerIsAlive()
        {
            $helperAPC = HelperAPC::getSingleton();
            $APCkey = 'remoteDebugCheck';
            $APCttl = 60 * 5; //em segundos

            if ($helperAPC->exists($APCkey))
            {
                return $helperAPC->get($APCkey);
            }
            else
            {
                $url = static::__JAVASCRIPT_REMOTE_DEBUG_DEFAULT_SERVER;
                try
                {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
                    curl_setopt($ch, CURLOPT_NOBODY, true);
                    curl_setopt($ch, CURLINFO_CONTENT_TYPE, 'text/javascript');
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->getRemoteDebugDetectionTimeoutInSeconds());

                    $result = curl_exec($ch);

                    if (!curl_errno($ch))
                    {
                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        $returnCode = ($httpcode >= 200 && $httpcode < 300);
                        $helperAPC->add($APCkey, $returnCode, $APCttl);

                        return $returnCode;
                    }
                    else
                    {
                        $helperAPC->add($APCkey, false, $APCttl);

                        return false;
                    }
                }
                catch (Exception $e)
                {
                    if (!is_null($ch))
                    {
                        curl_close($ch);
                    }
                    $helperAPC->add($APCkey, false, $APCttl);

                    return false;
                }
            }
        }

        public function getOmegaLogInstanceName()
        {
            return is_null($this->omegaLogInstanceName) ? self::__DEFAULT_OMEGALOG_INSTANCE_NAME : $this->omegaLogInstanceName;
        }

        public function setOmegaLogInstanceName($omegaLogInstanceName)
        {
            $this->omegaLogInstanceName = $omegaLogInstanceName;
        }

        public function getOmegaLogPopupAppenderInstanceName()
        {
            return is_null($this->omegaLogPopupAppenderInstanceName) ? self::__DEFAULT_OMEGALOG_POPUP_APPENDER_INSTANCE_NAME : $this->omegaLogPopupAppenderInstanceName;
        }

        public function setOmegaLogPopupAppenderInstanceName($omegaLogPopupAppenderInstanceName)
        {
            $this->omegaLogPopupAppenderInstanceName = $omegaLogPopupAppenderInstanceName;
        }

        public function getRemoteDebugDetectionTimeoutInSeconds()
        {
            return is_null($this->remoteDebugDetectionTimeoutInSeconds) || !is_numeric($this->remoteDebugDetectionTimeoutInSeconds)
                ? self::__JAVASCRIPT_REMOTE_DEBUG_DEFAULT_TIMEOUT_SECONDS : $this->remoteDebugDetectionTimeoutInSeconds;
        }

        public function setRemoteDebugDetectionTimeoutInSeconds($remoteDebugDetectionTimeoutInSeconds)
        {
            $this->remoteDebugDetectionTimeoutInSeconds = $remoteDebugDetectionTimeoutInSeconds;
        }

        public function getAngularAppModuleName()
        {
            return $this->angularAppModuleName;
        }

        public function setAngularAppModuleName($angularAppModuleName)
        {
            $this->angularAppModuleName = $angularAppModuleName;
        }

        private static function getFrameworkLogLevel($omegaLogLevel)
        {
            switch ($omegaLogLevel)
            {
                case self::LOGLEVEL_DEBUG:

                    return "log4javascript.Level.DEBUG";
                    break;

                case self::LOGLEVEL_INFO:

                    return "log4javascript.Level.INFO";
                    break;

                case self::LOGLEVEL_WARNING:

                    return "log4javascript.Level.WARN";
                    break;

                case self::LOGLEVEL_ERROR:

                    return "log4javascript.Level.ERROR";
                    break;

                case self::LOGLEVEL_FATAL:

                    return "log4javascript.Level.FATAL";
                    break;
            }
        }

        public static function getLogLibraryScriptFileByEnrionment()
        {
            $enrironment = self::getCurrentEnvironment();
            switch ($enrironment)
            {
                case 'production':
                    return 'log4javascript.js'; //'log4javascript_production.js';
                    break;

                case 'development':
                    return 'log4javascript.js';
                    break;
            }
        }

        private static $__alreadyImportedLogLibrary = false;

        public static function importLogLibary()
        {
            if (!self::$__alreadyImportedLogLibrary)
            {
                $omegaLogBasePath = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/omegalog/";
                $libraryBasePath = Helper::pathCombine($omegaLogBasePath, "log4javascript/" . self::__JAVASCRIPT_LOGGER_FRAMEWORK_VERSION);
                $strScript = self::getLogLibraryScriptFileByEnrionment();

                $fullOmegaLogPath = Helper::pathCombine($omegaLogBasePath, "OmegaLog.js");
                $fullLibrayPath = Helper::pathCombine($libraryBasePath, $strScript);

                self::$__alreadyImportedLogLibrary = true;

                return "<script type=\"text/javascript\"src=\"{$fullLibrayPath}\"></script>" .
                    "<script type=\"text/javascript\"src=\"{$fullOmegaLogPath}\"></script>";
            }
        }

        private static $__alreadyImportedRemoteDebugLibrary = false;

        public function importRemoteDebugLibrary()
        {
            if ($this->isRemoteDebugActive() && $this->detectIfRemoteDebugServerIsAlive())
            {
                $strUrlRemoteDebug = self::__JAVASCRIPT_REMOTE_DEBUG_DEFAULT_SERVER;
                if (!static::$__alreadyImportedRemoteDebugLibrary)
                {
                    return "<script type=\"text/javascript\"src=\"{$strUrlRemoteDebug}\"></script>";
                }
            }
        }

        public static function getPHPSessionIdentifier()
        {
            if (defined('IDENTIFICADOR_SESSAO'))
            {
                return IDENTIFICADOR_SESSAO;
            }
            else
            {
                return session_id();
            }
        }

        public static function getUserId()
        {
            if (defined('IDENTIFICADOR_SESSAO'))
            {
                return IDENTIFICADOR_SESSAO;
            }
            else
            {
                return session_id();
            }
        }


        public function defineLogInstanceWithDefaultOptions()
        {
            //identifica a sess�o php para iniciar inst�ncia do log
            $identificadorSessao = self::getPHPSessionIdentifier();
            $omegalogInstance = $this->getOmegaLogInstanceName();

            return self::__SCRIPT_START_TAG . "
        
            var {$omegalogInstance} = new OmegaLog('{$identificadorSessao}');
            
            " . self::__SCRIPT_END_TAG;
        }

        public function setGoogleAnalyticsAppender($threshold = self::LOGLEVEL_DEBUG)
        {
            $omegalogInstance = $this->getOmegaLogInstanceName();
            $appenderVariableName = "gaAppender";

            $strVariaveis = "";
            $strConfig = "";

            if(defined(FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO))
            {
                $forcarDesativacao = FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO ? "true" : "false";
                $strVariaveis .= "var FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO = {$forcarDesativacao};\r\n";
            }

            if(defined(FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO))
            {
                $forcarAtivacao = FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO ? "true" : "false";
                $strVariaveis .= "var FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO = '{$forcarAtivacao}';\r\n";
            }

            $strConfig .= "{$appenderVariableName}.setThreshold({$threshold});\r\n";

            if(static::getUserId())
            {
                $userId = static::getUserId();
                $strConfig .= "{$appenderVariableName}.setUserId('{$userId}');\r\n";
            }

            if(defined(IDENTIFICADOR_GOOGLE_ANALYTICS) && defined(DOMINIO_GOOGLE_ANALYTICS))
            {
                $identificador = IDENTIFICADOR_GOOGLE_ANALYTICS;
                $dominio = DOMINIO_GOOGLE_ANALYTICS;

                $strRetorno =

                    self::__SCRIPT_START_TAG . "
        
                    {$strVariaveis}
                    
                    var {$appenderVariableName} = new GoogleAnalyticsAppender('{$identificador}', '{$dominio}');
                    
                    //Configura��es default do appender Google Analytics
                    {$strConfig}
                    
                    //Adicionar appender ao log
                    {$omegalogInstance}.addGoogleAnalyticsAppender(gaAppender);
                    
                    " . self::__SCRIPT_END_TAG;

            }



            return $strRetorno;

        }

        public function setPopupAppender($threshold = self::LOGLEVEL_DEBUG)
        {
            $omegalogInstance = $this->getOmegaLogInstanceName();
            $popupAppenderInstance = $this->getOmegaLogPopupAppenderInstanceName();
            $frameworkThresholf = self::getFrameworkLogLevel($threshold);

            return self::__SCRIPT_START_TAG . "
        
            var {$popupAppenderInstance} = new log4javascript.PopUpAppender();
    
            //Configura��es default do appender de popup
            {$popupAppenderInstance}.setFocusPopUp(false);            
            {$popupAppenderInstance}.setShowCommandLine(true);
            {$popupAppenderInstance}.setNewestMessageAtTop(true);            
            {$popupAppenderInstance}.setInitiallyMinimized(false);
            {$popupAppenderInstance}.setScrollToLatestMessage(true);
            {$popupAppenderInstance}.setThreshold({$frameworkThresholf});
        
            //Adicionar appender ao log
            {$omegalogInstance}.addAppender({$popupAppenderInstance});
            
            " . self::__SCRIPT_END_TAG;
        }

        public function setBrowserConsoleAppender($threshold = self::LOGLEVEL_DEBUG)
        {
            $omegalogInstance = $this->getOmegaLogInstanceName();
            $frameworkThresholf = self::getFrameworkLogLevel($threshold);

            return self::__SCRIPT_START_TAG . "
        
            var consoleAppender = new log4javascript.BrowserConsoleAppender();
            
            //Configura��es default do appender de console
            consoleAppender.setThreshold({$frameworkThresholf});
            
            //Adicionar appender ao log
            {$omegalogInstance}.addAppender(consoleAppender);
            
            " . self::__SCRIPT_END_TAG;
        }

        public function setAjaxAppender($threshold = self::LOGLEVEL_DEBUG, $batchSize = self::__DEFAULT_AJAX_APPENDER_BATCHSIZE, $timerInterval = self::__DEFAULT_AJAX_APPENDER_TIMERINTERVAL_IN_SECONDS, $remoteEndpoint = null)
        {
            $omegalogInstance = $this->getOmegaLogInstanceName();
            $frameworkThresholf = self::getFrameworkLogLevel($threshold);

            if (is_null($remoteEndpoint))
            {
                $remoteEndpoint = self::getAjaxDefaultRemoteEndpoint();
            }

            if (!is_numeric($batchSize))
            {
                $batchSize = self::__DEFAULT_AJAX_APPENDER_BATCHSIZE;
            }

            if (!is_numeric($timerInterval))
            {
                $timerInterval = self::__DEFAULT_AJAX_APPENDER_TIMERINTERVAL_IN_SECONDS;
            }

            return self::__SCRIPT_START_TAG . "
            
            var ajaxAppender = new log4javascript.AjaxAppender('{$remoteEndpoint}');
            
            //Configura��es default do appender Ajax
            ajaxAppender.setThreshold({$frameworkThresholf});
            
            //enviar em pacotes, para minimizar n�mero de chamadas ao servidor
            ajaxAppender.setBatchSize({$batchSize});
            
            //envia ao servidor todas as mensagens de log n�o-enviadas antes de fechar o browser
            ajaxAppender.setSendAllOnUnload();
            
            //tempo padr�o para envio dos requests caso o 'Batch Size' n�o tenha sido atingido
            ajaxAppender.setTimed(true);
            ajaxAppender.setTimerInterval({$timerInterval}*1000);
            
            //define saida em JSON
            var jsonLayout = new log4javascript.JsonLayout();
            ajaxAppender.setLayout(jsonLayout);
            
            //define header para envio via Payload (padr�o da aplica��o)
            ajaxAppender.addHeader(\"Content-Type\", \"application/json\");
            
            {$omegalogInstance}.addAppender(ajaxAppender);        
            
            " . self::__SCRIPT_END_TAG;
        }

        public function setHotkeyCombinationToOpenLoggerPopup()
        {
            $popupAppenderInstance = $this->getOmegaLogPopupAppenderInstanceName();

            return self::__SCRIPT_START_TAG . "
        
                //listener para escutar combina��o de teclas CTRL + SHIFT + L
                //para mostrar janela popup do log
                var omegaLogKeyPressListener = function(e) 
                {              
                      var evtobj = window.event ? event : e;
                      if (evtobj.keyCode == 76 && evtobj.ctrlKey && evtobj.shiftKey)
                      {
                            {$popupAppenderInstance}.show();
                      }              
                };
                
                //setar listener no documento
                document.onkeydown = omegaLogKeyPressListener;
        
        " . self::__SCRIPT_END_TAG;
        }

        public function overrideAngularLogger()
        {
            $identificadorSessao = self::getPHPSessionIdentifier();
            $omegalogInstance = $this->getOmegaLogInstanceName();

            if (!is_null($this->angularAppModuleName))
            {
                return self::__SCRIPT_START_TAG . "

              angular.module('{$this->angularAppModuleName}').factory('\$log', function() {
                              
                angularLogger = log4javascript.getLogger('{$identificadorSessao}');                
                return angularLogger;
                
              });
              
            " . self::__SCRIPT_END_TAG;
            }
        }

        public function setListenterToLogAllUncatchErrors()
        {
            $omegalogInstance = $this->getOmegaLogInstanceName();

            return self::__SCRIPT_START_TAG . "
        
            window.onerror = function(errorMsg, url, lineNumber) 
            {
                {$omegalogInstance}.fatal(\"Exce��o n�o tratada: \" + errorMsg + \" em \" + url + \":\" + lineNumber);
            };
                
        " . self::__SCRIPT_END_TAG;
        }

        public function serverSideReceiver()
        {
            $jsonResponse = file_get_contents("php://input");

            try
            {
                if (is_array($jsonResponse))
                {
                    for ($i = 0; $i < count($jsonResponse); $i++)
                    {
                        $item = $jsonResponse[ $i ];
                        $loggerName = $item->logger;
                        $timestamp = $item->timestamp;
                        $level = $item->level;
                        $url = $item->url;
                        $message = $item->message;
                    }
                }
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }

        public static function factory()
        {
            return new JavascriptLogger();
        }

    }

?>
<?php
    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 12/08/2017
     * Time: 11:04
     */

    class BOSicobComum
    {

        public static function getNomeCorporacao($idCorporacao)
        {
            $singleton = new HelperRedis(HelperRedis::PREFIXO_REDIS_COMUM);

            $corporacao = $singleton->get("NC_{$idCorporacao}");
            if (!empty($corporacao))
            {
                return $corporacao;
            }

            $url = __DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getNomeCorporacao&id_corporacao=$idCorporacao";

            $json = Helper::chamadaCurlJson($url, null, null, false);

            if (Interface_mensagem::checkOk($json))
            {
                $v = $json->mValor;
                if (!Helper::startsWith($v, HelperRedis::PREFIXO_REDIS_COMUM))
                {
                    $singleton->set("NC_{$idCorporacao}", $v);
                }

                return $v;
            }
            else
            {
                if (Interface_mensagem::checkErro($json))
                {
                    throw new Exception($json->mMensagem);
                }
                else
                {
                    return null;
                }
            }
        }

    }
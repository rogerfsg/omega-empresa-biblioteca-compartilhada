<?php

    class Link
    {

        public $url;
        public $larguraGreyBox;
        public $alturaGreyBox;
        public $tituloGreyBox;
        public $cssClass;
        public $label;
        public $onclick;
        public $demaisAtributos;
        public $organograma;
        public $numeroNiveisPai;

        public function __construct()
        {
            $this->url = "javascript:void(0);";
            $this->larguraGreyBox = LARGURA_PADRAO_GREYBOX;
            $this->alturaGreyBox = ALTURA_PADRAO_GREYBOX;
            $this->tituloGreyBox = "";
            $this->cssClass = "link_padrao";
            $this->label = "";
            $this->onclick = "";
            $this->demaisAtributos = "";
            $this->numeroNiveisPai = 1;
        }

        private function montarStringGreyBox()
        {
            $strRetorno = "";

            if ($this->larguraGreyBox && $this->alturaGreyBox && $this->url)
            {
                if ($this->organograma)
                {
                    $strRetorno = "onclick=\"gerarLinkOrganograma(this);\"";

                    $this->demaisAtributos = array("rel" => "{$this->url}", "title" => $this->tituloGreyBox);
                }
                else
                {
                    $strRetorno = Helper::getLinkParaGreyBox($this->tituloGreyBox, $this->url, $this->larguraGreyBox, $this->alturaGreyBox, false, $this->numeroNiveisPai);
                }

                $this->url = "javascript:void(0);";
            }

            return $strRetorno;
        }

        public function montarStringClass()
        {
            $strRetorno = "class=\"{$this->cssClass}\"";

            return $strRetorno;
        }

        public function montarStringOnClick()
        {
            $string = "";
            if (strlen($this->onclick))
            {
                $string = "{$this->onclick};";
            }
//			else{
//
//			    if(strlen($complemento)){
//
//			        $string = "javascript: ";
//
//			    }
//			    else{
//
//			        return "";
//
//			    }
//
//			}

//			$strRetorno = "onclick=\"{$string} {$complemento}\"";
            $strRetorno = "onclick=\"{$string} \"";

            return $strRetorno;
        }

        public function montarStringDemaisAtributos()
        {
            $strRetorno = "";

            if (is_array($this->demaisAtributos))
            {
                foreach ($this->demaisAtributos as $atributo => $valor)
                {
                    $strRetorno .= "{$atributo}=\"{$valor}\" ";
                }
            }

            return $strRetorno;
        }

        public function montarLink()
        {
            $strRetorno = "<a {$this->montarStringGreyBox()} href=\"{$this->url}\" {$this->montarStringOnClick()} {$this->montarStringClass()} {$this->montarStringDemaisAtributos()}>{$this->label}</a>";

            return $strRetorno;
        }

    }

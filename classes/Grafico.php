<?php

    class Grafico
    {

        public $tituloDoGrafico;
        public $larguraDoGrafico;
        public $alturaDoGrafico;
        public $somenteAreasDaPesquisa = false;
        public $arrayNiveisAConsultar;
        public $consultarVagas = false;
        public $tipoDeGrafico = "padrao";

        public function __construct($titulo, $logicaPadrao = true)
        {
            $this->tituloDoGrafico = $titulo;

            if ($logicaPadrao)
            {
                $this->setLogicaPadrao();
            }
        }

        public function setLogicaPadrao()
        {
            $this->larguraDoGrafico = Helper::GET("largura") ? Helper::GET("largura") : LARGURA_GRAFICOS_INICIAL;
            $this->alturaDoGrafico = Helper::GET("altura") ? Helper::GET("altura") : ALTURA_GRAFICOS_INICIAL;

            $objEstruturaOrg = new Estrutura_Organizacional();

            if (is_numeric(Helper::GET("id_nivel0")))
            {
                $this->arrayNiveisAConsultar = $objEstruturaOrg->getArrayDeNodosAPartirDaVariavelGET();
            }
            elseif (is_numeric(Helper::GET("id_vaga0")))
            {
                $this->arrayNiveisAConsultar = $objEstruturaOrg->getArrayDeVagasAPartirDaVariavelGET();
                $this->consultarVagas = true;
            }

            if (Helper::GET("somente_areas_envolvidas") == "true")
            {
                $this->somenteAreasDaPesquisa = true;
            }

            if (count($this->arrayNiveisAConsultar) == 1 && !$this->consultarVagas)
            {
                $this->tituloDoGrafico .= "\n" . $objEstruturaOrg->getNomeDoNodo($this->arrayNiveisAConsultar[0]);
            }
            else
            {
                $this->tituloDoGrafico .= "\n" . TITULO_GRAFICO_ESTRATIFICADO;
            }
        }

        public function setLargura($largura)
        {
            $this->larguraDoGrafico = $largura;
        }

        public function setAltura($altura)
        {
            $this->alturaDoGrafico = $altura;
        }

        public function setTitulo($titulo)
        {
            $this->tituloDoGrafico = $titulo;
        }

        public function setTipoDeGrafico($tipo)
        {
            $this->tipoDeGrafico = $tipo;
        }

        public static $numeroPessoasGraficoAporte;
        public static $numeroAnosGraficoAporte;

        public static function valorImpressoGraficoAporteConhecimentoTacito($valor)
        {
            $chave = array_search($valor, self::$numeroAnosGraficoAporte);

            $numeroPessoas = self::$numeroPessoasGraficoAporte[ $chave ];

            if ($numeroPessoas == 1)
            {
                $legenda = "pessoa";
            }
            else
            {
                $legenda = "pessoas";
            }

            $impressao = "  {$valor} anos \n($numeroPessoas $legenda)";

            self::$numeroAnosGraficoAporte[ $chave ] = -1;

            return $impressao;
        }

        public static $legendaGraficosSingular;
        public static $legendaGraficosPlural;
        public static $valorTotal;

        public static function valorAbsolutoERelativoGraficos($valor)
        {
            if ($valor == 1)
            {
                $legendaGrafico = self::$legendaGraficosSingular;
            }
            else
            {
                $legendaGrafico = self::$legendaGraficosPlural;
            }

            $legenda = sprintf($legendaGrafico, $valor, round(($valor / self::$valorTotal) * 100, 1));

            return $legenda;
        }

        public static function getStringComMesEAnoDeTodosOsMesesDoIntervalo($mesMin, $mesMax)
        {
            $processando = true;
            $arrRetorno = array();

            $mesEAnoMin = explode(" ", $mesMin);
            $mesMin = $mesEAnoMin[1];
            $anoMin = $mesEAnoMin[0];

            $mesEAnoMax = explode(" ", $mesMax);
            $mesMax = $mesEAnoMax[1];
            $anoMax = $mesEAnoMax[0];

            $mesAtual = $mesMin;
            $anoAtual = $anoMin;

            while ($processando)
            {
                $arrRetorno[][0] = sprintf("%04d %02d", $anoAtual, $mesAtual);

                if ($mesAtual == $mesMax && $anoAtual == $anoMax)
                {
                    break;
                }

                $mesAtual++;

                if ($mesAtual > 12)
                {
                    $mesAtual = 1;
                    $anoAtual++;
                }
            }

            return $arrRetorno;
        }

    }

?>

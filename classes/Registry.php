<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Registry
 *
 * @author home
 */
class Registry {
    //put your code here
    static private $_store = array();
    
    static public function printr(){
        print_r(Registry::$_store);
    }
    
    static public function add($object, $name = null){
        $name = (!is_null($name)) ? $name : get_class($object);
        try{
            if($name == null) return null;

            if(isset(self::$_store[$name])){
                $return = self::$_store[$name];
                return $return;
            }
            self::$_store[$name] = $object;
            return null;
        }catch (Exception $ex){
            throw new Exception("Construcao do obj: ".print_r($object, true), 0, $ex);
        }

    }
    
    static public function get($name, $throwsException = true){
        if(!self::contains($name) ){
            if($throwsException)
            throw new Exception("Object '$name' does not exist in registry");
            else return null;
        }
        return self::$_store[$name];
    }
    static public function contains($name){
        if(!isset(self::$_store[$name])){
            return false;
        }
        return true;
    }
    
    static public function remove($name){
        if(self::contains($name)){
            unset(self::$_store[$name]);
        }
    }
}

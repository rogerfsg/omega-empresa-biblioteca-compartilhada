<?php

    class Planilha
    {

        private $classe;
        private $objeto;
        private $database;
        private $arrColunas;
        private $arrLabels;

        public function __construct($classe)
        {
            $this->classe = $classe;

            $this->objeto = call_user_func_array(array($this->classe, "factory"), array());

            $this->database = new Database();

            if (!is_object($this->objeto))
            {
                Helper::imprimirMensagem("Erro ao gerar Planilha.", MENSAGEM_ERRO);
                exit();
            }

            $this->construirArrColunas();
        }

        public function imprimirPlanilha()
        {
            echo "<table class=\"tabela_list\" style=\"width: 3000px;\">";

            echo $this->construirCabecalho();
            echo $this->construirLista();

            echo "</table>";
        }

        public function construirArrColunas()
        {
            $tabela = $this->objeto->nomeTabela;

            $this->database->query("SHOW COLUMNS FROM {$tabela}");

            $atributos = get_object_vars($this->objeto);

            while ($colunas = $this->database->fetchArray())
            {
                if ($colunas["Field"] != "dataCadastro" && $colunas["Field"] != "dataEdicao")
                {
                    $this->arrColunas[] = $colunas["Field"];
                    $this->arrLabels[] = $atributos["label_{$colunas["Field"]}"];
                }
            }
        }

        public function construirCabecalho()
        {
            $strRetorno = "<tr class=\"\">";

            foreach ($this->arrLabels as $label)
            {
                $strRetorno .= "<td class=\"\">{$label}</td>";
            }

            $strRetorno .= "</tr>";

            return $strRetorno;
        }

        public function construirCampo($coluna, $objArg, $idCorrente)
        {
            $strRetorno = "";

            if ($coluna == $this->objeto->campoId)
            {
                $strRetorno .= "<input type=\"checkbox\" name=\"check_{$idCorrente}\" id=\"check_{$idCorrente}\" />" . $objArg->valor;
            }
            else
            {
                $objArg->onFocus = "javascript: document.getElementById('check_{$idCorrente}').checked=true;";

                if (strpos($coluna, "_DATETIME") !== false)
                {
                    $objArg->valor = Helper::formatarDataTimeParaExibicao($objArg->valor);

                    $strRetorno .= "{$this->objeto->campoDataTime($objArg)}";
                }
                elseif (strpos($coluna, "_DATE") !== false)
                {
                    $objArg->valor = Helper::formatarDataParaExibicao($objArg->valor);

                    $strRetorno .= "{$this->objeto->campoData($objArg)}";
                }
                elseif (strpos($coluna, "_FLOAT") !== false)
                {
                    $objArg->valor = Helper::formatarFloatParaExibicao($objArg->valor);

                    $strRetorno .= "{$this->objeto->campoFloat($objArg)}";
                }
                elseif (strpos($coluna, "_id_INT") !== false)
                {
                    $colunaUC = str_replace("_id_INT", "", ucfirst($coluna));

                    $strRetorno .= eval("return \$this->objeto->obj{$colunaUC}->getComboBox(\$objArg);");
                }
                elseif (strpos($coluna, "_INT") !== false)
                {
                    $strRetorno .= "{$this->objeto->campoInteiro($objArg)}";
                }
                elseif (strpos($coluna, "_BOOLEAN") !== false)
                {
                    $objArg->largura = "";

                    $strRetorno .= "{$this->objeto->campoBoolean($objArg)}";
                }
                else
                {
                    $strRetorno .= "{$this->objeto->campoTexto($objArg)}";
                }
            }

            return $strRetorno;
        }

        public function construirLista()
        {
            $tabela = $this->objeto->nomeTabela;
            $campoId = $this->objeto->campoId;

            $strRetorno = "";

            $listaDeCampos = implode(", ", $this->arrColunas);

            $objBanco = new Database();

            $objBanco->query("SELECT {$listaDeCampos} FROM {$tabela} ORDER BY {$campoId}");

            for ($i = 0; $dados = $objBanco->fetchArray(MYSQL_ASSOC); $i++)
            {
                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";

                $strRetorno .= "<tr class=\"{$classTr}\">";

                $idCorrente = $dados[ $campoId ];

                foreach ($this->arrColunas as $coluna)
                {
                    $objArg = new Generic_Argument();

                    $objArg->largura = "100%";
                    $objArg->nome = "{$coluna}_{$idCorrente}";
                    $objArg->id = "{$coluna}_{$idCorrente}";
                    $objArg->valor = $dados[ $coluna ];

                    $campo = $this->construirCampo($coluna, $objArg, $idCorrente);

                    $strRetorno .= "<td class=\"\">{$campo}</td>";
                }

                $strRetorno .= "</tr>";
            }

            return $strRetorno;
        }

    }

?>

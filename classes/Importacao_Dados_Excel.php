<?php

    class Importacao_Dados_Excel
    {

        public $arquivoImportacao;
        public $numRegistrosInseridos;
        public $numRegistrosModificados;
        public $numRegistrosErro;
        public $numRegistrosWarning;
        public $recuperacaoErro;

        public static $arquivosDeModelo = array(

            "startup_area_1" => "secagem.xls",

        );

        public static $metodoDeImportacao = array(

            "startup_area_1" => "importarDadosDaMedicaoSecagem",

        );

        public function __construct()
        {
            $arquivo = $_FILES["arquivo_xls"];
            $path = Helper::acharRaiz() . "temp/";

            $objUpload = new Upload($arquivo, $path, true);

            $this->arquivoImportacao = $path . $objUpload->uploadFile();

            $this->numRegistrosInseridos = 0;
            $this->numRegistrosModificados = 0;
            $this->numRegistrosErro = 0;
            $this->numRegistrosWarning = 0;
            $this->recuperacaoErro = "";
        }

        public function acoesInicializacao($nomeDaImportacao)
        {
            Helper::imprimirCabecalhoParaFormatarAction();
            echo Javascript::importarTodasAsBibliotecas();

            $objBanco = new Database();
            $objBanco->iniciarTransacao();

            $objNivelSimilaridade = new EXTDAO_Nivel_similaridade();
            $objNivelSimilaridade->corrigirIndices();

            //faz um backup de todo o banco de dados antes de importar os dados
            $objBackup = new Database_Backup();

            $_POST["descricao"] = "Backup automático anterior à importação <b>{$nomeDaImportacao}</b> em " . Helper::getDiaEHoraAtual() . " feita por " . Helper::getNomeDoUsuarioCorrente();

            if (!$objBackup->fazerBackup())
            {
                Helper::mudarLocation(Helper::getNomeDoScriptAtual() . "?msgErro=A importação não foi concluída devido à falha no backup prévio do banco de dados");
                exit();
            }
        }

        public function acoesFinalizacao()
        {
            $objBanco = new Database();
            $objBanco->commitTransacao();

            $msg = Helper::imprimirMensagem("Operação concluída. A importação foi feita de acordo com o resumo abaixo:

        						  &bull; Registros inseridos no BD: {$this->numRegistrosInseridos}
        						  &bull; Registros do BD que foram alterados: {$this->numRegistrosModificados}
        						  &bull; Registros que não foram importados para o BD: {$this->numRegistrosErro}
        						  &bull; Registros que deram Warning mas foram importados: {$this->numRegistrosWarning}");

            $referer = $_SERVER["HTTP_REFERER"];

            $botaoOK = new Botao();
            $botaoOK->label = "OK";
            $botaoOK->tipo = "button";
            $botaoOK->action = "javascript:document.location.href='{$referer}'";
            $arrBotoes = array($botaoOK);

            echo Helper::imprimirBarraBotoes($arrBotoes);
        }

        public static function factory()
        {
            return new Importacao_Dados_Excel();
        }

        /*
         *
         * A PARTIR DAQUI, OS METO.DOS SAO OS ACTIONS PARA CADA TIPO DE ENTIDADE
         *
         */

        public function __verificarValorValidoExcel($valor)
        {
            return $valor;
        }

        public function __transformarData($data)
        {
            $pedacos = explode("/", $data);

            if (count($pedacos) == 3)
            {
                return "{$pedacos[1]}/{$pedacos[0]}/{$pedacos[2]}";
            }
        }

        public function __verificarDataValida($data)
        {
            $pedacosData = explode("/", $data);

            if (count($pedacosData) == 3 && strlen($pedacosData[0]) && strlen($pedacosData[1]) &&
                intval($pedacosData[2]) > 1980 && intval($pedacosData[2]) < 2020)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function __verificarHoraValida($hora)
        {
            if (substr_count($hora, ":") == 1)
            {
                $pedacosHora = explode(":", $hora);

                if (strlen($pedacosHora[0]) && strlen($pedacosHora[1]) &&
                    intval($pedacosHora[0]) >= 0 && intval($pedacosHora[0]) <= 23 &&
                    intval($pedacosHora[1]) >= 0 && intval($pedacosHora[1]) <= 59)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                return false;
            }
            elseif (substr_count($hora, ":") == 2)
            {
                $pedacosHora = explode(":", $hora);

                if (strlen($pedacosHora[0]) && strlen($pedacosHora[1] && strlen($pedacosHora[2])) &&
                    intval($pedacosHora[0]) >= 0 && intval($pedacosHora[0]) <= 23 &&
                    intval($pedacosHora[1]) >= 0 && intval($pedacosHora[1]) <= 59 &&
                    intval($pedacosHora[2]) >= 0 && intval($pedacosHora[2]) <= 59)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

    }

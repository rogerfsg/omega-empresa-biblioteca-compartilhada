<?php

    abstract class InterfaceMenu
    {
        const CHAVE_DE_CACHE_DO_MENU = 'menu_usuario';

        //Atributos
        private $arrMenu;
        private $arrPermissoes;
        private $crypt;
        private $usarCriptografia;
        public static $pathImagensMenu;

        //M�todos
        public function __construct($usarCriptografia = false)
        {
            $this->arrMenu = $this->getArrayDoMenuDoUsuarioLogado();
            $this->usarCriptografia = $usarCriptografia;
        }

        public abstract function getArrayDoConteudoDoMenuCompleto();

        public function getArrayDoMenuDoUsuarioLogado()
        {
            $conteudoMenuCache = Helper::SESSION(static::CHAVE_DE_CACHE_DO_MENU, true);
            if($conteudoMenuCache != null)
            {
                return $conteudoMenuCache;
            }
            else
            {
                //$conteudoMenuRestrito = $this->montarMenuDoUsuario();
                $conteudoMenuRestrito = $this->getArrayDoConteudoDoMenuCompleto();
                Helper::setSession(static::CHAVE_DE_CACHE_DO_MENU, $conteudoMenuRestrito);

                return $conteudoMenuRestrito;
            }
        }

        protected function montarMenuDoUsuario($idDoUsuario = null, $idDoTipoDoUsuario = null, $db = null)
        {
            //menu completo
            $arrayMenu = $this->getArrayDoConteudoDoMenuCompleto();

            if ($db == null)
            {
                $db = new Database();
            }

            if ($idDoUsuario == null)
            {
                $idDoUsuario = Seguranca::getId();
            }

            if ($idDoUsuario == null)
            {
                return $arrayMenu;
            }

            if ($idDoTipoDoUsuario == null)
            {
                $idDoTipoDoUsuario = Seguranca::getIdDoTipoDoUsuarioLogado();
            }

            $db->query("SELECT area_menu
                                  FROM usuario_menu
                                  WHERE usuario_id_INT={$idDoUsuario}");

            if ($db->rows() == 0)
            {
                $db->query("SELECT area_menu
                                  FROM usuario_tipo_menu
                                  WHERE usuario_tipo_id_INT={$idDoTipoDoUsuario}");
            }

            //caso n�o tenha sido definido nenhuma permiss�o para o usu�rio ou tipo de usu�rio, libera menu completo
            if ($db->rows() == 0)
            {
                return $arrayMenu;
            }
            //caso contr�rio, restringe o menu de acordo com as permiss�es definidas
            else
            {
                $arrPermissoesMenu = Helper::getResultSetToMatriz($db->getResultSet());
                $arrPermissoesMenu = Helper::getMatrizLinearToArray($arrPermissoesMenu);
                $this->restringirMenu($arrayMenu, $arrPermissoesMenu);
            }

            return $arrayMenu;
        }

        protected function restringirMenu(&$arrayArea, &$arrayDePermissoes)
        {
            foreach ($arrayArea as $chave => &$valor)
            {
                if ($chave == "config")
                {
                    continue;
                }

                if (!in_array($chave, $arrayDePermissoes))
                {
                    unset($arrayArea[ $chave ]);
                    continue;
                }

                if (is_array($valor))
                {
                    $this->restringirMenu($valor, $arrayDePermissoes);
                }
            }
        }

        private function varrerAreasDoMenu($area, $urlVerificacao, &$aba)
        {
            if (is_array($area))
            {
                foreach ($area as $chave => $valor)
                {
                    $retorno = $this->varrerAreasDoMenu($area[ $chave ], $urlVerificacao, $aba);

                    if ($retorno === true)
                    {
                        return true;
                    }
                }
            }
            else
            {
                if ($area != null)
                {
                    if (defined('MENUCONFIG_MENU_ABA') && $area->tipo == MENUCONFIG_MENU_ABA)
                    {
                        $aba = $area->link;
                    }
                    elseif (strpos($area->link, $urlVerificacao) !== false)
                    {
                        return true;
                    }
                }
            }
        }

        private function getPagina($arrSubMenu)
        {
            $separador = strpos($arrSubMenu, "_");

            $posSeparadorVariaveis = strpos($arrSubMenu, "#");
            $diferenca = $posSeparadorVariaveis - $separador;

            if ($diferenca > 0)
            {
                $page = $this->returnPage(substr($arrSubMenu, $separador + 1, $diferenca - 1));
            }
            else
            {
                $page = $this->returnPage(substr($arrSubMenu, $separador + 1));
            }

            return $page;
        }

        private function getTipo($arrSubMenu)
        {
            $separador = strpos($arrSubMenu, "_");
            $tipo = substr($arrSubMenu, 0, $separador);

            if ($tipo == "ajaxpages")
            {
                $tipo = "ajax_pages";
            }
            elseif ($tipo == "ajaxforms")
            {
                $tipo = "ajax_forms";
            }

            return $tipo;
        }

        public function criarMenus()
        {
            //$this->criarMenu();
            $this->criarMenuSeguro();
        }

        private function getVariaveis($arrSubMenu)
        {
            $posSeparadorVariaveis = strpos($arrSubMenu, "#");

            if ($posSeparadorVariaveis > 0)
            {
                $strVariaveis = substr($arrSubMenu, $posSeparadorVariaveis + 1);
            }
            else
            {
                $strVariaveis = "";
            }

            return $strVariaveis;
        }

        public function criarMenuPrincipal()
        {
            $pathImagens = MenuConfig::getPathImagensMenu();

            $objLink = new Link();
            $objLink->alturaGreyBox = 175;
            $objLink->larguraGreyBox = 450;
            $objLink->tituloGreyBox = "Alterar Senha";
            $objLink->url = "popup.php?page=alterar_senha&tipo=pages&navegacao=false";
            $objLink->demaisAtributos = array();
            $objLink->label = "<img src=\"{$pathImagens}alterar_senha.png\" alt=\"Alterar Senha\" /><span>Alterar Senha</span>";
            $objLink->numeroNiveisPai = 0;

            $linkAlterarSenha = $objLink->montarLink();

            echo "<ul class=\"orb\">
                    <li><a href=\"javascript:void(0);\" accesskey=\"1\" class=\"orbButton\">&nbsp;</a><span>Menu</span>
                        <ul>
                            <li><a href=\"index.php\">
                                <img src=\"{$pathImagens}pagina_inicial.png\" alt=\"P�gina Inicial\" /><span>P�gina Inicial</span></a> </li>
                    
                                        <li><a href=\"index.php?page=o_estudo&tipo=view\">
                                <img src=\"{$pathImagens}projeto.png\" alt=\"Projeto\" /><span>Projeto</span></a> </li>
                    
                            <li>{$linkAlterarSenha}</li>
                    
                            <li><a href=\"actions.php?class=Seguranca&action=logout\">
                                <img src=\"{$pathImagens}sair.png\" alt=\"Sair\" /><span>Sair</span></a> </li>
                            </ul>
                        </li>
                    </ul>";
        }

        public function criarMenuSeguro()
        {
            echo "<ul id=\"MenuBar1\" style=\"display: none;\" class=\"MenuBarHorizontal\">";
            if (is_array($this->arrMenu))
            {
                foreach ($this->arrMenu as $titulo => $arrSubMenu)
                {
                    if (is_array($arrSubMenu))
                    {
                        $this->criarMenuAreaSeguro($titulo, $arrSubMenu);
                    }
                    else
                    {
                        if ($titulo != "config")
                        {
                            $pageLink = $this->getPagina($arrSubMenu);
                            $tipoLink = $this->getTipo($arrSubMenu);
                            $variaveisLink = $this->getVariaveis($arrSubMenu);

                            echo "<li>
                                      <a href=\"index.php?page={$pageLink}&tipo={$tipoLink}&{$variaveisLink}\">$titulo</a>
                                  </li>";
                        }
                    }
                }
            }

            echo "</ul>";
        }

        private function criarMenuAreaSeguro($titulo, $arr, $criarNovoMenu = true)
        {
            if ($criarNovoMenu)
            {
                echo "
    		<li><a href=\"#\" class=\"MenuBarItemSubmenu\">{$titulo}</a>
    			<ul>";
            }

            foreach ($arr as $tituloArea => $paginaArea)
            {
                if (is_array($paginaArea))
                {
                    if (trim($tituloArea) == trim($titulo))
                    {
                        $criarNovoMenuArea = false;
                    }
                    else
                    {
                        $criarNovoMenuArea = true;
                    }

                    $this->criarMenuAreaSeguro($tituloArea, $paginaArea, $criarNovoMenuArea);
                }
                else
                {
                    if ($tituloArea != "config")
                    {
                        $link = $paginaArea->link;

                        $pageLink = $this->getPagina($link);
                        $tipoLink = $this->getTipo($link);
                        $variaveisLink = $this->getVariaveis($link);

                        echo "<li>
                                 <a href=\"index.php?page={$pageLink}&tipo={$tipoLink}&{$variaveisLink}\">{$tituloArea}</a>
                              </li>";
                    }
                }
            }

            if ($criarNovoMenu)
            {
                echo "
    			</ul>
    		</li>";
            }
        }

        public function criarMenu()
        {
            echo "<ul class=\"ribbon\" id=\"menu_ribbon\" style=\"display: none;\">"
                . "<li>";

            $this->criarMenuPrincipal();

            echo "<ul class=\"menu\">";
            if (is_array($this->arrMenu))
            {
                foreach ($this->arrMenu as $titulo => $arrSubMenu)
                {
                    if (is_array($arrSubMenu))
                    {
                        $this->criarMenuArea($titulo, $arrSubMenu);
                    }
                    else
                    {
                        $pageLink = $this->getPagina($arrSubMenu);
                        $tipoLink = $this->getTipo($arrSubMenu);
                        $variaveisLink = $this->getVariaveis($arrSubMenu);

                        echo "
                        <li>
                                <a class=\"orbButton\" href=\"index.php?page={$pageLink}&tipo={$tipoLink}&{$variaveisLink}\">{$titulo}</a>
                        </li>";
                    }
                }
            }

            echo "</ul>
                 </li>
              </ul>";
        }

        private function criarMenuArea($titulo, $arr, $tipoPai = false)
        {
            $impressaoDoNivelSuperior = false;

            foreach ($arr as $tituloArea => $paginaArea)
            {
                $tipo = null;
                if ($tituloArea == "config")
                {
                    $impressaoDoNivelSuperior = true;

                    $tituloAlt = $paginaArea->label;
                    $link = $paginaArea->link;
                    $accesskey = $paginaArea->accesskey;
                    $tipo = $paginaArea->tipo;

                    if (strlen($tituloAlt))
                    {
                        $titulo = $tituloAlt;
                    }

                    if ($tipo == MENUCONFIG_MENU_ABA)
                    {
                        $id = str_replace("#", "", $link);

                        echo "<li><a class=\"orbButton\" id=\"$id\" href=\"javascript:void(0);\" accesskey=\"{$accesskey}\">$titulo</a>
                                                <ul>";
                    }
                    elseif ($tipo == MENUCONFIG_LISTA_HORIZONTAL)
                    {
                        echo "<li>
                            <h2>
                            <span>{$titulo}</span>
                        </h2>";
                    }
                    elseif ($tipo == MENUCONFIG_LISTA_VERTICAL)
                    {
                        $tipoPaiOriginal = $tipoPai;

                        $margin = floor((90 - ((count($arr) - 1) * 20)) / 2);

                        if ($tipoPai == MENUCONFIG_LISTA_HORIZONTAL)
                        {
                            echo " <div class=\"ribbon-list\" style=\"margin-top: {$margin}px;\">";
                        }
                        else
                        {
                            echo "<li>
                                        <h2>
                                                <span>{$titulo}</span>
                                        </h2>
                                        <div class=\"ribbon-list\" style=\"margin-top: {$margin}px;\">";
                        }
                    }
                    elseif ($tipo == MENUCONFIG_DROPDOWN)
                    {
                        $imagem = $paginaArea->imagem;

                        $strStyle = $this->getStringCssComDimensoesDaImagem($tipo);

                        echo "<div>
                                    <img style=\"{$strStyle}\" src=\"{$imagem}\" alt=\"{$titulo}\" />
                                    {$titulo}
                                    <ul>";
                    }

                    $tipoPai = $tipo;
                }
                else
                {
                    if (is_array($paginaArea))
                    {
                        $this->criarMenuArea($tituloArea, $paginaArea, $tipo);
                    }
                    else
                    {
                        $link = $paginaArea->link;
                        $imagem = $paginaArea->imagem;

                        $pageLink = $this->getPagina($link);
                        $tipoLink = $this->getTipo($link);
                        $variaveisLink = $this->getVariaveis($link);

                        $strStyle = $this->getStringCssComDimensoesDaImagem($tipoPai);

                        $strOnClick = "document.location.href='index.php?page={$pageLink}&tipo={$tipoLink}&{$variaveisLink}'";

                        if ($tipoPai === MENUCONFIG_DROPDOWN)
                        {
                            echo "<li><span onclick=\"{$strOnClick}\">{$tituloArea}</span></li>";
                        }
                        else
                        {
                            echo "<div onclick=\"{$strOnClick}\">
                                            <img style=\"{$strStyle}\" src=\"{$imagem}\"/>
                                            {$tituloArea}
                                      </div>";
                        }
                    }
                }
            }

            if ($impressaoDoNivelSuperior)
            {
                if ($tipo == MENUCONFIG_MENU_ABA)
                {
                    echo "</ul>
                		</li>";
                }
                elseif ($tipo == MENUCONFIG_LISTA_HORIZONTAL)
                {
                    echo "</li>";
                }
                elseif ($tipo == MENUCONFIG_LISTA_VERTICAL)
                {
                    if ($tipoPaiOriginal == MENUCONFIG_LISTA_HORIZONTAL)
                    {
                        echo " </div>";
                    }
                    else
                    {
                        echo "</div>
                                        </li>";
                    }
                }
                elseif ($tipo == MENUCONFIG_DROPDOWN)
                {
                    echo "</ul>
                                </div>";
                }
            }
        }

        private function returnPage($page)
        {
            if ($this->usarCriptografia)
            {
                return $this->crypt->crypt64($page);
            }
            else
            {
                return $page;
            }
        }

        public function getStringCssComDimensoesDaImagem($tipoDeLista)
        {
            switch ($tipoDeLista)
            {
                case MENUCONFIG_LISTA_HORIZONTAL:
                    return "width: 32px; height: 32px;";
                    break;
                case MENUCONFIG_LISTA_VERTICAL:
                    return "width: 16px; height: 16px;";
                    break;
                case MENUCONFIG_DROPDOWN:
                    return "width: 16px; height: 16px;";
                    break;
            }
        }

        public function imprimirTelaDeSelecionarAreasDoMenu($arrayDePermissoes)
        {
            $arrayDoMenu = $this->getArrayDoConteudoDoMenuCompleto();

            echo "<table class=\"tabela_discreta\">

	        	    	<colgroup>
                            <col width=\"100%\" />
                 		</colgroup>";

            self::imprimirAreaDoMenu($arrayDoMenu, $arrayDePermissoes);

            echo "</table>";
        }

        public static function imprimirAreaDoMenu($arrayArea, &$arrayDePermissoes, $nivel = 0, $stringId = "")
        {
            if ($nivel == 0)
            {
                $estiloAdicional = "font-weight: bold; padding-top: 15px;";
            }
            else
            {
                $estiloAdicional = "";
            }

            $tabulacao = Helper::getStringDeTabulacao($nivel);

            foreach ($arrayArea as $chave => $valor)
            {
                if ($chave == "config")
                {
                    continue;
                }

                if (in_array($chave, $arrayDePermissoes))
                {
                    $strSelecionado = "checked=\"checked\"";
                }
                else
                {
                    $strSelecionado = "";
                }

                if (is_array($valor))
                {
                    print "<tr><td style=\"{$estiloAdicional}\">{$tabulacao} <input id=\"checkbox_{$stringId}{$chave}_\" type=\"checkbox\" {$strSelecionado} name=\"areas_menu[]\" onchange=\"javascript:selecaoAutomaticaDeAreasDoMenu(this);\" value=\"{$chave}\">&nbsp;{$chave}</td></tr>";
                    self::imprimirAreaDoMenu($valor, $arrayDePermissoes, $nivel + 1, $stringId . "{$chave}_");
                }
                else
                {
                    print "<tr><td>{$tabulacao} <input id=\"checkbox_{$stringId}{$chave}_\" type=\"checkbox\" {$strSelecionado} name=\"areas_menu[]\" onchange=\"javascript:selecaoAutomaticaDeAreasDoMenu(this);\" value=\"{$chave}\">&nbsp;{$chave}</td></tr>";
                }
            }
        }

        public function getVetorDasOpcoesDoMenu()
        {
            $vMatriz = $this->getArrayDoConteudoDoMenuCompleto();
            $vListaRetorno = array();

            foreach ($vMatriz as $vOpcao => $vValor)
            {
                if ($vOpcao == "config")
                {
                    continue;
                }
                else
                {
                    if (is_array($vValor))
                    {
                        $vListaRetorno[] = $vOpcao;
                        $vVetorOpcaoInterna = static::getVetorDasOpcoesDoMenuInterno($vValor);
                        if (count($vVetorOpcaoInterna) > 0)
                        {
                            $vListaRetorno = array_merge($vListaRetorno, $vVetorOpcaoInterna);
                        }
                    }
                    else
                    {
                        $vListaRetorno[] = $vOpcao;
                    }
                }
            }

            return $vListaRetorno;
        }

        public static function getVetorDasOpcoesDoMenuInterno($pVetorInterno)
        {
            $vListaRetorno = array();
            foreach ($pVetorInterno as $vOpcao => $vValor)
            {
                if ($vOpcao == "config")
                {
                    continue;
                }
                else
                {
                    if (is_array($vValor))
                    {
                        $vListaRetorno[] = $vOpcao;
                        $vVetorOpcaoInterna = static::getVetorDasOpcoesDoMenuInterno($vValor);
                        if (count($vVetorOpcaoInterna) > 0)
                        {
                            $vListaRetorno = array_merge($vListaRetorno, $vVetorOpcaoInterna);
                        }
                    }
                    else
                    {
                        $vListaRetorno[] = $vOpcao;
                    }
                }
            }

            return $vListaRetorno;
        }

    }

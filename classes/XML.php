<?php

    class XML
    {

        public $database;
        public $conteudo;

        public function __construct($query = false)
        {
            $this->database = new Database();

            if ($query)
            {
                $this->database->query($query);
            }
        }

        public function gerarArquivo($pathApartirRaiz, $nomeArquivoComExtensao)
        {
            $this->conteudo = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
            $this->conteudo .= "\t<valores>\n";

            $pathApartirRaiz = Helper::getPathComBarra($pathApartirRaiz);

            $handler = fopen($pathApartirRaiz . $nomeArquivoComExtensao, "w+");

            for ($i = 0; $dados = $this->database->fetchArray(MYSQL_ASSOC); $i++)
            {
                $this->conteudo .= "\t\t<valor>\n";

                foreach ($dados as $chave => $valor)
                {
                    if (substr_count($chave, "_HTML") > 0)
                    {
                        $this->conteudo .= "\t\t\t<$chave>" . html_entity_decode($valor) . "</$chave>\n";
                    }
                    else
                    {
                        $this->conteudo .= "\t\t\t<$chave>$valor</$chave>\n";
                    }
                }

                $this->conteudo .= "\t\t</valor>\n";
            }

            $this->conteudo .= "\t</valores>\n";

            fwrite($handler, $this->conteudo);
            fclose($handler);
        }

    }

?>

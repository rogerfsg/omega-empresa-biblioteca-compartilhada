<?php

    define('COM_ICONE', 1);
    define('SEM_ICONE', 2);

    class MenuItem
    {

        public $link;
        public $label;
        public $imagem;
        public $tipo;
        public $alt;

        public function getLabel()
        {
            return $this->label;
        }

        public function getImagem()
        {
            return $this->imagem;
        }

        public function getTipo()
        {
            return $this->tipo;
        }

        public function getAlt()
        {
            return $this->alt;
        }

        public function getLink()
        {
            return $this->link;
        }

        public function getIcone()
        {
            return $this->imagem;
        }

        public function __construct($link, $imagem = IMAGEM_PADRAO_MENU, $label = false, $tipo = COM_ICONE, $alt = true)
        {
            if ($alt === true && $label !== false)
            {
                $alt = $label;
            }

            $this->link = $link;
            $this->label = $label;
            $this->imagem = MenuConfig::getPathImagensMenu() . $imagem;
            $this->tipo = $tipo;
            $this->alt = $alt;
        }

        public function __toString()
        {
            return $this->link;
        }

    }

?>

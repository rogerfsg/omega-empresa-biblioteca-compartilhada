<?php

    define('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', true);

    define('EMAIL_PADRAO_REMETENTE_MENSAGENS', 'remetente@workoffline.com.br');
    if (php_sapi_name() == 'cli')
    {
        define('MODO_CONSOLE', true);
    }
    else
    {
        define('MODO_CONSOLE', false);
    }

    if (!MODO_CONSOLE)
    {
        if (substr_count($_SERVER["HTTP_HOST"], "workoffline.com.br") >= 1 
		|| substr_count($_SERVER["HTTP_HOST"], "incubadoradacerveja.com.br") >= 1)
        {
			if(isset($_SERVER['HTTPS'])){
				define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'https://shared.workoffline.com.br/');
				define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "https://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
				define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "https://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
				define('__DOMINIO_DE_ACESSO_SICOB', "https://my.workoffline.com.br");            

			} else {
				define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://shared.workoffline.com.br/');
				define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
				define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
				define('__DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br");            
			}
            define('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');

        }
        elseif (strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp64/www")
            && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br")
        { //maquina Eduardo

            define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/OmegaEmpresa/BibliotecaCompartilhada/Trunk/');
            define('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
            define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br");
        }
        else
        { //maquina Roger

            define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/BibliotecasCompartilhadas/BC10002Corporacao/');
            define('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
            define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br");
        }
    }
    else
    {
        $nomeMaquina = strtoupper(php_uname("n"));
        if ($nomeMaquina == "LI1047-176.MEMBERS.LINODE.COM")
        {
            define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://shared.workoffline.com.br/');
            define('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');
            define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br");
        } else {
            define('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/BibliotecasCompartilhadas/BC10002Corporacao/');
            define('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
            define('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
            define('__DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br");
        }

    }

    define('MODO_SESSION_REDIS', 'REDIS');
    define('MODO_SESSION_PHP', 'PHP');

    define('MODO_SESSION', MODO_SESSION_REDIS);

    define('WINDOWS', 1);
    define('LINUX', 2);

    define('SISTEMA_OPERACIONAL', WINDOWS);

//deve ser instalado os packages awk e iconv
    define('PATH_CYGWIN', "C:\\cygwin\\bin\\");

    define('LOG_ARQUIVO', 1);
    define('LOG_BANCO', 2);
    define('LOG_UDP', 3);

    define('TIPO_LOG', LOG_ARQUIVO);
    define('MODO_DE_DEPURACAO', true);

    define('REMETENTE_PADRAO_MENSAGENS_AUTOMATICAS', "automatico@omegasoftware.com.br");

    define('SESSAO_TIPO_LIMITER', "nocache");
    define('SESSAO_TEMPO_EXPIRACAO', 120);  //em minutos
    define('SESSAO_TEMPO_DE_VIDA', 60); //em minutos

    define('PORTA_PADRAO_HTTP', 80);
    define('PORTA_PADRAO_HTTPS', 443);

    define('PORTA_HTTP', PORTA_PADRAO_HTTP);
    define('PORTA_HTTPS', PORTA_PADRAO_HTTPS);

    define('LINGUA_PADRAO', "pt-br");
    define('EMAIL_PADRAO_RELATORIO_ERROS', "modelo@projeto.br");
    define('CODIGO_DA_CONTA_GOOGLE_ANALYTICS', "UA-19008673-1");

//CONSTANTES DO SISTEMA
    define('CAMPO_TEXTO', 1);
    define('CAMPO_INTEIRO', 2);
    define('CAMPO_MOEDA', 3);
    define('CAMPO_DATA', 4);
    define('CAMPO_DATATIME', 5);
    define('CAMPO_TELEFONE', 6);
    define('CAMPO_CPF', 7);
    define('CAMPO_CNPJ', 8);

    define('REGISTROS_POR_PAGINA', 20);

    define('MENSAGEM_OK', 1);
    define('MENSAGEM_INFO', 2);
    define('MENSAGEM_WARNING', 3);
    define('MENSAGEM_ERRO', 4);
    define('MENSAGEM_ASK', 5);

    define('CHAVE_DE_ACESSO', "123");

    define('CALENDARIO_INDIVIDUAL', "individual");
    define('CALENDARIO_INICIAL', "inicial");
    define('CALENDARIO_FINAL', "final");

    define('INVISIVEL', 0);
    define('VISIVEL', 1);

    define('OBJ_SEGURANCA', "SECURITY");
    define('TIMEOUT_TEMPORARIOS_EM_HORAS', 24);

    define('LARGURA_PADRAO_GREYBOX', 1000);
    define('ALTURA_PADRAO_GREYBOX', 600);

    define('LARGURA_PADRAO_RELATORIO', 800);
    define('ALTURA_PADRAO_RELATORIO', 600);

    define('LARGURA_GRAFICOS_INICIAL', 500);
    define('ALTURA_GRAFICOS_INICIAL', 500);

    define('LARGURA_GRAFICOS_AMPLIADO', 700);
    define('ALTURA_GRAFICOS_AMPLIADO', 700);

    define('COLUNAS_FORMULARIOS', 2);

    define('SEGUNDOS_EM_UM_MINUTO', 60);
    define('SEGUNDOS_EM_UMA_HORA', 3600);
    define('SEGUNDOS_EM_UM_DIA', 86400);

    define('QUEBRA_LINHA_PADRAO', "\r\n");

    define('COMANDO_VOLTAR_PAGINA', "window.history.go(-1);");
    define('COMANDO_FECHAR_DIALOG', "parent.$('#div_dialog').dialog('close');");

    define('COMANDO_FECHAR_DIALOG_ATUALIZANDO', "parent.document.location = parent.document.location.toString().replace(/nivel_visualizacao/gi, \"old\").replace(/vaga_visualizacao/gi, \"old\")");

    define('TEMPO_PADRAO_FECHAR_DIALOG', 1.8);

    define('IMAGEM_PADRAO_MENU', "padrao.png");



<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of HelperAPCRedis
     *
     * @author home
     */
    class HelperCookieRedis
    {
        static $singletonR = null;

        public static function getSingleton()
        {
            if (HelperCookieRedis::$singletonR == null)
            {
                HelperCookieRedis::$singletonR = new HelperCookieRedis();
            }

            return HelperCookieRedis::$singletonR;
        }

        private $helperRedis = null;
        private $hash = null;
        private $prefixo = null;

        public function __construct($prefixo = null)
        {
            // since we connect to default setting localhost
            // and 6379 port there is no need for extra
            // configuration. If not then you can specify the
            // scheme, host and port to connect as an array
            // to the constructor.
            if ($prefixo == null)
            {
                $prefixo = IDENTIFICADOR_SESSAO;
            }

            $this->prefixo = $prefixo;
            $this->helperRedis = new HelperRedis($prefixo);
            $this->hash = array();
        }

        //put your code here
        public function add($key, $value, $ttlApc = 0)
        {
            Helper::setCookie($key, $value, $ttlApc);
            $this->helperRedis->set($key, $value);
        }

        public function exists($key)
        {
            if (!Helper::isSetCookie($key))
            {
                return $this->helperRedis->exists($key);
            }
            else
            {
                return true;
            }
        }

        public function getIfExists($key, $default = null, $ttlApc = 0)
        {
            if (!Helper::isSetCookie($key))
            {
                if ($this->helperRedis->exists($key))
                {
                    $value = $this->helperRedis->get($key);
                    Helper::setCookie($key, $value, $ttlApc);

                    return $value;
                }
                else
                {
                    return $default;
                }
            }
            else
            {
                return Helper::COOKIE($key);
            }
        }

    }

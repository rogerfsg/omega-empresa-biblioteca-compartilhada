<?php

    Helper::includePHPDaBibliotecaCompartilhada('plugins/Facebook/autoload.php');

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 07/03/2018
     * Time: 20:09
     */
    class HelperFacebook
    {
        private $appId;
        private $appSecret;
        private $fb;
        private $friends;
        private $sessaoFacebook;
        private $strIdsFriends;
        private $photos;
        //foto do face nao est� liberada para uso comercial

        //const PERMISSIONS = "user_photos,user_location,email,public_profile,user_birthday";
        const PERMISSIONS = "user_location,email,public_profile,user_birthday";
        const PERMISSIONS_FRIENDS = "user_location,email,public_profile,user_birthday";

        public function __construct($sessaoFacebook = null)
        {
            $this->appId = FACEBOOK_APP_ID;
            $this->appSecret = FACEBOOK_APP_SECRET;
            $this->fb = new \Facebook\Facebook([
                                                   'app_id' => "{$this->appId}",
                                                   'app_secret' => "{$this->appSecret}",
                                                   'default_graph_version' => 'v2.10',
                                                   //'default_access_token' => '{access-token}', // optional

                                               ]);
            $this->sessaoFacebook = $sessaoFacebook;
            if ($this->sessaoFacebook != null)
            {
                $this->loadMe();
            }
        }

        public function loadMe()
        {
            if ($this->me != null)
            {
                return $this->me;
            }

            // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
            //   $helper = $fb->getRedirectLoginHelper();
            //   $helper = $fb->getJavaScriptHelper();
            //   $helper = $fb->getCanvasHelper();
            //   $helper = $fb->getPageTabHelper();

            try
            {
                // Get the \Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
                $response = $this->fb->get('/me', $this->sessaoFacebook->accessToken);
            }
            catch (\Facebook\Exceptions\FacebookResponseException $e)
            {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            }
            catch (\Facebook\Exceptions\FacebookSDKException $e)
            {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $me = $response->getGraphUser();
            $this->me = $me;

            return $me;
        }

        public function getStrIdsFriends()
        {
            if ($this->strIdsFriends != null)
            {
                return $this->strIdsFriends;
            }

            $this->friends = $this->getFriends();
            if (count($this->friends))
            {
                $str = "";
                for ($i = 0; $i < count($this->friends); $i++)
                {
                    if (strlen($str))
                    {
                        $str .= ", ";
                    }
                    $str .= "'{$this->friends[$i]->id}'";
                }
                $this->strIdsFriends = $str;

                return $str;
            }
            $this->strIdsFriends = '';

            return $this->strIdsFriends;
        }

        public function getFriends()
        {
            if (count($this->friends) > 0)
            {
                return $this->friends;
            }

            try
            {
                if ($this->me == null)
                {
                    return null;
                }
                // Returns a `FacebookFacebookResponse` object
                $response = $this->fb->get(
                    "/{$this->me->getId()}/friends?" . HelperFacebook::PERMISSIONS_FRIENDS,
                    $this->sessaoFacebook->accessToken
                );

                //{"data":[{"name":"Dorothy Albdhfjbhchij Occhinostein","id":"102646790573997"}],"paging":{"cursors":{"before":"QVFIUk81dU41YXNaY3Y0eUFVZAWZADV1BKSUUyQXJHT3JrOXVyNGFSOHFVbXNaYk1WUTJFc2wzdXQ3d21GWjZAKT2JaLVFUczFsME01cTNwOXpMdGdiM284b1ln","after":"QVFIUk81dU41YXNaY3Y0eUFVZAWZADV1BKSUUyQXJHT3JrOXVyNGFSOHFVbXNaYk1WUTJFc2wzdXQ3d21GWjZAKT2JaLVFUczFsME01cTNwOXpMdGdiM284b1ln"}},"summary":{"total_count":1}}
            }
            catch (FacebookExceptionsFacebookResponseException $e)
            {
                HelperLog::logErro($e);
            }
            catch (FacebookExceptionsFacebookSDKException $e)
            {
                HelperLog::logErro($e);
            }
            $graph = $response->getGraphEdge();

            $vetor = array();
            foreach ($graph as $key => $graphNode)
            {
                $objAmigo = new stdClass();

//                print_r($graphNode);

                $objAmigo->id = $graphNode->getField('id'); // From GraphNode

                // User example
                $objAmigo->nome = $graphNode->getField('name'); // From GraphNode
//                $objAmigo->link = $graphNode->getField('link'); // From GraphNode
//                $objAmigo->nascimento = $graphNode->getField('birthday'); // From GraphNode
//                $objAmigo->pais = $graphNode->getField('country'); // From GraphNode
//                $objAmigo->lat = $graphNode->getField('lat'); // From GraphNode
//                $objAmigo->lng = $graphNode->getField('lng'); // From GraphNode
//                $objAmigo->street = $graphNode->getField('street'); // From GraphNode
//                $objAmigo->cidade = $graphNode->getField('city'); // From GraphNode
//                $objAmigo->estado = $graphNode->getField('state'); // From GraphNode
//                $objAmigo->pais = $graphNode->getField('country'); // From GraphNode
//                $objAmigo->cep = $graphNode->getField('zip'); // From GraphNode

                $vetor[ count($vetor) ] = $objAmigo;
            }
            $this->friends = $vetor;

            return $this->friends;
        }

        public function getPhotos()
        {
            if ($this->photos != null)
            {
                return $this->photos;
            }
            /* PHP SDK v5.0.0 */
            /* make the API call */
            try
            {
                if ($this->me == null)
                {
                    return null;
                }
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->get(
                    "/{$this->me->getId()}/photos",
                    $this->sessaoFacebook->accessToken
                );
            }
            catch (Facebook\Exceptions\FacebookResponseException $e)
            {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            }
            catch (Facebook\Exceptions\FacebookSDKException $e)
            {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $graphNode = $response->getGraphNode();
            $vetor = array();
            foreach ($graphNode as $key => $value)
            {
                $objAmigo = new stdClass();

                $objAmigo->id = $graphNode->getId(); // From GraphNode

                // User example
                $objAmigo->nome = $graphNode->getField('name'); // From GraphNode
                $objAmigo->link = $graphNode->getLink(); // From GraphNode
                $objAmigo->nascimento = $graphNode->getField('birthday'); // From GraphNode
                $objAmigo->pais = $graphNode->getField('country'); // From GraphNode
                $objAmigo->lat = $graphNode->getField('lat'); // From GraphNode
                $objAmigo->lng = $graphNode->getField('lng'); // From GraphNode
                $objAmigo->street = $graphNode->getField('street'); // From GraphNode
                $objAmigo->cidade = $graphNode->getField('city'); // From GraphNode
                $objAmigo->estado = $graphNode->getField('state'); // From GraphNode
                $objAmigo->pais = $graphNode->getField('country'); // From GraphNode
                $objAmigo->cep = $graphNode->getField('zip'); // From GraphNode

                $vetor[ count($vetor) ] = $objAmigo;
            }
            $this->photos = $vetor;

            return $this->photos;
        }

        public function hasFriend($idFacebook1){
            $this->me =  $this->loadMe();
            //eu sou considerado meu amigo no sistema
            if($this->me->getId() == $idFacebook1) return true;

            $friends = $this->getFriends();
//            echo "Id facebook: $idFacebook1<br/>";
//            print_r($friends);
            foreach ($friends as $f){
                if($f->id == $idFacebook1)
                    return true;
            }
            return false;
        }
    }
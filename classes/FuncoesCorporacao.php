<?php

    //O vetor abaixo deve ser inicializado no site filho
    class FuncoesCorporacao
    {
        private $vVetorDiretoriosPercorridos = null;
        static $singleton = null;

        public static function getSingleton()
        {
            if (static::$singleton == null)
            {
                static::$singleton = new FuncoesCorporacao();
            }

            return static::$singleton;
        }

        public function setDiretorios($vetor)
        {
            $this->vVetorDiretoriosPercorridos = $vetor;
            foreach ($this->vVetorDiretoriosPercorridos as $vetorParametro)
            {
                $vRetorno = $this->__loadFilesInPath($vetorParametro[0], $vetorParametro[1], $vetorParametro[2]);
            }
        }

        private function __construct()
        {
            $this->setAutoregister();

            $workspaceRaiz = acharRaizWorkspace();

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/constants.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Helper.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperLog.php';
            $helperLog = new HelperLog ();
            $helperLog->setMyErrorHandler();

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Registry.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/AES.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Database.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/PROTOCOLO_SISTEMA.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Interface_mensagem.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/protocolo/Mensagem.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/InterfaceSeguranca.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/InterfaceSegurancaUsuario.php';

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Crypt.php';

            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Manutencao.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperAPC.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperRedis.php';
            include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/HelperAPCRedis.php';

            Manutencao::checkIfAssinaturaDaCorporacaoEstaEmManutencao();
        }

        function setAutoregister()
        {
            spl_autoload_register(array($this, 'carregarClasses'));
        }

        function carregarClasses($className)
        {
            //Fun��o que ir� carregar as classes do projeto automaticamente
            $class = $this->getClass($className);

            if (file_exists($class))
            {
                $retorno = require_once($class);
            }
        }

        public function getClass($classe)
        {
            if (!strlen($classe))
            {
                return;
            }

            if (is_array($this->vVetorDiretoriosPercorridos))
            {
                foreach ($this->vVetorDiretoriosPercorridos as $vetorParametro)
                {
                    $vRetorno = $this->__loadFilesInPath($classe, $vetorParametro[1], $vetorParametro[2]);
                    if ($vRetorno != false)
                    {
                        return $vRetorno;
                    }
                }
            }

        }

        function __loadFilesInPath($classe, $classPath, $pVetorDiretorio)
        {
            if (is_dir($classPath))
            {
                $pathFinal = $classPath . $classe . ".php";

                if (file_exists($pathFinal))
                {
                    return $pathFinal;
                }
                foreach ($pVetorDiretorio as $dir)
                {
                    $pathFinal = $classPath . $dir . $classe . ".php"; // Classes espec�ficas - criptografia, banco...
                    if (file_exists($pathFinal))
                    {
                        return $pathFinal;
                    }
                }
            }

            return false;
        }

    }




<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of ControleManutencaoAssinatura
     *
     * @author home
     */
    class Manutencao
    {

        const PREFIXO = "MM_";
        //TODO em producao deixar em 10 min ao inv�s de 1
        const TEMPO_CACHE_APC_MINUTOS = 10;

        //http://127.0.0.1/PontoEletronico/10002Corporacao/public_html/adm/index.php?corporacao=AO&tipo=pages&page=teste&CHECK_MANUTENCAO=false
        public static function checkIfAssinaturaDaCorporacaoEstaEmManutencao(
            $interfaceGrafica = false,
            $idCorporacao = null,
            $exitInTheEnd = true)
        {
            $check = Helper::GET("CHECK_MANUTENCAO");
            if ($check == 'false')
            {
                return false;
            }
            if ($idCorporacao == null)
            {
                $configSite = Registry::get('ConfiguracaoSite');
                $idCorporacao = $configSite->ID_CORPORACAO;
            }

            $helperAR = new HelperAPCRedis(Manutencao::PREFIXO);
            $emManutencao = $helperAR->getIfExists(
                $idCorporacao,
                false,
                Manutencao::TEMPO_CACHE_APC_MINUTOS * 60);

            if ($emManutencao)
            {
                $phpFile = Helper::getPhpFileName();
                $action = Helper::POSTGET("action");
                if ($phpFile == "gerar_id_avulso.php"
                    || $action == "getIdCorporacao"
                    || $action == "getNumeroDeUsuariosDaAssinatura"
                    || $action == "getEspacoEmMBOcupadoPelaAssinatura"
                    || $action == "assinaturaMigrada")
                {
                    return false;
                }

                if ($exitInTheEnd)
                {
                    if ($interfaceGrafica)
                    {
                        //TODO para interface grafica deve haver um retorno com uma pagina de manutencao
                        echo json_encode(new Mensagem(PROTOCOLO_SISTEMA::EM_MANUTENCAO, "Em manutencao"));
                    }
                    else
                    {
                        echo json_encode(new Mensagem(PROTOCOLO_SISTEMA::EM_MANUTENCAO, "Em manutencao"));
                    }
                    exit();
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public static function setAssinaturaDaCorporacaoEmManutencao($idCorporacao, $emManutencao = true)
        {
            try
            {
                $helperAR = new HelperAPCRedis(Manutencao::PREFIXO);
                $helperAR->add($idCorporacao, $emManutencao, Manutencao::TEMPO_CACHE_APC_MINUTOS * 60);
            }
            catch (Exception $ex)
            {
                HelperLog::logErro($ex);

                return false;
            }
        }

    }

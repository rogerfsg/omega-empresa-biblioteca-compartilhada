<?php

    class HelperLog
    {
        //TODO lembrar do
        const VERBOSE_ENABLED = false;
        const FORMAT = "d/m/Y H:i:s";

        const GOOGLE_ANALYTICS_DEFAULT_IDENTIFIER_GRANULARITY = self::LOG_IDENTIFIER_GRANULARITY_HOURS;

        const LOG_TYPE_FATAL_ERROR = 'erro_fatal';
        const LOG_TYPE_ERROR = 'erro';
        const LOG_TYPE_EXCEPTION = 'exception';
        const LOG_TYPE_WARNING = 'warning';
        const LOG_TYPE_DEBUG = 'debug';
        const LOG_TYPE_INFO = 'info';
        const LOG_TYPE_NOTICE = 'notice';
        const LOG_TYPE_VERBOSE = 'verbose';
        const LOG_TYPE_WEBSERVICE = 'webservice';
        const LOG_TYPE_EXEC = 'exec';
        const LOG_TYPE_ZIP = 'zip';
        const LOG_TYPE_DOWNLOAD = 'download';
        const LOG_TYPE_HTTP = 'http';
        const LOG_TYPE_CURL = 'curl';
        const LOG_TYPE_DATABASE = 'database';
        const LOG_TYPE_REDIS = 'redis';
        const LOG_TYPE_SEGURANCA = 'seguranca';
        const LOG_TYPE_PERIGO = 'perigo';
        const LOG_TYPE_SINCRONIZACAO = 'sincronizacao';
        const LOG_TYPE_EMAIL = 'email';
        const LOG_TYPE_EMAIL_ERROR = 'email_erro';
        const LOG_TYPE_DISPONIBILIZA_HOSPEDAGEM = 'disponibiliza_hospedagem';
        const LOG_TYPE_TEMP = 'temp';
        const LOG_TYPE_REDIRECT = 'redirect';
        const LOG_TYPE_ROTINAS = 'rotinas';
        const LOG_TYPE_PROCESSO = 'processo';
        const     LOG_TYPE_MKDIR = 'mkdir';
        const LOG_TYPE_CADASTRO = 'cadastro';
        const LOG_TYPE_UPLOAD = 'upload';
        const LOG_TYPE_CRYPT = 'crypt';
        const LOG_TYPE_WEBHOOK='webhook';
        const LOG_IDENTIFIER_GRANULARITY_DAYS = 'days';
        const LOG_IDENTIFIER_GRANULARITY_HOURS = 'hours';
        const LOG_IDENTIFIER_GRANULARITY_MINUTES = 'minutes';
        const LOG_IDENTIFIER_GRANULARITY_SECONDS = 'seconds';

        public $dataInicio = null;
        public $dataFim = null;
        public $post = null;
        public $get = null;
        public $server = null;
        public $msg = null;

        public function __construct()
        {
            $this->dataInicio = date(static::FORMAT);
        }

        private static function getGoogleAnalyticsLabel($logType)
        {
            $logLabel = "";
            switch($logType)
            {
                case static::LOG_TYPE_FATAL_ERROR:
                    $logLabel = 'Erro Fatal';
                    break;

                case static::LOG_TYPE_ERROR:
                    $logLabel = 'Erro';
                    break;

                case static::LOG_TYPE_EXCEPTION:
                    $logLabel = 'Exception';
                    break;

                case static::LOG_TYPE_WARNING:
                    $logLabel = 'Warning';
                    break;

                case static::LOG_TYPE_DEBUG:
                    $logLabel = 'Debug';
                    break;

                case static::LOG_TYPE_INFO:
                    $logLabel = 'Info';
                    break;

                case static::LOG_TYPE_NOTICE:
                    $logLabel = 'Notice';
                    break;

                case static::LOG_TYPE_VERBOSE:
                    $logLabel = 'Verbose';
                    break;

                case static::LOG_TYPE_WEBSERVICE:
                    $logLabel = 'Webservice';
                    break;

                case static::LOG_TYPE_EXEC:
                    $logLabel = 'Exec';
                    break;

                case static::LOG_TYPE_ZIP:
                    $logLabel = 'Zip';
                    break;

                case static::LOG_TYPE_DOWNLOAD:
                    $logLabel = 'Download';
                    break;

                case static::LOG_TYPE_HTTP:
                    $logLabel = 'Http';
                    break;

                case static::LOG_TYPE_CURL:
                    $logLabel = 'Curl';
                    break;

                case static::LOG_TYPE_DATABASE:
                    $logLabel = 'Database';
                    break;

                case static::LOG_TYPE_REDIS:
                    $logLabel = 'Redis';
                    break;

                case static::LOG_TYPE_SEGURANCA:
                    $logLabel = 'Seguran�a';
                    break;

                case static::LOG_TYPE_SINCRONIZACAO:
                    $logLabel = 'Sincroniza��o';
                    break;

                case static::LOG_TYPE_EMAIL:
                    $logLabel = 'Email';
                    break;

                case static::LOG_TYPE_EMAIL_ERROR:
                    $logLabel = 'Email Erro';
                    break;

                case static::LOG_TYPE_DISPONIBILIZA_HOSPEDAGEM:
                    $logLabel = 'Disponibiliza Hospedagem';
                    break;

                case static::LOG_TYPE_TEMP:
                    $logLabel = 'Temp';
                    break;

                case static::LOG_TYPE_REDIRECT:
                    $logLabel = 'Redirect';
                    break;

                case static::LOG_TYPE_ROTINAS:
                    $logLabel = 'Rotinas';
                    break;

                case static::LOG_TYPE_PROCESSO:
                    $logLabel = 'Processo';
                    break;

                case static::LOG_TYPE_MKDIR:
                    $logLabel = 'Mkdir';
                    break;

            }

            return $logLabel;

        }

        private static function generateLogIdentifierBasedOnTimeGranularity()
        {
            $format = "";
            switch(static::GOOGLE_ANALYTICS_DEFAULT_IDENTIFIER_GRANULARITY)
            {
                case static::LOG_IDENTIFIER_GRANULARITY_DAYS:
                    $format = 'Y/m/d';
                    break;

                case static::LOG_IDENTIFIER_GRANULARITY_HOURS:
                    $format = 'Y/m/d H:00:00';
                    break;

                case static::LOG_IDENTIFIER_GRANULARITY_MINUTES:
                    $format = 'Y/m/d H:i:00';
                    break;

                case static::LOG_IDENTIFIER_GRANULARITY_SECONDS:
                default:
                    $format = 'Y/m/d H:i:s';
                    break;
            }

            return date($format);
        }

        private static function logOnGoogleAnalytics($logType = self::LOG_LEVEL_DEBUG, $logLabel = null, $value)
        {
            try
            {
                if (defined('IDENTIFICADOR_GOOGLE_ANALYTICS') && defined('DOMINIO_GOOGLE_ANALYTICS'))
                {
                    if(is_null($logLabel))
                    {
                        $logLabel = static::generateLogIdentifierBasedOnTimeGranularity();
                    }

                    $googleAnaytics = new GoogleAnalytics(IDENTIFICADOR_GOOGLE_ANALYTICS, DOMINIO_GOOGLE_ANALYTICS);
                    $googleAnaytics->set_event('Log', $logType, $logLabel, $value);
                    $googleAnaytics->send();
                }
            }
            catch(Exception $ex)
            {
                //n�o faz nada
            }
        }

        private static function log($logType, $logContent, $logOnAnalytics = false, $logIdentifier = null)
        {
            try{
                $fp = null;
                if ($logContent != null)
                {
                    $logDir = !is_null($logIdentifier) ? "{$logType}_{$logIdentifier}" : $logType;
                    $pathDir = Helper::acharRaiz() . "log/{$logDir}/";

                    if (! file_exists(Helper::acharRaiz() . "log/{$logDir}"))
                    {
                        if (!mkdir($pathDir, 0777, true))
                        {

                            //echo $pathDir."<br/>";
                            return;
                        }
                    }
                    $path = $pathDir . "{$logDir}_" . date("d_m_Y") . ".log";

                    if (!file_exists($path))
                    {
                        $fp = fopen($path, "w");
                    }
                    else
                    {
                        $fp = fopen($path, "a");
                    }
                    if($fp ){
                        fwrite($fp, "[" . Helper::getDiaEHoraEMilisegundoAtual() . "] - {$logContent}\n");
                        fflush($fp);
                        fclose($fp);
                    }
                }
            }catch(Exception $ex){

            }

            if(FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO
                || ($logOnAnalytics && !FORCAR_FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO))
            {
                static::logOnGoogleAnalytics($logType, $logIdentifier, $logContent);
            }
        }

        public static function verbose($complemento = null, $exc = null, $logOnGoogleAnalytics = false)
        {
            if (static::VERBOSE_ENABLED)
            {
                $token = '';
                if ($complemento != null)
                {
                    if (strlen($complemento) > 255)
                    {
                        $token = ". Retorno: " . substr($complemento, 254);
                    }
                    else
                    {
                        $token = ". Retorno: " . $complemento;
                    }
                }
                if ($exc != null && is_object($exc))
                {
                    $token .= ". Exception: " . $exc->getMessage() . ". Stack: " . $exc->getTraceAsString();
                }
                else
                {
                    if ($exc != null)
                    {
                        $token .= Helper::jsonEncode($exc);
                    }
                }

                static::log(static::LOG_TYPE_VERBOSE,
                            HelperLog::getIdentificador() . $token, $logOnGoogleAnalytics);
            }
        }
        private static function getIdentificador(){
            if (Helper::isConsole()){
                return  getcwd();
            } else {
                return $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING']. print_r($_POST, true);
            }
        }

        public static function logCadastro($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_CADASTRO, $query, $logOnGoogleAnalytics);
            }
        }


        public static function logWarning($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_WARNING, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logMkdir($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_MKDIR, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logException($exception, $logOnGoogleAnalytics = false)
        {
            $logContent = "";
            if ($exception != null && is_object($exception))
            {
                $logContent .= ". Exception: " . $exception->getMessage() . ". Stack: " . $exception->getTraceAsString();
            }
            else
            {
                if ($exception != null)
                {
                    $logContent .= Helper::jsonEncode($exception);
                }
            }

            if ($logContent != null && $logContent != "")
            {
                static::log(static::LOG_TYPE_EXCEPTION, $logContent, $logOnGoogleAnalytics);
            }
        }

        public static function webservice($complemento = null, $exc = null, $logOnGoogleAnalytics = false)
        {
            if (static::VERBOSE_ENABLED)
            {
                $token = '';
                if ($complemento != null)
                {
                    if (strlen($complemento) > 1024)
                    {
                        $token = ". Retorno: " . substr($complemento, 1024);
                    }
                    else
                    {
                        $token = ". Retorno: " . $complemento;
                    }
                }
                if ($exc != null && is_object($exc))
                {
                    $token .= ". Exception: " . $exc->getMessage() . ". Stack: " . $exc->getTraceAsString();
                }
                else
                {
                    if ($exc != null)
                    {
                        $token .= Helper::jsonEncode($exc);
                    }
                }

                static::log(static::LOG_TYPE_WEBSERVICE,
                    HelperLog::getIdentificador() . print_r($_POST, true) . $token, $logOnGoogleAnalytics);
            }
        }

        public static function logSeguranca($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_SEGURANCA, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logSincronizacao($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_SINCRONIZACAO, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logEmailErro($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_EMAIL_ERROR, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logEmail($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_EMAIL, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logDisponibilizaHospedagem($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_DISPONIBILIZA_HOSPEDAGEM, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logZip($msg = null, $logOnGoogleAnalytics = false)
        {
            if ($msg != null)
            {
                static::log(static::LOG_TYPE_ZIP, $msg, $logOnGoogleAnalytics);
            }
        }

        public static function logDownload($msg = null, $logOnGoogleAnalytics = false)
        {
            if ($msg != null)
            {
                $msg = "(vazio)";
            }
            static::log(static::LOG_TYPE_DOWNLOAD,
                HelperLog::getIdentificador() .$msg,
                        $logOnGoogleAnalytics);
        }

        public static function logErro($ex = null, $msg = null, $token = null)
        {
            $log = Registry::get('HelperLog');
            if ($msg == null && $ex != null)
            {
                $msg = new Mensagem(null, null, $ex);
            }

            return $log->gravarLogEmCasoErro($msg, $token);
        }

        public static function logHttp($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_HTTP, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logUpload($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_UPLOAD, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logCurl($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_CURL, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logDatabase($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_DATABASE, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logRedis($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_REDIS, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logErroFatal($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_FATAL_ERROR, $query, $logOnGoogleAnalytics);
            }
        }

        //put your code here

        public static function logDebug($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_DEBUG, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logRedirect($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_REDIRECT, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logNotice($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_NOTICE, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logExec($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_EXEC, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logInfo($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_INFO, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logTemp($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_TEMP, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logRotinas($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_ROTINAS, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logProcesso($id, $query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_PROCESSO, $query, $logOnGoogleAnalytics, $id);
            }
        }

        public static function getDebugStackTrace()
        {
            ob_start();
            debug_print_backtrace();
            $trace = ob_get_contents();
            ob_end_clean();

            return $trace;
        }

        public function gravarLogEmCasoErro($msg, $stringErro = null)
        {
            if ($msg != null && !is_object($msg))
            {
                static::logWarning("A mensagem deveria ser do tipo Message: " . print_r($msg, true) . ". " . debug_backtrace());

                return null;
            }
            else
            {
                if (($msg != null && Interface_mensagem::checkErro($msg)) || $stringErro != null)
                {
                    $this->dataFim = date(static::FORMAT);
                    $strPost = print_r($_POST, true);
                    $strGet = print_r($_GET, true);
                    $strServer = print_r(Helper::REQUEST_URI(), true);
                    $this->post = $strPost;
                    $this->get = $strGet;
                    $this->server = $strServer;

                    $this->msg = $msg;
                    $this->msgComplemento = null;
                    if ($stringErro != null && is_string($stringErro))
                    {
                        $this->msgComplemento = $stringErro;
                    }

                    $json = Helper::jsonEncode($this);
                    self::log(static::LOG_TYPE_ERROR, $json);

                    return $json;
                }
            }

            return false;
        }

        public function setMyErrorHandler()
        {
            error_reporting(E_ALL & ~(E_WARNING | E_NOTICE | E_DEPRECATED));
            //error_reporting(E_ALL & ~(E_DEPRECATED));
            //error_reporting(E_ALL ^ E_WARNING);
            set_error_handler("HelperLog::errorHandler");
            register_shutdown_function("HelperLog::shutdownHandler");
        }

        public static function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
        {
            $lasterror = error_get_last();

            $error = "lvl: " . $error_level . " | msg:" . $error_message . " | file:" . $error_file . " | ln:" . $error_line . " | " . print_r($lasterror, true);
            switch ($error_level)
            {
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_PARSE:
                    static::logErroFatal($error);
                    break;

                case E_USER_ERROR:
                case E_RECOVERABLE_ERROR:
                    static::logErro($error);
                    break;

                case E_WARNING:
                case E_CORE_WARNING:
                case E_COMPILE_WARNING:
                case E_USER_WARNING:
                    static::logWarning($error);
                    break;

                case E_NOTICE:
                case E_USER_NOTICE:
                    static::logInfo($error);
                    break;

                case E_STRICT:
                    static::logDebug($error);
                    break;

                default:
                    static::logWarning($error);
                    break;

            }
        }

        function exitPorScript($error)
        {
            $nomeDoScript = Helper::getNomeDoScriptAtual();

            if ($nomeDoScript == "pipeline.php")
            {
                // not exit
            }
            else
            {
                if ($nomeDoScript == "webservice.php")
                {
                    $ret = new stdClass();
                    $ret->mCodRetorno = 1;
                    $ret->mMensagem = $error;
                    echo Helper::jsonEncode($ret);
                    exit();
                }
                else
                {
                }
            }
            exit();
        }

        public static function shutdownHandler()
        {
            $lasterror = error_get_last();
            if ($lasterror)
            {
                $error = print_r($lasterror, true);

                static::logErroFatal($error);
                switch ($lasterror['type'])
                {
                    case E_CORE_WARNING:
                    case E_COMPILE_WARNING:
                    case E_NOTICE:
                        break;

                    case E_ERROR:
                    case E_CORE_ERROR:
                    case E_COMPILE_ERROR:
                    case E_USER_ERROR:
                    case E_RECOVERABLE_ERROR:
                    case E_PARSE:
                        static::exitPorScript($error);
                        break;

                }
            }
        }

        function getIdEventLog()
        {
            return uniqid();
        }

        public static function logPerigo($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_PERIGO, $query, $logOnGoogleAnalytics);
            }
        }


        public static function logCrypt($query = null, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_CRYPT, $query, $logOnGoogleAnalytics);
            }
        }

        public static function logWebhook($query, $logOnGoogleAnalytics = false)
        {
            if ($query != null)
            {
                static::log(static::LOG_TYPE_WEBHOOK, $query, $logOnGoogleAnalytics);
            }
        }
    }

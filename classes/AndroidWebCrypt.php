<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 03/01/2018
 * Time: 21:04
 */
class AndroidWebCrypt
{

    private static $OPENSSL_CIPHER_NAME = "aes-128-cbc"; //Name of OpenSSL Cipher
    private static $CIPHER_KEY_LEN = 16; //128 bits
    //TEM QUE TER 16 DIGITOS!!!!
    private static $CIPHER_KEY = "asdfawefqawefves"; //128 bits
    //TEM QUE TER 16 DIGITOS!!!!
    private static $IV = "testetesteteste1";
    function __construct()
    {
    }
    /**
     * Encrypt data using AES Cipher (CBC) with 128 bit key
     *
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $iv - initialization vector
     * @param type $data - data to encrypt
     * @return encrypted data in base64 encoding with iv attached at end after a :
     */
    static function encrypt($data) {
        $key = self::$CIPHER_KEY;
        $iv = self::$IV;

        $encodedEncryptedData = base64_encode(openssl_encrypt(
            $data, AndroidWebCrypt::$OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, $iv));
        $encodedIV = base64_encode($iv);
        $encryptedPayload = $encodedEncryptedData.":".$encodedIV;

        return $encryptedPayload;

    }
    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     *
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $data - data to be decrypted in base64 encoding with iv attached at the end after a :
     * @return decrypted data
     */
    static function decryptv2($data) {
        $key = self::$CIPHER_KEY;

        if (strlen($key) < AndroidWebCrypt::$CIPHER_KEY_LEN) {
            $key = str_pad("$key", AndroidWebCrypt::$CIPHER_KEY_LEN, "0"); //0 pad to len 16
        } else if (strlen($key) > AndroidWebCrypt::$CIPHER_KEY_LEN) {
            $key = substr($key, 0, AndroidWebCrypt::$CIPHER_KEY_LEN); //truncate to 16 bytes
        }

        $parts = explode(':', $data); //Separate Encrypted data from iv.

        $decryptedData = openssl_decrypt(
            base64_decode($parts[0]),
            AndroidWebCrypt::$OPENSSL_CIPHER_NAME,
            hex2bin($key),
            OPENSSL_RAW_DATA,
            base64_decode($parts[1]));

        return $decryptedData;
    }

    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     *
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $data - data to be decrypted in base64 encoding with iv attached at the end after a :
     * @return decrypted data
     */
    static function decrypt($data) {
        //$data = self::encrypt("&email=sff%40g.c&corporacao=SSFF");



        $key = self::$CIPHER_KEY;
        $parts = explode(':', $data); //Separate Encrypted data from iv.
        $encrypted = $parts[0];
        $iv = $parts[1];
        $decryptedData = openssl_decrypt(
            base64_decode($encrypted),
            "AES-128-CBC",
                $key,
                OPENSSL_RAW_DATA,
                base64_decode($iv));


        return $decryptedData;
    }
    protected function hex2bin($hexdata)
    {
        $bindata = '';
        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }
        return $bindata;
    }

    public static function decryptGet($str){

        $d= AndroidWebCrypt::decrypt($str);

        if(!strlen($d)) return null;
        $ret = Helper::formatarParametrosStrGet($d);

        return $ret;
    }
}
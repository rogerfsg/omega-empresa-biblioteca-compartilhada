<?php

    //define('FACEBOOK_TIMEOUT_CONEXAO', 25); //em segundos
    //define('FACEBOOK_APP_ID', '181290998608403');
    //define('FACEBOOK_APP_SECRET', '6e3c06db570e5420310dc6812835718e');

    class Facebook_Handler
    {

        private $email;
        private $senha;
        public $handler;
        public $cookie;
        public $objFacebookAPI;
        public $idUsuarioLogado;

        private static $permissoes = "email,user_checkins,friends_about_me,friends_birthday,friends_hometown";

        public function __construct()
        {
            $this->handler = null;

            if ($_SERVER["HTTP_HOST"] != "localhost")
            {
                $this->objFacebookAPI = new Facebook(array('appId' => FACEBOOK_APP_ID, 'secret' => FACEBOOK_APP_SECRET));
            }
        }

        public function setDadosDeLogin($email, $senha)
        {
            $this->email = $email;
            $this->senha = $senha;
        }

        public function setIdDoUsuarioLogado($idUsuario = false)
        {
            if ($idUsuario === false)
            {
                $this->idUsuarioLogado = $this->objFacebookAPI->getUser();
            }
            else
            {
                $this->idUsuarioLogado = $idUsuario;
            }
        }

        public function setArquivoDeCookie()
        {
            if (is_numeric($this->idUsuarioLogado) && $this->idUsuarioLogado > 0)
            {
                $pathToCookie = dirname(__FILE__) . "/cookie_facebook_{$this->idUsuarioLogado}.txt";
                $this->cookie = $pathToCookie;
            }
            else
            {
                Helper::imprimirMensagem("Erro ao setar arquivo de cookie, ID do usuário corrente não informado.", MENSAGEM_ERRO);
            }
        }

        public function limparCookies()
        {
            if (is_file($this->cookie))
            {
                $filePointer = fopen($this->cookie, "w");
                fclose($filePointer);
                chmod($this->cookie, 0777);
                Helper::imprimirMensagem("Truncando arquivo de Cookie <b>{$this->cookie}</b>...", MENSAGEM_OK);
            }
        }

        public function acessarPagina($url, $urlDeReferencia, $dadosPOST = false, $login = false, $proxyStatus = false)
        {
            if ($this->handler === null)
            {
                $this->handler = curl_init();
            }

            if ($login === true)
            {
                if ($dadosPOST !== false)
                {
                    $dadosPOST .= "&";
                }
                else
                {
                    $dadosPOST = "";
                }

                $dadosPOST .= "email={$this->email}&pass={$this->senha}";
            }

            $statusCookie[] = curl_setopt($this->handler, CURLOPT_COOKIEJAR, $this->cookie);
            $statusCookie[] = curl_setopt($this->handler, CURLOPT_COOKIEFILE, $this->cookie);

            foreach ($statusCookie as $valor)
            {
                if ($valor === false)
                {
                    Helper::imprimirMensagem("Erro ao setar parametros de configuração de cookies.", MENSAGEM_ERRO);
                }
            }

            curl_setopt($this->handler, CURLOPT_TIMEOUT, FACEBOOK_TIMEOUT_CONEXAO);
            curl_setopt($this->handler, CURLOPT_RETURNTRANSFER, true);

            if ($proxyStatus === true)
            {
                Helper::imprimirMensagem("Definindo configurações de proxy...", MENSAGEM_OK);

                $statusProxy[] = curl_setopt($this->handler, CURLOPT_HTTPPROXYTUNNEL, true);
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_PROXY, "http://150.164.255.201:3128");
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_PROXYPORT, 3128);
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_PROXYUSERPWD, "eduardoo:27sete86@");
                $statusProxy[] = curl_setopt($this->handler, CURLOPT_VERBOSE, true);

                foreach ($statusProxy as $valor)
                {
                    if ($valor === false)
                    {
                        Helper::imprimirMensagem("Erro ao setar parametros de configuração de proxy.", MENSAGEM_ERRO);
                    }
                }
            }
            else
            {
                curl_setopt($this->handler, CURLOPT_HTTPPROXYTUNNEL, false);
            }

            curl_setopt($this->handler, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($this->handler, CURLOPT_SSL_VERIFYPEER, 0);

            curl_setopt($this->handler, CURLOPT_URL, $url);
            curl_setopt($this->handler, CURLOPT_REFERER, $urlDeReferencia);
            curl_setopt($this->handler, CURLOPT_FAILONERROR, true);

            curl_setopt($this->handler, CURLOPT_HEADER, true);
            curl_setopt($this->handler, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

            curl_setopt($this->handler, CURLOPT_FOLLOWLOCATION, true);

            if ($dadosPOST !== false)
            {
                curl_setopt($this->handler, CURLOPT_POST, true);
                curl_setopt($this->handler, CURLOPT_POSTFIELDS, $dadosPOST);
            }

            $retorno = curl_exec($this->handler); // execute the curl command

            if (!$this->verificarErrosDeConexao())
            {
                return $retorno;
            }
        }

        public function verificarErrosDeConexao()
        {
            $erro = curl_errno($this->handler);

            if ($erro)
            {
                Helper::imprimirMensagem("Erro de conexao nº {$erro}: " . curl_error($this->handler), MENSAGEM_ERRO);
            }

            return $erro;
        }

        public function login()
        {
            if (Helper::verificarEmail($this->email) && strlen($this->senha))
            {
                //$this->acessarPagina("http://www.facebook.com", null, false, true);
                $retorno = $this->acessarPagina("https://login.facebook.com/login.php", "http://www.facebook.com/login.php", false, true);

                if (substr_count($retorno, "temporariamente bloqueada.") > 0)
                {
                    $url = "https://login.facebook.com/checkpoint/";

                    $string1 = "name=\"post_form_id\" value=\"";

                    $posicao1 = strpos($retorno, $string1) + strlen($string1);
                    $posicao2 = strpos($retorno, "\"", $posicao1);

                    $diferenca = $posicao2 - $posicao1;

                    $chave = substr($retorno, $posicao1, $diferenca);

                    $dadosPOST = "lsd=LERez&post_form_id={$chave}&submit[Continue]=Continuar";

                    $retorno = $this->acessarPagina("https://login.facebook.com/checkpoint/", "https://login.facebook.com/checkpoint/", $dadosPOST);

                    $retorno = utf8_decode($retorno);

                    $string1 = "<script type=";
                    $posicao1 = strpos($retorno, $string1);

                    $substring = substr($retorno, $posicao1);

                    $substring = str_replace("</head><body class=\"vcard hasLeftCol fbx UIPage_LoggedOut ff4 win Locale_pt_BR\">", "", $substring);
                    $substring = str_replace("</body>", "", $substring);

                    $substring = str_replace("action=\"/checkpoint/\"", "action=\"index.php5?page=facebook&tipo=forms&acao=captcha\"", $substring);

                    echo $substring;

                    return "captcha";
                }
                elseif (substr_count($retorno, "Digite sua senha novamente") > 0)
                {
                    return false;
                }
                elseif (!strlen($retorno))
                {
                    Helper::imprimirMensagem("Falha na conexao.", MENSAGEM_ERRO);

                    return false;
                }
                else
                {
                    Helper::imprimirMensagem("Logado com sucesso.", MENSAGEM_OK);

                    return true;
                }
            }
            else
            {
                Helper::imprimirMensagem("Os dados de acesso informados são inválidos.", MENSAGEM_ERRO);

                return false;
            }
        }

        public function passarPeloCaptcha()
        {
            $dadosPOST = "";

            foreach ($_POST as $chave => $valor)
            {
                if ($chave != "submit")
                {
                    $dadosPOST .= "{$chave}={$valor}&";
                }
            }

            $dadosPOST .= "submit[Submit]=Enviar";

            $retorno = $this->acessarPagina("https://login.facebook.com/checkpoint/", "https://login.facebook.com/checkpoint/", $dadosPOST);

            echo $retorno;
        }

        public function getEmailDoContato($idContato, $dadosAcessoDB)
        {
            $idUsuario = $idContato;

            $objBanco = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);
            $objBanco2 = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);

            $objBanco->query("SELECT array_de_parametros FROM facebook_contato WHERE id_contato='{$idUsuario}'");

            if ($objBanco->rows > 0)
            {
                $strDadosFacebook = $objBanco->getPrimeiraTuplaDoResultSet(0);
                $dadosFacebook = unserialize($strDadosFacebook);

                $nomeUsuario = $dadosFacebook["name"];
            }
            else
            {
                $dadosFacebook = false;
                $nomeUsuario = "&lt;desconhecido&gt;";
            }

            if (!is_array($dadosFacebook))
            {
                Helper::imprimirMensagem("Não foi possível coletar estrutura de dados com dados do Facebook.", MENSAGEM_ERRO);
            }

            $objBanco->query("SELECT id FROM pessoa 
                          WHERE id_facebook_INT={$idUsuario}");

            if ($objBanco->rows == 0)
            {
                $email = $this->getEmail($idUsuario);

                if ($email !== false)
                {
                    $objBanco->query("SELECT id FROM pessoa WHERE email='{$email}'");

                    if ($objBanco->rows > 0)
                    {
                        $idPessoa = $objBanco->getPrimeiraTuplaDoResultSet(0);

                        Helper::imprimirMensagem("Pessoa encontrada no banco de dados: {$email}", MENSAGEM_OK);

                        $objBanco2->query("UPDATE pessoa SET id_facebook_INT={$idUsuario} WHERE id={$idPessoa}");

                        if (is_array($dadosFacebook))
                        {
                            $this->atualizarDadosDaPessoa($dadosFacebook, $dadosAcessoDB);
                        }
                    }
                    else
                    {
                        //adicionar pessoa encontrada no Facebook
                        Helper::imprimirMensagem("Novo Contato: {$nomeUsuario} &lt;{$email}&gt;.", MENSAGEM_OK);

                        $objPessoa = new EXTDAO_Pessoa();
                        $objPessoa->database = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);

                        $objPessoa->setEmail($email);
                        $objPessoa->setId_facebook_INT($idUsuario);
                        $objPessoa->formatarParaSQL();

                        $objPessoa->insert();

                        if (is_array($dadosFacebook))
                        {
                            $this->atualizarDadosDaPessoa($dadosFacebook, $dadosAcessoDB);
                        }
                    }
                }
                else
                {
                    Helper::imprimirMensagem("Email do contato {$nomeUsuario} não disponível.", MENSAGEM_WARNING);
                }
            }
            else
            {
                Helper::imprimirMensagem("Pessoa de Facebook ID {$idUsuario} já existente no banco de dados, atualizando dados...", MENSAGEM_INFO);

                //ja existe e pessoa, conferir se os dados estao desatualizados
                $objBanco->query("SELECT id FROM pessoa 
                              WHERE id_facebook_INT={$idUsuario}
                              AND DATE_ADD(data_dados_facebook_DATETIME, INTERVAL 7 DAY) < NOW()");

                //dados desatualizados
                if ($objBanco->rows == 1)
                {
                    if (is_array($dadosFacebook))
                    {
                        $this->atualizarDadosDaPessoa($dadosFacebook, $dadosAcessoDB);
                    }
                }
            }
        }

        public function atualizarDadosDaPessoa($dadosFacebook, $dadosAcessoDB)
        {
            $objBanco = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);

            $idUsuarioFacebook = $dadosFacebook["id"];
            $dataNascimento = $this->__formatarDataDeNascimento($dadosFacebook["birthday"]);
            $sexo = $this->__formatarSexo($dadosFacebook["gender"]);

            if (is_array($dadosFacebook["hometown"]) && isset($dadosFacebook["hometown"]["name"]))
            {
                $cidade = $this->__formatarCidade($dadosFacebook["hometown"]["name"]);
            }

            $primeiroNome = $dadosFacebook["first_name"];
            $ultimoNome = $dadosFacebook["last_name"];

            $objBanco->query("SELECT id FROM pessoa 
                          WHERE id_facebook_INT={$idUsuarioFacebook}");

            if ($objBanco->rows > 0)
            {
                $objPessoa = new EXTDAO_Pessoa();
                $objPessoa->database = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);

                $idPessoa = $objBanco->getPrimeiraTuplaDoResultSet(0);

                $objPessoaExistente = new EXTDAO_Pessoa();
                $objPessoaExistente->database = new Database($dadosAcessoDB->db_nome, $dadosAcessoDB->db_host, $dadosAcessoDB->db_porta, $dadosAcessoDB->db_usuario, $dadosAcessoDB->db_senha);

                $objPessoaExistente->select($idPessoa);

                if (!$objPessoaExistente->getPrimeiro_nome())
                {
                    if ($primeiroNome)
                    {
                        $objPessoa->setPrimeiro_nome($primeiroNome);
                        $arrCamposUpdate["primeiro_nome"] = true;
                    }
                }

                if (!$objPessoaExistente->getSobrenomes())
                {
                    if ($ultimoNome)
                    {
                        $objPessoa->setSobrenomes($ultimoNome);
                        $arrCamposUpdate["sobrenomes"] = true;
                    }
                }

                if (!$objPessoaExistente->getData_nascimento_DATE())
                {
                    if ($dataNascimento)
                    {
                        $objPessoa->setData_nascimento_DATE($dataNascimento);
                        $arrCamposUpdate["data_nascimento_DATE"] = true;
                    }
                }

                if (!$objPessoaExistente->getSexo_id_INT())
                {
                    if ($sexo)
                    {
                        $objPessoa->setSexo_id_INT($sexo);
                        $arrCamposUpdate["sexo_id_INT"] = true;
                    }
                }

                if (!$objPessoaExistente->getCidade())
                {
                    if ($cidade)
                    {
                        $objPessoa->setCidade($cidade);
                        $arrCamposUpdate["cidade"] = true;
                    }
                }

                $objPessoa->update($idPessoa, $arrCamposUpdate, "");
                $objPessoa->database->commitTransacao();
            }
        }

        public function getEmail($idUsuario)
        {
            $retorno = $this->acessarPagina("https://www.facebook.com/profile.php?id={$idUsuario}&sk=info", "http://www.facebook.com/");

            $retorno = html_entity_decode($retorno);

            if (substr_count($retorno, "Esqueceu sua senha?") > 0)
            {
                Helper::imprimirMensagem("Erro ao coletar dados: o usuário não está logado.", MENSAGEM_ERRO);

                return false;
            }

            $stringBase1 = "<th class=\"label\">E-mail</th>";
            $posicao1 = strpos($retorno, $stringBase1);

            if ($posicao1 !== false)
            {
                Helper::imprimirMensagem("Lista de Emails do Contato disponível.", MENSAGEM_INFO);
            }

            $stringBase2 = "</li></ul></td>";

            $posicao2 = strpos($retorno, $stringBase2, $posicao1 + strlen($stringBase1));

            $retorno2 = substr($retorno, $posicao1, $posicao2 - $posicao1);

            $retorno2 = str_replace("E-mail", "", $retorno2);

            if ($posicao1 === false || $posicao2 === false)
            {
                Helper::imprimirMensagem("Não foram encontrados endereços de email desta pessoa.", MENSAGEM_WARNING);

                return false;
            }
            else
            {
                $strEmails = explode("<span class=\"prs\">", $retorno2);

                foreach ($strEmails as $strEmail)
                {
                    $email = strip_tags($strEmail);

                    if (Helper::verificarEmail($email))
                    {
                        return $email;
                    }
                }

                Helper::imprimirMensagem("O endereço de email coletado não é um endereço de email válido.");

                return false;
            }
        }

        public function __formatarSexo($sexoFacebook)
        {
            if ($sexoFacebook == "male")
            {
                return "2";
            }
            elseif ($sexoFacebook == "female")
            {
                return "1";
            }
            else
            {
                return "";
            }
        }

        public function __formatarCidade($cidadeFacebook)
        {
            if (strlen($cidadeFacebook))
            {
                if (substr_count($cidadeFacebook, ",") > 0)
                {
                    $dadosCidade = explode(",", $cidadeFacebook);

                    return $dadosCidade[0];
                }
                else
                {
                    return $dadosCidade;
                }
            }
            else
            {
                return "";
            }
        }

        public function __formatarDataDeNascimento($dataNascimentoFacebook)
        {
            if (substr_count($dataNascimentoFacebook, "/") == 2)
            {
                $dadosDataNascimento = explode("/", $dataNascimentoFacebook);

                return "{$dadosDataNascimento[1]}/{$dadosDataNascimento[0]}/{$dadosDataNascimento[2]}";
            }
            else
            {
                return "";
            }
        }

        public static function getPermissoes()
        {
            return self::$permissoes;
        }

        public static function corrigirDadosOriginadosDoFacebook(&$item, $chave)
        {
            $item = addslashes(utf8_decode($item));
        }

    }

    class Facebook_Estutura_Coleta
    {

        public $db_nome;
        public $db_host;
        public $db_porta;
        public $db_usuario;
        public $db_senha;

        public $facebook_usuario;
        public $facebook_senha;
        public $facebook_id;

        public $arrIdsContatos;

    }

?>

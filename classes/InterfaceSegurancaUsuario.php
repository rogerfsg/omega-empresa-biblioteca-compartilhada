<?php

    abstract class InterfaceSegurancaUsuario extends InterfaceSeguranca
    {
        public $tipoUsuario;
        protected $listaDeFuncionalidades;

        public static function getTipoDoUsuarioLogado()
        {
            return Helper::getSession("usuario_tipo");
        }

        public static function getIdDoTipoDoUsuarioLogado()
        {
            //TODO-EDUARDO: Remover linha abaixo
            //return 2;

            return Helper::getSession("usuario_id_tipo");
        }

        public static function getArrayDePermissoesDoUsuarioLogado()
        {
            return Helper::getSession("usuario_permissoes");
        }

        public static function getPaginaInicialDoUsuario()
        {
            return Helper::getSession("usuario_pagina_inicial");
        }

        public static function getIsSomenteAcessoInterno()
        {
            return Helper::getSession("usuario_somente_acesso_interno") ? true : false;
        }

        protected function getSenhaDecrypt($dados, $db)
        {
            $objCriptografia = Registry::get('Crypt');
            $senhaC = null;
            if (is_array($dados))
            {
                $senhaC = EXTDAO_Usuario::getSenhaUsuarioDoEmail($dados["email"], $db);
            }
            else
            {
                if (is_string($dados) && !empty($dados))
                {
                    $senhaC = EXTDAO_Usuario::getSenhaUsuarioDoEmail($dados, $db);
                }
                else
                {
                    return null;
                }
            }

            if (empty($senhaC))
            {
                return null;
            }

            return $objCriptografia->decrypt($senhaC);
        }

        public static function imprimirMensagemDeSenhaPadrao()
        {
            $objLink = new Link();
            $objLink->alturaGreyBox = 175;
            $objLink->larguraGreyBox = 450;
            $objLink->tituloGreyBox = "Alterar Senha";
            $objLink->url = "popup.php?page=alterar_senha&tipo=pages&navegacao=false";
            $objLink->cssClass = "link_padrao";
            $objLink->label = " clique aqui";
            $objLink->numeroNiveisPai = 0;

            Helper::imprimirMensagem("Por questões de segurança, você deve alterar a sua senha atual, para fazê-lo, {$objLink->montarLink()}.", MENSAGEM_WARNING);
        }

        public static function verificarPermissao($identificadorDaFuncionalidade)
        {
            if (in_array($identificadorDaFuncionalidade, Helper::getSession("usuario_permissoes")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function getListaDeFuncionalidades()
        {
            return $this->listaDeFuncionalidades;
        }

        public function getLabel()
        {
            $token = self::getEmail();

            return $token;
        }

        public function existeUsuarioDoEmail($email, $db = null)
        {
            return EXTDAO_Usuario::existeUsuarioDoEmail($email, $db);
        }

        public function verificaSenha(
            $dados,
            $senha,
            $db = null,
            $registrarAcesso = false)
        {
            $email = $dados["email"];
            $criptografia = Registry::get('Crypt');
            $senhaC = $criptografia->crypt($senha);
            $q = "SELECT DISTINCT u.id AS usuario_id,
                                    u.nome AS usuario_nome,
                                    u.email AS usuario_email,
                                    u.status_BOOLEAN AS usuario_status,
                                    t.nome AS usuario_tipo,
                                    t.nome_visivel AS usuario_tipo_visivel,
                                    t.id AS usuario_id_tipo,
                                    IF(u.pagina_inicial IS NULL or u.pagina_inicial = '', t.pagina_inicial, u.pagina_inicial) as usuario_pagina_inicial
                    FROM usuario AS u JOIN usuario_tipo AS t ON  t.id=u.usuario_tipo_id_INT
                    WHERE u.email='$email' 
                     AND u.senha='$senhaC' ";

            if (BANCO_COM_TRATAMENTO_EXCLUSAO)
            {
                $q .= " AND t.excluido_DATETIME IS NULL
                AND u.excluido_DATETIME IS NULL";
            }

            $msg = $db->queryMensagem($q);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            if ($db->rows() > 0)
            {
                $us = $db->getPrimeiraTuplaDoResultSet("usuario_status");

                if ($us == 1)
                {
                    if ($registrarAcesso)
                    {
                        $objCliente = Helper::getPrimeiroObjeto($db->result, 1, 0);

                        $objCliente["usuario_permissoes"] = $this->getArrayDeFuncionalidadesDoUsuario(
                            $objCliente["usuario_id"],
                            $db,
                            $dados);

                        return new Mensagem_protocolo(
                            $objCliente,
                            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                            "Login realizado com suceso.");
                    }
                    else
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                            "Login realizado com suceso.");
                    }
                }
                else
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                        "Usuário foi bloqueado pelo administrador do site.");
                }
            }
            else
            {
                $idUsuario = EXTDAO_Usuario::getIdUsuarioDoEmail($email, $db);
                if (!strlen($idUsuario))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::EMAIL_DO_USUARIO_INEXISTENTE,
                        "Não existe usuário cadastrado com o email $email");
                }
                else
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::SENHA_DO_USUARIO_INCORRETA,
                        "Senha incorreta");
                }
            }
        }

        public function gravarLogin($id, $db)
        {
            $objAcesso = new EXTDAO_Acesso($db);
            $objAcesso->gravarLogin($id);
        }

        public function onRegistraAcesso($dados)
        {
            $this->montarListaDeFuncionalidades();
        }

        public static function registrarOperacao($objArgumento)
        {
            $objOperacaoBanco = new EXTDAO_Operacao_sistema();

            $objArgumento = new Seguranca_Argument();

            $objOperacaoBanco->setTipo_operacao($objArgumento->operacao);
            $objOperacaoBanco->setPagina_operacao($objArgumento->pagina);
            $objOperacaoBanco->setChave_registro_operacao_INT($objArgumento->chaveDoRegistroOperado);
            $objOperacaoBanco->setData_operacao_DATETIME($objArgumento->dataHora);
            $objOperacaoBanco->setDescricao_operacao($objArgumento->descricaoOperacao);
            $objOperacaoBanco->setEntidade_operacao($objArgumento->entidadeOperacao);
            $objOperacaoBanco->setUsuario_id_INT($objArgumento->usuarioOperacao);
            $objOperacaoBanco->setUrl_completa(Helper::getNomeDoArquivoEmPathCompleto($objArgumento->urlOperacao));
            $objOperacaoBanco->formatarParaSQL();
            $objOperacaoBanco->insert();
        }

        public function getArrayDeFuncionalidadesDoUsuario($idUsuario = false, $db = null, $dados = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            if (!$idUsuario)
            {
                $idUsuario = Seguranca::getId();
            }

            if (is_numeric($idUsuario))
            {
                //verifica se estao definidas as permissoes do usuario
                $db->query("SELECT DISTINCT identificador_funcionalidade
                                    FROM usuario_privilegio
                                    WHERE usuario_id_INT={$idUsuario}");

                //monta array de funcionalidades
                $retorno = array();

                if ($db->rows() > 0)
                {
                    while ($funcionalidadesDoUsuario = $db->fetchArray(MYSQL_NUM))
                    {
                        $retorno[] = $funcionalidadesDoUsuario[0];
                    }

                    return $retorno;
                }
                else
                {
                    $objUsuario = new EXTDAO_Usuario($db);
                    $objUsuario->select($idUsuario);
                    if (MODO_BANCO_COM_CORPORACAO)
                    {
                        $tipoDeUsuario = $objUsuario->getUsuario_tipo_id_INT($dados["corporacao_id"]);
                    }
                    else
                    {
                        $tipoDeUsuario = $objUsuario->getUsuario_tipo_id_INT();
                    }

                    if ($tipoDeUsuario)
                    {
                        //verifica se estao definidas as permissoes do tipo de usuario
                        $db->query("SELECT DISTINCT identificador_funcionalidade
                                            FROM usuario_tipo_privilegio
                                            WHERE usuario_tipo_id_INT={$tipoDeUsuario}");

                        if ($db->rows() > 0)
                        {
                            while ($funcionalidadesDoTipoDeUsuario = $db->fetchArray(MYSQL_NUM))
                            {
                                $retorno[] = $funcionalidadesDoTipoDeUsuario[0];
                            }

                            return $retorno;
                        }
                    }
                    else
                    {
                        return array();
                    }
                }
            }
            else
            {
                return array();
            }
        }

        ///Retorna o vetor de acoes liberadas apenas para usuairos que estao na area logada
        public abstract function getAcoesLiberadasParaUsoAutenticado();

        /*
         *  Funcionalidades -> descrevem as funcionalidades do sistema, e as paginas e respectivas variaveis GET que tem permissao para acessar esta funcionalidade

          Na autenticacao de paginas ->  busca as funcionalidades que possuem esta pagina como pagina que possui permissao para acessa-la

         *
         *
         */
        public function verificarPermissaoEmAcoesDoUsuarioCorrente($classe, $metodo)
        {
            switch ($metodo)
            {
                case("remove"):
                    $retorno = "__actionRemove";
                    break;

                case("add"):
                    $retorno = "__actionAdd";
                    break;

                case("edit"):
                    $retorno = "__actionEdit";
                    break;

                case("add_ajax"):
                    $retorno = "__actionAddAjax";
                    break;
                default:
                    $retorno = $metodo;
                    break;
            }

            $metodo = $retorno;

            $acoesPrivadas = $this->getAcoesLiberadasParaUsoAutenticado();
            if ($acoesPrivadas != null && !empty($acoesPrivadas))
            {
                if (in_array($metodo, $acoesPrivadas))
                {


                    return true;
                }
            }

            $indicesAPesquisar = array();

            $this->montarListaDeFuncionalidades();

            $arrayDeFuncionalidades = $this->getListaDeFuncionalidades();

            if (is_array($arrayDeFuncionalidades))
            {
                //procura funcionalidades que posssuem esta pagina
                foreach ($arrayDeFuncionalidades as $funcionalidade)
                {
                    $identificadorFuncionalidade = $funcionalidade->getId();

                    if ($funcionalidade->classe == $classe && $funcionalidade->metodo == $metodo)
                    {
                        if (is_array($funcionalidade->restricoesNoObjeto))
                        {
                            $condicaoSatisfeita = true;
                            $verificarProximoValor = false;
                            $chaveDoAnterior = false;

                            foreach ($funcionalidade->restricoesNoObjeto as $variavel => $valor)
                            {
                                if ($verificarProximoValor == true)
                                {
                                    if (Helper::GET($chaveDoAnterior) != null)
                                    {
                                        $valorASelecionar = Helper::GET($chaveDoAnterior);
                                    }
                                    elseif (Helper::POST($chaveDoAnterior) != null)
                                    {
                                        $valorASelecionar = Helper::POST($chaveDoAnterior);
                                    }

                                    $objVerifica = Helper::getObjeto($funcionalidade->classe);
                                    //print_r($objVerifica);
                                    Helper::getRetornoDeMetodo($objVerifica, "select", array($valorASelecionar));

                                    $valorAVerificar = Helper::getAtributoDeObjeto($objVerifica, $variavel);

                                    if ($valor == "child")
                                    {
                                        //verifica se o valor da variavel eh filho do nivel de permissao do usuario
                                        $condicaoSatisfeita = true;
                                    }
                                    elseif ($valor == "all")
                                    {
                                        $condicaoSatisfeita = true;
                                    }
                                    else
                                    {
                                        if ($valorAVerificar == $valor)
                                        {
                                            $condicaoSatisfeita = true;
                                        }
                                        else
                                        {
                                            $condicaoSatisfeita = false;
                                        }
                                    }
                                }
                                elseif (Helper::GET($variavel) != null && $valor == "key")
                                {
                                    $verificarProximoValor = true;
                                    $chaveDoAnterior = $variavel;
                                }
                                else
                                {
                                    //nao pesquisar
                                    $condicaoSatisfeita = false;
                                    break;
                                }

                                if ($condicaoSatisfeita)
                                {
                                    $indicesAPesquisar[] = $identificadorFuncionalidade;
                                }
                            }
                        }
                        else
                        {
                            $indicesAPesquisar[] = $identificadorFuncionalidade;
                        }
                    }
                }
            }

            return $this->verificarPermissaoAFuncionalidades($indicesAPesquisar);
        }

        public function verificarPermissaoEmPaginasDoUsuarioCorrente()
        {
            $paginaAtual = Helper::GET("page");
            $diretorioPagina = Helper::GET("tipo");
            $idUsuarioCorrente = self::getId();
            $idTipoUsuario = self::getIdDoTipoDoUsuarioLogado();

            $nivelEstruturaPermissao = true;

            $diretorioEPagina = "{$diretorioPagina}/{$paginaAtual}";

            $indicesAPesquisar = array();

            $iterador = 0;

            $this->montarListaDeFuncionalidades();

            $arrayDeFuncionalidades = $this->getListaDeFuncionalidades();

            if (is_array($arrayDeFuncionalidades))
            {
                //procura funcionalidades que posssuem esta pagina
                foreach ($arrayDeFuncionalidades as $funcionalidade)
                {
                    $identificadorFuncionalidade = $funcionalidade->getId();

                    if (($iterador = array_search($diretorioEPagina, $funcionalidade->paginasQueTemPermissao)) !== false)
                    {
                        if (is_array($funcionalidade->variaveisEValoresPermitidos[ $iterador ]))
                        {
                            $condicaoSatisfeita = true;
                            $verificarProximoValor = false;
                            $chaveDoAnterior = false;

                            foreach ($funcionalidade->variaveisEValoresPermitidos[ $iterador ] as $variavel => $valor)
                            {
                                if ($verificarProximoValor == true)
                                {
                                    $objVerifica = Helper::getObjeto($funcionalidade->classe);

                                    Helper::getRetornoDeMetodo($objVerifica, "select", array(Helper::GET("$chaveDoAnterior")));

                                    $valorAVerificar = Helper::getRetornoDeMetodo($objVerifica, "get" . ucfirst($variavel));

                                    if ($valor == "child")
                                    {
                                        if ($nivelEstruturaPermissao === true)
                                        {
                                            $condicaoSatisfeita = true;
                                        } //verifica se o valor da variavel eh filho do nivel de permissao do usuario
                                        else
                                        {
                                            $condicaoSatisfeita = false;
                                        }
                                    }
                                    else
                                    {
                                        if ($valorAVerificar == $valor)
                                        {
                                            $condicaoSatisfeita = true;
                                        }
                                        else
                                        {
                                            $condicaoSatisfeita = false;
                                        }
                                    }

                                    $verificarProximoValor = false;
                                }
                                elseif (Helper::GET($variavel) != null && $valor == "key")
                                {
                                    $verificarProximoValor = true;
                                    $chaveDoAnterior = $variavel;
                                }
                                elseif (Helper::GET($variavel) != null && $valor == "all")
                                {
                                    $condicaoSatisfeita = true;
                                }
                                elseif (Helper::GET($variavel) == $valor)
                                {
                                    $condicaoSatisfeita = true;
                                }
                                elseif (Helper::GET($variavel) != null && $valor == "child")
                                {
                                    $valorAVerificar = Helper::GET($variavel);

                                    //verifica se o valor da variavel eh filho do nivel de permissao do usuario
                                    $condicaoSatisfeita = true;
                                }
                                else
                                {
                                    //nao pesquisar
                                    $condicaoSatisfeita = false;
                                    break;
                                }
                            }

                            if ($condicaoSatisfeita)
                            {
                                $indicesAPesquisar[] = $identificadorFuncionalidade;
                            }
                        }
                        else
                        {
                            $indicesAPesquisar[] = $identificadorFuncionalidade;
                        }
                    }
                }
            }

            return $this->verificarPermissaoAFuncionalidades($indicesAPesquisar);
        }

        public function verificarPermissaoAFuncionalidades($indicesAPesquisar, $db = null)
        {
            $idUsuarioCorrente = self::getId();
            $idTipoUsuario = self::getIdDoTipoDoUsuarioLogado();

            //verifica se o usuario tem permissao para acessar alguma destas funcionalidades

            $stringBusca = "";

            foreach ($indicesAPesquisar as $indices)
            {
                $identificador = $this->listaDeFuncionalidades[ $indices ]->getId();

                $stringBusca .= "(identificador_funcionalidade='{$identificador}') OR ";
            }

            if (strlen($stringBusca))
            {
                $stringBusca = "AND " . Helper::removerOsUltimosCaracteresDaString($stringBusca, 3);
            }
            else
            {
                //se nao achou a pagina, eh porque nao esta mapeada, entao passa
                return true;
            }
            if ($db == null)
            {
                $db = new Database();
            }
            //verifica se estao definidas as permissoes do usuario corrente
            $db->query("SELECT COUNT(id) FROM usuario_privilegio WHERE usuario_id_INT={$idUsuarioCorrente}");

            if ($db->getPrimeiraTuplaDoResultSet(0) > 0)
            {
                //procura permissao no usuario
                $db->query("SELECT COUNT(id) FROM usuario_privilegio WHERE usuario_id_INT={$idUsuarioCorrente} {$stringBusca}");
            }
            else
            {
                //procura permissao no tipo de usuario
                $db->query("SELECT COUNT(id) FROM usuario_tipo_privilegio WHERE usuario_tipo_id_INT={$idTipoUsuario} {$stringBusca}");
            }

            if ($db->getPrimeiraTuplaDoResultSet(0) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function adicionarFuncionalidadeNaLista(&$objFuncionalidade)
        {
            $this->listaDeFuncionalidades[ $objFuncionalidade->getId() ] = clone $objFuncionalidade;
            $objFuncionalidade->limparListaDePaginasQueTemPermissao();
        }

        public function montarListaDeFuncionalidades()
        {
            $objFuncionalidade = new Seguranca_Funcionalidade();

            $this->adicionarFuncionalidadeNaLista($objFuncionalidade);
        }

        public function verificaUsuarioESenha($idUsuario, $senha, $db = null)
        {
            if (!(strlen($idUsuario) > 0 && strlen($senha) > 0))
            {
                return false;
            }
            $criptografia = Registry::get('Crypt');
            if ($db == null)
            {
                $db = new Database();
            }
            $senhaC = $criptografia->crypt($senha);
            $db->query("SELECT u.status_BOOLEAN AS status
                    FROM usuario AS u
                    WHERE u.id='$idUsuario' AND u.senha='$senhaC'");

            if ($db->rows() == 1)
            {
                if ($db->getPrimeiraTuplaDoResultSet("status") == 1)
                {
                    return true;
                }
            }

            return false;
        }

    }

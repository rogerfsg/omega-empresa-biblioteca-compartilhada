<?php

    class Seguranca_Argument
    {

        public $pagina;
        public $operacao;
        public $dataHora;
        public $chaveDoRegistroOperado;
        public $usuarioOperacao;
        public $descricaoOperacao;
        public $entidadeOperacao;
        public $urlOperacao;

        public function __construct()
        {
            $this->pagina = $_SERVER["PHP_SELF"];
            $this->dataHora = date("Y-m-d H:i:s");
        }

    }

?>

<?php

    /**
     * N/X API to Google Maps
     * Uses Google Maps API 2.0 to create customizable maps
     * that can be embedded on your website
     *
     *    Copyright (C) 2006  Sven Weih <sven@nxsystems.org>
     *
     *    This program is free software; you can redistribute it and/or modify
     *    it under the terms of the GNU General Public License as published by
     *    the Free Software Foundation; either version 2 of the License, or
     *    (at your option) any later version.
     *
     *    This program is distributed in the hope that it will be useful,
     *    but WITHOUT ANY WARRANTY; without even the implied warranty of
     *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *    GNU General Public License for more details.
     *
     *    You should have received a copy of the GNU General Public License
     *    along with this program; if not, write to the Free Software
     *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
     */
    /**
     * Allowed Controls:
     * GLargeMapControl - a large pan/zoom control used on Google Maps. Appears in the top left corner of the map.
     * GSmallMapControl - a smaller pan/zoom control used on Google Maps. Appears in the top left corner of the map.
     * GSmallZoomControl - a small zoom control (no panning controls) used in the small map blowup windows used to display driving directions steps on Google Maps.
     * GScaleControl - a map scale
     * GMapTypeControl - buttons that let the user toggle between map types (such as Map and Satellite)
     * GOverviewMapControl - a collapsible overview map in the corner of the screen
     */

    /**
     * API-Class for accessing Google Maps
     */
    class NXGoogleMapsAPI
    {
//
//    const panControlPresent = 'panControl';
//    const zoomControlPresent = 'zoomControl';
//    const mapTypeControlPresent = 'mapTypeControl';
//    const scaleControlPresent = 'scaleControl';
//    const streetViewControlPresent = 'streetViewControl';
//    const overviewMapControlPresent = 'overviewMapControl';
//
//    const zoomControlStyleSMALL = 'style: google.maps.ZoomControlStyle.SMALL';
//    const zoomControlStyleLARGE = 'style: google.maps.ZoomControlStyle.LARGE';
//    const zoomControlStyleDEFAULT = 'style: google.maps.ZoomControlStyle.DEFAULT';
//
//    const mapTypeControleStyleHORIZONTAL_BAR = 'style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR';
//    const mapTypeControleStyleDROPDOWN_MENU = 'style: google.maps.MapTypeControlStyle.DROPDOWN_MENU';
//    const mapTypeControleStyleDEFAULT = 'style: google.maps.MapTypeControlStyle.DEFAULT';
//
//    const controlPositionTOP_CENTER = 'position: google.maps.ControlPosition.TOP_CENTER';
//    const controlPositionTOP_LEFT = 'position: google.maps.ControlPosition.TOP_LEFT';
//    const controlPositionTOP_RIGHT = 'position: google.maps.ControlPosition.TOP_RIGHT';
//    const controlPositionLEFT_TOP = 'position: google.maps.ControlPosition.LEFT_TOP';
//    const controlPositionRIGHT_TOP = 'position: google.maps.ControlPosition.RIGHT_TOP';
//    const controlPositionLEFT_CENTER = 'position: google.maps.ControlPosition.LEFT_CENTER';
//    const controlPositionRIGHT_CENTER = 'position: google.maps.ControlPosition.RIGHT_CENTER';
//    const controlPositionLEFT_BOTTOM = 'position: google.maps.ControlPosition.LEFT_BOTTOM';
//    const controlPositionRIGHT_BOTTOM = 'position: google.maps.ControlPosition.RIGHT_BOTTOM';
//    const controlPositionBOTTOM_CENTER = 'position: google.maps.ControlPosition.BOTTOM_CENTER';
//    const controlPositionBOTTOM_LEFT = 'position: google.maps.ControlPosition.BOTTOM_LEFT';
//    const controlPositionBOTTOM_RIGHT = 'position: google.maps.ControlPosition.BOTTOM_RIGHT';
//
//    // Width and Height of the Control
//    var $width;
//    var $height;
//    // GoogleMaps output div id
//    var $divId;
//    // ZoomFactor
//    var $zoomFactor;
//    // Map Center Coords
//    var $centerX;
//    var $centerY;
//    // DragMarker
//    var $dragX;
//    var $dragY;
//    // Address Array
//    var $addresses;
//    // GeoPoint Array
//    var $geopoints;
//    // Arrays with the controls that will be displayed
//    var $controls;
//
//    var $pathGeopoints;
//    var $pathColor;
//
//    function __construct()
//    {
//
//        $this->_initialize();
//    }
//
        static function getAddress($lat, $long)
        {
//
//        if (strlen($lat) && strlen($long)) {
//
//            // format this string with the appropriate latitude longitude
//            $url = "http://maps.google.com/maps/geo?q={$lat},{$long}&output=json&sensor=false";
//            // make the HTTP request
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//
//            $dados = curl_exec($ch);
//
//            // parse the json response
//            $jsondata = json_decode($dados, true);
//
//            // if we get a placemark array and the status was good, get the addres
//            if (is_array($jsondata) && $jsondata ['Status']['code'] == 200) {
//
//                $addr = utf8_decode($jsondata ['Placemark'][0]['address']);
//                return $addr;
//            } else {
//
//                return null;
//            }
//        } else {
//
//            return null;
//        }
        }
//
//    /**
//     * Add an address-marker to the map. The address is resolved by the webbrowser.
//     * with the Google Geocoder.
//     *
//     * @param string address which should be add. test with google maps
//     * @param string HTML-Code which will be displayed when the user clicks the address
//     * @param boolean Set the Center to this point(true) or not (false)
//     */
//    function addAddress($address, $htmlinfo, $setCenter = true, $urlMarcador = false, $draggable = false)
//    {
//        $ar = array(addSlashes($address), addSlashes($htmlinfo), $setCenter, $urlMarcador, $draggable);
//        array_push($this->addresses, $ar);
//    }
//
//    /**
//     * Add a dragable marker to the map. Only one Drag-Marker is allowed!
//     *
//     * @param integer $longitude Longitude of the point
//     * @param integer $latitude Lattitude of the point
//     */
//    function addDragMarker($longitude, $latitude)
//    {
//        $this->dragX = $longitude;
//        $this->dragY = $latitude;
//    }
//
//    /**
//     * Add a geopoint to the map.
//     *
//     * @param integer Longitude of the point
//     * @param integer Latitude of the point
//     * @param string HTML-Code which will be displayed when the user clicks the address
//     * @param boolean Set the Center to this point(true) or not (false)
//     */
//    function addGeoPoint($longitude, $latitude, $htmlinfo, $setCenter, $urlMarcador = false, $draggable = false)
//    {
//        $ar = array($longitude, $latitude, addSlashes($htmlinfo), $setCenter, addSlashes($urlMarcador), $draggable);
//        array_push($this->geopoints, $ar);
//    }
//
//    function addPathGeoPoint($indicePath, $longitude, $latitude)
//    {
//
//        if (!isset($this->pathGeopoints[$indicePath])) {
//
//            $this->pathGeopoints[$indicePath] = array();
//
//        }
//
//        array_push($this->pathGeopoints[$indicePath], array($longitude, $latitude));
//
//    }
//
//    function addControl($control, $arrControlOptions = null)
//    {
//
//        if (strlen($control)) {
//
//            $strControl .= "{$control} : true";
//
//        }
//
//        if (is_array($arrControlOptions) && count($arrControlOptions) > 0) {
//
//            $strControl .= ",\n {$control}Options: {\n";
//
//            for ($i = 0; $i < count($arrControlOptions); $i++) {
//
//                $atributo = $arrControlOptions[$i];
//                $strControl .= "{$atributo}";
//
//                if ($i < count($arrControlOptions) - 1) {
//
//                    $strControl .= ",\n";
//
//                }
//
//            }
//
//            $strControl .= "}";
//
//        }
//
//        array_push($this->controls, $strControl);
//    }
//
//    function setPathColor($indicePath, $corHexadecimal)
//    {
//
//        $this->pathColor[$indicePath] = $corHexadecimal;
//
//    }
//
//    /**
//     * Set the ZoomFactor
//     * The ZoomFactor is a value between 0 and 17
//     *
//     * @param integer $zoomFactor Value of the Zoom-Factor
//     */
//    function setZoomFactor($zoomFactor)
//    {
//        if ($zoomFactor > -1 && $zoomFactor < 18) {
//            $this->zoomFactor = $zoomFactor;
//        }
//    }
//
//    /**
//     * Set the width of the map
//     *
//     * @param integer $width The Height in pixels
//     */
//    function setWidth($width)
//    {
//        $this->width = $width;
//    }
//
//    /**
//     * Set the height of the map
//     *
//     * @param integer $height The Height in pixels
//     */
//    function setHeight($height)
//    {
//        $this->height = $height;
//    }
//
//    /**
//     * Center the map to the coordinates
//     *
//     * @param integer $x Longitude
//     * @param integer $y Latitude
//     */
//    function setCenter($x, $y)
//    {
//        $this->centerX = $x;
//        $this->centerY = $y;
//    }
//
//    /**
//     * Returns the HTML-Code, which must be placed within the <HEAD>-Tags of your page.
//     *
//     * @returns string The Code for the <Head>-Tag
//     */
//    public static $carregarScriptRemoto = false;
//
//    function getHeadCode()
//    {
//
//        if (!self::$carregarScriptRemoto) {
//
//            $out = '
//            <style type="text/css">
//                    v\:* {
//                  behavior:url(#default#VML);
//                }
//               </style>
//               
//               ';
//
//            $pathToRaiz = Helper::acharRaiz();
//            $api = '<script src="https://maps.google.com/maps/api/js?sensor=false"  type="text/javascript"></script>';
//            $api .= "<script src=\"{$pathToRaiz}recursos/libs/google-maps/oms.min.js\"  type=\"text/javascript\"></script>";
//
//            $out .= $api;
//
//            self::$carregarScriptRemoto = true;
//        }
//
//        $out .= $this->_getGMapInitCode();
//        return $out;
//    }
//
//    /**
//     * Get the BodyCode and draw the map.
//     *
//     * @returns string Returns the code which is to be placed wight the <body>-tags.
//     */
//    function getBodyCode()
//    {
//        $out = '<div id="' . $this->divId . '" style="width:' . $this->width . 'px;height:' . $this->height . 'px;"></div>';
//        return $out;
//    }
//
//    /**
//     * Get the code, which must be passed to the <body>-attribute onLoad.
//     *
//     * @returns string The onload Code
//     */
//    function getOnLoadCode()
//    {
//        $out = "{$this->divId}_initNXGMap(document.getElementById('$this->divId'));";
//        return $out;
//    }
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////// Internal functions /////////////////////////////////////////////////////////////////////////////////  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
//
//    /**
//     * Compiles the Javascript to initialize the map.
//     * Is automatically called, so do not call yourself.
//     */
//    function _getGMapInitCode()
//    {
//        $out = "
//<script type=\"text/javascript\">
//
//    //<![CDATA[
//    var {$this->divId}_map = null;
//    var {$this->divId}_geocoder = null;
//    var {$this->divId}_center = null;
//    var {$this->divId}_updateX = null;
//    var {$this->divId}_updateY = null;
//    var {$this->divId}_marker = null;
//    var {$this->divId}_pathGeoPoints = null;
//    var {$this->divId}_pathColor = null;
//    var {$this->divId}_arrMarkers = Array();
//    var {$this->divId}_infoWindow = new google.maps.InfoWindow();
//    var {$this->divId}_oms = null;
//    ";
//
//        if (count($this->pathColor) > 0) {
//
//            $out .= "var {$this->divId}_pathColor = Array(";
//
//            for ($i = 0; $i < count($this->pathColor); $i++) {
//
//                $out .= "'{$this->pathColor[$i]}'";
//                if ($i < (count($this->pathColor) - 1))
//                    $out .= ', ';
//
//            }
//
//            $out .= ");\n";
//
//        } else {
//            $out .= "var {$this->divId}_pathColor = new Array();\n";
//        }
//
//        // Add Geopoints Array
//        if (count($this->pathGeopoints) > 0) {
//
//            $out .= "var {$this->divId}_pathGeoPoints = new Array(";
//
//            foreach ($this->pathGeopoints as $arrPontos) {
//
//                $out .= "new Array(";
//
//                for ($i = 0; $i < count($arrPontos); $i++) {
//                    $out .= " new Array(" . $arrPontos[$i][0] . "," . $arrPontos[$i][1] . ")";
//                    if ($i < (count($arrPontos) - 1))
//                        $out .= ', ';
//                }
//
//                $out .= ")";
//
//            }
//
//            $out .= ");\n";
//
//        } else {
//            $out .= "var {$this->divId}_pathGeoPoints = new Array();\n";
//        }
//        $out .= "\n";
//        if (count($this->geopoints) > 0) {
//            $out .= "var {$this->divId}_geopoints = new Array(";
//            for ($i = 0; $i < count($this->geopoints); $i++) {
//                $out .= " new Array(" . $this->geopoints[$i][0] . "," . $this->geopoints[$i][1] . " ,\"" . $this->geopoints[$i][2] . "\", ";
//                // move to this address?
//                if ($this->geopoints[$i][3]) {
//                    $out .= 'true';
//                } else {
//                    $out .= 'false';
//                }
//                $out .= ",";
//                if ($this->geopoints[$i][4]) {
//                    $out .= "'{$this->geopoints[$i][4]}'";
//                } else {
//                    $out .= "null";
//                }
//                $out .= ",";
//                if ($this->geopoints[$i][5]) {
//                    $out .= "true";
//                } else {
//                    $out .= "false";
//                }
//                $out .= ')';
//                if ($i < (count($this->geopoints) - 1))
//                    $out .= ', ';
//            }
//            $out .= ");\n";
//        } else {
//            $out .= "var {$this->divId}_geopoints = new Array();\n";
//        }
//
//        // Add Addresses Array      
//        $out .= "\n";
//        if (count($this->addresses) > 0) {
//            $out .= "var {$this->divId}_addresses = new Array(";
//            for ($i = 0; $i < count($this->addresses); $i++) {
//                $out .= ' new Array("' . $this->addresses[$i][0] . '", "' . $this->addresses[$i][1] . '", ';
//                // move to this address?
//                if ($this->addresses[$i][2]) {
//                    $out .= 'true';
//                } else {
//                    $out .= 'false';
//                }
//                $out .= ",";
//                if ($this->addresses[$i][3]) {
//                    $out .= "'{$this->addresses[$i][3]}'";
//                } else {
//                    $out .= "null";
//                }
//                $out .= ",";
//                if ($this->addresses[$i][4]) {
//                    $out .= "true";
//                } else {
//                    $out .= "false";
//                }
//                $out .= ')';
//                if ($i < (count($this->addresses) - 1))
//                    $out .= ', ';
//            }
//            $out .= ");\n";
//        } else {
//            $out .= "var {$this->divId}_addresses = new Array();\n";
//        }
//
//        // Draw standard js-functions and initialization code.
//        $out .= "
//            
//function {$this->divId}_setBounds(){
//
//    var bounds = new google.maps.LatLngBounds();
//    for(var i in {$this->divId}_arrMarkers)
//    {
//    
//      bounds.extend({$this->divId}_arrMarkers[i].getPosition());
//          
//    }
//        
//    {$this->divId}_map.fitBounds(bounds);
//      
//}
//
//function {$this->divId}_setPaths(){
//    
//    for(i=0; i < {$this->divId}_pathGeoPoints.length; i++){
//
//        var arrPontos = {$this->divId}_pathGeoPoints[i];
//        var cor = {$this->divId}_pathColor[i];
//        {$this->divId}_setPath(arrPontos, cor);
//        
//    }
//
//}
//
//function {$this->divId}_setPath(arrPontos, cor){
//
//    if({$this->divId}_pathGeoPoints.length > 0){
//
//        var path = Array();
//        for (i=0; i < arrPontos.length; i++){
//
//            path.push(new google.maps.LatLng(arrPontos[i][0], arrPontos[i][1]));
//
//        }
//
//        var line = new google.maps.Polyline({
//
//            path: path,
//            strokeColor: cor,
//            strokeOpacity: 0.6,
//            strokeWeight: 4,
//            map: {$this->divId}_map
//
//          });
//        
//    }
//
//}
//
//function {$this->divId}_showAddresses() {
//  for (i=0; i < {$this->divId}_addresses.length; i++) {
//     {$this->divId}_showAddress({$this->divId}_addresses[i][0], {$this->divId}_addresses[i][1], {$this->divId}_addresses[i][2], {$this->divId}_addresses[i][3], {$this->divId}_addresses[i][4]);
//  }	
//}
//    	
//function {$this->divId}_showAddress(address, htmlInfo, moveToPoint, urlMarcador, draggable) {
// if ({$this->divId}_geocoder) {
//   {$this->divId}_geocoder.geocode(
//     {'address': address },
//     function(results, status) {
//
//       if (status != google.maps.GeocoderStatus.OK) {
//         //alert(\"Location not found:\" + address);
//       } else {              
//         if (moveToPoint) {
//           {$this->divId}_map.setCenter(results[0].geometry.location);
//           {$this->divId}_map.setOptions({zoom: {$this->zoomFactor}});    
//         }
//         var marker = new google.maps.Marker({
//            position : results[0].geometry.location,
//            map: {$this->divId}_map,
//            icon: urlMarcador,
//            'draggable': draggable,
//            descricao: htmlInfo
//            });
//            
//         {$this->divId}_arrMarkers.push(marker);
//         {$this->divId}_oms.addMarker(marker);
//             
//       }
//     }
//   );
//  }
//}
//
//function {$this->divId}_showGeopoints() {
//  for (i=0; i < {$this->divId}_geopoints.length; i++) {
//        {$this->divId}_showGeopoint({$this->divId}_geopoints[i][0], {$this->divId}_geopoints[i][1], {$this->divId}_geopoints[i][2], {$this->divId}_geopoints[i][3], {$this->divId}_geopoints[i][4], {$this->divId}_geopoints[i][5]);
//  }	
//}
//
//function {$this->divId}_showGeopoint(longitude, latitude, htmlInfo, moveToPoint, urlMarcador, draggable){
//    
//    var point = new google.maps.LatLng(longitude, latitude);    
//    if (moveToPoint) {
//        {$this->divId}_map.setCenter(point);
//        {$this->divId}_map.setOptions({zoom: {$this->zoomFactor}});    
//      }
//      var marker = new google.maps.Marker({
//         position : point,
//         map: {$this->divId}_map,
//         icon: urlMarcador,
//         'draggable': draggable,
//         descricao: htmlInfo
//         });
//      {$this->divId}_arrMarkers.push(marker);
//      {$this->divId}_oms.addMarker(marker);
//          
//}
//
//function {$this->divId}_moveToGeopoint(index) {
//	{$this->divId}_map.panTo(new google.maps.LatLng(geopoints[index][0], geopoints[index][1]));
//}
//
//function {$this->divId}_moveToAddress(index) {
//  {$this->divId}_moveToAddressEx({$this->divId}_addresses[index][0]); 
//}
//
//function {$this->divId}_moveToAddressEx(addressString) {
//  if ({$this->divId}_geocoder) { 
//   {$this->divId}_geocoder.getLatLng(
//     addressString,
//     function(results, status) {       
//       if (status != google.maps.GeocoderStatus.OK) {
//         //alert(\"Location not found:\" + addressString);
//       } else {                                    
//          {$this->divId}_center = results[0].geometry.location;
//          {$this->divId}_map.panTo(results[0].geometry.location);           
//       }
//     });    
//  }
//}
//
//function {$this->divId}_setZoomFactor(factor) {
//	  {$this->divId}_map.setZoom(factor);
//}
//
//function {$this->divId}_addDragableMarker(latLong, urlMarcador, jQuerySelectorLat, jQuerySelectorLong) {
//    
//    var marker = new google.maps.Marker({
//        position : latLong,
//        map: {$this->divId}_map,
//        icon: urlMarcador,
//        draggable: true
//    });
//       
//    google.maps.event.addListener(marker, \"dragend\", function() {      
//      var tpoint =  {$this->divId}_marker.getPoint();      
//      $(jQuerySelectorLat).val(tpoint.lat());
//      $(jQuerySelectorLong).val(tpoint.long());         
//  });
//             
//}
//    	
//function {$this->divId}_initNXGMap(mapElement) {
//
//";
//
//        // Add controls to the map
//        if (count($this->controls) > 0) {
//            for ($i = 0; $i < count($this->controls); $i++) {
//                $strControls .= $this->controls[$i] . ",\n";
//            }
//        }
//
//        $out .= "
//        
// 	if(document.getElementById) {
//
//            var opcoes = {
//                zoom: 16,
//                {$strControls}
//                mapTypeId: google.maps.MapTypeId.ROADMAP                    
//            }
//
//            {$this->divId}_map = new google.maps.Map(mapElement, opcoes);        
//            {$this->divId}_geocoder = new google.maps.Geocoder();";
//
//        // Center the map
//        if (($this->centerX != -1000) && ($this->centerY != -1000)) {
//            $out .= "    {$this->divId}_map.setCenter(new google.maps.LatLng(" . $this->centerX . ', ' . $this->centerY . '), ' . $this->zoomFactor . ');' . "\n";
//        } else {
//            $out .= "    {$this->divId}_map.setCenter(new google.maps.LatLng(0,0),1);\n";
//        }
//
//        $out .= "{$this->divId}_oms = new OverlappingMarkerSpiderfier({$this->divId}_map);\n";
//
//        $out .= "{$this->divId}_oms.addListener('click', function(marker) {
//                     {$this->divId}_infoWindow.setContent(marker.descricao);        
//                     {$this->divId}_infoWindow.setPosition(marker.position);
//                     {$this->divId}_infoWindow.open({$this->divId}_map);
//                    });              
//                     ";
//
//        $out .= "    {$this->divId}_showAddresses();\n";
//        $out .= "    {$this->divId}_showGeopoints();\n";
//        $out .= "    {$this->divId}_setPaths();\n";
//
//        $out .= "    setTimeout('{$this->divId}_setBounds()', 5000);\n
//
//            }
//            
//    	}
//        
//     //]]>
//        </script>";
//        return $out;
//    }
//
//    /**
//     * Initializes the standard values of the class.
//     * Is automatically called by the constructor.
//     */
//    function _initialize()
//    {
//        $this->width = 800;
//        $this->height = 600;
//        $this->divId = 'map';
//        $this->zoomFactor = 14;
//        $this->centerX = -1000;
//        $this->centerY = -1000;
//        $this->dragX = -1000;
//        $this->dragY = -1000;
//        $this->addresses = array();
//        $this->geopoints = array();
//        $this->controls = array();
//        $this->pathGeopoints = array();
//        $this->pathColor = array();
//    }
//
    }

?>
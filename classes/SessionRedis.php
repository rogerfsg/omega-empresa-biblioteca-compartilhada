<?php

    class SessionRedis
    {
        private $session;
        private $sessionKey;

        const LOGIN_NAO_REALIZADO = 1;
        const LOGIN_REALIZADO = 3;

        const ANONIMO = 4;
        const AUTENTICADO = 5;
        const NAO_AUTENTICADO = 6;
        const NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO = 7;

        private $prefixo;
        private $estado;

        public function getSessionKey()
        {
            return $this->sessionKey;
        }

        public function __construct($prefixo)
        {
            $this->estado = SessionRedis::NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO;
            $this->prefixo = $prefixo;
        }

        public function loadSession($realizarLoginAnonimo = true)
        {
            HelperLog::logRedis("SessionRedis::loadSession");
            if ($this->estado != SessionRedis::NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO)
            {
                HelperLog::logRedis("SessionRedis::loadSession. Cached Autenticado: " . $this->estado);
                if ($this->estado == SessionRedis::AUTENTICADO)
                {
                    return SessionRedis::LOGIN_REALIZADO;
                }
                else
                {
                    return SessionRedis::LOGIN_NAO_REALIZADO;
                }
            }

            $chaveSession = Helper::COOKIEPOSTGET(ConstanteSeguranca::CHAVE_SESSION);
//        echo "entrou:".$this->getChaveCookie()." ".$chaveSession;
//        exit();
            if ($chaveSession != null)
            {
                //verifica a existencia de um registro no redis [key] => [value], tal que:
                // key = sessionKey = PT231354 ou CO213243, ou BL532131 (N�mero que fica salvo na sess�o do cliente - android, cookie do browser)
                //TODO deve ser desenvolvido um mecanismo de login entre os sistemas com a chave de um deles.
                //Ex. key = PT23354
                // helperRedis = prefixo == null => PREFIXO_SISTEMA

                $redis = HelperRedis::getSingleton(null);
                $this->sessionKey = $chaveSession;
                if ($redis->exists($chaveSession))
                {
//                HelperLog::logRedis("exists($chaveSession) - validando ip");
//                if(Helper::getIp() == $session["ip"]){

                    HelperLog::logRedis("exists($chaveSession) - validado");
                    $this->session = array();
                    if ($this->get("anonimo", false, true) == 1)
                    {
                        $this->estado = SessionRedis::ANONIMO;

                        return SessionRedis::LOGIN_NAO_REALIZADO;
                    }
                    else
                    {
                        $this->estado = SessionRedis::AUTENTICADO;

                        return SessionRedis::LOGIN_REALIZADO;
                    }
//                }
//                else {
//                    HelperLog::logRedis("exists($chaveSession) - invalido");
//                    $this->logout();
//                    if($realizarLoginAnonimo){
//                        $this->loginAnonimo();
//                        return SessionRedis::LOGIN_NAO_REALIZADO; 
//                    } else {
//                        $this->estado = SessionRedis::NAO_AUTENTICADO;
//                    }
//                    return SessionRedis::LOGIN_NAO_REALIZADO;
//                }
                }
                else
                {
                    HelperLog::logRedis("loadSession ($chaveSession) - chave expirada no Redis!");
                    $this->estado = SessionRedis::NAO_AUTENTICADO;

                    Helper::clearCookie(ConstanteSeguranca::CHAVE_SESSION);
                    return SessionRedis::LOGIN_NAO_REALIZADO;
                }
            }
            else
            {
                HelperLog::logRedis("loadSession - cliente n�o possui chave ainda. Logo n�o foi autenticado.");
            }

            if ($realizarLoginAnonimo)
            {
                HelperLog::logRedis("loadSession ($chaveSession) - primeira tentativa de login, realizando loginAnonimo: $realizarLoginAnonimo...");
                $this->loginAnonimo();
            }
            else
            {
                HelperLog::logRedis("loadSession ($chaveSession) - nao autenticado");
                $this->estado = SessionRedis::NAO_AUTENTICADO;
            }

            return SessionRedis::LOGIN_NAO_REALIZADO;
        }

        //Retorna - AUTENTICADO, NAO_AUTENTICADO, ANONIMO(S� � ativado sob demanda)
        public function getEstadoAutenticacao()
        {
//        if($this->estado ){
//            HelperLog::logRedis("getEstadoAutenticacao - Estado: ".SessionRedis::AUTENTICADO);
//            return SessionRedis::AUTENTICADO;
//        }
//        $estado = $this->session['anonimo'] ? SessionRedis::ANONIMO : SessionRedis::NAO_AUTENTICADO;
//        HelperLog::logRedis("getEstadoAutenticacao - Estado:  $estado");
            return $this->estado;
        }

        public function logout()
        {
            $estado = $this->getEstadoAutenticacao();
            HelperLog::logRedis("logout. Estado: " . $estado);

            if (!strlen($this->sessionKey))
            {
                $this->sessionKey = Helper::COOKIE(ConstanteSeguranca::CHAVE_SESSION);
            }

            Helper::clearCookie(ConstanteSeguranca::CHAVE_SESSION);
            Helper::clearCookie(ConstanteSeguranca::CHAVE_AUTO_LOGIN);

            $redis = HelperRedis::getSingleton(null);
            HelperLog::logRedis("logout - del({$this->sessionKey})");

            if (strlen($this->sessionKey))
            {
                $redis->del($this->sessionKey);
            }

            unset($this->session);
            unset($this->estado);
            unset($this->sessionKey);

            $this->estado = SessionRedis::NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO;
        }

        public static function getSingleton($prefixo = HelperRedis::PREFIXO_IDENTIFICADOR_SESSAO)
        {
            $sr = Registry::get("SessionRedis{$prefixo}", false);
            if ($sr == null)
            {
                HelperLog::logRedis("SessionRedis{$prefixo} construindo...");
                $sr = new SessionRedis($prefixo);
                Registry::add($sr, "SessionRedis{$prefixo}");
            }

            return $sr;
        }

        public function set($key, $value)
        {
            $valueLog = is_array($value) || is_object($value) ? serialize($value) : $value;
            HelperLog::logRedis("realizando set {$key}, {$valueLog}.");

            if ($this->getEstadoAutenticacao() == SessionRedis::ANONIMO
                || $this->getEstadoAutenticacao() == SessionRedis::AUTENTICADO)
            {
                $redis = HelperRedis::getSingleton(null);
                $value = serialize($value);
                $redis->hset($this->sessionKey, $key, $value);
                $this->session[ $key ] = $value;
            }
        }

        public function clear($key)
        {
            HelperLog::logRedis("clear {$key}");
            if ($this->getEstadoAutenticacao() == SessionRedis::ANONIMO
                || $this->getEstadoAutenticacao() == SessionRedis::AUTENTICADO)
            {
                $redis = HelperRedis::getSingleton(null);
                $redis->hdel($this->sessionKey, $key);
            }
        }

        public function exists($key)
        {
            HelperLog::logRedis("exists $key");
            if ($this->getEstadoAutenticacao() == SessionRedis::ANONIMO
                || $this->getEstadoAutenticacao() == SessionRedis::AUTENTICADO)
            {
                return isset($this->session[ $key ]);
            }
        }

        public function get($key, $isObjectOrArray = false, $callFromLoadSession = false)
        {
            HelperLog::logRedis("get $key");
            //echo $this->getEstadoAutenticacao() ;
            if (!$callFromLoadSession)
            {
                $this->loadSession(false);
            }
            if ($this->getEstadoAutenticacao() == SessionRedis::ANONIMO
                || $this->getEstadoAutenticacao() == SessionRedis::AUTENTICADO)
            {
                if (isset($this->session[ $key ]))
                {
                    HelperLog::logRedis("get {$key} - ja foi carregado no Redis");
                    return $this->session[ $key ];
                }

                $redis = HelperRedis::getSingleton(null);
                $valor = $redis->hget($this->sessionKey, $key);
                HelperLog::logRedis("get $key - $valor");

                if ($valor == null)
                {
                    return null;
                }
                $unserialized = unserialize($valor);
//            if($isObjectOrArray)
                $this->session[ $key ] = $unserialized;

//            else $this->session[$key] = $valor;

                return $unserialized;
            }
            else
            {
                HelperLog::logRedis("get $key - N�o autorizado, estado atual: " . $this->getEstadoAutenticacao());

                return null;
            }
        }
//    public static function hasToSerialize(&$valor){
//        return !is_string($valor)
//                && (  is_object($valor) || is_array($valor));
//    }
        private function formatSession($session)
        {
            if (!is_array($session))
            {
                return;
            }
            foreach ($session as $chave => &$valor)
            {
//            if(is_array($valor)){
////                if(isset($valor[null])){
////                    $valor["null"] = $valor[null];
////                    unset($valor[null]);
////                }
//                
//                Helper::formatHtmlSpecialChars($valor);
//                
//                $valor = json_encode($valor);
//                
//            }
//            else if( is_object($valor)){
//                $valor = json_encode($valor);
//            }
                //if(SessionRedis::hasToSerialize($valor))
                $valor = serialize($valor);
            }

            return $session;
        }

        public function login($session, $email)
        {
            $this->loadSession(false);

            if ($this->getEstadoAutenticacao() != SessionRedis::NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO)
            {
                $this->logout();
            }

            $session['ip'] = Helper::getIp();
            $sessionFormatada = $this->formatSession($session);

            $this->estado = SessionRedis::AUTENTICADO;
            $redis = HelperRedis::getSingleton(null);
            $identificadorSessao = null;

            if (defined('IDENTIFICADOR_SESSAO'))
            {
                $identificadorSessao = IDENTIFICADOR_SESSAO;
            }
            else
            {
                $configuracaoSite = Registry::get('ConfiguracaoSite', null);
                if ($configuracaoSite != null)
                {
                    $identificadorSessao = $configuracaoSite->IDENTIFICADOR_SESSAO;
                }
            }

            $key = $identificadorSessao . uniqid();
            HelperLog::logRedis("login $key - " . print_r($sessionFormatada, true));

            $this->sessionKey = $key;

            $this->session = $session;

            $redis->hmset($this->sessionKey, $sessionFormatada);
            Helper::setCookie(
                ConstanteSeguranca::CHAVE_SESSION,
                $key,
                time() + (ConstanteSeguranca::TEMPO_LIMITE_SESSAO_MINUTOS * 60));

            $redis->expireAt(
                $this->sessionKey,
                time() + (ConstanteSeguranca::TEMPO_LIMITE_SESSAO_MINUTOS * 60));

            return $key;
        }
//    private function setCookieAutoLogin($email, $senha){
//          $crypt = Registry::get('Crypt');
//        $link = $crypt->cryptGet(
//            array("email"),
//            array($email),
//            3 * 60);
//        
//         Helper::setCookie(
//            ConstanteSeguranca::CHAVE_AUTO_LOGIN,
//            $link,
//            time()+ $this->TEMPO_LIMITE_LOGIN_SEGUNDOS);
//        
//    }
        public function loginAnonimo()
        {
            $redis = HelperRedis::getSingleton(null);
            $key = uniqid();
            $this->sessionKey = $key;
            $this->session = array(
                'anonimo' => true,
                'ip' => Helper::getIp(),
            );
            $redis->hmset($key, $this->session);
            HelperLog::logRedis("loginAnonimo: $key");

            HelperLog::logRedis("loginAnonimo - setCookie(" . ConstanteSeguranca::CHAVE_SESSION . ",$key) ....");
            Helper::setCookie(
                ConstanteSeguranca::CHAVE_SESSION,
                $key,
                time() + (ConstanteSeguranca::TEMPO_LIMITE_SESSAO_MINUTOS * 60));
            HelperLog::logRedis("loginAnonimo - cookie modified.");
            $this->estado = SessionRedis::ANONIMO;

            return $key;
        }
    }

<?php

    if (!defined("DOWNLOADFILE_H"))
    {
        define("DOWNLOADFILE_H", 1);

        class Download
        {
            public $df_path = "";
            public $df_contenttype = "";
            public $df_contentdisposition = "";
            public $df_filename = "";

            function Download($df_path, $df_contenttype = "application/octet-stream", $df_contentdisposition = "attachment", $df_filename = "")
            {
                $this->df_path = $df_path;
                $this->df_contenttype = $df_contenttype;
                $this->df_contentdisposition = $df_contentdisposition;
                $this->df_filename = ($df_filename) ? $df_filename : basename($df_path);
            }

            // check is specified file exists?
            function df_exists()
            {
                if (file_exists($this->df_path))
                {
                    return true;
                }

                return false;
            }

            // get file size
            function df_size()
            {
                if ($this->df_exists())
                {
                    return filesize($this->df_path);
                }

                return false;
            }

            // return permission number for user 'other'
            function df_permitother()
            {
                return substr(decoct(fileperms($this->df_path)), -1);
            }

            // download file
            function df_download()
            {
                if ($this->df_exists() && $this->df_permitother() >= 4)
                {
                    header("Content-type: " . $this->df_contenttype);
                    header("Content-Disposition: " . $this->df_contentdisposition . "; filename=\"" . $this->df_filename . "\"");
                    header("Content-Length: " . $this->df_size());

                    $fp = readfile($this->df_path, "r");

                    return $fp;
                }

                return false;
            }

            //download string
            function ds_download($string)
            {
                header("Content-type: " . $this->df_contenttype);
                header("Content-Disposition: " . $this->df_contentdisposition . "; filename=\"" . $this->df_filename . "\"");
                header("Content-Length: " . strlen($string));

                return $string;
            }

        }
    } //if ( !defined("DOWNLOADFILE_H") )

?>

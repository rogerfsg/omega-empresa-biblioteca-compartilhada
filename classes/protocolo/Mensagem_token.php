<?php

    class Mensagem_token extends Interface_mensagem
    {

        public $mValor;

        public function __construct($pCodRetorno = null, $pMensagem = null, $pValor = null)
        {
            parent::__construct($pCodRetorno, $pMensagem);
            $this->mValor = $pValor;
        }

        protected function procedimentoInicializa($objJson)
        {
            $this->mValor = $objJson->mValor;
        }

        public function getJson()
        {
            $objRetorno = new stdClass();
            $objRetorno->mCodRetorno = $this->mCodRetorno;
            $objRetorno->mMensagem = $this->mMensagem;
            $objRetorno->mValor = $this->mValor;

            //faz passagem por referÍncia
            Helper::corrigirObjetoParaJsonEncode($objRetorno);

            return json_encode($objRetorno);
        }

        public function printJson()
        {
            echo $this->getJson();
        }

    }

?>

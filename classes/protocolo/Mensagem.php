<?php

    class Mensagem extends Interface_mensagem
    {
        public function __construct($pCodRetorno = null, $pMensagem = null, $exc = null)
        {
            parent::__construct($pCodRetorno, $pMensagem, $exc);
        }

        protected function procedimentoInicializa($objJson)
        {
        }

        public static function factoryParametroInvalido()
        {
            $msg = new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                "Par�metro inv�lido");

            return $msg;
        }

        public static function factoryAcessoNegado($mensagem = null, $id = null)
        {
            if(!strlen($mensagem  ))
                $mensagem  = I18N::getExpression("O seu usu�rio n�o possui permiss�o para essa opera��o!");
            $msg = new Mensagem(
                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                $mensagem);

            return $msg;
        }


        public static function factoryJson($json)
        {
            $msg = new Mensagem();
            $msg->inicializa($json);

            return $msg;
        }

        public static function factoryErroComuniqueACentralSePersistir($protocoloSistema = PROTOCOLO_SISTEMA::ACESSO_NEGADO){

            return new Mensagem(
                $protocoloSistema,
                I18N::getExpression("Falha ao realizar o cadastro, tente novamente mais tarde. Se persistir o problema reporte o erro na workoffline.com.br ou via app Ponto Eletr�nico."));
        }

        public static function factoryErroFatal($msg = "Ocorreu um erro faltal")
        {
            $msg = new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                $msg);

            return $msg;
        }

    }

<?php

    class Mensagem_vetor_token extends Interface_mensagem
    {

        public $mVetor;

        public function __construct($pCodRetorno = null, $pMensagem = null, $pVetor = null)
        {
            parent::__construct($pCodRetorno, $pMensagem);
            $this->mVetor = $pVetor;
        }

        protected function procedimentoInicializa($objJson)
        {
            $this->mVetor = $objJson->mVetor;
        }

    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
?>

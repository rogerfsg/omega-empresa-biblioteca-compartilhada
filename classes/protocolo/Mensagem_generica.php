<?php

    class Mensagem_generica extends Interface_mensagem
    {
        public $mObj;

        public function __construct($pObjProtocolo, $pCodRetorno = null, $pMensagem = null)
        {
            parent::__construct($pCodRetorno, $pMensagem);
            $this->mObj = $pObjProtocolo;
        }

        public function json_encode()
        {
            $array = array(
                "mObj" => Helper::corrigirObjetoParaJsonEncode($this->mObj),
                "mCodRetorno" => $this->mCodRetorno,
                "mMensagem" => $this->mMensagem
            );

            return json_encode($array);
        }
    }

?>

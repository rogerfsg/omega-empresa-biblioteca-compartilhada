<?php

    abstract class Interface_mensagem
    {
        //PROTOCOLO_SISTEMA
        public $mCodRetorno = PROTOCOLO_SISTEMA::FALHA_DURANTE_CHAMADA_CURL;
        public $mMensagem = null;
        public $mStack = "";

        public function appendMsg($msg)
        {
            if ($this->mMensagem == null)
            {
                $this->mMensagem = $msg;
            }
            else
            {
                $this->mMensagem .= $msg;
            }
        }

        public function __construct($pCodRetorno = null, $pMensagem = null, $exc = null)
        {
            //NUNCA ALTERE A ORDEM
            if ($pCodRetorno != null)
            {
                $this->mCodRetorno = $pCodRetorno;
            }
            else
            {
                if ($exc != null)
                {
                    $this->mCodRetorno = PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR;
                }
                else
                {
                    $this->mCodRetorno = PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO;
                }
            }
            if ($exc != null && is_object($exc))
            {
                $msg = "Message: " . $exc->getMessage() . ". Stacktrace: " . $exc->getTraceAsString().". Complemento: $pMensagem";
                HelperLog::logErro(null, null, $msg);
                $this->mMensagem = utf8_encode($msg);
            }
            else
            {
                $this->mMensagem = utf8_encode($pMensagem);
            }

            $this->appendIdentificadorSistema();
        }

        public function appendIdentificadorSistema()
        {
            if (MODO_DE_DEPURACAO == true)
            {
                if (!empty($this->mStack))
                {
                    $this->mStack .= ",";
                }
                if (defined('IDENTIFICADOR_SISTEMA'))
                {
                    $this->mStack .= IDENTIFICADOR_SISTEMA;
                }
                else
                {
                    $this->mStack .= "ANONIMO";
                }
            }
        }

        public function getMensagemUrl()
        {
            return urlencode("[" . $this->mCodRetorno . "] - " . $this->mMensagem);
        }

        public function inicializa($objJson)
        {
            if ($objJson == null)
            {
                return;
            }
            $this->mCodRetorno = $objJson->mCodRetorno;
            $this->mMensagem = strlen($objJson->mMensagem) ? utf8_decode($objJson->mMensagem) : null;
            if (!empty($objJson->mStack))
            {
                $this->mStack = $objJson->mStack . "," . $this->mStack;
            }
            $this->procedimentoInicializa($objJson);
        }

        public static function checkErro($msg)
        {
            if (isset($msg)
                && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR
                || $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL))
            {
                return true;
            }

            return false;
        }

        public static function checkOk($msg)
        {
            if ($msg == null || $msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                return true;
            }

            return false;
        }

        public function erro()
        {
            if ($this->erroComServidor() || $this->erroConsultaSql())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function resultadoVazio()
        {
            if ($this->mCodRetorno == PROTOCOLO_SISTEMA::RESULTADO_VAZIO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function erroComServidor()
        {
            if ($this->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function algumErroDeBancoDeDados()
        {
            return $this->erroConsultaSql() || $this->erroBancoForaDoAr();
        }

        public function erroConsultaSql()
        {
            if ($this->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function erroSqlChaveDuplicada()
        {
            if ($this->erroConsultaSql())
            {
                return $this->mValor == Database::CHAVE_DUPLICADA;
            }

            return false;
        }

        public function erroSqlSemSerChaveDuplicada()
        {
            if ($this->erroConsultaSql())
            {
                return $this->mValor != Database::CHAVE_DUPLICADA;
            }

            return false;
        }

        public function erroBancoForaDoAr()
        {
            if ($this->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function ok()
        {
            if ($this->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


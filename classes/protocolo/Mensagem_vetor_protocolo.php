<?php

    class Mensagem_vetor_protocolo extends Interface_mensagem
    {

        public $mVetorObj;
        public $mObjProtocolo;

        public function __construct($pObjProtocolo, $pCodRetorno = null, $pMensagem = null, $pVetorObj = null)
        {
            parent::__construct($pCodRetorno, $pMensagem);
            $this->mVetorObj = $pVetorObj;
            $this->mObjProtocolo = $pObjProtocolo;
        }

        protected function procedimentoInicializa($objJson)
        {
            $this->mVetorObj = $objJson->mVetorObj;
            for ($i = 0; $i < count($this->mVetorObj); $i++)
            {
                $termoJson = $this->mVetorObj[ $i ];
                $this->mVetorObj[ $i ] = $this->mObjProtocolo->factory($termoJson);
            }
        }

    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
?>

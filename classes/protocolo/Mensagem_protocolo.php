<?php

    class Mensagem_protocolo extends Interface_mensagem
    {

        public $mObj;

        public function __construct($pObjProtocolo, $pCodRetorno = null, $pMensagem = null)
        {
            parent::__construct($pCodRetorno, $pMensagem);
            $this->mObj = $pObjProtocolo;
        }

        protected function procedimentoInicializa($objJson)
        {
            if ($this->mObj != null)
            {
                $this->mObj = $this->mObj->factory($objJson->mObj);
            }
        }

    }

?>

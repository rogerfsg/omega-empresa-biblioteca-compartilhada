<?php

    class WebserviceResponse
    {

        private $success;
        private $content;
        private $message;

        public function __construct($success, $content, $message = null)
        {
            $this->success = $success;
            $this->content = $content;
            $this->message = $message;
        }

        public function getJson()
        {
            $objRetorno = new stdClass();
            $objRetorno->content = $this->content;
            $objRetorno->message = $this->message;
            $objRetorno->success = $this->success;

            //faz passagem por referÍncia
            Helper::corrigirObjetoParaJsonEncode($objRetorno);

            return json_encode($objRetorno);
        }

        public function printJson()
        {
            echo $this->getJson();
        }

    }

?>
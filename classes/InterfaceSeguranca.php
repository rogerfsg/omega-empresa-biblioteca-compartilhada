<?php
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of InterfaceSeguranca
     *
     * @author home
     */
    abstract class InterfaceSeguranca
    {
        //put your code here
        public $IPsAutorizadosNaDepuracao = array();

        public static function getIPsAutorizadosNaDepuracao()
        {
            return self::$IPsAutorizadosNaDepuracao;
        }

        public static function startSession()
        {
//        session_name(IDENTIFICADOR_SESSAO);
//        session_cache_limiter(SESSAO_TIPO_LIMITER);
//        session_cache_expire(SESSAO_TEMPO_EXPIRACAO);
//        session_set_cookie_params(SESSAO_TEMPO_DE_VIDA * SEGUNDOS_EM_UM_MINUTO, EXTDAO_Configuracao::getRaizDaAplicacao());
//        session_start();
        }

        public static function getCampo($nomeCampo)
        {
            $singleton = SessionRedis::getSingleton();
            return $singleton->get($nomeCampo);
        }

        public static function getNome()
        {
            $singleton = SessionRedis::getSingleton();
            return $singleton->get("usuario_nome");
        }

        public static function getId()
        {
            //TODO-EDUARDO: Remover linha abaixo
            //return 7;

            $singleton = SessionRedis::getSingleton();
            $id = $singleton->get("usuario_id");

            return $id;
        }

        public static function getEmail()
        {
            //TODO-EDUARDO: Remover linha abaixo
            //return "teste@teste.com";

            $singleton = SessionRedis::getSingleton();
            return $singleton->get("usuario_email");
        }

        public static function getDadosParaLoginEntreSistemas()
        {
            $email = self::getEmail();
            if (empty($email))
            {
                return null;
            }

            return array("email" => getEmail());
        }

        public static function registrarOperacao($objArgumento)
        {
            $objOperacaoBanco = new EXTDAO_Operacao_sistema();
            $objOperacaoBanco->setTipo_operacao($objArgumento->operacao);
            $objOperacaoBanco->setPagina_operacao($objArgumento->pagina);
            $objOperacaoBanco->setChave_registro_operacao_INT($objArgumento->chaveDoRegistroOperado);
            $objOperacaoBanco->setData_operacao_DATETIME($objArgumento->dataHora);
            $objOperacaoBanco->setDescricao_operacao($objArgumento->descricaoOperacao);
            $objOperacaoBanco->setEntidade_operacao($objArgumento->entidadeOperacao);
            $objOperacaoBanco->setUsuario_id_INT($objArgumento->usuarioOperacao);
            $objOperacaoBanco->setUrl_completa(Helper::getNomeDoArquivoEmPathCompleto($objArgumento->urlOperacao));
            $objOperacaoBanco->formatarParaSQL();
            $objOperacaoBanco->insert();
        }

        public static function getSistemaOperacionalDoServidor()
        {
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
            {
                return WINDOWS;
            }
            else
            {
                return LINUX;
            }
        }

        public static function getUsuarioTipo()
        {
            $tipo = Helper::getSession("usuario_tipo_visivel");
            if ($tipo != null)
            {
                return $tipo;
            }
            else
            {
                return Helper::getSession("usuario_tipo");
            }
        }

        public function getParametrosLogin()
        {
            return array("email" => Helper::POST("txtLogin"));
        }

        public function logout()
        {
            $singleton = SessionRedis::getSingleton();
            $singleton->logout();
            Helper::clearCookie("txtEmail");
        }

        public function __isAutenticado()
        {
            $singleton = SessionRedis::getSingleton();
            $estado = $singleton->getEstadoAutenticacao();
            if ($estado == SessionRedis::NAO_HOUVE_TENTATIVA_DE_AUTENTICACAO
                || $estado == SessionRedis::NAO_AUTENTICADO
            )
            {
                if ($singleton->loadSession(false) == SessionRedis::LOGIN_REALIZADO)
                {
                    return true;
                }
                else
                {
                    return $this->autoLogin();
                }
            }
            if ($singleton->getEstadoAutenticacao() == SessionRedis::AUTENTICADO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function autoLogin()
        {
            HelperLog::logSeguranca("autoLogin");
            $dados = $this->getUsuarioDadosCookie();
            $desc = "anonimo";
            if($dados != null)
                $desc = $dados["usuario_email"];
            HelperLog::logSeguranca("autoLogin - getUsuarioDadosCookie: " . $desc );
            if ($dados != null)
            {
                $db = new Database();
                $senha = $this->getSenhaDecrypt($dados, $db);
                if ($senha != null)
                {
                    HelperLog::logSeguranca("autoLogin - login... ");
                    $msg = $this->login($dados, $senha, $db, true);
                    HelperLog::logSeguranca("autoLogin - login: " . print_r($msg, true));
                    if ($msg == null || $msg->ok())
                    {
                        return true;
                    }
                }
                else
                {
                    HelperLog::logSeguranca("autoLogin - falha ao recuperar senha ");
                }
                Helper::clearCookie(ConstanteSeguranca::CHAVE_AUTO_LOGIN);
            }

            return false;
        }

        private function getUsuarioDadosCookie()
        {
            $autoLoginCrypt = Helper::COOKIEPOSTGET(ConstanteSeguranca::CHAVE_AUTO_LOGIN);

            if (!empty($autoLoginCrypt))
            {
                $crypt = Registry::get('Crypt');
                $ret = $crypt->decryptGet($autoLoginCrypt);

                if ($ret == null || $ret == Crypt::CHAVE_EXPIRADA)
                {
                    Helper::clearCookie(ConstanteSeguranca::CHAVE_AUTO_LOGIN);

                    return false;
                }
                $dados = unserialize($ret['hash']['usuario_dados']);

                return $dados;
            }

            return null;
        }

        public function login(
            $dados,
            $senha,
            $db = null,
            $registrarAcesso = false)
        {
            try
            {
                if ($db == null)
                {
                    $db = new Database();
                }
                $msg = $this->verificaSenha(
                    $dados,
                    $senha,
                    $db,
                    $registrarAcesso);
                if ($msg != null && $msg->erro())
                {
                    HelperLog::logSeguranca("Verifica senha n�o ok - " . print_r($msg, true));

                    return $msg;
                }

                if ($msg != null && $msg->ok() && $registrarAcesso)
                {
//                   HelperLog::logInfo(print_r($msg, true));
                    $objUsuario = $msg->mObj;
                    HelperLog::logSeguranca("Login realizado com sucesso {$objUsuario["usuario_email"]}, registrando acesso...");
                    $singleton = SessionRedis::getSingleton();
                    //aqui dentro ocorre a setagem do cookie com a chave de seguranca
                    $singleton->login(
                        $objUsuario,
                        $objUsuario["usuario_email"]);
                    Helper::setCookie("txtLogin", $objUsuario["usuario_email"]);
                    $this->setLinkAutoLogin($dados);
                    //gerando o cookie responsavel pela verificacao de Logout X Expiracao X Primeiro Acesso
                    $this->setCookieEmailTelaLogin($objUsuario["usuario_email"]);
                    //TODO validar se a linha de cima ou a de baixo estao corretas
                    //$this->setCookieEmailTelaLogin($dados);
                    $this->gravarLogin($objUsuario["usuario_id"], $db);
                    $this->onRegistraAcesso($dados);

                    return $msg;
                }
                else
                {
                    HelperLog::logSeguranca(print_r($msg, true));
                }

                return $msg;
            }
            catch (Exception $exc)
            {
                return new Mensagem(null, null, $exc);
            }
        }

        public function setLinkAutoLogin($dados)
        {
            $crypt = Registry::get('Crypt');
            $strDados = serialize($dados);
            $str = $crypt->cryptHashGet(array("usuario_dados" => $strDados), ConstanteSeguranca::TEMPO_LIMITE_SESSAO_MINUTOS);
            Helper::setCookie(
                ConstanteSeguranca::CHAVE_AUTO_LOGIN,
                $str,
                time() + ConstanteSeguranca::TEMPO_LIMITE_AUTOLOGIN_MINUTOS);
        }

        public function setCookieEmailTelaLogin($email)
        {
            Helper::setCookie(
                "txtEmail",
                $email,
                time() + ConstanteSeguranca::TEMPO_LIMITE_AUTOLOGIN_MINUTOS);
        }

        public function verificarSeASenhaEAPadrao()
        {
            $senha = $this->getSenhaDoUsuarioLogado();
            if ($senha == SENHA_PADRAO_USUARIOS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function getSenhaDoUsuarioLogado($db = null)
        {
            //TODO-EDUARDO: Remover linha abaixo
            //return "123456";

            return $this->getSenhaDecrypt($this->getUsuarioDadosCookie(), $db);
        }

        public abstract function gravarLogin($id, $db);

        public abstract function onRegistraAcesso($dados);

        public abstract function existeUsuarioDoEmail($email, $db = null);

        public abstract function __actionLogin($dados, $senha, $db = null);

        public abstract function __actionLogout($mensagemUsuario = null);

        public abstract function getLabel();

        public abstract function isPaginaRestrita($pagina);

        public abstract function isAcaoRestrita($classe, $acao);

        public abstract function factory();

        public abstract function verificarPermissaoEmAcoesDoUsuarioCorrente(
            $classe, $metodo);

        protected abstract function getSenhaDecrypt($dados, $db);

        public abstract function verificaSenha(
            $dados,
            $senha,
            $db = null,
            $registrarAcesso = false);

    }

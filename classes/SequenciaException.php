<?php

    class SequenciaException extends Exception
    {

        const THROW_FALHA_UPDATE_SEQUENCIA = 0;
        const THROW_FALHA_SELECT_SEQUENCIA = 1;
        const THROW_FALHA_INSERT_SEQUENCIA = 2;
        const THROW_FALHA_OBTER_LOCK = 3;
        const THROW_FALHA_SELECT_TABELA = 4;

        // Redefine a exce��o de forma que a mensagem e c�digo n�o sejam opcionais
        public function __construct($message, $code, Exception $previous = null)
        {
            parent::__construct($message, $code, $previous);
        }

        public function __toString()
        {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }

    }

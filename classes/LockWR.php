<?php
    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 19/08/2017
     * Time: 14:23
     */

    class LockWR
    {

        public $helperRedisLocks;
        public $idProcesso;
        public $totalReaders = 0;
        public $tempoMaximoLeituraSegundos = 0;
        public $tempoMaximoEscritaSegundos = 0;

        public function __construct(
            $nome,
            $idProcesso,
            $totalReaders,
            $tempoMaximoLeituraSegundos)
        {
            $this->helperRedisLocks = new HelperRedis($nome);
            $this->idProcesso = $idProcesso;
            $this->totalReaders = $totalReaders;

            $this->tempoMaximoLeituraSegundos = $tempoMaximoLeituraSegundos;
            //tem que ser muito superior, pois tem que aguardar a leitura terminar para s� entao come�ar seu processo
            $this->tempoMaximoEscritaSegundos = $tempoMaximoLeituraSegundos * 3;
        }

        public function getTotalReaders($idCorporacao)
        {
            $r = 0;
            for ($i = 0; $i < $this->totalReaders; $i++)
            {
                if ($this->helperRedisLocks->exists("R_{$idCorporacao}_{$i}"))
                {
                    $r++;
                }
            }

            return $r;
        }

        public function isWriting($idCorporacao)
        {
            return $this->helperRedisLocks->exists("W_{$idCorporacao}");
        }

        public function tryRead($idCorporacao)
        {
            if ($this->isWriting($idCorporacao))
            {
                return false;
            }
            else
            {
                if ($this->helperRedisLocks->exists("R_{$idCorporacao}_{$this->idProcesso}"))
                {
                    return false;
                }
                else
                {
                    $this->helperRedisLocks->set(
                        "R_{$idCorporacao}_{$this->idProcesso}",
                        true,
                        "ex",
                        $this->tempoMaximoLeituraSegundos);

                    return true;
                }
            }
        }

        public function closeReader($idCorporacao)
        {
            $this->helperRedisLocks->del("R_{$idCorporacao}_{$this->idProcesso}");
        }

        public function closeWriter($idCorporacao)
        {
            $this->helperRedisLocks->del("W_{$idCorporacao}");
        }

        const GOT_LOCK_WRITE_BUT_EXIST_READERS = 1;
        const CANNOT_TAKE_LOCK_WRITE = 2;
        const GOT_LOCK_WRITE = 3;

        public function tryWrite($idCorporacao)
        {
            if ($this->helperRedisLocks->exists("W_{$idCorporacao}"))
            {
                return LockWR::CANNOT_TAKE_LOCK_WRITE;
            }

            $this->renewLockWrite($idCorporacao);

            for ($i = 1; $i <= $this->totalReaders; $i++)
            {
                if ($this->helperRedisLocks->exists("R_{$idCorporacao}_{$i}"))
                {
                    return LockWR::GOT_LOCK_WRITE_BUT_EXIST_READERS;
                }
            }

            return LockWR::GOT_LOCK_WRITE;
        }

        public function waitReaders($idCorporacao)
        {
            for ($i = 1; $i <= $this->totalReaders; $i++)
            {
                if ($this->helperRedisLocks->exists("R_{$idCorporacao}_{$i}"))
                {
                    //renova a chave
                    $this->renewLockWrite($idCorporacao);

                    return false;
                }
            }
            //renova a chave
            $this->renewLockWrite($idCorporacao);

            return true;
        }

        private function renewLockWrite($idCorporacao)
        {
            //renova a chave
            $this->helperRedisLocks->set("W_{$idCorporacao}",
                                         1,
                                         "ex",
                                         $this->tempoMaximoEscritaSegundos);
        }
    }
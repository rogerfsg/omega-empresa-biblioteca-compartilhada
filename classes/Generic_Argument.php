<?php

    class Generic_Argument
    {

        public $nome;
        public $label;
        public $labelTrue;       //Campos BOOLEAN
        public $labelFalse;      //Campos BOOLEAN
        public $id;
        public $valor;
        public $readOnly;
        public $disabled;
        public $textArea;
        public $lowerCase;
        public $minLength;
        public $campoSenha;
        public $email;
        public $funcaoOnKeyUp;
        public $dataRuledRequired;
        public $formato;
        public $upperCase;
        public $maxLength;
        public $obrigatorio;
        public $classeCss;
        public $placeHolder;
        public $classeCssFocus;
        public $largura;
        public $altura;
        public $calendario;
        public $tipoCalendario;
        public $idOutroCalendario;
        public $strFiltro;         //Campos ComboBox
        public $numeroDoRegistro;
        public $query1;
        public $arrQueriesFilhos;
        public $onChange;
        public $onFocus;
        public $onBlur;
        public $primeiroEmBranco;
        public $valueReplaceId;
        public $funcaoGetOptions;
        public $style;
        public $valorMinimo;
        public $valorMaximo;
        public $aceitarValorNegativo;
        public $isChecked;
        public $aceitarDatasComZeros;
        public $prefixoMoeda;
        public $carregarFilhosAutomaticamente;
        public $isDisabled;
        public $mensagemValidacao;

        public function __construct()
        {
            //$this->largura = 120;
            //$this->altura = 330;

            $this->obrigatorio = false;
            $this->classeCss = "input_text";
            $this->classeCssFocus = "focus_text";
            $this->disabled = false;
            $this->readOnly = false;
            $this->maxLength = 255;
            $this->calendario = false;
            $this->upperCase = false;
            $this->tipoCalendario = "individual";

            $this->onChange = false;
            $this->primeiroEmBranco = true;
            $this->valueReplaceId = false;
            $this->funcaoGetOptions = "";
            $this->style = "";
            $this->valorMinimo = false;
            $this->valorMaximo = false;
            $this->aceitarValorNegativo = false;
            $this->isChecked = false;
            $this->aceitarDatasComZeros = false;
            $this->carregarFilhosAutomaticamente = false;
        }

        public function getLabel()
        {
            $strRetorno = $this->label;

            if ($this->obrigatorio)
            {
                $strRetorno = "<span class=\"obrigatorio\">*</span> " . $strRetorno;
            }

            return $strRetorno . ":";
        }

    }


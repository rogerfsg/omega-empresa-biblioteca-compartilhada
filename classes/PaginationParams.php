<?php

    class PaginationParams
    {
        public $recordsPerPage;
        public $currentPage;
        public $lastPage;

        public function __construct($recordsPerPage, $currentPage, $lastPage)
        {
            $this->recordsPerPage = $recordsPerPage;
            $this->currentPage = $currentPage;
            $this->lastPage = $lastPage;
        }

    }

?>
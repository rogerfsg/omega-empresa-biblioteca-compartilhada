<?php

class Database
{// Classe : in�cio

    private $host;
    private $DBName;
    private $usuario;
    private $senha;

    public $result;
    public $array;
    public $object;
    public $indiceLink;

    public static $arrLinks = array();
    public static $arrDBNames = array();
    public static $arrGlobal = array();

    static private $cacheDatabaseTabelaPorAtributos = null;
    static public $cacheDatabaseIsSistemaTabelaPorAtributosFk = null;
    static public $cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados = null;

    const CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE = '-24';
    const CHAVE_DUPLICADA = '1062';
    const CHAVE_FK_INEXISTENTE = '1452';
    const CHAVE_INEXISTENTE = '-22';
    const NAO_IDENTIFICADO = -1;
    const ERRO_EXECUCAO = -2;
    const ERRO_FALTA_DADOS_HISTORICO_SINCRONIZACAO = -3;

    const OPCAO_NAO_BUSCAR_ID = 1234;
    const OPCAO_BUSCAR_ID = 1;
    const OPCAO_GERAR_ID = 2;
    const OPCAO_LANCAR_EXCECAO = 65487;
    const OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR = 6548865;

    const TAMANHO_MAXIMO_TINYTEXT = 255;
    const TAMANHO_MAXIMO_TEXT = 65535;
    const TAMANHO_MAXIMO_MEDIUM_TEXT = 16777215;
    const TAMANHO_MAXIMO_LONGTEXT = 4294967295;

    const TIPO_CONFIG_REGISTRY = 1;
    const TIPO_CONFIG_CONSTANT = 2;

    public function __construct(
        $databaseName = false,
        $host = false,
        $porta = false,
        $usuario = false,
        $senha = false,
        $novaConexao = false)
    {// M�todo : in�cio - Construtor
        try {
            $this->indiceLink = -1;
            if ($databaseName == false || is_string($databaseName)) {
                $tipo = null;

                $configDatabasePrincipal = Registry::get('ConfiguracaoDatabasePrincipal', false);
                if ($configDatabasePrincipal != null) {
                    $tipo = Database::TIPO_CONFIG_REGISTRY;
                } else {
                    if (defined('NOME_BANCO_DE_DADOS_PRINCIPAL')) {
                        $tipo = Database::TIPO_CONFIG_CONSTANT;
                    }
                }

                if ($databaseName == false || $databaseName == null) {
                    if ($tipo == Database::TIPO_CONFIG_REGISTRY) {
                        $this->DBName = $configDatabasePrincipal->DBName;
                    } else {
                        if ($tipo == Database::TIPO_CONFIG_CONSTANT) {
                            $this->DBName = NOME_BANCO_DE_DADOS_PRINCIPAL;
                        }
                    }
                } else {
                    $this->DBName = $databaseName;
                }

                if ($host === false) {
                    if ($tipo == Database::TIPO_CONFIG_REGISTRY) {
                        $this->host = $configDatabasePrincipal->host;
                    } else {
                        if ($tipo == Database::TIPO_CONFIG_CONSTANT) {
                            $this->host = BANCO_DE_DADOS_HOST;
                        }
                    }
                } else {
                    $this->host = $host;
                }

                if ($porta === false) {
                    if ($tipo == Database::TIPO_CONFIG_REGISTRY) {
                        $this->porta = $configDatabasePrincipal->porta;
                    } else {
                        if ($tipo == Database::TIPO_CONFIG_CONSTANT) {
                            $this->porta = BANCO_DE_DADOS_PORTA;
                        }
                    }
                } else {
                    $this->porta = $porta;
                }

                if ($usuario === false) {
                    if ($tipo == Database::TIPO_CONFIG_REGISTRY) {
                        $this->usuario = $configDatabasePrincipal->usuario;
                    } else {
                        if ($tipo == Database::TIPO_CONFIG_CONSTANT) {
                            $this->usuario = BANCO_DE_DADOS_USUARIO;
                        }
                    }
                } else {
                    $this->usuario = $usuario;
                }

                if ($senha === false) {
                    if ($tipo == Database::TIPO_CONFIG_REGISTRY) {
                        $this->senha = $configDatabasePrincipal->senha;
                    } else {
                        if ($tipo == Database::TIPO_CONFIG_CONSTANT) {
                            $this->senha = BANCO_DE_DADOS_SENHA;
                        }
                    }
                } else {
                    $this->senha = $senha;
                }

                HelperLog::logDatabase("Nova instancia [A]");
                $this->openLink($novaConexao);
            } else {
                if (is_object($databaseName)) {
                    $this->host = $databaseName->host;
                    $this->porta = $databaseName->porta;
                    $this->usuario = $databaseName->usuario;
                    $this->senha = $databaseName->senha;
                    $this->DBName = $databaseName->DBName;

                    HelperLog::logDatabase("Nova instancia [B]");
                    $this->openLink($databaseName->novaConexao);
                } else {
                    if (is_array($databaseName)) {
                        $this->host = $databaseName["host"];
                        $this->porta = $databaseName["porta"];
                        $this->usuario = $databaseName["usuario"];
                        $this->senha = $databaseName["senha"];
                        $this->DBName = $databaseName["DBName"];

                        HelperLog::logDatabase("Nova instancia [C]");
                        $this->openLink($databaseName["novaConexao"]);
                    }
                }
            }
        } catch (Exception $exception) {

            HelperLog::logDatabase("Falha ao conectar no banco: " . $this->getIdentificadorCompleto());
            throw $exception;
        }

    }

// M�todo : Fim - Construtor

    public function iniciarTransacao()
    {
        mysqli_autocommit(self::$arrLinks[$this->indiceLink], false);
        mysqli_query(self::$arrLinks[$this->indiceLink], "START TRANSACTION;");
    }

    public function commitTransacao()
    {
        mysqli_query(self::$arrLinks[$this->indiceLink], "COMMIT;");
    }

    public function rollbackTransacao()
    {
        mysqli_query(self::$arrLinks[$this->indiceLink], "ROLLBACK;");
    }

    public function getUser()
    {
        return $this->usuario;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getDBName()
    {
        return $this->DBName;
    }

    public function __destruct()
    {
    }

    public static function closeAll()
    {
        try {
            foreach (self::$arrLinks as $key => $link) {
                if ($link != null) {
                    mysqli_close($link);
                }
                unset(static::$arrDBNames[$key]);
                unset(static::$arrLinks[$key]);
            }
            Registry::remove('Database');
        } catch (Exception $exc) {
            $h = new HelperLog();
            $h->gravarLogEmCasoErro(new Mensagem(null, null, $exc));
        }
    }

    public function close()
    {
        $this->closeResult();
        if ($this->indiceLink != null) {
            $key = $this->indiceLink;
            $link = static::$arrLinks[$key];
            if ($link != null) {
                mysqli_close($link);
            }
            unset(static::$arrDBNames[$key]);
            unset(static::$arrLinks[$key]);
        }
    }

// M�todo : fim

    public function closeResult()
    {
        if (isset($this->result) && is_object($this->result)) {
            $this->result->close();

            unset($this->result);
        }
        unset($this->object);
        unset($this->array);
    }

    public function getIdentificador()
    {
        return $this->getHost() . "::" . $this->getDBName();
    }

    public function getIdentificadorCompleto()
    {

        return $this->getHost() . "::" . $this->getDBName() . " - " ;
    }
//    public function throwExceptionQuery(){
//        $msgErro = "Consulta invalida!"
//            . " Banco".$this->getIdentificador()
//            . ". Query: " .$query 
//            .". CodErro: $codErro" 
//            .". Erro: $erro" ;
//        throw new QueryException($msgErro, $codErro);
//    }

    public function query($query)
    {
        // M�todo : in�cio

        HelperLog::logDatabase($query);
        try {
            $this->result = mysqli_query(self::$arrLinks[$this->indiceLink], $query);
        } catch (Exception $ex) {
            HelperLog::logDatabase("Ex 1: " . Helper::getDescricaoException($ex));
            throw new DatabaseException(Helper::getDescricaoException($ex));
        }

        if (!$this->result) {
            $codErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);
            $erro = mysqli_error(self::$arrLinks[$this->indiceLink]);
            if (!$this->isLinkInitialized()) {
                $msgErro = "Link com o banco n?o est? inicializado!"
                    . " Banco" . $this->getIdentificador()
                    . ". CodErro: $codErro"
                    . ". Erro: $erro"
                    . ". Connection: {$this->getDescricaoLink()} "
                    . ". Query: " . $query;
                $exq = new DatabaseException($msgErro, $codErro);
                HelperLog::logDatabase("Ex 4: " . Helper::getDescricaoException($exq));
                throw $exq;
            }
            if (!$this->isErroRelativoABancoForaDoAr($codErro)) {
                $msgErro = "Consulta invalida! "
                    . " Banco" . $this->getIdentificador()
                    . ". CodErro: $codErro"
                    . ". Erro: $erro"
                    . ". Connection: {$this->getDescricaoLink()} "
                    . ". Query: " . $query;
                $exq = new QueryException($msgErro, $codErro);
                HelperLog::logDatabase("Ex 2: " . Helper::getDescricaoException($exq));
                throw $exq;
            } else {
                $msgErro = "Erro Banco Fora do Ar!"
                    . " Banco" . $this->getIdentificador()
                    . ". CodErro: $codErro"
                    . ". Erro: $erro"
                    . ". Connection: {$this->getDescricaoLink()} "
                    . ". Query: " . $query;
                $exq = new DatabaseException($msgErro, $codErro);
                HelperLog::logDatabase("Ex 3: " . Helper::getDescricaoException($exq));
                throw $exq;
            }
        } else {
            return $this->result;
        }
    }

    public function isLinkInitialized()
    {
        if ($this->indiceLink < 0) {
            return false;
        } else {
            if (!isset(self::$arrLinks[$this->indiceLink])) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function getDescricaoLink()
    {
        if ($this->indiceLink < 0) {
            return null;
        } else {
            if (!isset(self::$arrLinks[$this->indiceLink])) {
                return null;
            } else {
                return "[{$this->indiceLink}] - {$this->DBName}";
            }
        }
    }

    public function voltarPonteiroDoResultSet()
    {
        return mysqli_data_seek($this->result, 0);
    }

    public function fetchArray($constante = MYSQLI_BOTH)
    {
        $this->array = mysqli_fetch_array($this->result, $constante);

        return $this->array;
    }

    public function fetchObject()
    {
        $this->object = mysqli_fetch_object($this->result);

        return $this->object;
    }

    public function getPrimeiraTuplaDoResultSet($aliasOuIndice)
    {
        return self::mysqli_result($this->result, 0, $aliasOuIndice);
    }

    public function getResultSet()
    {
        return $this->result;
    }

    public function resultSet($linha, $coluna)
    {
        return Database::mysqli_result($this->result, $linha, $coluna);
    }

    public static function mysqli_result($resultSet, $linha, $coluna)
    {
        if (!is_bool($resultSet) && mysqli_data_seek($resultSet, $linha))
        {
            $linha = $resultSet->fetch_array(MYSQLI_BOTH);
            return $linha[$coluna];
        }

        return null;
    }

    public function isErroRelativoABancoForaDoAr($codErro)
    {
        if ($codErro == 1053
            || $codErro == 1076 || $codErro == 1078 || $codErro == 1079
            || $codErro == 1080 || $codErro == 1081 || $codErro == 1105
            || $codErro == 1114 || $codErro == 1126 || $codErro == 1129
            || $codErro == 1135 || $codErro == 1137 || $codErro == 1151
            || $codErro == 1152 || $codErro == 1158 || $codErro == 1159
            || $codErro == 1160 || $codErro == 1161 || $codErro == 1165
            || $codErro == 1184 || $codErro == 1197 || $codErro == 1198
            || $codErro == 1205 || $codErro == 1206 || $codErro == 1213
            || $codErro == 1218 || $codErro == 2006
        ) {
            return true;
        }

        return false;
    }

    public function queryMensagemThrowException($query)
    {
        return $this->queryMensagem($query, Database::OPCAO_LANCAR_EXCECAO);
    }

    public function queryMensagem($query, $opcao = null)
    {
        $msg = null;
        $exc = null;
        $erro = null;
        $codErro = null;

        try {
            HelperLog::logDatabase($query);
            $this->result = mysqli_query(self::$arrLinks[$this->indiceLink], $query);
            if (!$this->result) {
                $codErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);
                $erro = mysqli_error(self::$arrLinks[$this->indiceLink]);
            } else {
                return null;
            }
        } catch (Exception $ex) {
            $exc = $ex;
        }

        if ($codErro != null) {
            $log = Registry::get('HelperLog');
            if ($this->isErroRelativoABancoForaDoAr($codErro)) {
                $msg = "Banco fora do ar! Banco: {$this->getIdentificador()}. CodErroMySql[$codErro]: $erro. Query: $query";
                if ($opcao != null
                    &&
                    ($opcao == Database::OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR
                        || $opcao == Database::OPCAO_LANCAR_EXCECAO)
                ) {
                    $ex = new DatabaseException($msg, $codErro);

                    if ($log != null) {
                        $log->gravarLogEmCasoErro(new Mensagem(null, null, $ex));
                    }
                    throw $ex;
                } else {
                    $msg = new Mensagem_token (
                        PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,
                        $msg,
                        $codErro);

                    if ($log != null) {
                        $log->gravarLogEmCasoErro($msg);
                    }
                }
            } else {
                $msg = "Consulta invalida! Banco: {$this->getIdentificador()}."
                    . " CodErroMySql[$codErro]: $erro. Query: $query. Stack: "
                    . Helper::getDebugStackTrace();
                if ($opcao != null
                    && $opcao == Database::OPCAO_LANCAR_EXCECAO
                ) {
                    $ex = new QueryException($msg, $codErro);
                    if ($log != null) {
                        $log->gravarLogEmCasoErro(new Mensagem(null, null, $ex));
                    }
                    throw $ex;
                } else {
                    $msg = new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL,
                        $msg,
                        $codErro);

                    if ($log != null) {
                        $log->gravarLogEmCasoErro($msg);
                    }
                }
            }
        } else {
            if ($exc != null) {
                $msg = new Mensagem(
                    PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,
                    "Excecao capturada durante a execucao da query. " . $query,
                    $exc);
                $log = Registry::get('HelperLog');
                if ($log != null) {
                    $log->gravarLogEmCasoErro($msg);
                }

                if ($opcao != null
                    && ($opcao == Database::OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR
                        || $opcao == Database::OPCAO_LANCAR_EXCECAO)
                ) {
                    throw new DatabaseException ($msg->mMensagem);
                }
            }
        }

        return $msg;
    }

    public function rows()
    {
        return mysqli_num_rows($this->result);
    }

    public function selectLastId()
    {
        $sql = "SELECT last_insert_id()";
        $con = mysqli_query(self::$arrLinks[$this->indiceLink], $sql);

        if (!$con) {
            //$this->rollbackTransacao();
            $codErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);
            $erro = mysqli_error(self::$arrLinks[$this->indiceLink]);
            $msg = new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL,
                "Erro[$codErro]: $erro. Query: $sql");

            return $msg;
        } else {
            $rs = mysqli_fetch_row($con);
            $con->close();
            $msg = new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                null,
                $rs[0]);

            return $msg;
        }
    }

    public function getLastInsertId()
    {
        $sql = "SELECT last_insert_id()";
        $con = mysqli_query(self::$arrLinks[$this->indiceLink], $sql);

        if (!$con) {
            //$this->rollbackTransacao();
            $codErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);
            $erro = mysqli_error(self::$arrLinks[$this->indiceLink]);

            throw new DatabaseException("Erro[$codErro]: $erro. Query: $sql");
        } else {
            $rs = mysqli_fetch_row($con);
            $con->close();

            return $rs[0];
        }
    }

    private function openLink($novaConexao = false)
    {// M?do : in?o
        $indexConexaoGlobal = -1;
        if (!$novaConexao) {
            $indexConexaoGlobal = $this->getIndexConexaoGlobal($this->DBName);
        }
        if ($novaConexao
            || $indexConexaoGlobal == -1
        ) {
            $this->connect($novaConexao);
        } else {
            $this->indiceLink = $indexConexaoGlobal;

            if (!isset(self::$arrLinks[$this->indiceLink])
                || (self::$arrLinks[$this->indiceLink]
                    && mysqli_errno(self::$arrLinks[$this->indiceLink]))
            ) {
                HelperLog::logDatabase("Link expirou [{$this->indiceLink}] - {$this->DBName} ", true);
                unset(self::$arrLinks[$this->indiceLink]);
                unset(self::$arrGlobal[$this->indiceLink]);
                unset(self::$arrDBNames[$this->indiceLink]);
                $this->indiceLink = -1;
                $this->connect($novaConexao);
            } else {
                HelperLog::logDatabase("Link reutilizado com sucesso! [{$this->indiceLink}] - {$this->DBName}");
            }
        }

        return null;
    }

    static $__sequenceId = 1;

    private function connect($novaConexao = false)
    {
        $indiceLink = Database::$__sequenceId++;
        $this->indiceLink = $indiceLink;

        self::$arrLinks[$this->indiceLink] = @mysqli_connect($this->host, $this->usuario, $this->senha, $this->DBName, $this->porta);

        if (mysqli_connect_errno()) {
            unset(self::$arrLinks[$this->indiceLink]);
            $this->indiceLink = -1;
            $msgErro = "CONEX�O COM O BANCO DE DADOS N�O PODE SER ESTABELECIDA"
                . ". Identificador " . $this->getIdentificadorCompleto()
                . ". Numero do Erro: " . mysqli_connect_errno()
                . ". Descri??o: " . mysqli_connect_error();
            HelperLog::logDatabase($msgErro);
            throw new DatabaseException($msgErro, mysqli_connect_errno());
        } else {
            HelperLog::logDatabase("Link realizado com sucesso! [{$this->indiceLink}] - {$this->DBName}");
        }


        if (!mysqli_select_db(self::$arrLinks[$this->indiceLink], $this->DBName)) {
            unset(self::$arrLinks[$this->indiceLink]);
            $this->indiceLink = -1;
            $msgErro = "[3] CONEX?O COM O BANCO DE DADOS N�O PODE SER ESTABELECIDA - mysqli_select_db."
                . ". Numero do Erro: " . mysqli_connect_errno()
                . ". Descri??o: " . mysqli_connect_error();
            HelperLog::logDatabase($msgErro);
            throw new DatabaseException($msgErro, mysqli_connect_errno());
        } else {
            HelperLog::logDatabase("Database {$this->DBName} selected! ");
            mysqli_set_charset(self::$arrLinks[$this->indiceLink], "utf8");
        }
        self::$arrDBNames[$this->indiceLink] = $this->DBName;
        self::$arrGlobal[$this->indiceLink] = !$novaConexao;
    }

    private function getIndexConexaoGlobal($dbname)
    {
        foreach (self::$arrDBNames as $key => $value) {
            if (self::$arrDBNames[$key] == $dbname && self::$arrGlobal[$key]) {
                return $key;
            }
        }

        return -1;
    }

    public function isOpen()
    {
        if ($this->indiceLink == -1) {
            return false;
        } else {
            return true;
        }
    }

    //TODO criar rotina para somar a quantidade ocupada
    public function getTamanhoBancoOcupadoMB($idCorporacao)
    {
        $q = "SELECT Round(Sum(data_length + index_length) / 1024 / 1024, 1) size 
                FROM   information_schema.tables
                WHERE table_schema = '" . $this->getDBName() . "'
                GROUP  BY table_schema";
        $this->query($q);

        return $this->getPrimeiraTuplaDoResultSet(0);
    }

    public function getListaAtributoDaTabela($p_strTableName)
    {
        if (strlen($p_strTableName)) {
            $this->query("SHOW COLUMNS FROM " . $p_strTableName . ";");

            return Helper::getResultSetToArrayDeUmCampo($this->result);
        }
    }

    public function existeAtributo($tabela, $atributo)
    {
        $q = "show columns from $tabela where field = '$atributo'";
        $this->query($q);

        return $this->rows() > 0;
    }

    public function getListaDosNomesDasTabelas()
    {   // Pega todo pedido no status requisitado, seje relativo a um produto individual, ou a um combo, ou promocao.
        $this->query("SHOW TABLES");
        $v_listaNomeTabela = Helper::getResultSetToArrayDeUmCampo($this->result);
        $v_arrayRetorno = array();
        $v_index = 0;
        foreach ($v_listaNomeTabela as $v_strTabela) {
            $v_arrayRetorno[$v_index++] = trim(strtolower($v_strTabela));
        }

        return $v_arrayRetorno;
    }

    public function getConfiguracaoDatabase()
    {
        $config = new ConfiguracaoDatabase();
        $config->DBName = $this->DBName;
        $config->host = $this->host;
        $config->porta = $this->porta;
        $config->usuario = $this->usuario;
        $config->senha = $this->senha;

        return $config;
    }

    public function getJsonConfiguracaoDatabase()
    {
        $config = $this->getConfiguracaoDatabase();
        if ($config != null) {
            return json_encode($config);
        }

        return null;
    }

    public function getAtributosDaTabela($tabela)
    {
        if (strlen($tabela)) {
            if (Database::$cacheDatabaseTabelaPorAtributos == null) {
                Database::$cacheDatabaseTabelaPorAtributos = array();
            }

            if (!isset(Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()])) {
                Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()] = array();
            } else {
                if (isset(Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()][$tabela])) {
                    return Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()][$tabela];
                }
            }
            $q = "SHOW COLUMNS FROM " . $tabela;
            $this->queryMensagem($q, Database::OPCAO_LANCAR_EXCECAO);
            Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()][$tabela] = Helper::getResultSetToArrayDeUmCampo($this->result);

            return Database::$cacheDatabaseTabelaPorAtributos[$this->getDBName()][$tabela];
        }

        return null;
    }

    public function dumpSql($pathCompleto)
    {
        $user = $this->getUser();
        $senha = $this->getSenha();
        $nome = $this->getDBName();
        $host = $this->getHost();

        $resp = "mysqldump --host={$host} --user={$user} --password={$senha} --databases {$nome} --skip-set-charset --skip-comments --compact > $pathCompleto";

        $output = Helper::shellExec($resp);

        return $output;
    }

    public function criaBancoDeDados($nomeDoBanco)
    {
        if ($this->indiceLink == -1) {
            $this->openLink();
            $this->selectDB();
        }
        $sql = 'CREATE DATABASE  IF NOT EXISTS ' . $nomeDoBanco . ' CHARACTER SET latin1 COLLATE latin1_swedish_ci ';
        $cmd = $this->getComandoMySql();

        HelperLog::logDatabase("criaBancoDeDados: $cmd");
        if ($cmd == null) {
            return false;
        }

        $cmd .= ' -e "' . $sql . '"';
        $codRetorno = null;
        $vetor = array();

        Helper::exec($cmd, $vetor, $codRetorno);

        if ($codRetorno == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getComandoMySqlDump($pathScript)
    {
        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS) {
            $path = Helper::getPathComBarraDiretorio(PATH_MYSQL);
            $cmd = $path . 'mysqldump.exe ';
        } else {
            if (Helper::getSistemaOperacionalDoServidor() == LINUX) {
                $cmd = 'mysqldump ';
            } else {
                return null;
            }
        }

        $cmd .= $this->getStringConexao() . " --default-character-set=utf8 > $pathScript";

        return $cmd;
    }

    public function getComandoMySql($prefixo = '')
    {
        $cmd = "";

        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS) {
            $path = Helper::getPathComBarraDiretorio(PATH_MYSQL);
            $cmd = $path . 'mysql.exe ' . $prefixo . ' ';
        } else {
            if (Helper::getSistemaOperacionalDoServidor() == LINUX) {
                $cmd = 'mysql ' . $prefixo . ' ';
            } else {
                return null;
            }
        }

        $cmd .= $this->getStringConexao() . "  --default-character-set=utf8 ";

        return $cmd;
    }

    public function getStringConexao()
    {
        $cmd = '';
        $cmd .= '-h ' . $this->host . ' -u ' . $this->usuario . ' ';
        if (strlen($this->senha)) {
            $cmd .= " -p" . $this->senha;
        }
        if (strlen($this->porta)) {
            $cmd .= " -P " . $this->porta;
        }
        $cmd .= ' ';

        return $cmd;
    }

    public function executaScriptNoBancoDeDados($pathArquivoDoDump)
    {
        $cmd = "";
        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS) {
            $cmd .= $this->getComandoMySql();
        } else {
            $cmd .= $this->getComandoMySql();
        }

        $cmd .= $this->DBName . ' < ' . $pathArquivoDoDump;
        $codRetorno = null;
        $vetor = array();

        Helper::exec($cmd, $vetor, $codRetorno);

        if ($codRetorno == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function dumpBancoDeDados($pathArquivoDoDump)
    {
        $cmd = $this->getComandoMySqlDump();
        if ($cmd == null) {
            return false;
        }
        $cmd .= $this->DBName . ' > ' . $pathArquivoDoDump;

        $vetor = array();
        $ret = null;

        Helper::exec($cmd, $vetor, $ret);

        if ($ret == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deletaBancoDeDados($nomeDoBanco)
    {
        try {
            if ($this->indiceLink == -1) {
                $this->openLink();
                $this->selectDB();
            }
            $sql = "drop database $nomeDoBanco";

            $cmd = $this->getComandoMySql();
            if ($cmd == null) {
                return false;
            }
            $cmd .= ' -e "' . $sql . '" ';
            $codRetorno = null;
            $vetor = array();
            Helper::exec($cmd, $vetor, $codRetorno);
            if ($codRetorno == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function realEscapeString($token)
    {
//        if (is_array($token)) {
//            throw new Exception ("Token invalid: " . print_r($token, true));
//        }
        if(self::$arrLinks[$this->indiceLink]==null)
            throw new DatabaseException("Banco fora do ar[asdfwae]");
        return mysqli_real_escape_string(
            self::$arrLinks[$this->indiceLink],
            $token);
    }

    public function getDescricao()
    {
        return $this->getHost() . "::" . $this->getDBName();
    }

    //Returns the number of rows affected by the last INSERT, UPDATE, REPLACE or DELETE query.
    public function getAffectedRows()
    {
        return mysqli_affected_rows(self::$arrLinks[$this->indiceLink]);
    }

    public function getLink()
    {
        return self::$arrLinks[$this->indiceLink];
    }

    public function checkLinkState($appendMsg = "")
    {
        $codErro = mysqli_errno(self::$arrLinks[$this->indiceLink]);
        if ($codErro == null) return null;

        $erro = mysqli_error(self::$arrLinks[$this->indiceLink]);

        $log = Registry::get('HelperLog');
        if ($this->isErroRelativoABancoForaDoAr($codErro)) {
            $msg = "Banco fora do ar! Banco: {$this->getIdentificador()}. CodErroMySql[$codErro]: $erro. Developer message: $appendMsg";

            $msg= new Mensagem_token (
                PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,
                $msg,
                $codErro);
            if ($log != null) {
                $log->gravarLogEmCasoErro($msg);
            }
            return $msg;
        } else {
            $msg = "Consulta invalida! Banco: {$this->getIdentificador()}."
                . " CodErroMySql[$codErro]: $erro.  Developer message: $appendMsg. Stack: "
                . Helper::getDebugStackTrace();

            $msg = new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL,
                $msg,
                $codErro);

            if ($log != null) {
                $log->gravarLogEmCasoErro($msg);
            }
            return $msg;
        }

    }


    public function formatarDados($valor)
    {

        if ($valor == null)
        {
            return "null";
        }
        else
        {
            if (!strlen($valor))
            {
                return "null";
            }
            else
            {
                $tratado = "'{$this->realEscapeString($valor)}'";

                return $tratado;
            }
        }
    }
}

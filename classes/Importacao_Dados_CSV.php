<?php

    class Importacao_Dados_CSV
    {

        public $arquivoImportacao;
        public $numRegistrosInseridos;
        public $numRegistrosModificados;
        public $numRegistrosErro;
        public $numRegistrosWarning;
        public $recuperacaoErro;

        public function __construct()
        {
            $arquivo = $_FILES["arquivo_csv1"];
            $path = Helper::acharRaiz() . "temp/";

            $objUpload = new Upload($arquivo, $path, true);

            $this->arquivoImportacao = $objUpload->uploadFile();

            $this->numRegistrosInseridos = 0;
            $this->numRegistrosModificados = 0;
            $this->numRegistrosErro = 0;
            $this->numRegistrosWarning = 0;
            $this->recuperacaoErro = "";
        }

        public function __recuperarCabecalho($objCSV)
        {
            $this->recuperacaoErro = implode(";", $objCSV->cabecalho) . "";
        }

        public function __recuperarLinha($arrLinha, $objCSV, $mensagem = "")
        {
            if (strlen($this->recuperacaoErro) == 0)
            {
                $this->__recuperarCabecalho($objCSV);
            }

            if (strlen($mensagem))
            {
                $mensagem = ";" . $mensagem;
            }

            $this->recuperacaoErro .= str_replace("\n", "", implode(";", $arrLinha)) . "{$mensagem}\n";
            $this->numRegistrosErro++;
        }

        public function acoesInicializacao($nomeDaImportacao)
        {
            Helper::imprimirCabecalhoParaFormatarAction();
            echo Javascript::importarTodasAsBibliotecas();

            $objBanco = new Database();
            $objBanco->iniciarTransacao();

            $objNivelSimilaridade = new EXTDAO_Nivel_similaridade();
            $objNivelSimilaridade->corrigirIndices();

            //faz um backup de todo o banco de dados antes de importar os dados
            $objBackup = new Database_Backup();

            $_POST["descricao"] = "Backup automático anterior à importação <b>{$nomeDaImportacao}</b> em " . Helper::getDiaEHoraAtual() . " feita por " . Helper::getNomeDoUsuarioCorrente();

            if (!$objBackup->fazerBackup())
            {
                Helper::mudarLocation(Helper::getNomeDoScriptAtual() . "?msgErro=A importação não foi concluída devido à falha no backup prévio do banco de dados");
                exit();
            }
        }

        public function acoesFinalizacao()
        {
            $objBanco = new Database();
            $objBanco->commitTransacao();

            $msg = Helper::imprimirMensagem("Operação concluída. A importação foi feita de acordo com o resumo abaixo:

        						  &bull; Registros inseridos no BD: {$this->numRegistrosInseridos}
        						  &bull; Registros do BD que foram alterados: {$this->numRegistrosModificados}
        						  &bull; Registros que não foram importados para o BD: {$this->numRegistrosErro}
        						  &bull; Registros que deram Warning mas foram importados: {$this->numRegistrosWarning}");

            if ($this->numRegistrosErro > 0)
            {
                ?>

                <form id="form_recuperacao" action="download.php?tipo_download=csv" method="post">
                    <textarea id="texto_recuperacao" name="texto_recuperacao"
                              style="display: none;"><?= $this->recuperacaoErro ?></textarea>
                    <input type="hidden" name="arquivo" value="<?= $this->arquivoImportacao ?>"/>

                    <table class="tabela_form">
                        <tr class="tr_form">
                            <td class="td_form_label" colspan="4">
                                Para fazer o download do arquivo CSV com os registros que não foram importados
                                <a href="javascript:void(0)"
                                   onclick="javascript:document.getElementById('form_recuperacao').submit()">clique
                                    aqui</a>
                            </td>
                        </tr>
                    </table>

                </form>

                <?php

            }
        }

        public static function factory()
        {
            return new Importacao_Dados_CSV();
        }

        private static function __corrigirDadosOriginadosDoExcel($dados)
        {
            $search = array("#N/D", "#n/d", "#REF!", "  ", "#N/A", "#n/a");
            $replace = array("", "", "", " ", "", "");

            $dados = str_replace($search, $replace, $dados);

            if (substr_count($dados, "!$") > 0)
            {
                $dados = "";
            }

            return $dados;
        }

        private static function __corrigirDiretoria($nomeImportacao)
        {
            if (($posicao = strpos(strtolower($nomeImportacao), "-")) !== false)
            {
                return substr($nomeImportacao, $posicao + 1, strlen($nomeImportacao));
            }

            return Helper::getStringComPrimeirasLetrasDasPalavrasUC($nomeImportacao);
        }

        private static function __corrigirGerenciaGeral($nomeImportacao)
        {
            if (strpos(strtolower($nomeImportacao), "gabinete") !== false)
            {
                return "Gabinete";
            }
            elseif (($posicao = strpos(strtolower($nomeImportacao), "-")) !== false)
            {
                return substr($nomeImportacao, $posicao + 1, strlen($nomeImportacao));
            }

            return Helper::getStringComPrimeirasLetrasDasPalavrasUC($nomeImportacao);
        }

        private static function __corrigirGerenciaDeArea($nomeImportacao)
        {
            if (strpos(strtolower($nomeImportacao), "gabinete") !== false)
            {
                return "Gabinete";
            }
            elseif (($posicao = strpos(strtolower($nomeImportacao), "_")) !== false)
            {
                return substr($nomeImportacao, $posicao + 1, strlen($nomeImportacao));
            }

            return Helper::getStringComPrimeirasLetrasDasPalavrasUC($nomeImportacao);
        }

        private static function __corrigirSupervisao($nomeImportacao)
        {
            $arrSearch = array("Sup.", "Gabinete da Gerência", "  ");
            $arrReplace = array("Supervisão", "Gabinete", " ");

            $nomeImportacao = str_replace($arrSearch, $arrReplace, $nomeImportacao);

            return Helper::getStringComPrimeirasLetrasDasPalavrasUC($nomeImportacao);
        }


    }
?>
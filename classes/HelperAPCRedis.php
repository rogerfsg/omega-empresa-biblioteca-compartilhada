<?php

class HelperAPCRedis
{
    static $singletonR = null;

    public static function getSingleton()
    {
        if (HelperAPCRedis::$singletonR == null)
        {
            HelperAPCRedis::$singletonR = new HelperAPCRedis();
        }

        return HelperAPCRedis::$singletonR;
    }

    private $helperRedis = null;
    private $helperAPC = null;
    private $prefixo = null;

    public function __construct($prefixo = null)
    {
        if ($prefixo == null)
        {
            if(defined('IDENTIFICADOR_SESSAO'))
            {
                $prefixo = IDENTIFICADOR_SESSAO;
            }
            else
            {
                $prefixo = "";
            }
        }

        $this->prefixo = $prefixo;
        $this->helperRedis = new HelperRedis($prefixo);
        $this->helperAPC = new HelperAPC($prefixo);
    }

    public function add($key, $value, $ttlApc = 0)
    {
        $this->helperAPC->add($key, $value, $ttlApc);
        if ($ttlApc > 0)
        {
            $this->helperRedis->set($key, $value, "ex", $ttlApc);
        }
        else
        {
            $this->helperRedis->set($key, $value);
        }
    }

    public function exists($key)
    {
        if (!$this->helperAPC->exists($key))
        {
            return $this->helperRedis->exists($key);
        }
        else
        {
            return true;
        }
    }

    public function getIfExists($key, $default = null, $ttlApc = 0)
    {
        if (!$this->helperAPC->exists($key))
        {
            if ($this->helperRedis->exists($key))
            {
                $value = $this->helperRedis->get($key);
                $this->helperAPC->add($key, $value, $ttlApc);

                return $value;
            }
            else
            {
                return $default;
            }
        }
        else
        {
            return $this->helperAPC->get($key);
        }
    }

    public function hgetIfExists($hash, $key, $default = null, $ttlApc = 0)
    {
        if (!$this->helperAPC->exists($hash . $key))
        {
            if ($this->helperRedis->exists($hash . $key))
            {
                $value = $this->helperRedis->hget($hash, $key);
                $this->helperAPC->add($hash . $key, $value, $ttlApc);

                return $value;
            }
            else
            {
                return $default;
            }
        }
        else
        {
            return $this->helperAPC->get($hash . $key);
        }
    }

    public function hset($hash, $key, $value, $ttlApc = 0)
    {
        $this->helperAPC->add($hash . $key, $value, $ttlApc);
        $this->helperRedis->hset($key, $value);
    }

    public function hmsetAPCRedis($hash, $matrixHash, $ttlApc = 0)
    {
        foreach ($matrixHash as $h => $v)
        {
            $this->helperAPC->add($hash . $h, $v, $ttlApc);
        }

        $this->helperRedis->hmset($hash, $matrixHash);
    }

    public function hmsetOnlyRedis($hash, $matrixHash, $ttlApc = 0)
    {
        $this->helperRedis->hmset($hash, $matrixHash);
    }
}

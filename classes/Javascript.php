<?php

class Javascript
{

    public static $pathNiveisAteARaizDoWorkspace = false;
    public static $numeroNiveisAteARaizDoWorkspace = false;

    public static $pathNiveisAteARaizDoProjeto = false;
    public static $numeroNiveisAteARaizDoProjeto = false;
    public static $MODULOS_ANGULARJS_BASICOS = array('animate.min', 'route.min', 'sanitize.min');
    public static $MODULOS_ANGUJARJS_TODOS = array('animate.min', 'aria.min', 'cookies.min', 'loader.min', 'message-format.min', 'messages.min', 'mocks',
        'parse-ext.min', 'resource.min', 'route.min', 'sanitize.min', 'touch.min');

    const OMEGA_ANGULARJS_THEME_NEON = 'omega-neon-theme';

    public static function getPathAteRaizDoProjeto()
    {
        self::setRaizDaEstrutura();

        return self::$pathNiveisAteARaizDoProjeto;
    }

    public static function setRaizDaEstrutura()
    {
        if (self::$pathNiveisAteARaizDoWorkspace === false)
        {
            self::$pathNiveisAteARaizDoWorkspace = Helper::acharRaizWorkspace();
            self::$numeroNiveisAteARaizDoWorkspace = Helper::acharRaizWorkspace(true);
        }

        if (self::$pathNiveisAteARaizDoProjeto === false)
        {
            self::$pathNiveisAteARaizDoProjeto = Helper::acharRaiz();
            self::$numeroNiveisAteARaizDoProjeto = Helper::acharRaiz(true);
        }
    }

    public static function importarBibliotecasBackbone()
    {
        $str = "";
        $str .= "<script data-main=\"public/front/main\" src=\"public/node_modules/requirejs/require.js\"></script>";

        return $str;
    }

    public static function importarBibliotecaMoment()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/moment";

        $str = "";
        $str .= "<script src=\"{$strPathBase}/moment.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/moment-duration-format.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecaFullCalendar()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/fullcalendar";

        $str = "";
        $str .= "<link href=\"{$strPathBase}/fullcalendar.min.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<script src=\"{$strPathBase}/fullcalendar.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/locale/pt-br.js\" type=\"text/javascript\" charset='iso-8859-1'></script>";

        return $str;
    }

    public static function importarBibliotecasNeonCabecalho()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/neon";

        $str = "<link href=\"{$strPathBase}/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/font-icons/entypo/css/entypo.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/bootstrap.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/neon-core.css\" media=\"all\" id=\"color-settings-body-color\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/neon-theme.css\" media=\"all\" id=\"color-settings-body-color\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/neon-forms.css\" media=\"all\" id=\"color-settings-body-color\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/custom.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/skins/white.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/css/font-icons/entypo/css/entypo.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        $str .= "<script src=\"{$strPathBase}/js/jquery-1.11.0.min.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecasNeonRodape()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/neon";

        $str = "<link href=\"{$strPathBase}/css/font-icons/font-awesome/css/font-awesome.min.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/js/jvectormap/jquery-jvectormap-1.2.2.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/js/rickshaw/rickshaw.min.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        $str .= "<script src=\"{$strPathBase}/js/gsap/main-gsap.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/bootstrap.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/joinable.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/resizeable.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/neon-api.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/jvectormap/jquery-jvectormap-1.2.2.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/jvectormap/jquery-jvectormap-europe-merc-en.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/jquery.sparkline.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/rickshaw/vendor/d3.v3.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/rickshaw/rickshaw.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/raphael-min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/morris.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/toastr.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/neon-chat.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/neon-custom.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/neon-demo.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/bootstrap-datepicker.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/js/jquery.knob.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecasFlattyCabecalho()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/flatty";

        $str = "<link href=\"{$strPathBase}/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/stylesheets/plugins/common/bootstrap-wysihtml5.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/stylesheets/bootstrap/bootstrap.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/stylesheets/light-theme.css\" media=\"all\" id=\"color-settings-body-color\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/stylesheets/theme-colors.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/stylesheets/demo.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        $str .= "<!--[if lt IE 9]><script src=\"{$strPathBase}/javascripts/ie/html5shiv.js\" type=\"text/javascript\"></script>
                 <script src=\"{$strPathBase}/javascripts/ie/respond.min.js\" type=\"text/javascript\"></script><![endif]-->";

        $str .= "<script src=\"{$strPathBase}/javascripts/jquery/jquery.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/jquery/jquery.mobile.custom.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/jquery/jquery-migrate.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/jquery/jquery-ui.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js\" type=\"text/javascript\"></script>";

        $str .= "<link href=\"{$strPathBase}/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        return $str;
    }

    public static function importarBibliotecasFlattyRodape()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/flatty";

        $str = "<script src=\"{$strPathBase}/javascripts/bootstrap/bootstrap.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/modernizr/modernizr.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/retina/retina.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/theme.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/validate/jquery.validate.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/validate/additional-methods.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/bootbox/bootbox.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/jgrowl/jquery.jgrowl.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/tabdrop/bootstrap-tabdrop.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/typeahead/typeahead.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/timeago/jquery.timeago.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/nestable/jquery.nestable.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/lightbox/lightbox.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/bootbox/bootbox.min.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/bootbox/bootbox.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/slimscroll/jquery.slimscroll.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/timeago/jquery.timeago.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/common/wysihtml5.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/common/bootstrap-wysihtml5.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/javascripts/plugins/bootbox/bootbox.min.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecaJqueryResizable()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/jquery";

        $str = "<script src=\"{$strPathBase}/jquery-resizable-0.20.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/jquery-resizableTableColumns-0.17.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecaBootstrapSwitch()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/bootstrap/bootstrap-switch";

        $str = "<script src=\"{$strPathBase}/bootstrap-switch.js\" type=\"text/javascript\"></script>";
        $str .= "<link href=\"{$strPathBase}/bootstrap-switch.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        return $str;
    }

    public static function importarBibliotecaAngularJS()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/angularjs";

        $str = "<script src=\"{$strPathBase}/1.6.1/angular.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/1.6.1/i18n/angular-locale_pt-br.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/1.6.1/i18n/angular-locale_en-us.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarModulosDoAngularJS($modulos = 'todos')
    {
        $str = "";
        if ($modulos == 'todos')
        {
            $modulos = static::$MODULOS_ANGUJARJS_TODOS;
        }

        if (is_array($modulos))
        {
            self::setRaizDaEstrutura();
            $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/angularjs";

            foreach ($modulos as $modulo)
            {
                $str .= "<script src=\"{$strPathBase}/1.6.1/angular-{$modulo}.js\" type=\"text/javascript\"></script>";
            }
        }

        return $str;
    }

    private static $angularAppImported = false;
    public static function importarAngularApp()
    {
        $raizProjeto = self::getPathAteRaizDoProjeto();
        $str = "";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/Constants.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/Utils.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularApp.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularFilters.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularRootScope.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";


        self::$angularAppImported = true;
        return $str;
    }

    public static function importarBibliotecaJavascriptLogger()
    {
        self::setRaizDaEstrutura();

        if (self::$angularAppImported)
        {
            $angularApp = 'omegaApp';
        }
        else
        {
            $angularApp = null;
        }

        $jsLogger = new JavascriptLogger($angularApp);
        $str = $jsLogger->importByEnvironment();

        return $str;
    }

    public static function importarBibliotecaOmegaAngularJS($theme = null)
    {
        self::setRaizDaEstrutura();

        $str = "";
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/UI";

        if (!is_null($theme))
        {
            $str .= "<link href=\"{$strPathBase}/CSS/Themes/{$theme}.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        }

        $str .= "<link href=\"{$strPathBase}/CSS/default.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<script src=\"{$strPathBase}/JS/Constants.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/Message.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/Utils.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/AngularFilters.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/AngularRootScope.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/JS/Directives/OmegaControlTypeDirective.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/Directives/UpperCaseTextDirective.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/Directives/LowerCaseTextDirective.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/JS/Directives/GenericPhoneDirective.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarPluginsDoAngularJS()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/angularjs/plugins";


        $str = "<script src=\"{$strPathBase}/translate/angular-translate.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/input_mask/angular-input-masks.br.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/mask_money/angular.maskMoney.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/mask.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/ui-bootstrap-tpls-2.5.0.min.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/checklist-model.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/validate.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/select/select.js\" type=\"text/javascript\"></script>";
        $str .= "<link href=\"{$strPathBase}/select/select.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/select/themes/select2/select2.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";
        $str .= "<link href=\"{$strPathBase}/select/themes/select2/select2-bootstrap.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        $str .= "<script src=\"{$strPathBase}/dialogs/dialogs.js\" type=\"text/javascript\"></script>";
        $str .= "<link href=\"{$strPathBase}/dialogs/dialogs.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        /*
        $str .= "<script src=\"{$strPathBase}/ui-grid/ui-grid.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/ui-grid/i18n/en.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/ui-grid/i18n/pt-br.js\" type=\"text/javascript\"></script>";
        $str .= "<link href=\"{$strPathBase}/ui-grid/ui-grid.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";*/

        $str .= "<script src=\"{$strPathBase}/file_upload/ng-file-upload-shim.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/file_upload/ng-file-upload.js\" type=\"text/javascript\"></script>";


        $str .= "<script src=\"{$strPathBase}/bootstrap_switch/angular-bootstrap-switch.js\" type=\"text/javascript\"></script>";

        $str .= "<script src=\"{$strPathBase}/spinner/jquery.blockUI.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/spinner/canvas-loader.js\" type=\"text/javascript\"></script>";
        $str .= "<script src=\"{$strPathBase}/spinner/spin.min.js\" type=\"text/javascript\"></script>";

        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/UI";
        $str .= "<script src=\"{$strPathBase}/JS/AngularTranslations.js\" type=\"text/javascript\"></script>";

        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/jquery";
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.autocomplete");

        $str .= '<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.1/jquery.jgrowl.min.js"></script>';

        return $str;
    }

    public static function getPathRaizBibliotecaFlatty()
    {
        self::setRaizDaEstrutura();
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/flatty";

        return $strPathBase;
    }

    public static function importarTodasAsBibliotecasProjetoFlatty()
    {
        $trace = "1, ";
        try{
            self::setRaizDaEstrutura();
            $str = "";
            $str .= self::importarBibliotecasFlatty();
            $trace .= "2, ";
            $str .= self::importarBibliotecaJQueryCookie();
            $trace .= "3, ";
            $str .= self::getComandosDialog();
            $trace .= "4, ";
            $str .= self::imprimirCorpoDoTooltip();
            $trace .= "5, ";
            $str .= self::importarBibliotecaJavascriptPadrao();
            $trace .= "6, ";
            $str .= self::importarBibliotecaDeValidacao();
            $trace .= "7, ";
            $str .= self::importarBibliotecaErro();
            $trace .= "8, ";
            $str .= self::importarProtocoloSistema();

            return $str;
        }
        catch (Exception $ex)
        {
            throw new Exception($trace , 0, $ex);
        }

    }

    public static function importarBibliotecasIncubadoraDaCervejaCabecalho()
    {

        try
        {
            self::setRaizDaEstrutura();
            $str = "";

            $str .= self::importarBibliotecaTemplateIncubadoraDaCervejaCabecalho();       ;
            $str .= self::importarProtocoloSistema();
            $str .= self::importHtml2Canvas();
            $str .= self::importarBibliotecaJsQrCode();
            $str .= self::importarBibliotecaJsQrCodeGenerator();
            $str .= self::importarBibliotecaJQueryCookie();

            $str .= self::importarBibliotecaBootstrapSwitch();
            $str .= self::importarBibliotecaAngularJS();
            $str .= self::importarModulosDoAngularJS(Javascript::$MODULOS_ANGUJARJS_TODOS);
            $str .= self::importarPluginsDoAngularJS();

            $str .= self::importarAngularIncubadoraApp();
            $str .= self::importarBibliotecaJavascriptLogger();
            $str .= self::importarBibliotecaOmegaAngularJS();

            return $str;
        }
        catch (Exception $ex)
        {
            throw new Exception("" , 0, $ex);
        }

    }

    public static function importarBibliotecasIncubadoraDaCervejaRodape()
    {
        try
        {
            self::setRaizDaEstrutura();

            $str = "";
            $str .= self::importarBibliotecaTemplateIncubadoraDaCervejaRodape();



            return $str;
        }
        catch (Exception $ex)
        {
            throw new Exception("" , 0, $ex);
        }

    }

    public static function importarBibliotecaTemplateIncubadoraDaCervejaCabecalho()
    {
        $pathJS = self::$pathNiveisAteARaizDoProjeto . "recursos/js";
        $pathCSS = self::$pathNiveisAteARaizDoProjeto . "client_area/css";

        $str = "<script src=\"{$pathJS}/template/jquery-2.2.4.min.js\"></script>";

        $str .= "<link href=\"{$pathCSS}/template/bootstrap.min.css\" rel=\"stylesheet\">
                     <link href=\"{$pathCSS}/template/fakeLoader.css\" rel=\"stylesheet\">
                     <link href=\"{$pathCSS}/template/owl-carousel.min.css\" rel=\"stylesheet\">
                     <link href=\"{$pathCSS}/template/style.css\" rel=\"stylesheet\">
                     <link href=\"https://fonts.googleapis.com/css?family=Catamaran:400,500,600,700,800%7CGrand+Hotel\" rel=\"stylesheet\">
                     <link href=\"{$pathCSS}/_custom.css\" rel=\"stylesheet\">
                     <link href=\"{$pathCSS}/incubadora-da-cerveja.css\" rel=\"stylesheet\">";

        return $str;

    }

    public static function importarBibliotecaTemplateIncubadoraDaCervejaRodape()
    {
        $path = self::$pathNiveisAteARaizDoProjeto . "recursos/js/template";

        $str = "<script src=\"{$path}/jquery-ui.min.js\"></script>
                    <script src=\"{$path}/bootstrap.min.js\"></script>
                    <script src=\"{$path}/bootbox.min.js\"></script>
                    <script src=\"{$path}/headhesive.min.js\"></script>
                    <script src=\"{$path}/matchHeight.min.js\"></script>
                    <script src=\"{$path}/modernizr.custom.js\"></script>
                    <script src=\"{$path}/waypoints.min.js\"></script>
                    <script src=\"{$path}/counterup.js\"></script>
                    <script src=\"{$path}/scrollme.min.js\"></script>
                    <script src=\"{$path}/fakeLoader.min.js\"></script>
                    <script src=\"{$path}/owl.carousel.js\"></script>
                    <script src=\"{$path}/owl.autoplay.js\"></script>
                    <script src=\"https://use.fontawesome.com/4dfd2d448a.js\"></script>
                    <script src=\"{$path}/custom.js\"></script>";

        $str .= "<script src=\"{$path}/custom.js\"></script>";
        $str .= "<script src=\"{$path}/jquery.maskMoney.js\" type=\"text/javascript\"></script>";

        return $str;
    }

    public static function importarBibliotecaJsQrCodeGenerator()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = "<script type=\"text/javascript\" src=\"{$path}/libs/qrcodegenerator/qrcode.min.js\"></script>
                    
                    ";

        return $str;

    }

    public static function importHtml2Canvas()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = "<script type=\"text/javascript\" src=\"{$path}/libs/html2canvas/html2canvas.min.js\"></script>
                    ";

        return $str;

    }

    public static function importarBibliotecaJsQrCode()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = "<script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/grid.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/version.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/detector.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/formatinf.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/errorlevel.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/bitmat.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/datablock.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/bmparser.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/datamask.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/rsdecoder.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/gf256poly.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/gf256.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/decoder.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/qrcode.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/findpat.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/alignpat.js\"></script>
                    <script type=\"text/javascript\" src=\"{$path}/libs/js-qrcode/databr.js\"></script>";

        return $str;

    }

    public static function importarBibliotecasFlatty()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = '<script src="' . $path . '/libs/flatty/javascripts/jquery/jquery.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/jquery/jquery-migrate.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/jquery/jquery-ui.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/bootstrap/bootstrap.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/retina/retina.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/theme.js" type="text/javascript"></script>';

        //$str .= '<script src="'.$path.'/libs/flatty/javascripts/demo.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/validate/additional-methods.js" type="text/javascript"></script>';

        //$str .= '<script src="'.$path.'/libs/flatty/javascripts/demo.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/jgrowl/jquery.jgrowl.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/tabdrop/bootstrap-tabdrop.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/typeahead/typeahead.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/timeago/jquery.timeago.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/nestable/jquery.nestable.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/lightbox/lightbox.min.js" type="text/javascript"></script>';
        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>';

        $str .= '<script src="' . $path . '/libs/flatty/javascripts/plugins/bootstrap_switch/bootstrapSwitch.min.js" type="text/javascript"></script>';
        $str .= "<link href=\"{$path}/libs/flatty/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />";

        return $str;
    }


    public static function importarBibliotecaJQueryCookie()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/jquery-cookie";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.cookie");

        return $str;
    }

    public static function getComandosDialog()
    {
        $str = Helper::getComandoJavascript("$(document).ready(function () {
			$(\"#div_dialog\").dialog({autoOpen: false, height: 50, maxHeight: 600,
			close: function(event, ui){ $(\"#div_dialog\").dialog(\"option\", \"height\", 50);
			document.getElementById('conteudo_dialog').src='about:blank'; }

			});
		 });");

        $str .= "<div id='div_dialog' style='display: none;'>
            		 <iframe id='conteudo_dialog' class='conteudo_dialog' 
                         src='' frameborder='0' style='width:100%; border:0; 
                         background-color:#ffffff; height:100%; overflow:auto;'>
                         </iframe>
            	 </div>";

        return $str;
    }

    public static function imprimirCorpoDoTooltip()
    {
        $raiz = self::$pathNiveisAteARaizDoWorkspace;
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/tooltip/";

        $str = "<input type=\"hidden\" id=\"ultimo_popup_selecionado\" value=\"\" />
        		<div id=\"repositorio_invisivel\" style=\"display: none;\"></div>
                <div id=\"ToolTip\"></div>";

        $str .= Helper::carregarArquivoJavascript(0, "{$path}", "tooltip");

        print $str;
    }

    public static function importarBibliotecaJavascriptPadrao()
    {
        $numeroNiveisRaiz = self::$numeroNiveisAteARaizDoProjeto;

        Helper::includePHP($numeroNiveisRaiz, 'recursos/php/js_constants.php');
        $str = "";
        $str .= Helper::carregarArquivoJavascript($numeroNiveisRaiz, "recursos/js/", "core", true);

        $str .= Helper::carregarArquivoJavascript($numeroNiveisRaiz, "recursos/js/", "ajax", true);
        $str .= Helper::carregarArquivoJavascript($numeroNiveisRaiz, "recursos/js/", "especifico", true);

        return $str;
    }

    public static function importarBibliotecaDeValidacao()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryValidationTextField");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryValidationCheckbox");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryValidationTextarea");

        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryValidationRadio");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryValidationSelect");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/libs/spry_validation", "SpryUtils");

        $str .= Helper::carregarArquivoCss(0, "$path/libs/spry_validation", "SpryValidationTextField");

        $str .= Helper::carregarArquivoCss(0, "$path/libs/spry_validation", "SpryValidationTextarea");
        $str .= Helper::carregarArquivoCss(0, "$path/libs/spry_validation", "SpryValidationSelect");
        //inclui de dentro do projeto que a aplicação estiver rodando
        $path = self::$pathNiveisAteARaizDoProjeto . "recursos";
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/js/", "validacoes");

        return $str;
    }

    public static function importarBibliotecaErro()
    {
        $path = self::$pathNiveisAteARaizDoProjeto . "recursos";
        $str = Helper::carregarArquivoJavascript(0, "{$path}/js", "erro");
        return $str;
    }

    public static function importarProtocoloSistema()
    {
        $str = "";
        $numeroNiveisRaiz = self::$numeroNiveisAteARaizDoProjeto;
        $str .= Helper::carregarArquivoJavascript($numeroNiveisRaiz, "recursos/js/", "protocolo_sistema", true);

        return $str;
    }

    public static function importarTodasAsBibliotecas()
    {
        self::setRaizDaEstrutura();
        $str = "";
        $str .= self::importarBibliotecaJavascriptPadrao();
        $str .= self::importarBibliotecaJQuery();
        $str .= self::importarDialogJs();
        $str .= self::importarBibliotecaAutocomplete();
        $str .= self::importarBibliotecaDeValidacao();
        $str .= self::importarBibliotecaTooltip();
        $str .= self::importarBibliotecaComumOmegasoftware();
        $str .= self::imprimirCorpoDoTooltip();
        $str .= Helper::getComandoJavascript("var dialogo = false");
        $str .= self::importarBibliotecaSpryMenuBar();
        //$str .= self::importarBibliotecaDragAndDrop();
        //$str .= self::importarBibliotecaTree();

        return $str;
    }

    public static function importarBibliotecaJQuery()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/jquery";

        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery-1.5.1.min");
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery-ui-1.8.6.custom.min");
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.pngFix");
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.maskMoney");

        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.autocomplete");
        $str .= Helper::carregarArquivoCss(0, $path . "/css/autocomplete", "autocomplete");

        $str .= Helper::carregarArquivoCss(0, $path . "/css/smoothness", "jquery-ui-1.8.6.custom");

        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.ribbon");
        $str .= Helper::carregarArquivoCss(0, $path . "/css/ribbon/windows7", "ribbon");

        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery-ui-timepicker-addon");
        $str .= Helper::carregarArquivoCss(0, $path . "/css/timepicker", "style");

        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.ui.datepicker-pt-BR");

        $str .= Helper::getComandoJavascript("$(document).ready(function () {

												$(\"#div_dialog\").dialog({autoOpen: false, height: 50, maxHeight: 600,
																		   close: function(event, ui){ $(\"#div_dialog\").dialog(\"option\", \"height\", 50);
																					                   document.getElementById('conteudo_dialog').src='about:blank'; }

													 });
			     					});");

        $str .= "<div id='div_dialog' style='display: none;'>
            		 <iframe id='conteudo_dialog' class='conteudo_dialog' src='' frameborder='0' style='width:100%; border:0; background-color:#ffffff; height:100%; overflow:auto;'></iframe>
            	 </div>";

        return $str;
    }

    public static function importarDialogJs()
    {
        $str = "";
        $str .= Helper::getComandoJavascript("
            $(document).ready(function () {
                    $(\"#div_dialog_js\").dialog({autoOpen: false, height: 50, maxHeight: 600,
                                close: function(event, ui){ $(\"#div_dialog_js\").dialog(\"option\", \"height\", 50);
                                $('#div_dialog_js').html(''); }
                    });
            });");

        $str .= "<div id='div_dialog_js' style='display: none;'>
            		 
            	 </div>";

        return $str;
    }

    public static function importarBibliotecaAutocomplete()
    {
        self::setRaizDaEstrutura();
        $str = "";
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/autocomplete";
        $str .= Helper::carregarArquivoCss(0, $path, "autocomplete");

        return $str;
    }

    public static function importarBibliotecaTooltip()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/tooltip/";
        $str = Helper::carregarArquivoCss(0, $path, "tooltip");

        return $str;
    }

    public static function importarBibliotecaComumOmegasoftware()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/comum_omegasoftware/protocolo_sistema.js";
        $str = Helper::carregarArquivoJavascriptDaUrl($path);

        return $str;
    }

    public static function importarBibliotecaSpryMenuBar()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/spry_menubar";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, $path, "SpryMenuBar");
        $str .= Helper::carregarArquivoCss(0, $path, "SpryMenuBar");

        return $str;
    }

    public static function importarBibliotecaFusionCharts()
    {
        $path = self::$pathNiveisAteARaizDoWorkspace . __PATH_BIBLIOTECAS_COMPARTILHADAS . "/libs/fusion_charts";

        Helper::includePHP(0, "{$path}/FusionCharts.php");
        $str = "";
        $str .= Helper::carregarArquivoJavascript(
            0, __DOMINIO_BIBLIOTECAS_COMPARTILHADAS, "FusionCharts");

        return $str;
    }

    public static function importarBibliotecaJSColor()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/jscolor";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, $path, "jscolor");

        return $str;
    }

    public static function importarBibliotecaTree()
    {
        $str = "";
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/treeview/";
        $str .= Helper::carregarArquivoCssDaURL($path . "/dhtmlxtree.css");
        $str .= Helper::carregarArquivoJavascriptDaUrl($path . "/dhtmlxcommon.js");
        $str .= Helper::carregarArquivoJavascriptDaUrl($path . "/dhtmlxtree.js");
        $str .= Helper::carregarArquivoJavascriptDaUrl($path . "/Arvore.js");
        $str .= Helper::carregarArquivoJavascriptDaUrl($path . "/Tab.js");

        return $str;
    }

    public static function importarBibliotecaDragAndDrop()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/drag_and_drop";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.event.drag.min");
        $str .= Helper::carregarArquivoJavascript(0, $path, "jquery.event.drop.min");

        return $str;
    }

    public static function importarBibliotecaScriptaculous($arrayBibliotecas = array("effects"))
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/scriptaculous";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/lib/", "prototype");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/src/", "scriptaculous");

        if (!is_array($arrayBibliotecas))
        {
            $arrayBibliotecas = array("builder", "controls", "dragdrop", "effects", "slider", "sound");
        }

        foreach ($arrayBibliotecas as $modulo)
        {
            $str .= Helper::carregarArquivoJavascript(0, "{$path}/src/", "{$modulo}");
        }

        return $str;
    }

    public static function importarBibliotecaDaTela($tela = null)
    {
        if (!strlen($tela))
        {
            return "";
        }
        self::setRaizDaEstrutura();

        $path = Helper::acharRaiz() . "recursos";
        $str = Helper::carregarArquivoJavascript(0, "{$path}/js/tela", $tela);

        return $str;
    }

    public static function importarBibliotecaDaEntidade($entidade = null)
    {
        if (!strlen($entidade))
        {
            return "";
        }
        self::setRaizDaEstrutura();

        $path = self::$pathNiveisAteARaiz . "recursos";
        $str = Helper::carregarArquivoJavascript(1, "{$path}/js/entidade", $entidade);

        return $str;
    }

    public static function importarBibliotecaDoGoogleAnalytics()
    {
//    	if(!in_array(Helper::getEnderecoIPDoCliente(), Seguranca::getIPsAutorizadosNaDepuracao())){

        $codigoCliente = CODIGO_DA_CONTA_GOOGLE_ANALYTICS;

        $script = "<script type=\"text/javascript\">

	                      var _gaq = _gaq || [];
	                      _gaq.push(['_setAccount', '{$codigoCliente}']);
	                      _gaq.push(['_trackPageview']);

	                      (function() {
	                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	                      })();

	                    </script>";

        return $script;
//    	}

    }

    public static function importarBibliotecaCalendario()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/jscalendar";
        $str = "";
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/js", "jscal2");
        $str .= Helper::carregarArquivoJavascript(0, "{$path}/js/lang", "pt");

        $str .= Helper::carregarArquivoCss(0, "$path/css", "jscal2");
        $str .= Helper::carregarArquivoCss(0, "$path/css", "border-radius");
        $str .= Helper::carregarArquivoCss(0, "$path/css/steel", "steel");

        $str .= "

        <script type=\"text/javascript\">//<![CDATA[

        	var cal = Calendar.setup({
              onSelect: function(cal) { cal.hide() }
          });

        //]]></script>";

        return $str;
    }

    public static function getComandoParaAtualizarDialog($arrVariaveisGetAdicionais = false)
    {
        $strAdicional = "";

        if (is_array($arrVariaveisGetAdicionais))
        {
            foreach ($arrVariaveisGetAdicionais as $chave => $valor)
            {
                $strAdicional .= "{$chave}={$valor}&";
            }

            return COMANDO_FECHAR_DIALOG_ATUALIZANDO . " + '&{$strAdicional}'";
        }
        else
        {
            return COMANDO_FECHAR_DIALOG_ATUALIZANDO;
        }
    }

    public static function getStringArrayJavascript($arrayPHP)
    {
        $strRetorno = "new Array(";

        foreach ($arrayPHP as $chave => $valor)
        {
            $strRetorno .= "'{$valor}', ";
        }

        if (count($arrayPHP))
        {
            $strRetorno = Helper::removerOsUltimosCaracteresDaString($strRetorno, 2);
        }

        $strRetorno .= ")";

        return $strRetorno;
    }

    public static function importarBibliotecaSelect2()
    {
        self::setRaizDaEstrutura();

        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/select2";
        $str = "";

        $str .= Helper::carregarArquivoJavascript(0, $path, "select2.full");

        $str .= Helper::carregarArquivoCss(0, $path, "select2.min");

        return $str;
    }

    public static function importarBibliotecaDoQRCode()
    {
        $path = self::$pathNiveisAteARaizDoWorkspace . __PATH_BIBLIOTECAS_COMPARTILHADAS . "/libs/fusion_charts";

        Helper::includePHP(0, "{$path}/libs/phpqrcode/qrlib.php");
    }

    public static function importarBibliotecaDoFacebook()
    {
        $path = self::$pathNiveisAteARaizDoWorkspace . __PATH_BIBLIOTECAS_COMPARTILHADAS . "/libs/fusion_charts";

        Helper::includePHP(0, "{$path}libs/facebook/base_facebook.php");
        Helper::includePHP(0, "{$path}libs/facebook/facebook.php");
    }

    public static function importarJavascriptDoProjeto($pathDirRelativo, $arquivoSemExtensao)
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . $pathDirRelativo;

        $str = "<script type=\"text/javascript\" src=\"{$path}{$arquivoSemExtensao}.js\" ></script>\n";

        return $str;
    }

    public static function importarJavascriptDaBibliotecaCompartilhada($pathDirRelativo, $arquivoSemExtensao)
    {
        $path = self::$pathNiveisAteARaizDoWorkspace . $pathDirRelativo;

        $str = "<script type=\"text/javascript\" src=\"{$path}{$arquivoSemExtensao}.js\" ></script>\n";

        return $str;
    }



    public static function importarAngularIncubadoraApp()
    {
        $raizProjeto = self::getPathAteRaizDoProjeto();
        $str = "";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/Constants.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/Utils.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularApp.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularFilters.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "recursos/js/AngularApp/AngularRootScope.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/comum.service.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/facebook.service.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";



        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/barril.service.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/barril.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/barris.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/fazer.pedido.simples.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/acompanhamento.festa.degustador.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/cadastro.degustador.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/cadastro.cervejeiro.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/exibir.camera.qrcode.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";

        $fullPath = Helper::pathCombine($raizProjeto, "client_area/angular/app/delivery.controller.js");
        $str .= "<script src=\"{$fullPath}\" type=\"text/javascript\"></script>";


        self::$angularAppImported = true;
        return $str;
    }

}
<?php

    class ConstanteSeguranca
    {
        const CHAVE_SESSION = "SS_CHAVE";
        const CHAVE_AUTO_LOGIN = "SS_AL";
        //30 dias
        const TEMPO_LIMITE_AUTOLOGIN_MINUTOS = 43200;
        //1 dia
        const TEMPO_LIMITE_SESSAO_MINUTOS = 1440;
    }

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of InterfaceScript
     *
     * @author home
     */
    abstract class InterfaceScript
    {
        public $interfaceGrafica = false;

        public function __construct(
            $init = false,
            $header = 'HTML')
        {
            if (!$init)
            {
                return;
            }

            Javascript::setRaizDaEstrutura();
            Helper::includePHPDaBibliotecaCompartilhada('imports/lingua.php');
            $this->beforeHeader();

            if ($header == 'XML')
            {
                header("Content-type: text/xml; charset=iso-8859-1", true);
            }
            else
            {
                if ($header == 'JSON')
                {
                    header("Content-type: application/json; charset=iso-8859-1", true);
                }
                else
                {
                    if ($header == 'OCTET-STREAM')
                    {
                        header("Content-type: application/octet-stream; charset=iso-8859-1", true);
                    }
                    else
                    {
                        if ($header == 'PDF')
                        {
                            header("Content-type: application/pdf; charset=iso-8859-1", true);
                        }
                        else
                        {
                            $this->interfaceGrafica = true;
                            header("Content-type: text/html; charset=iso-8859-1", true);
                        }
                    }
                }
            }
        }

        public function beforeHeader()
        {
        }

        public abstract function render();

    }

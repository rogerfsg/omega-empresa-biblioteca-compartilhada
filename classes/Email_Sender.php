<?php

class Email_Sender
{
    //TEM QUE SER MINUSCIULO por conta dos htmls adicionados ao diretorio de template_EMAIL
    const TIPO_RELEMBRAR_SENHA = 'relembrar_senha';
    const TIPO_SENHA_ALTERADA = 'senha_alterada';
    const TIPO_CADASTRO_USUARIO = "cadastro_usuario";
    const TIPO_BEM_VINDO = "bem_vindo";
    public $remetente;
    public $destinatario;
    public $assunto;
    public $cabecalhoEmail;
    public $conteudo;
    public $anexos;
    public $mimeBoundary;

    public function __construct($remetente = "", $destinatario = "", $assunto = "", $conteudo = "")
    {
        $this->anexos = array();

        if (strlen($remetente))
        {
            //$this->remetente = $remetente;
            $this->remetente = "user@li1047-176.members.linode.com";
        }

        if (strlen($destinatario))
        {
            $this->destinatario = $destinatario;
        }

        if (strlen($assunto))
        {
            $this->$assunto = $assunto;
        }

        if (strlen($conteudo))
        {
            $this->conteudo = $conteudo;
        }
    }

    public function setRemetente($remetente)
    {
        $this->remetente = $remetente;
    }

    public function setDestinatario($destinatario)
    {
        $this->destinatario = $destinatario;
    }

    public function setAssunto($assunto)
    {
        $this->assunto = $assunto;
    }

    public function setConteudo($conteudo, $isFormatado = true)
    {
//            if ($isFormatado)
//            {
//                $this->conteudo = $this->__montarTextoFormatado($conteudo);
//            }
//            else
//            {
        $this->conteudo = $conteudo;
//            }
    }

    private function __montarCabecalhoEmail()
    {
        $this->cabecalhoEmail = "";

        $this->mimeBoundary = "WO" . md5(time());

        $this->cabecalhoEmail .= "MIME-Version: 1.1\n";
        //$this->cabecalhoEmail .= "Content-Type: text/html; charset=UTF-8\n";
        //$this->cabecalhoEmail .= "Content-Type: multipart/alternative; boundary=\"{$this->mimeBoundary}\"\n";
        $this->cabecalhoEmail .= "Content-Type: text/html; charset=UTF-8; boundary=\"{$this->mimeBoundary}\"\n";

        if ($this->remetente)
        {
            $nomeRemetente = "WorkOffline.com.br";

            $this->cabecalhoEmail .= "From: {$nomeRemetente} <{$this->remetente}>\r\n";
        }

//            if (array_count_values($this->anexos))
//            {
//            }
    }

    private function __montarTextoFormatado($stringTexto)
    {
        $str = "";
        //$headers = "MIME-Version: 1.0" . "\r\n";
        //$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        //
        //// More headers
        //$headers .= 'From: <webmaster@example.com>' . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";

        $str .= "MIME-Version: 1.0\n";
        $str .= "Content-Type: text/html; charset=ISO-8859-1\n";
        $str .= "Content-Transfer-Encoding: 8bit\n\n";

        $str .= $stringTexto;

        return $str;
    }

    public function incluirAnexo($arquivo)
    {
        $this->anexos[] = $arquivo;
    }

    public function incluirAnexos($arrayArquivo)
    {
        if (is_array($arrayArquivo))
        {
            $this->anexos = array_merge($this->anexos, $arrayArquivo);
        }
    }

    public function enviarEmail()
    {
        $idEmail = HelperString::gerarStringRandomica();
        HelperLog::logEmail("[$idEmail] " . $this->remetente . " " . $this->destinatario . " " . $this->assunto . " " . $this->conteudo . " ...");

        if (strlen($this->remetente) && strlen($this->destinatario) && strlen($this->assunto) && strlen($this->conteudo))
        {
            $this->__montarCabecalhoEmail();

            if (!mail($this->destinatario, $this->assunto, $this->conteudo, $this->cabecalhoEmail))
            {
                HelperLog::logEmailErro("[$idEmail] Erro: " . $this->destinatario);

                return false;
            }
            else
            {
                HelperLog::logEmail("[$idEmail] Email enviado: {$this->destinatario}");

                return true;
            }
        }
        else
        {
            HelperLog::logEmailErro("[$idEmail] Parametros invalidos para envio de email: " . json_encode($this));

            return false;
        }
    }

    ///$template = "TEMPLATE_BEM_VINDO"
    public static function sendEmailTemplate($template, $assunto, $emailDestino, $parametros = null)
    {
        try
        {
            $s = new HelperAPCRedis();
            $html = null;
            if ($s->exists($template))
            {
                $html = $s->getIfExists($template);
            }
            else
            {
                $path = Helper::formatarPath(Helper::acharRaiz() . "template_email/{$template}.html");
                if (!file_exists($path))
                {
                    HelperLog::logEmailErro("Erro[wefwer32] tentativa de envio de email para $emailDestino. Arquivo $path nao existe.");

                    return false;
                }
                $html = file_get_contents($path);

                if (!strlen($html))
                {
                    HelperLog::logEmailErro("Erro[wefwer32] tentativa de envio de email para $emailDestino. Path: $path. Motivo: Html do Template $template esta VAZIO.");
                    return false;
                }

                $s->add($template, $html);
            }
            $mensagem = $html;
            if (count($parametros))
            {
                foreach ($parametros as $k => $v)
                {
                    $mensagem = str_replace($k, $v, $mensagem);
                }
            }

            $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
            $objEmail->setAssunto($assunto);
            $objEmail->setConteudo($mensagem, false);
            return $objEmail->enviarEmail();
        }
        catch (Exception $ex)
        {
            HelperLog::logErro($ex);
            HelperLog::logEmailErro("Exception $emailDestino. Ex: " . Helper::getDescricaoException($ex));
            return false;
        }
    }
}


<?php

    class Seguranca_Funcionalidade
    {

        public $identificadorUnicoDaFuncionalidade;
        public $classe;
        public $metodo;
        public $restricoesNoObjeto;
        public $nomeFuncionalidade;
        public $paginasQueTemPermissao;
        public $variaveisEValoresPermitidos;
        public $somenteSuaPropriaArea;

        public function __construct()
        {
            $this->paginasQueTemPermissao = array();
            $this->somenteSuaPropriaArea = false;
        }

        public function adicionarPaginaQueTemPermissao($diretorio, $paginaSemExtensao, $arrayDeVariavelEValor = false)
        {
            $pagina = "{$diretorio}/$paginaSemExtensao";
            $complemento = "";

            $pagina .= $complemento;

            $this->paginasQueTemPermissao[] = $pagina;
            $this->variaveisEValoresPermitidos[] = $arrayDeVariavelEValor;
        }

        public function adicionarRestricaoNoObjeto($atributo, $valor)
        {
            $this->restricoesNoObjeto[ $atributo ] = $valor;
        }

        public function limparListaDePaginasQueTemPermissao()
        {
            $this->paginasQueTemPermissao = array();
        }

        public function getId()
        {
            return $this->identificadorUnicoDaFuncionalidade;
        }

        public function __clone()
        {
        }

    }

?>

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class SingletonLog
    {

        private static $fp = null;

        public function __construct()
        {
        }

        public static function init()
        {
            if (MODO_DE_DEPURACAO && (self::$fp == null || self::$fp == false))
            {
                $raiz = Helper::acharRaiz();
                $path = $raiz . "/conteudo/";
                $path .= "sincronizacoes_" . Helper::getDiaNomeArquivo() . ".log";
                if (!file_exists($path))
                {
                    self::$fp = fopen($path, "w");
                }
                else
                {
                    self::$fp = fopen($path, "a");
                }

            }

        }

        public static function writeLogStr($str, $msg = null)
        {
            if (!MODO_DE_DEPURACAO)
            {
                return;
            }
            if(self::$fp != null) {
                $str = "[" . Helper::getDiaEHoraEMilisegundoAtual() . "] - $str.";
                if ($msg != null) {
                    $str .= " Resultado: " . json_encode($msg) . " \n";
                } else {
                    $str .= " \n ";
                }

                fwrite(self::$fp, $str);
                fflush(self::$fp);
            }
        }

        public static function writeLogException($exc)
        {
            if (!MODO_DE_DEPURACAO)
            {
                return;
            }
            if(self::$fp != null){
                if ($exc != null && $exc instanceof Exception)
                {
                    $desc = "Message: " . $exc->getMessage() . ". Stacktrace: " . $exc->getTraceAsString();
                }
                else
                {
                    if ($exc != null)
                    {
                        $desc = "Exception: " . print_r($exc, true);
                    }
                }

                $str = "[" . Helper::getDiaEHoraEMilisegundoAtual() . "] - $desc \n";
                fwrite(self::$fp, $str);
                fflush(self::$fp);
            }

        }

        public static function writeMessage($msg)
        {
            if (!MODO_DE_DEPURACAO)
            {
                return;
            }
            if(self::$fp != null){
                $desc = json_encode($msg);

                $str = "[" . Helper::getDiaEHoraEMilisegundoAtual() . "] - $desc \n";
                fwrite(self::$fp, $str);
                fflush(self::$fp);
            }

        }

        public static function close()
        {
            if (self::$fp != null)
            {
                fclose(self::$fp);
                self::$fp = null;
            }

        }

    }
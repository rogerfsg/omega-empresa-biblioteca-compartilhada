<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of CacheWebservice
     *
     * @author home
     */
    class SingletonCacheWebservice
    {
        //put your code here

        private $webservices = array();
        static $singleton = null;
        private $redisDbName = null;
        private $startedCaching = false;

        public static function getSingleton()
        {
            if (SingletonCacheWebservice::$singleton == null)
            {
                SingletonCacheWebservice::$singleton = new SingletonCacheWebservice();
            }

            return SingletonCacheWebservice::$singleton;
        }

        public function __construct()
        {
        }

        public function setWebservices($redisDbName, $vetor)
        {
            HelperLog::logRedis("SingletonCacheWebservice - setWebservices - $redisDbName. " . print_r($vetor, true));
            $this->webservices = $vetor;
            $this->redisDbName = $redisDbName;
        }

        public function hasToCache($webservice)
        {
            $hasToCache = isset($this->webservices[ $webservice ]);

            HelperLog::logRedis("SingletonCacheWebservice - hasToCache - $webservice - . $hasToCache");

            return $hasToCache;
        }

        public function getWebserviceKeyContext()
        {
            $webservice = $this->getCurrentWebservice();

            $keys = Helper::getKeysPost();
            $strKey = "";
            foreach ($keys as $key)
            {
                if (strlen($key) > 2 && substr($key, 0, 2) == "id")
                {
                    $strKey .= $key . "_";
                }
            }
            HelperLog::logRedis("SingletonCacheWebservice - getWebserviceKeyContext - {$webservice}{$strKey}");

            return $webservice . $strKey;
        }

        public function getCurrentWebservice()
        {
            return Helper::POSTGET("action");
        }

        public function exitIfCached()
        {
            //O vetor dos webservices devem ser repassados
            $webservice = $this->getCurrentWebservice();
            if ($this->hasToCache($webservice))
            {
                $key = $this->getWebserviceKeyContext();

                $redis = HelperRedis::getSingleton();
                $json = $redis->get($key);
                HelperLog::logRedis("SingletonCacheWebservice - exitIfCached - [$key] - $json");
                if ($json != null)
                {
                    echo $json;
                    exit();
                }
            }
        }

        public function startCachingIfNecessary()
        {
            $webservice = $this->getCurrentWebservice();
            if ($this->hasToCache($webservice)
                && $this->startedCaching)
            {
                ob_start();
                $this->startedCaching = true;
            }
        }

        public function stopCaching()
        {
            if ($this->startedCaching)
            {
                $webservice = $this->getCurrentWebservice();

                $out2 = ob_get_contents();
                ob_end_clean();
                $redis = HelperRedis::getSingleton();
                $redis->set($webservice, $out2);
                $this->startedCaching = false;
            }
        }

        public function setCachingCurrentWebservice($json)
        {
            $webservice = $this->getCurrentWebservice();
            if ($this->hasToCache($webservice))
            {
                $key = $this->getWebserviceKeyContext();
                $redis = HelperRedis::getSingleton();
                HelperLog::logRedis("SingletonCacheWebservice - setCachingCurrentWebservice - [$key] - $json");
                $redis->set($key, $json);
            }
        }
    }

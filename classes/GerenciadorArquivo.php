<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of GerenciadorArquivo
     *
     * @author home
     */
    class GerenciadorArquivo
    {
        const ID_FOTO = "foto";
        const ID_VIDEO = "video";
        const ID_ANEXO = "anexo";
        const ID_AUDIO = "audio";
        const ID_BANCO = "banco";
        const ID_SCRIPT = "script";
        const ID_APK = "apk";
        const ID_DEFAULT = "default";

        public static function getPathDoTipo(
            $pIdNaTabela,
            $pNomeTabela,
            $pIdSistemaTipoDownloadArquivo)
        {
            //C:\wamp\www\BibliotecaNuvem\10002\public_html\conteudo\arquivos\monitora_tela_mobile_para_web\1\default
            //$configuracaoSite = Registry::get('ConfiguracaoSite');
            $pathRaiz = Helper::acharRaiz();
            $strPath = "{$pathRaiz}conteudo/arquivos/{$pNomeTabela}/" . Helper::getPathComBarra($pIdNaTabela) . $pIdSistemaTipoDownloadArquivo . "/";
            if (!is_dir($strPath))
            {
                mkdir($strPath, 0777, true);
            }

            return $strPath;
        }
    }

<?php

    class Generic_DAO_AngularJS
    {

        public $nomeClasse;
        public $nomeTabela;
        public $campoId;
        public $campoLabel;
        public $database;
        public $opcoesDAO = null;

        public function __construct($configDAO = null)
        {
            $db = null;
            $configDatabase = null;

            if ($configDAO != null)
            {
                $strClassDAO = get_class($configDAO);
                if ($strClassDAO == "ConfiguracaoDAO")
                {
                    $db = $configDAO->db;
                    $configDatabase = $configDAO->configDatabase;
                    $this->opcoesDAO = $configDAO->opcoesDAO;
                }
                else
                {
                    if ($strClassDAO == "Database")
                    {
                        $db = $configDAO;
                    }
                }
            }

            if ($db != null)
            {
                $this->database = $db;
            }
            else
            {
                $this->database = new Database($configDatabase);
            }
        }

        public static $databaseFieldNames;

        public static function getDatabaseFieldName($propertyName)
        {
            if (Helper::isNullOrEmpty(static::$databaseFieldNames))
            {
                static::setDatabaseFieldNames();
            }

            return static::$databaseFieldNames->$propertyName;
        }

        public static $databaseFieldTypes;

        public static function getDatabaseFieldType($propertyName)
        {
            if (Helper::isNullOrEmpty(static::$databaseFieldTypes))
            {
                static::setDatabaseFieldTypes();
            }

            return static::$databaseFieldTypes->$propertyName;
        }

        public static $databaseFieldsRelatedAttributes;

        public static function getDatabaseFieldRelatedAttribute($databaseFieldName)
        {
            if (Helper::isNullOrEmpty(static::$databaseFieldsRelatedAttributes))
            {
                static::setDatabaseFieldsRelatedAttributeNames();
            }

            return isset(static::$databaseFieldsRelatedAttributes->$databaseFieldName) ? static::$databaseFieldsRelatedAttributes->$databaseFieldName : $databaseFieldName;
        }

        public static function getOtherTableDatabaseFieldRelatedAttribute($fieldAlias)
        {
            $partes = explode("__", $fieldAlias);

            $tableName = $partes[0];
            $fieldName = $partes[1];
        }

        public static function montarChavePrimariaParaWhere($objChavePrimaria)
        {
            $arrCondicoes = array();

            if (is_numeric($objChavePrimaria))
            {
                return "id = {$objChavePrimaria}";
            }

            foreach ($objChavePrimaria as $key => $value)
            {
                $arrCondicoes[] = "{$key} = {$value}";
            }

            return implode(" AND ", $arrCondicoes);
        }

        public static function isOperatorValid($operador)
        {
            return in_array($operador, array('STARTS_WITH', 'CONTAINS', 'ENDS_WITH', 'EQUALS', 'NOT_EQUALS', 'GREATER_THAN', 'LESS_THAN', 'GREATHER_THAN_OR_EQUALS', 'LESS_THAN_OR_EQUALS'));
        }

        public static function getOperatorWithTextValue($valorTexto, $operador)
        {
            $strRetorno = "";
            switch ($operador)
            {
                case 'STARTS_WITH':
                    $strRetorno = "LIKE '{$valorTexto}%'";
                    break;

                case 'CONTAINS':
                    $strRetorno = "LIKE '%{$valorTexto}%'";
                    break;

                case 'ENDS_WITH':
                    $strRetorno = "LIKE '%{$valorTexto}'";
                    break;

                case 'EQUALS':
                    $strRetorno = " = '{$valorTexto}'";
                    break;

                case 'NOT_EQUALS':
                    $strRetorno = " <> '{$valorTexto}'";
                    break;

                case 'GREATER_THAN':
                    $strRetorno = " > '{$valorTexto}'";
                    break;

                case 'LESS_THAN':
                    $strRetorno = " < '{$valorTexto}'";
                    break;

                case 'GREATHER_THAN_OR_EQUALS':
                    $strRetorno = " >= '{$valorTexto}'";
                    break;

                case 'LESS_THAN_OR_EQUALS':
                    $strRetorno = " <= '{$valorTexto}'";
                    break;

                default:
                    $strRetorno = "{$operador} '{$valorTexto}'";
                    break;
            }

            return $strRetorno;
        }

        public static function getOperatorWithNumericValue($valorNumerico, $operador)
        {
            switch ($operador)
            {
                case 'EQUALS':
                    $strRetorno = " = {$valorNumerico}";
                    break;

                case 'NOT_EQUALS':
                    $strRetorno = " <> {$valorNumerico}";
                    break;

                case 'GREATER_THAN':
                    $strRetorno = " > {$valorNumerico}";
                    break;

                case 'LESS_THAN':
                    $strRetorno = " < {$valorNumerico}";
                    break;

                case 'GREATHER_THAN_OR_EQUALS':
                    $strRetorno = " >= {$valorNumerico}";
                    break;

                case 'LESS_THAN_OR_EQUALS':
                    $strRetorno = " <= {$valorNumerico}";
                    break;

                default:
                    $strRetorno = "{$operador} {$valorNumerico}";
                    break;
            }

            return $strRetorno;
        }

        public static function getWhereClause($parametroFiltro, $aliasTabelaPrincipal = null)
        {
            $strRetorno = null;
            $atributoClasse = $parametroFiltro->attribute;
            $valor = $parametroFiltro->value;
            $operador = trim(strtoupper($parametroFiltro->operator));

            if (is_object($valor) && isset($valor->primaryKey))
            {
                $valor = $valor->primaryKey->id;
            }

            if (static::isOperatorValid($operador))
            {
                $campoBanco = isset(static::$databaseFieldNames->$atributoClasse) ? static::$databaseFieldNames->$atributoClasse : $atributoClasse;
                $tipoCampo = static::$databaseFieldTypes->$atributoClasse;
                $strAlias = is_null($aliasTabelaPrincipal) ? "" : "{$aliasTabelaPrincipal}.";

                switch ($tipoCampo)
                {
                    case "TEXT":
                    case "DATE":

                        if ($valor == null && trim($valor) == '')
                        {
                            $strRetorno = null;
                        }
                        else
                        {
                            $operadorMaisValor = static::getOperatorWithTextValue($valor, $operador);
                            $strRetorno = "{$strAlias}{$campoBanco} {$operadorMaisValor}";
                        }

                        break;

                    case "NUMERIC":
                    case "INTEGER":
                    case "INT":

                        if ($valor == null)
                        {
                            $strRetorno = null;
                        }
                        else
                        {
                            $operadorMaisValor = static::getOperatorWithNumericValue($valor, $operador);
                            $strRetorno = "{$strAlias}{$campoBanco} {$operadorMaisValor}";
                        }

                        break;
                }
            }

            return $strRetorno;
        }

        public static function getWhereClauseForFilter($arrParametrosFiltro, $aliasTabelaPrincipal = null)
        {
            $strRetorno = "";
            if (Helper::isNullOrEmpty($arrParametrosFiltro))
            {
                return $strRetorno;
            }

            $arrCondicoes = array();
            foreach ($arrParametrosFiltro as $parametroFiltro)
            {
                $condicao = static::getWhereClause($parametroFiltro, $aliasTabelaPrincipal);
                if (!is_null($condicao))
                {
                    $arrCondicoes[] = $condicao;
                }
            }

            $strRetorno = implode(" AND ", $arrCondicoes);

            return strlen($strRetorno) > 0 ? "WHERE {$strRetorno}" : "";
        }

        public static function getOrderByClauseForFilter($arrSortingParameters)
        {
            $strRetorno = "";
            if (Helper::isNullOrEmpty($arrSortingParameters))
            {
                return $strRetorno;
            }

            usort($arrSortingParameters, function ($a, $b) {
                return $a->order - $b->order;
            });

            $arrSQLSorting = array();
            foreach ($arrSortingParameters as $sortingParameter)
            {
                $atributoClasse = $sortingParameter->columnName;
                $campoBanco = isset(static::$databaseFieldNames->$atributoClasse) ? static::$databaseFieldNames->$atributoClasse : $atributoClasse;
                $tipoSort = $sortingParameter->sortingType == "ASC" ? "ASC" : "DESC";
                $arrSQLSorting[] = "{$campoBanco} {$tipoSort}";
            }

            $strRetorno = implode(", ", $arrSQLSorting);

            return strlen($strRetorno) > 0 ? "ORDER BY {$strRetorno}" : "";
        }

        public static function getLimitClauseForFilter($paginationParameters)
        {
            $strRetorno = "";
            if (Helper::isNullOrEmpty($arrParametrosFiltro))
            {
                return $strRetorno;
            }

            $offset = $paginationParameters->offset;
            $numberOfRecords = $paginationParameters->numberOfRecords;
            if (is_numeric($offset) && is_numeric($numberOfRecords))
            {
                $strRetorno = "LIMIT {$offset} {$numberOfRecords}";
            }

            return $strRetorno;
        }

        public function mapResultSetDataToList($resultSetData, $objAliasTypes, $isRelatedEntity = false)
        {
            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                foreach ($object as $key => $value)
                {
                    $attributeName = $key;

                    if ($isRelatedEntity)
                    {
                        $currentObject->removed = false;
                        $attributeName = str_replace("{$this->nomeTabela}__", "", $attributeName);
                    }
                    else
                    {
                        $currentObject->allowEdit = true;
                        $currentObject->allowRemove = true;
                        $currentObject->primaryKey = $this->getPrimaryKeyObject($currentObject);
                    }

                    $attributeType = $objAliasTypes->$key;
                    $valueForHuman = static::formatarValorParaExibicao($value, $attributeType);
                    $attributeNameForHuman = "{$attributeName}_forHuman";

                    $currentObject->$attributeName = $value;
                    $currentObject->$attributeNameForHuman = $valueForHuman;
                }

                $returnData[] = $currentObject;
            }

            return $returnData;
        }

        public function setByObject($object)
        {
            foreach ($object as $key => $value)
            {
                $this->$key = is_object($value) ? $value->id : $value;
            }
        }

        public function mapFromDatabaseFetchObject($databaseFetchObject)
        {
            if (Helper::isNullOrEmpty(static::$databaseFieldNames))
            {
                static::setDatabaseFieldNames();
            }

            $arrayMapeamento = Helper::objectToArray(static::$databaseFieldNames);
            foreach ($databaseFetchObject as $databaseField => $value)
            {
                $classProperty = array_search($databaseField, $arrayMapeamento);
                if (!Helper::isNullOrEmpty($classProperty))
                {
                    $this->$classProperty = $value;
                }
            }
        }

        public function closeDatabaseConnection()
        {
            $this->database->close();
        }

        public function getConfiguracaoDAO()
        {
            $config = new ConfiguracaoDAO();
            $config->db = $this->database;

            return $config;
        }

        public static function isCorporacaoExistenteNaTabela($pNomeTabela, $db = null)
        {
            if (strlen($pNomeTabela) > 0)
            {
                if ($db == null)
                {
                    $objBanco = new Database();
                }
                else
                {
                    $objBanco = $db;
                }

                $objBanco->query("SHOW COLUMNS FROM $pNomeTabela");
                $vVetorColuna = Helper::getResultSetToArrayDeUmCampo($objBanco->result);
                if (array_search("corporacao_id_INT", $vVetorColuna))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public static function getStrQueryWhereUnique($pObjTable, $pVetorNomeAtributo)
        {
            if (count($pVetorNomeAtributo) == 0 || $pObjTable == null)
            {
                return null;
            }
            $vQuery = "WHERE ";
            $vQueryWhere = "";

            foreach ($pVetorNomeAtributo as $vAttrName)
            {
                $vValorAtributo = call_user_func(array($pObjTable, "get_" . ucfirst($vAttrName)), array());
                if (strlen($vValorAtributo))
                {
                    if (strlen($vQueryWhere) == 0)
                    {
                        $vQueryWhere .= $vAttrName . " = " . $vAttrValue;
                    }
                    else
                    {
                        $vQueryWhere .= " AND " . $vAttrName . " = '" + $vAttrValue + "' ";
                    }
                }
            }

            return $vQuery + $vQueryWhere;
        }

        /// i come�a de 1
        public function addOrEditEndereco($db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $idPais = EXTDAO_Pais::cadastraSeNecessario(Helper::POSTGET("pais"), $db);
            $idEstado = EXTDAO_Uf::cadastraSeNecessario($idPais, Helper::POSTGET("estado"), $db);
            $idCidade = EXTDAO_Cidade::cadastraSeNecessario($idEstado, Helper::POSTGET("cidade"), $db);
            $idBairro = EXTDAO_Bairro::cadastraSeNecessario($idCidade, Helper::POSTGET("bairro"), $db);

            $obj = array();
            $obj["idCidade"] = $idCidade;
            $obj["idBairro"] = $idBairro;

            return $obj;
        }

        public function cadastraCampoLabelSeNecessario($token = null)
        {
            $token = strtoupper($token);
            $id = static::getIdPeloCampoLabel($token);
            if (!strlen($id))
            {
                $obj = $this->factory();
                Helper::set($obj, array($this->campoLabel => $token));

                $obj->formatarParaSQL();
                $obj->insert(true);

                return $obj->getIdDoUltimoRegistroInserido();
            }
            else
            {
                return $id;
            }
        }

        public function getIdPeloCampoLabel($token)
        {
            $q = "SELECT id "
                . " FROM " . $this->nomeTabela
                . " WHERE " . $this->campoLabel . " = '" . $token . "'";

            $this->database->query($q);

            return $this->database->getPrimeiraTuplaDoResultSet(0);
        }

        public function getObjsAutocomplete($campoSelect = null)
        {
            if ($campoSelect == null)
            {
                $campoSelect = $this->campoLabel;
            }
            $q = "SELECT id, " . $this->campoLabel . " nome "
                . " FROM " . $this->nomeTabela;

            $this->database->query($q);
            $objs = Helper::getResultSetToMatriz($this->database->result, 1, 0);

            return $objs;
        }


        //*******************************************
        //Fun��es para formata��o de dados
        //*******************************************
        public static function formatarFloatParaComandoSQL($numero)
        {
            return Helper::formatarFloatParaComandoSQL($numero);
        }

        public static function formatarFloatParaExibicao($numero, $casasDecimais = 2)
        {
            return Helper::formatarFloatParaExibicao($numero, $casasDecimais);
        }

        public static function formatarDataParaComandoSQL($valor)
        {
            return Helper::formatarDataParaComandoSQL($valor);
        }

        public static function formatarDataParaExibicao($valor)
        {
            return Helper::formatarDataParaExibicao($valor);
        }

        public static function formatarHoraParaComandoSQL($valor)
        {
            return Helper::formatarHoraParaComandoSQL($valor);
        }

        public static function formatarHoraParaExibicao($valor)
        {
            return Helper::formatarHoraParaExibicao($valor);
        }

        public static function formatarDataTimeParaComandoSQL($valor)
        {
            return Helper::formatarDataTimeParaComandoSQL($valor);
        }

        public static function formatarDataTimeParaExibicao($valor)
        {
            return Helper::formatarDataTimeParaExibicao($valor);
        }

        public static function formatarStringParaComandoSQL($valor)
        {
            return Helper::formatarStringParaComandoSQL($valor);
        }

        public static function formatarValorParaExibicao($valor, $tipoCampo)
        {
            switch ($tipoCampo)
            {
                case "INT":
                    return $valor;
                    break;

                case "FLOAT":
                    return static::formatarFloatParaExibicao($valor);
                    break;

                case "DATE":
                    return static::formatarDataParaExibicao($valor);
                    break;

                case "TIME":
                    return static::formatarHoraParaExibicao($valor);
                    break;

                case "DATETIME":
                    return static::formatarDataTimeParaExibicao($valor);
                    break;

                case "TEXT":
                default:
                    return $valor;
                    break;
            }
        }

        public function formatarDados($valor)
        {
            if ($valor == null)
            {
                return null;
            }
            else
            {
                $tratado = str_replace("\'", "'", $valor);

                return $tratado;
            }
        }

        public function formatarDadosParaSQL($valor)
        {
            if ($valor == null)
            {
                return "null";
            }
            else
            {
                if (!strlen($valor))
                {
                    return "null";
                }
                else
                {
                    $tratado = str_replace("'", "\'", $valor);

                    return "'" . $tratado . "'";
                }
            }
        }

        //************************************************************
        //METODO QUE RETORNA ID DO ULTIMO REGISTRO CRIADO NA TABELA
        //************************************************************
        function getIdDoUltimoRegistroInserido($pIdCorporacao = null)
        {
            $queryCorporacao = "";
            if ($this->nomeTabela != "corporacao"
                && Generic_DAO::isCorporacaoExistenteNaTabela($this->nomeTabela, $this->database)
            )
            {
                $vIdCorporacao = null;
                if ($pIdCorporacao == null)
                {
                    $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
                }
                else
                {
                    $vIdCorporacao = $pIdCorporacao;
                }

                if (strlen($vIdCorporacao))
                {
                    $queryCorporacao .= " WHERE corporacao_id_INT = " . $vIdCorporacao;
                }
            }

            $sql = "SELECT MAX({$this->campoId}) FROM {$this->nomeTabela} {$queryCorporacao}";
            $result = $this->database->query($sql);

            if ($this->database->rows > 0)
            {
                $retorno = Database::mysqli_result($result, 0, 0);
            }
            else
            {
                $retorno = null;
            }

            return $retorno;
        }

        function selectUltimoRegistroInserido($pIsCorporacaoSetada = true)
        {
            $queryCorporacao = "";
            if ($pIsCorporacaoSetada && Generic_DAO::isCorporacaoExistenteNaTabela($this->nomeTabela))
            {
                $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
                if (strlen($vIdCorporacao))
                {
                    $queryCorporacao .= " WHERE corporacao_id_INT = " . $vIdCorporacao;
                }
            }

            $sql = "SELECT MAX({$this->campoId}) FROM {$this->nomeTabela} $queryCorporacao";
            $result = $this->database->query($sql);
            if ($this->database->rows > 0)
            {
                $id = Database::mysqli_result($result, 0, 0);
                $this->select($id);

                return;
            }
            else
            {
                return;
            }
        }

        public function updateCampo($objChavePrimaria, $campo, $valor)
        {
            $strWhere = $this->montarChavePrimariaParaWhere($objChavePrimaria);
            if (!Helper::isNullOrEmpty($strWhere) && strlen($campo))
            {
                $this->database->query("UPDATE {$this->nomeTabela} SET {$campo}='{$valor}' WHERE {$strWhere}");
            }
        }

        public function getPrimaryKeyObject($fullObject)
        {
            if (is_null($fullObject))
            {
                $fullObject = $this;
            }

            $objRetorno = new stdClass();
            foreach (static::$arrPK as $pkProperty)
            {
                $objRetorno->$pkProperty = $fullObject->$pkProperty;
            }

            return $objRetorno;
        }

    }

?>

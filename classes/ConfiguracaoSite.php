<?php

    /**
     * Created by PhpStorm.
     * User: W10
     * Date: 02/08/2017
     * Time: 12:41
     */
    class ConfiguracaoSite
    {
        public $NOME_CORPORACAO;
        public $IDENTIFICADOR_SESSAO;
        public $TITULO_PAGINAS;

        public $NOME_BANCO_DE_DADOS_SECUNDARIO;
        public $BANCO_DE_DADOS_SECUNDARIO_HOST;
        public $BANCO_DE_DADOS_SECUNDARIO_PORTA;
        public $BANCO_DE_DADOS_SECUNDARIO_USUARIO;
        public $BANCO_DE_DADOS_SECUNDARIO_SENHA;

        public $NOME_BANCO_DE_DADOS_PRINCIPAL;
        public $BANCO_DE_DADOS_HOST;
        public $BANCO_DE_DADOS_PORTA;
        public $BANCO_DE_DADOS_USUARIO;
        public $BANCO_DE_DADOS_SENHA;

        public $NOME_BANCO_DE_DADOS_DE_CONFIGURACAO;
        public $BANCO_DE_DADOS_CONFIGURACAO_HOST;
        public $BANCO_DE_DADOS_CONFIGURACAO_PORTA;
        public $BANCO_DE_DADOS_CONFIGURACAO_USUARIO;
        public $BANCO_DE_DADOS_CONFIGURACAO_SENHA;

        public $PATH_CONTEUDO;

        public $ID_SISTEMA_SIHOP;
        public $ID_CORPORACAO;

        public static function getPathConteudo()
        {
            $configuracaoSite = Registry::get('ConfiguracaoSite');

            return $configuracaoSite->PATH_CONTEUDO;
        }

        public static function getCorporacao()
        {
            $configuracaoSite = Registry::get('ConfiguracaoSite');

            return $configuracaoSite->NOME_CORPORACAO;
        }

//    public static function getPathSistemaSequencia(){
//        $configuracaoSite = Registry::get('ConfiguracaoSite');
//        return $configuracaoSite->PATH_CONTEUDO.$configuracaoSite->BANCO_DE_DADOS_HOST."_".$configuracaoSite->NOME_BANCO_DE_DADOS_PRINCIPAL;
//    }

    }
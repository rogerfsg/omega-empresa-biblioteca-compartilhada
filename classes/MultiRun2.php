<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 15/09/2017
 * Time: 21:10
 */
class MultiRun2
{

    private $pathRaizPipeline;
    private $classes;
    private $categorias;
    public function __construct($pathRaizPipeline, $classes, $categorias) {

        $this->pathRaizPipeline = "C:\wamp64\www\SincronizadorWeb\SW10002Corporacao\adm\pipeline.php";
        $this->pathRaizPipeline = "/var/www/sincronizador/public_html/adm/pipeline.php";
        $this->pathRaizPipeline = $pathRaizPipeline;
        $this->classes = $classes;
        $this->categorias = $categorias;

    }
    public function run(){
        try{
            $tot = 0;
            for($i = 0 ; $i < count($this->categorias); $i++){
                for($j = 0 ; $j < count($this->classes); $j++){
                    $s = $this->categorias[$i];
                    $c = $this->classes[$j];
                    $this->initProccess($c, $s);
                    $tot++;
                }
            }
            if($tot == 0)
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "N�o existiam processos a serem inicializados, reveja as constantes para o construtor do MultiRun2: CLASSES_PIPELINE, CATEGORIAS_PIPELINE e PATH_RAIZ_PIPELINE");
            else
            return new Mensagem(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                "Run finalizado com sucesso. Ao todo $tot processos foram inicializados");
        }catch(Exception $ex){
            return new Mensagem(null,null,$ex);
        }
    }

    public function kill(){
        try{
            $tot = 0;
            for($i = 0 ; $i < count($this->categorias); $i++){
                for($j = 0 ; $j < count($this->classes); $j++){
                    $s = $this->categorias[$i];
                    $c = $this->classes[$j];
                    $this->killProccess($c, $s);
                    $tot ++;
                }
            }

            if($tot == 0)
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "N�o existiam processos a serem finalziados");
            else
                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Ao todo foram finalizados $tot processos");
        }catch(Exception $ex){
            return new Mensagem(null,null,$ex);
        }
    }

    private function initProccess($classe, $categoria){
        try{
            if($this->necessitaStartarNovoProcesso($classe, $categoria)){

                return Helper::execInBackground( "php {$this->pathRaizPipeline} -c $classe -a run  $categoria");
            } else
                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        "Processo j� estava executando");


        }catch(Exception $ex){
            return new Mensagem(null,null,$ex);
        }

    }



    private function necessitaStartarNovoProcesso($classe, $categoria){
        $pathFile = self::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe."_". $categoria);
        if(file_exists($pathFile)){
            $strprocessos = file_get_contents($pathFile);
            if(strlen($strprocessos)){
                $processos = explode(';', $strprocessos);
                if(count($processos) > 1){
                    unset($processos[0]);

                    $idsRunning  = Helper::getPidsRunning($processos);

                    if($idsRunning != null && count($idsRunning) == 1){

                        return false;
                    }else{
                        Helper::killAll($processos);
                        unlink($pathFile);
                        return true;
                    }
                }
            }
        }
        return true;


    }
    
    private function killProccess($classe, $categoria){
        try{
            $pathFile = self::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe."_". $categoria);
            if(file_exists($pathFile)){
                $strprocessos = file_get_contents($pathFile);
                if(strlen($strprocessos)){
                    $processos = explode(';', $strprocessos);
                    if(count($processos)){
                        unset($processos[0]);
                        Helper::killAll($processos);
                        unlink($pathFile);
                    }
                }
                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Os seguintes processos foram eliminados: $strprocessos");
            }else
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "N�o existiam processos no hist�rico.");
        }catch(Exception $ex){
            HelperLog::logErro($ex );
            return new Mensagem(null,null,$ex);
        }
    }

    public static function factory() {

        $classes = explode(",", CLASSES_PIPELINE);
        $categorias = explode(",", CATEGORIAS_PIPELINE);


        return new MultiRun2(PATH_RAIZ_PIPELINE, $classes ,  $categorias);
    }

    public static function PATH_LOG_PROCESSOS_EM_EXECUCAO($identificador){

        $dir = Helper::acharRaiz()."conteudo/multirun2";
        if(!is_dir($dir))
            Helper::mkdir($dir, 0777, true);
        return "{$dir}/{$identificador}.log";
    }


}

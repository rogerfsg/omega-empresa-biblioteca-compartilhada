<?php

    define('MENUCONFIG_LISTA_HORIZONTAL', 1);
    define('MENUCONFIG_LISTA_VERTICAL', 2);
    define('MENUCONFIG_MENU_ABA', 3);
    define('MENUCONFIG_DROPDOWN', 4);

    class MenuConfig
    {

        public static $pathImagensMenu;
        public $label;
        public $tipo;
        public $accesskey;
        public $link;
        public $imagem;

        public function getTipo()
        {
            return $this->tipo;
        }

        public function getAccessKey()
        {
            return $this->accesskey;
        }

        public function getImagem()
        {
            return $this->imagem;
        }

        public static $contador;

        public function __construct($label, $tipo, $imagem = IMAGEM_PADRAO_MENU, $nivel = false, $accesskey = true, $link = true)
        {
            $this->label = $label;
            $this->tipo = $tipo;

            if ($link === true && strlen($this->label))
            {
                $this->gerarLink($label);
            }

            if (is_numeric($nivel) && $nivel !== false)
            {
                $this->gerarAccessKey($nivel);
            }

            if ($imagem !== false)
            {
                $this->imagem = MenuConfig::getPathImagensMenu() . $imagem;
            }
            $this->accesskey = $accesskey;
        }

        public static function getPathImagensMenu()
        {
            if (self::$pathImagensMenu)
            {
                return self::$pathImagensMenu;
            }
            else
            {
                $niveisAbaixo = Helper::getStringNiveisAteRaiz();
                self::$pathImagensMenu = "{$niveisAbaixo}adm/imgs/menu/";

                return self::$pathImagensMenu;
            }
        }

        public function getIcone()
        {
            return $this->link;
        }

        public function gerarLink($label)
        {
            $this->link = "#" . str_replace(" ", "", $label);
        }

        public function getLabel()
        {
            return $this->label;
        }

        public function gerarAccessKey($nivel)
        {
            if (isset(self::$contador[ $nivel ]))
            {
                self::$contador[ $nivel ]++;
            }
            else
            {
                self::$contador[ $nivel ] = 1;
            }

            $this->accesskey = self::$contador[ $nivel ];
        }

    }

<?php

    class Crypt extends AES
    {
        const TEMPO_DEFAULT_MINUTOS_PARA_EXPIRAR_LINK_CRIPT = 1440;
        var $key128;
        var $key192;
        var $key256;
        const CHAVE_EXPIRADA = -1;
        const SUFIXO = "OMEGASOFTWAREBELOHORIZONTE";

        public function __construct()
        {
            $this->key128 = "2b7e151628aed2a6abf7158809cf4f3c";
            $this->key192 = "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b";
            $this->key256 = "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";

            parent::__construct();
        }

        public function geraChaveTime($chaves, $valores, $tempoLimiteParaExpirarMinutos = 10)
        {
            $str = null;
            $obj = array();
            if (count($valores))
            {
                for ($i = 0; $i < count($valores); $i++)
                {
                    $obj[ $chaves[ $i ] ] = $valores[ $i ];
                }

                $obj["__DATA_INICIO"] = Helper::getDiaEHoraAtualSQL();
                $obj["__TEMPO_LIMITE_PARA_EXPIRAR_MINUTOS"] = $tempoLimiteParaExpirarMinutos;
                $json = json_encode($obj);

                return $this->crypt($json);
            }
            else
            {
                return null;
            }
        }

        public function validaChaveTime($campoC)
        {
            $campo = $this->decrypt($campoC);
            $obj = json_decode($campo);

            if (!$obj)
            {
                return Mensagem(PROTOCOLO_SISTEMA::CHAVE_INVALIDA, "Chave invalida");
            }
            $dataSomada = Helper::somaSegundosAData($obj->__DATA_INICIO, $obj->__TEMPO_LIMITE_PARA_EXPIRAR_MINUTOS * 60);
            $now = Helper::getDiaEHoraAtualSQL();

            //passou da validade a data
            if ($dataSomada >= $now)
            {
                return new Mensagem_protocolo(
                    $obj,
                    PROTOCOLO_SISTEMA::CHAVE_TEMPO_EXPIRADA,
                    "Chave expirada");
            }
            else
            {
                return new Mensagem_protocolo($obj);
            }
        }

        function decryptGet($campoC)
        {

            if (strlen($campoC))
            {
                HelperLog::logCrypt("decryptGet: $campoC ...");

                $campo = $this->decrypt($campoC);

                if (strlen(Crypt::SUFIXO))
                {
                    if (strlen($campo) < strlen(Crypt::SUFIXO))
                    {
                        return null;
                    }
                    $sufixoCampo = substr($campo, 0, strlen(Crypt::SUFIXO));

                    if (strcmp($sufixoCampo, Crypt::SUFIXO) == 0)
                    {
                        $campo = substr($campo, strlen(Crypt::SUFIXO));
                    }
                    else
                    {
                        HelperLog::logCrypt("decryptGet: Sufixo invalido! ".print_r($campo, true));
                        return null;
                    }
                }
                $retorno = array();
                $parametros = explode("&", $campo);

                $chaves = array();
                $valores = array();
                $hash = array();
                for ($i = 0; $i < count($parametros); $i++)
                {
                    if (strlen($parametros[ $i ]))
                    {
                        $chaveValor = explode("=", $parametros[ $i ]);
//                        echo "CHAVE VALOR: ";
//                        print_r($chaveValor);
                        if (count($chaveValor) == 2)
                        {
                            $chaveParametro = $chaveValor[0];

                            if (strlen($chaveParametro))
                            {
                                $chaves[ count($chaves) ] = $chaveParametro;
                                $valores[ $chaveParametro ] = $chaveValor[1];
                                $hash[ $chaveParametro ] = $chaveValor[1];
                            }
                        }
                        else
                        {
                            if (count($chaveValor) == 1)
                            {
                                $chaveParametro = $chaveValor[0];

                                if (strlen($chaveParametro))
                                {
                                    $chaves[ count($chaves) ] = $chaveParametro;
                                    $valores[ $chaveParametro ] = null;
                                    $hash[ $chaveParametro ] = null;
                                }
                            }
                        }
                    }
                }

                $retorno['chaves'] = $chaves;
                $retorno['valores'] = $valores;
                $retorno['hash'] = $hash;

                $d = new DateTime($retorno['hash']['__data_limite']);

                $now = new DateTime();
                $now->setTimezone(new DateTimeZone('UTC'));

                if ($now > $d)
                {
                    HelperLog::logCrypt("decryptGet: Chave expirada $now > $d! ".print_r($retorno, true));
                    return Crypt::CHAVE_EXPIRADA;
                }
                else
                {
                    HelperLog::logCrypt("decryptGet: Ok! ".print_r($retorno, true));
                    return $retorno;
                }
            }
            else
            {
                HelperLog::logCrypt("decryptGet: $campoC - nulo!");
                return null;
            }
        }

        function cryptGet(
            $chaves,
            $valores,
            $minutosParaExpiracao = Crypt::TEMPO_DEFAULT_MINUTOS_PARA_EXPIRAR_LINK_CRIPT)
        {
            if (count($chaves) && count($valores)
                && count($chaves) == count($valores))
            {
                $get = Crypt::SUFIXO;
                for ($i = 0; $i < count($chaves); $i++)
                {
                    $get .= "&";
                    $get .= $chaves[ $i ] . "=" . $valores[ $i ];
                }

                $time = new DateTime();
                $time->setTimezone(new DateTimeZone('UTC'));
                $format = 'PT' . $minutosParaExpiracao . 'M';

                $time->add(new DateInterval($format));

                $get .= "&__data_limite=" . $time->format('Y-m-d H:i');

                return $this->crypt($get);
            }
            else
            {
                return null;
            }
        }

        function cryptHashGet($chavesEValores, $minutosParaExpiracao)
        {
            if (!empty($chavesEValores))
            {
                $get = Crypt::SUFIXO;

                foreach ($chavesEValores as $chave => $valor)
                {
                    $get .= "&";
                    $get .= $chave . "=" . $valor;
                }

                $time = new DateTime();
                $time->setTimezone(new DateTimeZone('UTC'));
                $time->add(new DateInterval('PT' . $minutosParaExpiracao . 'M'));

                $get .= "&__data_limite=" . $time->format('Y-m-d H:i');

                $strC = $this->crypt($get);

                return $strC;
            }
            else
            {
                return null;
            }
        }

        function crypt($t)
        {
            if (!$t)
            {
                return false;
            }
            else
            {
                $tamanho = strlen($t);
                $tcrypt = "";
                if ($tamanho > 16)
                {
                    $iteracoes = ceil($tamanho / 16);

                    for ($i = 0; $i < $iteracoes; $i++)
                    {
                        $pedaco = substr($t, $i * 16, 16);

                        $thex = $this->stringToHex($pedaco);
                        $tcrypt .= $this->encrypt($thex, $this->key128);
                    }
                }
                else
                {
                    $thex = $this->stringToHex($t);
                    $tcrypt = $this->encrypt($thex, $this->key128);
                }

                return $tcrypt;
            }
        }

        function decrypt($t)
        {
            $tstring = "";
            if (!$t)
            {
                return false;
            }
            else
            {
                $tamanho = strlen($t);

                if ($tamanho > 32)
                {
                    $iteracoes = ceil($tamanho / 32);

                    for ($i = 0; $i < $iteracoes; $i++)
                    {
                        $pedaco = substr($t, $i * 32, 32);

                        $tcrypt = $this->decryptAES($pedaco, $this->key128);
                        $tstring .= $this->hexToString($tcrypt);
                    }
                }
                else
                {
                    $tcrypt = $this->decryptAES($t, $this->key128);
                    $tstring = $this->hexToString($tcrypt);
                }

                return $tstring;
            }
        }

    }

?>
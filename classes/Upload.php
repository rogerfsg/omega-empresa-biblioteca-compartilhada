<?php

    class Upload
    {

        public $tamanhoMax;
        public $arrPermitido;
        public $uploadPath;
        public $file;
        public $nome;
        public $erro;
        public $isTemp;

        public function __construct($file = false, $uploadPath = false, $isTemp = false)
        {
            ini_set("post_max_size", "10M");
            ini_set("upload_max_filesize", "10M");

            $this->file = $file;
            $this->uploadPath = $uploadPath;
            $this->isTemp = $isTemp;
        }

        public function uploadFile()
        {
            HelperLog::logUpload("Dados: ".print_r($this, true)." uploadFile ... ");

            if ($this->file["name"] == "")
            {
                $this->erro = "Arquivo n�o encontrado.";
                HelperLog::logUpload($this->nome.":: ERRO! name vazio (file[\"name\"])");

                return false;
            }
            else
            {
                //ALTERAR A PERMISS�O DE ESCRITA DA PASTA
                //@chmod($this->uploadPath, 0777);

                //Se estiver sido setado quais os tipos permitidos
                if (is_array($this->arrPermitido))
                {
                    $permissao = false;

                    foreach ($this->arrPermitido as $permitido)
                    {
                        if ($permitido == $this->file["type"])
                        {
                            $permissao = true;
                        }
                    }

                    if (!$permissao)
                    {
                        $this->erro = "Tipo de arquivo enviado n�o permitido";
                        HelperLog::logUpload($this->nome.":: ERRO! ". $this->erro);
                        return false;
                    }
                }

                if ($this->tamanhoMax != "")
                {
                    if ($this->file["size"] > $this->tamanhoMax)
                    {
                        $this->erro = "O tamanho do arquivo excede o tamanho pr� estabelecido.";
                        HelperLog::logUpload($this->nome.":: ERRRO! ". $this->erro);
                        return false;
                    }
                }

                $nomeArquivoSaida = "";

                if ($this->isTemp)
                {
                    $nomeArquivoSaida = rand(1, 9999) . $this->file["name"];
                }
                else
                {
                    if ($this->nome)
                    {
                        $nomeArquivoSaida = $this->nome;
                    }
                    else
                    {
                        $nomeArquivoSaida = $this->file["name"];
                    }
                }

                if (move_uploaded_file($this->file["tmp_name"], $this->uploadPath . $nomeArquivoSaida) !== false)
                {
                    HelperLog::logUpload($this->nome.":: OK! - ". $nomeArquivoSaida);
                    return $nomeArquivoSaida;
                }
                else
                {
                    $strFile = print_r($this->file, true);
                    $identificador = $this->uploadPath . $nomeArquivoSaida;
                    $this->erro = "Falha no Upload do arquivo para o servidor. Arquivo temporario: {$this->file["tmp_name"]}. Arquivo desejado: $identificador. File: " . $strFile;
                    HelperLog::logUpload($this->nome.":: ERRO! - ". $this->erro );
                    return false;
                }
            }
        }

        public function uploadImagem($largura = "", $altura = "")
        {
            $retorno = $this->uploadFile();

            if ($retorno)
            {
                $pathImagem = $this->uploadPath . $this->nome;
                $objResize = new Image_Resizer($pathImagem);
                if ($largura && $altura)
                {

                    $objResize->resize($largura, $altura, $pathImagem);
                }
                else
                {
                    $objResize->resize_mantendo_proporcao($largura, $altura, $pathImagem);
                }
            }

            return $retorno;
        }

    }

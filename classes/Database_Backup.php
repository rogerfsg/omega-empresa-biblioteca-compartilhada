<?php

    class Database_Backup
    {

        public $databaseConn;
        public $dirBackup;
        public $nomeDoBancoDeDados;
        public $tabelasIgnoradas;

        public function __construct()
        {
            $this->dirBackup = "backups_db/";
            $this->databaseConn = new Database(NOME_BANCO_DE_DADOS_DE_CONFIGURACAO);
            $this->tabelasIgnoradas = array("acesso", "usuario", "usuario_menu", "usuario_privilegio", "usuario_tipo", "usuario_tipo_menu", "usuario_tipo_privilegio");
        }

        public function fazerBackup($niveisAteRaiz = false, $arquivoDoPontoDeInteresse = false, $retornarNomeDoArquivo = false)
        {
            $objBanco = new Database();

            if ($niveisAteRaiz !== false)
            {
                $strNiveis = Helper::getStringNiveisAteRaiz($niveisAteRaiz);
            }
            else
            {
                $strNiveis = Helper::acharRaiz();
            }

            $user = $objBanco->getUser();
            $senha = $objBanco->getSenha();
            $nome = $objBanco->getDBName();
            $host = $objBanco->getHost();

            $data = date("Y.m.d.H.i.s");

            if ($arquivoDoPontoDeInteresse !== false)
            {
                $arquivo = $arquivoDoPontoDeInteresse;
                $this->dirBackup = "pontos_interesse/";
            }
            else
            {
                $arquivo = $nome . "." . $data . ".sql";
            }

            $this->nomeDoBancoDeDados = $nome;

            $path = $strNiveis . Helper::getPathComBarra($this->dirBackup) . ".htaccess";

            $path = str_replace(".htaccess", $arquivo, ($path));

            $tabelasIgnoradas = $this->getStringDeTabelasIgnoradas();

            $resp = "mysqldump --host={$host} --user={$user} --password={$senha} --databases {$nome} {$tabelasIgnoradas} --compatible=ansi --skip-opt generator > {$path}";

            $output = Helper::shellExec($resp);

            if ($output !== false)
            {
                if ($arquivoDoPontoDeInteresse === false && $retornarNomeDoArquivo === false)
                {
                    //fechando as permissoes do arquivo
                    chmod($path, 0000);

                    $descricao = Helper::POST("descricao");

                    $respostaMysql = Helper::nl2br($output);

                    $this->databaseConn->query("INSERT INTO banco_dados(nome_arquivo, descricao, resposta_mysql, data_criacao_DATETIME)
                                            VALUES('{$arquivo}', '{$descricao}', '{$respostaMysql}', NOW())");
                }

                if ($retornarNomeDoArquivo)
                {
                    return $arquivo;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public function fazerBackupDeCertasTabelas($arrTabelas, $nomeArquivoSaida)
        {
            $objBanco = new Database();

            $strNiveis = Helper::acharRaiz();
            $path = $strNiveis . Helper::getPathComBarra($this->dirBackup) . "{$nomeArquivoSaida}";

            $user = $objBanco->getUser();
            $senha = $objBanco->getSenha();
            $nome = $objBanco->getDBName();
            $host = $objBanco->getHost();

            if (is_array($arrTabelas))
            {
                $strTabelas = implode(" ", $arrTabelas);

                $resp = "mysqldump --host={$host} --user={$user} --password={$senha} {$nome} {$strTabelas} > {$path}";

                $output = Helper::shellExec($resp);

                if ($output !== false)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public function restaurarBackup($idBackup)
        {
            if (!$idBackup)
            {
                return false;
            }

            $objBanco = new Database();

            $user = $objBanco->getUser();
            $senha = $objBanco->getSenha();
            $nome = $objBanco->getDBName();
            $host = $objBanco->getHost();

            $this->databaseConn->query("SELECT nome_arquivo FROM banco_dados WHERE id={$idBackup}");

            $dados = $this->databaseConn->fetchArray();

            $strNiveis = Helper::acharRaiz();

            if ($this->databaseConn->rows > 0)
            {
                $nomeArquivo = $dados[0];
                $pathArquivo = "{$strNiveis}{$this->dirBackup}{$nomeArquivo}";

                //alterando as permissoes do arquivo
                chmod($nomeArquivo, 0666);

                $resp = "mysql --host={$host} --user={$user} --password={$senha} --database {$nome} < {$strNiveis}{$this->dirBackup}{$nomeArquivo}";

                if (Helper::shellExec($resp) !== false)
                {
                    $this->databaseConn->query("UPDATE banco_dados SET data_ultima_restauracao_DATETIME=NOW() WHERE id={$idBackup}");

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public function executarArquivoSQL($bancoDeDados, $nomeArquivo)
        {
            $strNiveis = Helper::acharRaiz();
            $path = $strNiveis . Helper::getPathComBarra($this->dirBackup) . "{$nomeArquivo}";

            $objBanco = new Database();

            $user = $objBanco->getUser();
            $senha = $objBanco->getSenha();
            $host = $objBanco->getHost();

            $resp = "mysql --host={$host} --user={$user} --password={$senha} --database {$bancoDeDados} < {$path}";

            if (Helper::shellExec($resp) !== false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function getStringDeTabelasIgnoradas()
        {
            $strRetorno = "";

            if (strlen($this->nomeDoBancoDeDados))
            {
                foreach ($this->tabelasIgnoradas as $valor)
                {
                    $strRetorno .= "--ignore-table={$this->nomeDoBancoDeDados}.{$valor} ";
                }
            }

            return $strRetorno;
        }

    }

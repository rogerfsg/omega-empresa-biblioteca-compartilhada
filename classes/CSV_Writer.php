<?php

    class CSV_Writer
    {

        public $pathArquivo = "";
        public $pathArquivoAPartirRaiz = "";
        public $database;
        private $conteudoCSV = "";
        private $quebraLinha;
        private $downloadDireto;

        public function __construct($pathDiretorioAPartirRaiz = false, $arquivoSaida = false, $downloadDireto = false)
        {
            if ($pathDiretorioAPartirRaiz && $arquivoSaida)
            {
                $this->pathArquivo = Helper::acharRaiz() . Helper::getPathComBarra($pathDiretorioAPartirRaiz) . $arquivoSaida;
                $this->pathArquivoAPartirRaiz = Helper::getPathComBarra($pathDiretorioAPartirRaiz) . $arquivoSaida;
            }
            elseif ($arquivoSaida)
            {
                $this->pathArquivo = $arquivoSaida;
                $this->pathArquivoAPartirRaiz = Helper::getPathComBarra($getcwd()) . $arquivoSaida;
            }

            $this->database = new Database();
            $this->quebraLinha = QUEBRA_LINHA_PADRAO;
            $this->downloadDireto = $downloadDireto;
        }

        public function gerarAPartirSQL($consultaSQL)
        {
            $this->database->query($consultaSQL);

            for ($i = 0; $dados = $this->database->fetchArray(MYSQL_ASSOC); $i++)
            {
                foreach ($dados as $chave => $valor)
                {
                    if ($i == 0)
                    {
                        $conteudoCabecalho .= $chave . ";";
                    }

                    if (substr_count($chave, "_HTML"))
                    {
                        $this->conteudoCSV .= "\"" . html_entity_decode($valor) . "\";";
                    }
                    else
                    {
                        $this->conteudoCSV .= "\"{$valor}\";";
                    }
                }

                $this->conteudoCSV .= $this->quebraLinha;
            }

            $this->conteudoCSV = $conteudoCabecalho . $this->quebraLinha . $this->conteudoCSV;

            return $this->__exportar();
        }

        public function __exportar()
        {
            if (strlen($this->pathArquivo))
            {
                $fhandle = fopen($this->pathArquivo, "w");
                fwrite($fhandle, $this->conteudoCSV);

                if ($this->downloadDireto)
                {
                    $objDownload = new Download($this->pathArquivo);
                    $objDownload->df_download();
                }
                else
                {
                    return $this->pathArquivoAPartirRaiz;
                }
            }
            else
            {
                if ($this->downloadDireto)
                {
                    $objDownload = new Download($this->pathArquivo);
                    print $objDownload->ds_download($this->conteudoCSV);
                }
                else
                {
                    return $this->conteudoCSV;
                }
            }
        }

    }

?>

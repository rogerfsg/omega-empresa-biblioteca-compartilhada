<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class ConfiguracaoDatabase
    {
        public $DBName;
        public $host;
        public $porta;
        public $usuario;
        public $senha;
        public $novaConexao;

        public function __construct()
        {
            $this->DBName = defined('NOME_BANCO_DE_DADOS_PRINCIPAL') ? NOME_BANCO_DE_DADOS_PRINCIPAL : null;
            $this->host = defined('BANCO_DE_DADOS_HOST') ? BANCO_DE_DADOS_HOST : null;
            $this->porta = defined('BANCO_DE_DADOS_PORTA') ? BANCO_DE_DADOS_PORTA : null;
            $this->usuario = defined('BANCO_DE_DADOS_USUARIO') ? BANCO_DE_DADOS_USUARIO : null;
            $this->senha = defined('BANCO_DE_DADOS_SENHA') ? BANCO_DE_DADOS_SENHA : null;
            $this->novaConexao = false;
        }
    }

<?php

    class Ambiente
    {
        const AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER = "MAQUINA_DESENVOLVIMENTO_ROGER";
        const AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO = "MAQUINA_DESENVOLVIMENTO_EDUARDO";
        const AMBIENTE_MAQUINA_PRODUCAO_1 = "MAQUINA_PRODUCAO_1";

        const SISTEMA_COBRANCA = "SISTEMA_COBRANCA";
        const SISTEMA_HOSPEDAGEM = "SISTEMA_HOSPEDAGEM";
        const SISTEMA_BIBLIOTECA_NUVEM = "SISTEMA_BIBLIOTECA_NUVEM";
        const SISTEMA_BIBLIOTECA_COMPARTILHADA = "SISTEMA_BIBLIOTECA_COMPARTILHADA";
        const SISTEMA_SINCRONIZADOR_WEB = "SISTEMA_SINCRONIZADOR_WEB";
        const SISTEMA_PONTO_ELETRONICO = "SISTEMA_PONTO_ELETRONICO";

        public static function definirConstantesDoSistema($identificadorSistema)
        {
            if (static::validarIdentificadorSistema($identificadorSistema))
            {
                switch ($identificadorSistema)
                {
                    case static::SISTEMA_COBRANCA:

                        static::definirConstantesSistemaCobranca();
                        break;

                    case static::SISTEMA_HOSPEDAGEM:

                        static::definirConstantesSistemaHospedagem();
                        break;

                    case static::SISTEMA_BIBLIOTECA_NUVEM:

                        static::definirConstantesSistemaBibliotecaNuvem();
                        break;

                    case static::SISTEMA_BIBLIOTECA_COMPARTILHADA:

                        static::definirConstantesSistemaBibliotecaCompartilhada();
                        break;

                    case static::SISTEMA_SINCRONIZADOR_WEB:

                        static::definirConstantesSincronizadorWeb();
                        break;

                    case static::SISTEMA_PONTO_ELETRONICO:

                        static::definirConstantesPontoEletronico();
                        break;
                }
            }
            else
            {
                throw new Exception("Identificador de sistema inv�lido.");
            }
        }

        public static function definirConstantesDosSistemas($arrIdentificadoresSistemas)
        {
            if (is_array($arrIdentificadoresSistemas))
            {
                foreach ($arrIdentificadoresSistemas as $identificadorSistema)
                {
                    static::definirConstantesDoSistema($identificadorSistema);
                }
            }
        }

        public static function isConsoleMode()
        {
            return strtolower(PHP_SAPI) === 'cli';
        }

        public static $ambienteCorrente = null;

        public static function isAmbiente($identificadorAmbiente)
        {
            if (!is_null(static::$ambienteCorrente))
            {
                return $identificadorAmbiente == static::$ambienteCorrente;
            }

            $identificadorAmbienteCorrente = null;
            //modo console
            if (static::isConsoleMode())
            {
                $nomeMaquina = strtoupper(php_uname("n"));
                if ($nomeMaquina == "EDUARDO-DESKTOP")
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO;
                }
                elseif ($nomeMaquina == "ROGER_DESKTOP")
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER;
                }
                else
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_PRODUCAO_1;
                }
            }
            //modo servidor
            else
            {
                $documentRoot = strtolower($_SERVER["DOCUMENT_ROOT"]);
                $adminName = strtolower($_SERVER["SERVER_ADMIN"]);
                $serverHost = strtolower($_SERVER["HTTP_HOST"]);

                if ($documentRoot == "D:/wamp64/www" && $adminName == "eduardo@omegasoftware.com.br")
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO;
                }
                elseif ($serverHost == "empresa.omegasoftware.com.br")
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_PRODUCAO_1;
                }
                else
                {
                    $identificadorAmbienteCorrente = static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER;
                }
            }

            if (!is_null($identificadorAmbienteCorrente))
            {
                static::$ambienteCorrente = $identificadorAmbiente;
            }

            return $identificadorAmbiente == $identificadorAmbienteCorrente;
        }

        private static function validarIdentificadorSistema($identificadorSistema)
        {
            return Helper::startsWith($identificadorSistema, "SISTEMA_");
        }

        private static function definirConstantesSistemaCobranca()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-2');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'costumer.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_CC_ADM2_");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Site");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "prd_cobranca");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "prd_cobranca_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "vps.omegasoftware.com.br");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3307");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "EeXKjzeC4Rum");

                    static::defineConstant('PATH_MYSQL', '');

                    static::defineConstant('ENDERECO_DE_ACESSO_PAGAMENTO', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/webservice.php?class=Servicos_web&action=");

                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-2');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'costumer.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_CC_ADM_ADM");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Site");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "cobranca");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "omegasoftware_interno_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');

                    static::defineConstant('ENDERECO_DE_ACESSO_PAGAMENTO', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO', "127.0.0.1/OmegaEmpresa/Cobranca/Trunk");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");

                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-2');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'costumer.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_CC_ADM1");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Site");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "cobranca");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "omegasoftware_interno_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');

                    static::defineConstant('ENDERECO_DE_ACESSO_PAGAMENTO', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO', "http://127.0.0.1/cobranca/CO10003Corporacao/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");

                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                }
            }
            else
            {
                throw new Exception("Modo Console n�o suportado pela aplica��o corrente.");
            }
        }

        private static function definirConstantesSistemaHospedagem()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_HO1");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Hospedagem");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "prd_hospedagem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "omegasof_hospedagem_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "vps.omegasoftware.com.br");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3307");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "EeXKjzeC4Rum");

                    static::defineConstant('PATH_MYSQL', '');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_HO2");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Hospedagem");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_HO");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Hospedagem");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                }
            }
            else
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    throw new Exception("Ainda n�o implementado");
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_HO");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Hospedagem");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-3');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'hosting.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "OMG_HO");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Hospedagem");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('PATH_MYSQL', 'C:\wamp\bin\mysql\mysql5.5.20\bin');
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
                }
            }
        }

        private static function definirConstantesSistemaBibliotecaNuvem()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-5');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'cloud.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "BN_");
                    static::defineConstant('TITULO_PAGINAS', "WorkOffline - Cloud");

                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "prd_cloud");
                    static::defineConstant('NOME_BANCO_DE_DADOS_BACKUP', "prd_cloud_backup");
                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "prd_cloud_parametros");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "vps.omegasoftware.com.br");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3307");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "EeXKjzeC4Rum");

                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

                    static::defineConstant('DOMINIO_DE_ACESSO', 'http://127.0.0.1/BibliotecaNuvem/10003Corporacao/');
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-5');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'cloud.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "BN_");
                    static::defineConstant('TITULO_PAGINAS', "Biblioteca Nuvem Eduardo LocalHost");

                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "biblioteca_nuvem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "biblioteca_nuvem");
                    static::defineConstant('NOME_BANCO_DE_DADOS_BACKUP', "biblioteca_nuvem");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

                    static::defineConstant('DOMINIO_DE_ACESSO', 'http://127.0.0.1/OmegaEmpresa/BibliotecaNuvem/Trunk/');
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-5');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'cloud.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('IDENTIFICADOR_SESSAO', "BN_");
                    static::defineConstant('TITULO_PAGINAS', "Biblioteca Nuvem Roger LocalHost");

                    static::defineConstant('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "biblioteca_nuvem_corporacao");
                    static::defineConstant('NOME_BANCO_DE_DADOS_PRINCIPAL', "biblioteca_nuvem_corporacao");
                    static::defineConstant('NOME_BANCO_DE_DADOS_BACKUP', "biblioteca_nuvem_corporacao");

                    static::defineConstant('BANCO_DE_DADOS_HOST', "localhost");
                    static::defineConstant('BANCO_DE_DADOS_PORTA', "3306");
                    static::defineConstant('BANCO_DE_DADOS_USUARIO', "root");
                    static::defineConstant('BANCO_DE_DADOS_SENHA', "");

                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_HOST', BANCO_DE_DADOS_HOST);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_PORTA', BANCO_DE_DADOS_PORTA);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', BANCO_DE_DADOS_USUARIO);
                    static::defineConstant('BANCO_DE_DADOS_CONFIGURACAO_SENHA', BANCO_DE_DADOS_SENHA);

                    static::defineConstant('DOMINIO_DE_ACESSO', 'http://127.0.0.1/BibliotecaNuvem/BN10003Corporacao/');
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                }
            }
            else
            {
                throw new Exception("Modo Console n�o suportado pela aplica��o Biblioteca Nuvem.");
            }
        }

        private static function definirConstantesSistemaBibliotecaCompartilhada()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('DOMINIO_DE_ACESSO', "http://pontoeletronico.empresa.omegasoftware.com.br");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://empresa.omegasoftware.com.br/site/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/actions.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/OmegaEmpresa/BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'OmegaEmpresa/BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                }
            }
            else
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    throw new Exception("Ainda n�o implementado");
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('__DOMINIO_BIBLIOTECAS_COMPARTILHADAS', 'http://127.0.0.1/BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__PATH_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('__DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                }
            }
        }

        private static function definirConstantesSincronizadorWeb()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-4');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'sync.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "http://sincronizador.empresa.omegasoftware.com.br");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://empresa.omegasoftware.com.br/site/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
                    static::defineConstant('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-4');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'sync.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "http://127.0.0.1/OmegaEmpresa/PontoEletronico/Trunk/public_html/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/client_area/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-4');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'sync.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "http://127.0.0.1/OmegaEmpresa/PontoEletronico/Trunk/public_html/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/client_area/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/hospedagem/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                    static::defineConstant('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
                }
            }
            else
            {
                throw new Exception("Modo Console n�o suportado pela aplica��o corrente.");
            }
        }

        private static function definirConstantesPontoEletronico()
        {
            if (!static::isConsoleMode())
            {
                if (static::isAmbiente(static::AMBIENTE_MAQUINA_PRODUCAO_1))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-1');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'pontoeletronico.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "http://pontoeletronico.empresa.omegasoftware.com.br");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://empresa.omegasoftware.com.br/site/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://empresa.omegasoftware.com.br/site/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/actions.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_EDUARDO))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-1');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'pontoeletronico.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "127.0.0.1/OmegaEmpresa/PontoEletronicoAngularJS/Trunk/modelo/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/client_area/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/actions.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
                }
                elseif (static::isAmbiente(static::AMBIENTE_MAQUINA_DESENVOLVIMENTO_ROGER))
                {
                    static::defineConstant('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108282606-1');
                    static::defineConstant('DOMINIO_GOOGLE_ANALYTICS', 'pontoeletronico.omegasoftware.com.br');
                    static::defineConstant('FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO', false);
                    static::defineConstant('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO', false);

                    static::defineConstant('DOMINIO_DE_ACESSO', "localhost/PontoEletronico/PE10002Corporacao/public_html/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/client_area/");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
                    static::defineConstant('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/actions.php?class=Servicos_web&action=");
                    static::defineConstant('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
                }
            }
            else
            {
                throw new Exception("Modo Console n�o suportado pela aplica��o corrente.");
            }
        }

        private static function defineConstant($constantKey, $constantValue, $safeMode = false)
        {
            if ($safeMode && !defined($constantKey))
            {
                define($constantKey, $constantValue);
            }
            elseif (!$safeMode)
            {
                define($constantKey, $constantValue);
            }

        }

    }

?>
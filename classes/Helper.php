<?php

class Helper
{
    public static $arrCoresUtilizadas = array();
    public static $contadorDeIdsGreyBox = 1;
    public static $letrasDoAlfabeto = array("A" => "A",
        "B" => "B",
        "C" => "C",
        "D" => "D",
        "E" => "E",
        "F" => "F",
        "G" => "G",
        "H" => "H",
        "I" => "I",
        "J" => "J",
        "K" => "K",
        "L" => "L",
        "M" => "M",
        "N" => "N",
        "O" => "O",
        "P" => "P",
        "Q" => "Q",
        "R" => "R",
        "S" => "S",
        "T" => "T",
        "U" => "U",
        "V" => "V",
        "W" => "W",
        "X" => "X",
        "Y" => "Y",
        "Z" => "Z");
    static $__raiz = null;
    static $__numeroRaiz = null;
    static $__raizWorkspace = null;
    static $__numeroRaizWorkspace = null;
    public static $contadorNovaAba;
    static $vetorPostGet = null;
    public static $arrayDeCores = array('14C079', 'EBE37D', 'AAD6DF', '86F24E', 'C3BEE5', 'EAB19D', 'EEEDFF', 'F17A6A', '80A4F6');
    public static $contadorNoArrayDeCores = 0;
    public static $cacteresEspeciais = array('?' => 'S', '?' => 's', '?' => 'Z', '?' => 'z', '�' => 'A', '�' => 'A', '�' => 'A', '�' => 'A', '�' => 'A', '�' => 'A', '�' => 'A', '�' => 'C', '�' => 'E', '�' => 'E',
        '�' => 'E', '�' => 'E', '�' => 'I', '�' => 'I', '�' => 'I', '�' => 'I', '�' => 'N', '�' => 'O', '�' => 'O', '�' => 'O', '�' => 'O', '�' => 'O', '�' => 'O', '�' => 'U',
        '�' => 'U', '�' => 'U', '�' => 'U', '�' => 'Y', '�' => 'B', '�' => 'Ss', '�' => 'a', '�' => 'a', '�' => 'a', '�' => 'a', '�' => 'a', '�' => 'a', '�' => 'a', '�' => 'c',
        '�' => 'e', '�' => 'e', '�' => 'e', '�' => 'e', '�' => 'i', '�' => 'i', '�' => 'i', '�' => 'i', '�' => 'o', '�' => 'n', '�' => 'o', '�' => 'o', '�' => 'o', '�' => 'o',
        '�' => 'o', '�' => 'o', '�' => 'u', '�' => 'u', '�' => 'u', '�' => 'y', '�' => 'b', '�' => 'y');
    static $__currentOffset = null;

    public function __construct()
    {
    }

    public static function substring($val, $tamanho)
    {
        if ($val == null)
        {
            return $val;
        }
        if (strlen($val) < $tamanho)
        {
            return $val;
        }
        else
        {
            return substr($val, 0, $tamanho);
        }
    }

    public static function getNomeDaEmpresa()
    {
        return "WorkOffline";
    }

    public static function renomearDiretorio($pathAntigo, $pathNovo)
    {
        $idSO = Helper::getSistemaOperacionalDoServidor();
        switch ($idSO)
        {
            case WINDOWS:
                $pathAntigo = str_replace("\\", "/", $pathAntigo);
                $pathNovo = str_replace("\\", "/", $pathNovo);
                break;

            case LINUX:
                $pathAntigo = str_replace("\\", "/", $pathAntigo);
                $pathNovo = str_replace("\\", "/", $pathNovo);
                break;

            default:
                break;
        }

        if (is_dir($pathAntigo))
        {
            if (!rename($pathAntigo, $pathNovo))
            {
                if (Helper::copiaDiretorio($pathAntigo, $pathNovo))
                {
                    unlink($pathAntigo);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    static $sistemaOperacional = null;

    public static function getSistemaOperacionalDoServidor()
    {
        if (Helper::$sistemaOperacional != null)
        {
            return Helper::$sistemaOperacional;
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        {
            Helper::$sistemaOperacional = WINDOWS;
        }
        else
        {
            Helper::$sistemaOperacional = LINUX;
        }
        return Helper::$sistemaOperacional;
    }

    public static function copiaDiretorio($sourceDir, $targetDir)
    {
        $sourceDir = Helper::formatarPath($sourceDir);
        $targetDir = Helper::formatarPath($targetDir);

        if (!file_exists($sourceDir))
        {
            return false;
        }
        if (!is_dir($sourceDir))
        {
            return copy($sourceDir, $targetDir);
        }
        if (!Helper::mkdir($targetDir, 0777, true))
        {
            return false;
        }
        foreach (scandir($sourceDir) as $item)
        {
            if ($item == '.' || $item == '..')
            {
                continue;
            }
            if (!self::copiaDiretorio($sourceDir . DIRECTORY_SEPARATOR . $item, $targetDir . DIRECTORY_SEPARATOR . $item))
            {
                return false;
            }
        }

        return true;
    }

    public static function formatarPath($path)
    {
        $idSO = Helper::getSistemaOperacionalDoServidor();
        switch ($idSO)
        {
            case WINDOWS:
                $path = str_replace("\\", "/", $path);
                break;

            case LINUX:
                $path = str_replace("\\", "/", $path);
                break;

            default:
                break;
        }

        return $path;
    }

    public static function criaArquivoComOConteudo($path, $conteudo, $sobrescrever = true)
    {
        $path = Helper::formatarPath($path);
        if ($sobrescrever)
        {
            if (is_file($path))
            {
                unlink($path);
            }
        }
        HelperLog::verbose("criaArquivoComOConteudo: $path ...");
        $ret = file_put_contents($path, $conteudo, FILE_APPEND | LOCK_EX);
        HelperLog::verbose("criaArquivoComOConteudo: R.: $ret");
        return $ret;
    }

    public static function deletaDiretorio($dir)
    {
        if (!file_exists($dir))
        {
            return true;
        }
        if (!is_dir($dir))
        {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item)
        {
            if ($item == '.' || $item == '..')
            {
                continue;
            }
            if (!self::deletaDiretorio($dir . DIRECTORY_SEPARATOR . $item))
            {
                return false;
            }
        }

        return rmdir($dir);
    }

    public static function createZip($files = array(), $destination = '', $overwrite = false)
    {
        return self::criarArquivoZip($files, $destination, $overwrite);
    }

    public static function criarArquivoZip($files = array(), $destination = '', $overwrite = false)
    {
        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite)
        {
            HelperLog::logZip($destination . " - arquivo ja existia");

            return false;
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files))
        {
            //cycle through each file
            foreach ($files as $file)
            {
                //make sure the file exists
                if (file_exists($file))
                {
                    $valid_files[] = $file;
                }
            }
        }

        //if we have good files...
        if (!empty($valid_files))
        {
            if (file_exists($destination))
            {
                unlink($destination);
            }
            //create the archive
            $zip = new ZipArchive();

            if ($zip->open($destination, ZIPARCHIVE::CREATE) !== true)
            {
                HelperLog::logZip($destination . " - falha ao abrir o arquivo a ser gerado - " . $destination . ". Verifique o nivel de permissao do usuario da aplicacao para o path."
                                  . " Status: " . $zip->getStatusString());

                return false;
            }

            //add the files
            foreach ($valid_files as $file)
            {
                $fileName = Helper::getNomeDoArquivoEmPathCompleto($file);
                $zip->addFile($file, $fileName);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            $validade = file_exists($destination);
            if ($validade)
            {
                HelperLog::logZip($destination . " - arquivo gerado com sucesso!");
            }
            else
            {
                HelperLog::logZip($destination . " - falha ao gerar o arquivo.");
            }

            return $validade;
        }
        else
        {
            HelperLog::logZip($destination . " - arquivo n�o foi gerado devido a arquivos invalidos da entrada. Arquivos: " . Helper::arrayToString($files, ","));

            return false;
        }
    }

    public static function getNomeDoArquivoEmPathCompleto($path)
    {
        $inicioArquivo = strrpos($path, "/");
        $arquivo = substr($path, $inicioArquivo);
        $arquivo = str_replace("/", "", $arquivo);

        return $arquivo;
    }

    public static function arrayToString($array, $delimitador = ',')
    {
        if (!count($array) || !strlen($delimitador))
        {
            return false;
        }
        $str = "";
        $limit = count($array);
        //for($i = 0 ; $i < $limit; $i++)
        $i = 0 ;
        foreach ($array as $token)
        {
            if ($i > 0 && $i < $limit)
            {
                $str .= $delimitador;
            }
            $str .= $token;
            $i++;
        }

        return $str;
    }

    public static function arrayToApostrofeString($array, $delimitador = ',')
    {
        if (!count($array) || !strlen($delimitador))
        {
            return false;
        }
        $str = "";
        $limit = count($array);
        //for($i = 0 ; $i < $limit; $i++)
        $i = 0 ;
        foreach ($array as $token)
        {
            if ($i > 0 && $i < $limit)
            {
                $str .= "'$delimitador'";
            }
            $str .= $token;
            $i++;
        }

        return $str;
    }


    public static function chamadaCurlMensagem(
        $url,
        $parametros = null,
        $valores = null)
    {
        $strJson = Helper::chamadaCurl($url, $parametros, $valores);
        if ($strJson == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                "Url: " . $url
                                . " Parametros: " . print_r($parametros, true)
                                . " Valores: " . print_r($valores, true)
                                . ". Retorno json NULO.");
        }
        else
        {
            $json = json_decode($strJson);
            if ($json == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                    "Url: " . $url
                                    . " Parametros: " . print_r($parametros, true)
                                    . " Valores: " . print_r($valores, true)
                                    . ". Retorno json inv�lido: $json");
            }

            $msg = new Mensagem();
            $msg->inicializa($json);

            return $msg;
        }
    }

    public static function chamadaCurl(
        $url,
        $parametros = null,
        $valores = null)
    {
        $get = "";

        if ($parametros != null
            && $valores != null
            && count($parametros) == count($parametros)
        )
        {
            for ($i = 0; $i < count($parametros); $i++)
            {
                $valor = $valores[$i];
                $parametro = $parametros[$i];
                if (strlen($get))
                {
                    $get .= "&";
                }
                $get .= $parametro . "=" . urlencode($valor);
            }
        }
        if (strlen($get))
        {
            $url .= "&" . $get;
        }
        HelperLog::logCurl($url . " ...");
        if (Helper::isConsole())
        {
            $cmd = "";

            if (Helper::getSistemaOperacionalDoServidor() == WINDOWS)
            {
                $cmd = "\"" . Helper::getPathComBarra(PATH_CURL_CMD) . "curl.exe\" \"$url\" -s";
            }
            else
            {
                $cmd = PATH_CURL_CMD . "curl -s \"$url\" ";
            }
            $ret = Helper::exec($cmd . " ");
        }

        else
        {
            $handler = curl_init();
//                if(isset($_COOKIE["XDEBUG_SESSION"]))
//                {
//                    curl_setopt($handler, CURLOPT_COOKIE, "XDEBUG_SESSION={$_COOKIE["XDEBUG_SESSION"]}");
//                    $url .= "&XDEBUG_SESSION={$_COOKIE["XDEBUG_SESSION"]}";
//                }

            curl_setopt($handler, CURLOPT_TIMEOUT, 240);
            curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($handler, CURLOPT_URL, $url);
            curl_setopt($handler, CURLOPT_HEADER, false);
            if (isset($_SERVER["HTTP_USER_AGENT"]))
            {
                curl_setopt($handler, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
            }
            curl_setopt($handler, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handler, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handler, CURLOPT_HTTPHEADER, array('Accept: text/plain,text/html', 'Accept-Charset: iso-8859-1, utf-8', 'Accept-Language: en-us, en-gb, pt-br'));
            curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
            //        echo $url;

            $ret = curl_exec($handler);

            curl_close($handler);
        }

        HelperLog::logCurl($url . " - Result: '" . $ret . "''");

        return $ret;
    }

    public static function exec($cmd, array &$vetor = null, &$codRetorno = null)
    {
        HelperLog::logExec("Cmd: " . $cmd . ". Vetor: " . print_r($vetor, true) . " ...");
        $ret = exec($cmd, $vetor, $codRetorno);
        HelperLog::logExec("CodRetorno: $codRetorno.");

        return $ret;
    }

    public static function chamadaCurlMensagemToken(
        $url,
        $parametros = null,
        $valores = null)
    {
        $strJson = Helper::chamadaCurl($url, $parametros, $valores);
        if ($strJson == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                "Url: " . $url
                                . " Parametros: " . print_r($parametros, true)
                                . " Valores: " . print_r($valores, true)
                                . ". Retorno json NULO.");
        }
        else
        {
            $json = json_decode($strJson);
            if ($json == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                    "Url: " . $url
                                    . " Parametros: " . print_r($parametros, true)
                                    . " Valores: " . print_r($valores, true)
                                    . ". Retorno json inv�lido: $json");
            }
            if (Interface_mensagem::checkOk($json))
            {
                $msg = new Mensagem_token();
                $msg->inicializa($json);
            }
            else
            {
                $msg = new Mensagem();
                $msg->inicializa($json);
            }

            return $msg;
        }
    }

    const LIMITE_CHAMADAS_CURL_HTTP = 3;

    public static function chamadaCurlJson(
        $url,
        $parametros = null,
        $valores = null)
    {
        for ($i = 0; $i < Helper::LIMITE_CHAMADAS_CURL_HTTP; $i++)
        {
            $strJson = Helper::chamadaCurl($url, $parametros, $valores);
            if ($strJson == null || !$strJson)
            {
                if ($i == Helper::LIMITE_CHAMADAS_CURL_HTTP - 1)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                        "Url: " . $url
                                        . " Parametros: " . print_r($parametros, true)
                                        . " Valores: " . print_r($valores, true)
                                        . ". Retorno json NULO.");
                }
                else
                {
                    usleep(rand(500, 3000));
                }
            }
            else
            {
                $json = json_decode($strJson);
                if ($json == null)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                        "Url: " . $url
                                        . " Parametros: " . print_r($parametros, true)
                                        . " Valores: " . print_r($valores, true)
                                        . ". Retorno json inv�lido: '$strJson'");
                }
                else
                {
                    return $json;
                }
            }
        }
    }

    public static function arrayToUpper($vetor)
    {
        for ($i = 0; $i < count($vetor); $i++)
        {
            $vetor[$i] = strtoupper($vetor[$i]);
        }

        return $vetor;
    }

    public static function imprimirImgCarregandoFlatty($idHtml)
    {
        $pathImgCarregando = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/17.gif');
        $html = "<img id='" . $idHtml . "' src='" . $pathImgCarregando . "' style='display: none;'>";

        return $html;
    }

    public static function getUrlRaizDaBibliotecaCompartilhada($urlRelativoAoRootDaBibliotecaNuvem)
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/" . $urlRelativoAoRootDaBibliotecaNuvem;

        return $path;
    }

    public static function carregarCssFlatty()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/bootstrap", "bootstrap");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets", "light-theme");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets", "theme-colors");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/jquery/", "jquery_ui");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/plugins/tabdrop/", "tabdrop");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/plugins/jgrowl/", "jquery.jgrowl.min");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/plugins/dynatree/", "ui.dynatree");
        $str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets/plugins/lightbox/", "lightbox");

        //$str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets", "demo") ;
        return $str;
    }

    public static function carregarArquivoCss($niveisRaiz, $diretorioApartirRaiz, $arquivoSemExtensao)
    {
        if ($niveisRaiz === false)
        {
            $strRaiz = self::acharRaiz();
        }
        else
        {
            $strRaiz = self::getStringNiveisAteRaiz($niveisRaiz);
        }

        $diretorioApartirRaiz = self::getPathComBarra($diretorioApartirRaiz);

        $strPrint = "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$strRaiz}{$diretorioApartirRaiz}{$arquivoSemExtensao}.css\" />\n";

        return $strPrint;
    }

    public static function acharRaiz($retornarNumeroNiveis = false)
    {
        if (static::$__raiz != null)
        {
            if ($retornarNumeroNiveis)
            {
                return static::$__numeroRaiz;
            }
            else
            {
                return static::$__raiz;
            }
        }
        $TENTATIVA_MAX_RAIZ = 20;
        $retorno = "";
        $numero = 0;

        while ($numero < $TENTATIVA_MAX_RAIZ)
        {
            if (!file_exists("{$retorno}__root"))
            {
                $retorno .= "../";
                $numero++;
            }
            else
            {
                break;
            }
        }
        if ($numero >= $TENTATIVA_MAX_RAIZ)
        {
            throw new Exception("Deve existir uma pasta __root na raiz do seu projeto.");
        }
        static::$__numeroRaiz = $numero;
        static::$__raiz = $retorno;
        if ($retornarNumeroNiveis)
        {
            return $numero;
        }
        else
        {
            return $retorno;
        }
    }

    public static function getStringNiveisAteRaiz($int = false)
    {
        if ($int === false)
        {
            return self::acharRaiz();
        }

        $strRetorno = "";

        for ($i = 0; $i < $int; $i++)
        {
            $strRetorno .= "../";
        }

        return $strRetorno;
    }

    public static function getPathComBarra($path)
    {
        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS)
        {
            if (!(substr($path, strlen($path) - 1, 1) == "/"))
            {
                $path = $path . "/";
            }
        }
        else
        {
            if (!(substr($path, strlen($path) - 1, 1) == "/"))
            {
                $path = $path . "/";
            }
        }

        return $path;
    }

    public static function padronizarNomeDePessoa($nomePessoa)
    {
        $originais = array(" De ", " Da ", " Do ", " Das ", " Dos ", " E ", "'", "?", "?", "?", "?", "?", "?", "?", "?");
        $substituicoes = array(" de ", " da ", " do ", " das ", " dos ", " e ", "\'", "?", "?", "?", "?", "?", "?", "?", "?");

        $nomePessoa = ucwords(strtolower($nomePessoa));
        $nomePessoa = str_replace($originais, $substituicoes, $nomePessoa);
        $nomePessoa = self::removerEspacosDuplicados($nomePessoa);

        return $nomePessoa;
    }

    public static function removerEspacosDuplicados($valor)
    {
        $valor = preg_replace('~\s{2,}~', ' ', $valor);

        return $valor;
    }

    public static function getCorRandomica()
    {
        while (true)
        {
            mt_srand((double)microtime() * 1000000);
            $c = '';

            while (strlen($c) < 6)
            {
                $c .= sprintf("%02X", mt_rand(0, 255));
            }

            if (!in_array($c, self::$arrCoresUtilizadas))
            {
                self::$arrCoresUtilizadas[] = $c;

                return $c;
            }
        }
    }

    public static function atualizaIndiceArray($array)
    {
        if (!count($array))
        {
            return $array;
        }
        $i = 0;
        foreach ($array as $key => $value)
        {
            $array[$i++] = $value;
        }

        return $array;
    }

    public static function getDiaEHoraNomeArquivo()
    {
        return date("d_m_Y_H_i_s");
    }

    public static function getResultSetToArrayDeObjetos($resultSet)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        $arrayRetorno = array();
        for ($i = 0; $registro = mysqli_fetch_object($resultSet); $i++)
        {
            $indice = $i;
            $arrayRetorno[$indice] = $registro;
        }

        return $arrayRetorno;
    }

    public static function getPrimeiroRegistroDoResultSetAsObject($resultSet)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        for ($i = 0; $registro = mysqli_fetch_object($resultSet); $i++)
        {
            return $registro;
        }
    }

    public static function getArrayDePaginasIniciais()
    {
        $diretorios = array("forms", "lists", "pages");
        $arrListaOpcoes = array();

        foreach ($diretorios as $diretorio)
        {
            $dirHandler = opendir($diretorio);

            while (($arquivo = readdir($dirHandler)) !== false)
            {
                if ($arquivo != "." && $arquivo != "..")
                {
                    $arrListaOpcoes["{$diretorio}/{$arquivo}"] = "{$diretorio}/{$arquivo}";
                }
            }
        }

        return $arrListaOpcoes;
    }

    public static function getResultSetToArrayDeObjetosWithExplicitNull($resultSet)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }
        $arrayRetorno = array();

        for ($i = 0; $registro = mysqli_fetch_array($resultSet, MYSQL_BOTH); $i++)
        {
            $indice = $i;
//            print_r($registro);
            foreach ($registro as $vNomeAtributo => $value)
            {
                if ($value == "")
                {
                    $registro[$vNomeAtributo] = "null";
                }
            }
//            print_r($registro);
            $arrayRetorno[$indice] = $registro;
        }

//        print_r($arrayRetorno);
        return $arrayRetorno;
    }

    public static function getTituloDasPaginas()
    {
        $titulo = TITULO_PAGINAS;

        return $titulo;
    }

    public static function getValorParaListagem($valor, $nulo = "---")
    {
        if (!strlen($valor))
        {
            return $nulo;
        }
        else
        {
            return $valor;
        }
    }

    public static function getFiltroDeColuna()
    {
        $id = rand(0, 1000);

        $str = "<input id=\"filtro_{$id}\" type=\"text\" class=\"input_text\" value=\"\" onkeyup=\"javascript:filtrarColuna(this.parentNode, this.value);\" />";

        return $str;
    }

    public static function removerAspas($valor)
    {
        $search = array("\"", "'", "\'");
        $replace = array("", "", "");

        $valor = str_replace($search, $replace, $valor);

        return $valor;
    }

    public static function removerPontuacaoDeNumeros($valor)
    {
        $search = array(",", ".");
        $replace = array("", "");

        $valor = str_replace($search, $replace, $valor);

        return $valor;
    }

    public static function getCampoHidden($nome, $valor, $id = false)
    {
        if ($id)
        {
            $strId = "id=\"{$id}\"";
        }
        else
        {
            $strId = "";
        }

        $retorno = "<input type=\"hidden\" name=\"{$nome}\" value=\"{$valor}\" {$strId}/>";

        return $retorno;
    }

    public static function getLegenda($arrCorLegenda)
    {
        $strRetorno = "";

        foreach ($arrCorLegenda as $cor => $texto)
        {
            $strRetorno .= "<input type=\"button\" class=\"icone_legenda\" style=\"background-color: {$cor};\"> {$texto}<br />";
        }

        if (strlen($strRetorno))
        {
            $strRetorno = self::removerOsUltimosCaracteresDaString($strRetorno, 6);
        }

        return $strRetorno;
    }

    public static function removerOsUltimosCaracteresDaString($string, $numeroCaracteres = 1)
    {
        return substr($string, 0, strlen($string) - ($numeroCaracteres));
    }

    public static function getObjeto($stringNomeClasse)
    {
        $obj = call_user_func_array(array($stringNomeClasse, "factory"), "");

        return $obj;
    }

    public static function getRetornoDeMetodo(&$obj, $nomeMetodo, $arrParametros = false)
    {
        if (!$arrParametros)
        {
            $arrParametros = array();
        }

        return call_user_func_array(array(&$obj, $nomeMetodo), $arrParametros);
    }

    public static function getAtributoDeObjeto(&$obj, $atributo)
    {
        $arr = get_object_vars($obj);

        return $arr[$atributo];
    }

    public static function getBotaoDeAjuda($textoTooltip, $imprimirEmDivSeparada)
    {
        $raiz = self::acharRaiz();

        if ($imprimirEmDivSeparada)
        {
            $rand = rand(0, 1000);

            echo "<div id=\"botao_ajuda_{$rand}\" style=\"display: none;\">{$textoTooltip}</div>";

            $textoTooltip = "botao_ajuda_{$rand}";
        }

        $strRetorno = "<a href=\"javascript: void(0)\" onmouseover=\"javascript: tip('$textoTooltip', undefined, undefined, new Array(380, 200));\" onmouseout=\"javascript:notip();\">
                                <img src=\"{$raiz}adm/imgs/padrao/ajuda.png\" border=\"0\">
                        </a>";

        return $strRetorno;
    }

    public static function tab2nbsp($string, $numeroNbspPorTab = 3)
    {
        $strTab = self::getStringDeTabulacao($numeroNbspPorTab);

        $strRetorno = str_replace("\t", $strTab, $string);

        return $strRetorno;
    }

    public static function getStringDeTabulacao($numeroDeTabulacoes)
    {
        $strRetorno = "";

        for ($i = 0; $i < $numeroDeTabulacoes; $i++)
        {
            $strRetorno .= "&nbsp;&nbsp;&nbsp;";
        }

        return $strRetorno;
    }

    public static function getLinkParaGreyBox($titulo, $url, $largura = LARGURA_PADRAO_GREYBOX, $altura = ALTURA_PADRAO_GREYBOX, $id = false, $parent = false)
    {
        $strParent = "";
        $retorno = "";

        if ($id === false)
        {
            $idGB = Helper::$contadorDeIdsGreyBox++;

            $retorno = " id=\"link_greybox_{$idGB}\" ";
        }

        if ($parent !== false)
        {
            for ($i = 0; $i < $parent; $i++)
            {
                $strParent .= "parent.";
            }
        }

        Helper::imprimirComandoJavascript("$(function(){

                                         $(\"#link_greybox_{$idGB}\").click(function(){

                                             {$strParent}carregarConteudoDialog('{$url}');
                                             {$strParent}$(\"#div_dialog\").dialog(\"option\", \"width\", {$largura});
                                         {$strParent}$(\"#div_dialog\").dialog(\"option\", \"title\", \"{$titulo}\");
                                         {$strParent}$(\"#div_dialog\").dialog(\"open\");

                                        });

                                });"
        );

        return $retorno;
    }

    public static function imprimirComandoJavascript($stringComando, $onDocumentReady = true)
    {
        echo self::getComandoJavascript($stringComando, $onDocumentReady );
    }

    public static function getComandoJavascript($stringComando, $onDocumentReady = true)
    {
        if ($stringComando)
        {
            if($onDocumentReady ){
                $string = "

        	<script type=\"text/javascript\">
                try{
                    $(document).ready(function(){
                            {$stringComando};
                    });
                }catch(ex){
                    ReportarErroExcecao(ex);
                }
            </script>";

            } else {
                $string = "

        	<script type=\"text/javascript\">
                try{
                 
                  {$stringComando};
                 
                }catch(ex){
                    ReportarErroExcecao(ex);
                }
            </script>";

            }

            return $string;
        }
    }

    public static function getStringDasVariaveisPageETipo($incluirNomeDoScriptPHP5 = false)
    {
        $string = "";

        if ($incluirNomeDoScriptPHP5)
        {
            $string .= self::getNomeDoScriptAtual() . "?";
        }

        $string .= "tipo=" . self::GET("tipo") . "&page=" . self::GET("page");

        return $string;
    }

    public static function getNomeDoScriptAtual()
    {
        $fullName = $_SERVER["SCRIPT_NAME"];
        $scriptName = substr($fullName, strrpos($fullName, "/") + 1);

        return $scriptName;
    }

    public static function GET($variavel)
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                if (isset(Helper::$vetorPostGet[$variavel]))
                {
                    return Helper::$vetorPostGet[$variavel];
                }
                else
                {
                    return null;
                }
            }
        }
        else
        {
            if (isset($_GET[$variavel]))
            {
                return ($_GET[$variavel]);
            }
            else
            {
                return null;
            }
        }
    }

    public static function getStringReferenteADeterminadasVariaveisGET($arrayVariaveis)
    {
        $retorno = "";

        if (!is_array($arrayVariaveis))
        {
            $arrayVariaveis = array($arrayVariaveis);
        }

        if (count($arrayVariaveis) == 1)
        {
            return "{$arrayVariaveis[0]}=" . self::GET($arrayVariaveis[0]);
        }
        else
        {
            foreach ($arrayVariaveis as $chaveGET)
            {
                $valor = self::GET($chaveGET);

                $retorno .= "&{$chaveGET}={$valor}";
            }
        }

        return $retorno;
    }

    public static function imprimirCamposHiddenReferentesADeterminadasVariaveisGET($arrayVariaveis)
    {
        $retorno = "";

        if (!is_array($arrayVariaveis))
        {
            $arrayVariaveis = array($arrayVariaveis);
        }

        foreach ($arrayVariaveis as $chaveGET)
        {
            $valor = self::GET($chaveGET);

            $retorno .= "<input type=\"hidden\" name=\"{$chaveGET}\" value=\"{$valor}\">
	    			   ";
        }

        print $retorno;
    }

    public static function imprimirCamposHiddenReferentesATodasAsVariaveisGET()
    {
        $retorno = "";

        $pagina = self::GET("page");
        $tipo = self::GET("tipo");

        foreach ($_GET as $chave => $valor)
        {
            if (is_array($valor))
            {
                for ($i = 0; $i < count($valor); $i++)
                {
                    $retorno .= "<input type=\"hidden\" name=\"{$chave}[]\" value=\"{$valor[$i]}\">\n";
                }
            }
            else
            {
                $retorno .= "<input type=\"hidden\" name=\"{$chave}\" value=\"{$valor}\">\n";
            }
        }

        print $retorno;
    }

    public static function getDiaAtual()
    {
        return date("d/m/Y");
    }

    public static function getDiaAtualSQL()
    {
        return date("Y-m-d");
    }

    public static function getHoraAtual()
    {
        return date("H:i:s");
    }

    public static function getDiaEHoraAtual()
    {
        return date("d/m/Y H:i:s");
    }

    public static function getDiaEHoraAtualSQL()
    {
        return date("Y-m-d H:i:s");
    }

    public static function getNomeDoUsuarioCorrente()
    {
        return self::SESSION("usuario_nome");
    }

    public static function SESSION($variavel, $isObjectOrArray = false)
    {
        $s = SessionRedis::getSingleton();
        if ($s == null)
        {
            return null;
        }

        return $s->get($variavel, $isObjectOrArray);
    }

    public static function getNodoNivelEstruturaUsuarioCorrente()
    {
        return self::SESSION(NIVEL_ESTRUTURA_USUARIO);
    }

    public static function getNomeFuncao($funcaoComParametros)
    {
        $nomeFuncao = substr($funcaoComParametros, 0, strpos($funcaoComParametros, "("));

        return $nomeFuncao;
    }

    public static function getTamanhoDoDiretorio($path)
    {
        $sistemaOperacional = Seguranca::getSistemaOperacionalDoServidor();
        switch ($sistemaOperacional)
        {
            case WINDOWS:

                $size = Helper::getTamanhoDiretorioWindows($path) / 1024 / 1024;

                return $size;

            case LINUX:

                $io = popen('/usr/bin/du -sk ' . $path, 'r');
                $size = fgets($io, 4096);
                $size = substr($size, 0, strpos($size, ' '));
                pclose($io);

                return $size;

            default:
                return null;
        }
    }

    public static function getTamanhoDiretorioWindows($path)
    {
        $dh = opendir($path);
        $dir = $path;
        $size = 0;

        while ($file = readdir($dh))
        {
            $path = Helper::getPathComBarra($dir) . $file;

            if ($file != "." and $file != "..")
            {
                if (is_dir($path))
                {
                    $size += Helper::getTamanhoDiretorioWindows($path); // recursive in sub-folders
                }
                elseif (is_file($path))
                {
                    $size += filesize($path); // add file
                }
            }
        }
        closedir($dh);

        return $size;
    }

    public static function getParametrosFuncao($funcaoComParametros)
    {
        $temp1 = substr($funcaoComParametros, strpos($funcaoComParametros, "("));
        $temp2 = str_replace(array("(", ")"), array("", ""), $temp1);

        $array = explode(",", $temp2);

        return $array;
    }

    public static function getStrLinhaCSVDoVetorDeDados($pVetor)
    {
        $vReturn = "";
        if (count($pVetor) > 0)
        {
            $vLinha = "";
            for ($i = 0; $i < count($pVetor); $i++)
            {
                $vDado = $pVetor[$i];
                if (strlen($vLinha) > 0)
                {
                    $vLinha .= ";" . Helper::formatarDadosParaCSV($vDado);
                }
                else
                {
                    $vLinha .= Helper::formatarDadosParaCSV($vDado);
                }
            }
            if (strlen($vLinha) > 0)
            {
                $vReturn .= $vLinha . "\r\n";
            }
        }

        return $vReturn;
    }

    public static function formatarDadosParaCSV($pValor)
    {
        if ($pValor == null)
        {
            return "";
        }
        else
        {
            $pValor = str_replace(";", "\;", $pValor);

            return $pValor;
        }
    }

    public static function imprimirCabecalhoParaFormatarAction()
    {
        echo self::carregarArquivoCss(false, "adm/css/", "padrao");

        echo self::carregarArquivoJavascript(false, "recursos/js/", "core");
        echo self::carregarArquivoJavascript(false, "recursos/js/", "pngfix");

        echo Javascript::setRaizDaEstrutura();
        echo Javascript::importarBibliotecaJQuery();

        self::imprimirComandoJavascriptComTimer("document.title='" . TITULO_PAGINAS . "'", 0, false);
    }

    public static function carregarArquivoJavascript($niveisRaiz, $diretorioApartirRaiz, $arquivoSemExtensao, $forcarLimpezaDoCache = false, $complemento = "")
    {
        $complementoArquivo = "";
        if ($forcarLimpezaDoCache === true)
        {
            $complementoArquivo = "?" . self::getUnixTimestamp(date("Y-m-d"));
        }

        if ($niveisRaiz === false)
        {
            $strRaiz = self::acharRaiz();
        }
        else
        {
            $strRaiz = self::getStringNiveisAteRaiz($niveisRaiz);
        }

        $diretorioApartirRaiz = self::getPathComBarra($diretorioApartirRaiz);

        $strPrint = "<script type=\"text/javascript\" src=\"{$strRaiz}{$diretorioApartirRaiz}{$arquivoSemExtensao}.js{$complementoArquivo}\" {$complemento}></script>\n";

        return $strPrint;
    }

    public static function getUnixTimestamp($data)
    {
        $timeStamp = 0;
        if (substr_count($data, ":") > 0)
        {
            $data = self::formatarDataTimeParaComandoSQL($data);
            $data = str_replace("'", "", $data);

            $arrCampos = explode("-", $data);

            $ano = $arrCampos[0];
            $mes = $arrCampos[1];

            $arrCampos = explode(" ", $arrCampos[2]);

            $dia = $arrCampos[0];

            $arrCampos = explode(":", $arrCampos[1]);

            $hora = $arrCampos[0];
            $min = $arrCampos[1];
            $seg = $arrCampos[2];

            $timeStamp = mktime($hora, $min, $seg, $mes, $dia, $ano);

            return $timeStamp;
        }
        else
        {
            $data = self::formatarDataParaComandoSQL($data);
            $data = str_replace("'", "", $data);

            if (substr_count($data, "-") == 2)
            {
                $arrCampos = explode("-", $data);

                $ano = $arrCampos[0];
                $mes = $arrCampos[1];
                $dia = $arrCampos[2];

                $timeStamp = mktime(0, 0, 0, $mes, $dia, $ano);
            }
            elseif (substr_count($data, "/") == 2)
            {
                $arrCampos = explode("/", $data);

                $ano = $arrCampos[2];
                $mes = $arrCampos[1];
                $dia = $arrCampos[0];

                $timeStamp = mktime(0, 0, 0, $mes, $dia, $ano);
            }

            return $timeStamp;
        }

        return false;
    }

    public static function formatarDataTimeParaComandoSQL($valor)
    {
        $valor = str_replace("'", "", $valor);

        if ($valor == "null" || trim($valor) == "")
        {
            return "null";
        }
        elseif (substr_count($valor, "/") == 2 && substr_count($valor, ":") == 2)
        {
            $date = substr($valor, 0, 10);

            $hora = substr($valor, 11, 8);

            $partes = explode("/", $date);
            $date = $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return "'" . $date . " " . $hora . "'";
        }
        elseif (substr_count($valor, "/") == 2 && substr_count($valor, ":") == 1)
        {
            $date = substr($valor, 0, 10);

            $hora = substr($valor, 11, 8);

            $partes = explode("/", $date);
            $date = $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return "'" . $date . " " . $hora . "'";
        }
        elseif (substr_count($valor, "-") == 2 && substr_count($valor, ":") == 2)
        {
            return "'{$valor}'";
        }
        elseif (is_numeric($valor))
        {
            $retorno = date("'Y-m-d H:i:s'", $valor);

            return $retorno;
        }
        else
        {
            return "null";
        }
    }

    public static function formatarDataParaComandoSQL($valor)
    {
        if ($valor == "null" || trim($valor) == "")
        {
            return "null";
        }
        else
        {
            if (substr_count($valor, "/") == 2)
            {
                $partes = explode("/", $valor);
                $retorno = "'" . $partes[2] . "-" . $partes[1] . "-" . $partes[0] . "'";

                return $retorno;
            }
            elseif (substr_count($valor, "/") == 1)
            {
                $partes = explode("/", $valor);

                if (strlen($partes[1]) == 2)
                {
                    if ($partes[1] <= 99)
                    {
                        $partes[1] = "20{$partes[1]}";
                    }
                    else
                    {
                        $partes[1] = "19{$partes[1]}";
                    }
                }

                $retorno = "'" . $partes[1] . "-" . $partes[0] . "-01'";

                return $retorno;
            }
            elseif (substr_count($valor, "-") > 0)
            {
                $valor = str_replace("'", "", $valor);

                return "'{$valor}'";
            }
            elseif (substr_count($valor, "/") == 0 && strlen($valor) > 0)
            {
                $retorno = "'" . $valor . "-01-01'";

                return $retorno;
            }
        }

        return "null";
    }

    public static function imprimirComandoJavascriptComTimer($stringComando, $timerEmSegundos, $infinitamente = false)
    {
        echo self::getComandoJavascriptComTimer($stringComando, $timerEmSegundos, $infinitamente);
    }

    public static function getComandoJavascriptComTimer($stringComando, $timerEmSegundos, $infinitamente = true, $imprimirTagScript = true)
    {
        if ($stringComando)
        {
            $intervalo = 0;

            if (is_numeric($timerEmSegundos))
            {
                $intervalo = $timerEmSegundos * 1000;
            }

            if ($infinitamente)
            {
                $funcaoJs = "setInterval";
            }
            else
            {
                $funcaoJs = "setTimeout";
            }

            $string = "";
            if ($imprimirTagScript)
            {
                $string .= "<script type=\"text/javascript\">";
            }

            $string .= "{$funcaoJs}(function(){ {$stringComando} }, $intervalo);";

            if ($imprimirTagScript)
            {
                $string .= "</script>";
            }

            return $string;
        }
    }

    public static function getServerTimestampUtcAtual()
    {
        return static::getServerTimestampUtc(time());
    }

    public static function getServerTimestampUtc($serverTimestamp)
    {
        return $serverTimestamp - static::getServerTimezoneOffsetInSeconds();
    }

    public static function getServerTimezoneOffsetInSeconds()
    {
        $timezone = date_default_timezone_get();
        //echo $timezone;
        $machineTimezone = new DateTimeZone($timezone);

        $dateTimeMachine = new DateTime('now', $machineTimezone);

        $utcTimezone = new DateTimeZone("UTC");
        //print_r($utcTimezone);
        $ret = $dateTimeMachine->getOffset();
        //echo $ret;
        return $ret;
    }

    public static function getComandoDeFecharJanelaPopup($mensagem = false)
    {
        $print = "<script language=\"javascript\">self.close();</script>";

        if ($mensagem)
        {
            $print .= "

    			<script language=\"javascript\">

    				opener.document.getElementById('textoMensagem').innerHTML='$mensagem';
    				opener.document.getElementById('box').style.display='block';
    				opener.document.getElementById('tabelaMensagem').class='okTabela';

    			</script>";
        }

        print $print;
    }

    public static function getPorcentagem($numerador, $denominadorTotal, $casasDecimais = 1)
    {
        if (is_numeric($numerador) && is_numeric($denominadorTotal))
        {
            $porcentagem = round(($numerador / $denominadorTotal) * 100, $casasDecimais);

            return $porcentagem . " %";
        }
        else
        {
            return "$numerador";
        }
    }

    public static function gerarArrayCoresHarmoniosas($numeroCores)
    {
        $verde = array('#cdec22', '#3dec22', '#22ec97');
        $azul = array('#6D67EF', '#a822ec', '#d522ec');
        $vermelho = array('#FF7A51', '#ff2244', '#ff4322');
        $amarelo = array('#ff5e24', '#ffc420', '#ffff20');

        $arrayRetorno = array();

        for ($i = 0; $i < $numeroCores; $i++)
        {
            $indice = floor($i / 4);

            if ($i % 4 == 0)
            {
                $arrayRetorno[] = $vermelho[$indice];
            }
            elseif ($i % 4 == 1)
            {
                $arrayRetorno[] = $azul[$indice];
            }
            elseif ($i % 4 == 2)
            {
                $arrayRetorno[] = $verde[$indice];
            }
            elseif ($i % 4 == 3)
            {
                $arrayRetorno[] = $amarelo[$indice];
            }
        }

        return $arrayRetorno;
    }

    public static function getValorNumericoComLegenda($valor, $legendaSingular, $legendaPlural)
    {
        if ($valor == 1)
        {
            return "{$valor} {$legendaSingular}";
        }
        else
        {
            if (!strlen($valor))
            {
                $valor = 0;
            }

            return "{$valor} {$legendaPlural}";
        }
    }

    public static function getCheckBoxesAPartirDeArray($nomeCheckBox, $arrayValores, $valorSelecionado = false, $quebraDeLinha = false)
    {
        $strRetorno = "";

        if ($quebraDeLinha)
        {
            $strQuebraDeLinha = "<br />";
        }

        $registrosPorLinha = COLUNAS_FORMULARIOS;

        foreach ($arrayValores as $chave => $valor)
        {
            if (is_array($valorSelecionado) && in_array($chave, $valorSelecionado))
            {
                $complemento = "checked=\"checked\"";
            }
            elseif ($chave == $valorSelecionado)
            {
                $complemento = "checked=\"checked\"";
            }
            else
            {
                $complemento = "";
            }

            $strRetorno .= "<input type=\"checkbox\" name=\"{$nomeCheckBox}\" id=\"{$nomeCheckBox}_{$chave}\" value=\"{$chave}\" {$complemento}>{$valor}{$strQuebraDeLinha}";
        }

        return $strRetorno;
    }

    public static function getNivelSimilaridade($idNivel)
    {
        $idNivel = intval($idNivel, 10);
        $retorno = "";

        switch ($idNivel)
        {
            case 1:
                $retorno = "Baixa";
                break;
            case 2:
                $retorno = "M?dia";
                break;
            case 3:
                $retorno = "Alta";
                break;
        }

        return $retorno;
    }

    public static function getStringBoolean($valorBoolean)
    {
        if ($valorBoolean)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    public static function getStringDasVariaveisDaUrl()
    {
        return $_SERVER["QUERY_STRING"];
    }

    public static function getStringDasVariaveisDaUrlComExcessoes($arrExcecoes)
    {
        $string = urldecode($_SERVER["QUERY_STRING"]);

        if (strlen($string) > 1)
        {
            $pedacos = explode("&", $string);

            if (!is_array($arrExcecoes))
            {
                $arrExcecoes = array($arrExcecoes);
            }
            $strValor = "";
            for ($i = 0; $i < count($pedacos); $i++)
            {
                $excecao = false;

                foreach ($arrExcecoes as $valor)
                {
                    if (substr($pedacos[$i], 0, strlen($valor) + 1) == $valor . "=")
                    {
                        $excecao = true;
                    }
                }

                if (!$excecao)
                {
                    $strValor .= $pedacos[$i] . "&";
                }
            }

            return $strValor;
        }

        return "";
    }

    public static function getUrlCodificada()
    {
        return urlencode($_SERVER[REQUEST_URI]);
    }

    public static function getPathParaLibGrafica()
    {
        $retorno = self::acharRaiz();
        $retorno .= "recursos/libs/jpgraph/";

        return $retorno;
    }

    public static function getDiasToMesesAnos($dias)
    {
        $anos = 0;
        $meses = 0;

        $anos = floor($dias / 365);

        if ($anos % 365)
        {
            $meses = floor(($anos % 365) / 30);
        }
        elseif ($dias < 365)
        {
            $meses = floor($dias / 30);
        }

        $strRetorno = "";

        if ($anos)
        {
            $strRetorno .= ($anos == 1) ? "{$anos} ano" : "{$anos} anos";
        }

        if ($meses)
        {
            if (strlen($strRetorno))
            {
                $strRetorno .= " e ";
            }

            $strRetorno .= ($anos == 1) ? "{$anos} mes" : "{$anos} meses";
        }

        if (!strlen($strRetorno))
        {
            $strRetorno = "0 meses";
        }

        return "{$strRetorno}";
    }

    public static function acharRaizAngularJS()
    {
        $retorno = "";
        while (1)
        {
            if (!file_exists("{$retorno}__ng_root"))
            {
                $retorno .= "../";
            }
            else
            {
                break;
            }
        }

        return $retorno;
    }

    public static function REQUEST_URI()
    {
        if (Helper::isConsole())
        {
            return null;
        }
        else
        {
            return $_SERVER['REQUEST_URI'];
        }
    }

    public static function isConsole()
    {
        if (php_sapi_name() != 'cli')
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static function acharRaizWorkspace($retornarNumero = false)
    {
        if (static::$__raizWorkspace != null)
        {
            return $retornarNumero
                ? static::$__numeroRaizWorkspace
                : static::$__raizWorkspace;
        }
        if (php_sapi_name() != 'cli')
        {
            $niveis = substr_count($_SERVER["SCRIPT_FILENAME"], "/");
        }
        else
        {
            $pathDir = getcwd();
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
            {
                $niveis = substr_count($pathDir, "\\");
            }
            else
            {
                $niveis = substr_count($pathDir, "/");
            }
        }

        $root = "";
        $classPath = "";

        for ($i = 0; $i < $niveis; $i++)
        {
            if (is_dir("{$root}__workspaceRoot"))
            {
                static::$__raizWorkspace = $root;
                static::$__numeroRaizWorkspace = $i;

                return $retornarNumero ? $i : $root;
            }
            else
            {
                $root .= "../";
            }
        }
        if (is_dir("{$root}__workspaceRoot"))
        {
            static::$__raizWorkspace = $root;
            static::$__numeroRaizWorkspace = $i;

            return $retornarNumero ? $i : $root;
        }
        return $retornarNumero ? false : "";
    }

    public static function abrirNovaAba($location)
    {
        if (!is_numeric(self::$contadorNovaAba))
        {
            self::$contadorNovaAba = "1";
        }

        $idContadorAba = self::$contadorNovaAba;
        print "

        <a href=\"{$location}\" id=\"link_new_location{$idContadorAba}\" >
        <script language=\"javascript\">

        	$('#link_new_location{$idContadorAba}').click();

        </script>";

        self::$contadorNovaAba++;
    }

    public static function mudarLocation($location)
    {
        print "<script language=\"javascript\">
        	document.location.href=\"{$location}\";
              </script>";
    }

    public static function nl2br($texto)
    {
        $texto = str_replace("\n", "<br />", $texto);
        $texto = str_replace("\r", "", $texto);
        $texto = str_replace("\t", "", $texto);

        return $texto;
    }

    public static function substituirNulosPorZero($array)
    {
        if (is_array($array))
        {
            foreach ($array as $chave => $valor)
            {
                if (trim($valor) == "")
                {
                    $array[$chave] = 0;
                }
            }
        }

        return $array;
    }

    public static function getValorNumerico($texto)
    {
        if (!is_numeric($texto) || trim($texto) == "")
        {
            return 0;
        }
        else
        {
            return $texto;
        }
    }

    public static function getTagDoFavicon()
    {
        $path = self::acharRaiz();

        return "<link rel=\"shortcut icon\" href=\"{$path}favicon.ico\" type=\"image/x-icon\" />";
    }

    public static function importarBibliotecaParaGerarPDF()
    {
        $path = self::acharRaiz() . "recursos/libs/dompdf/";

        require_once("{$path}dompdf_config.inc.php");
    }

    public static function removerArquivosTemporarios()
    {
        $timeNow = time();

        $path = self::acharRaiz() . "temp/";

        $arquivos = self::getTodosOsArquivosDoDiretorio($path);
        if (!empty($arquivos))
        {
            foreach ($arquivos as $arquivo)
            {
                $timeArquivo = filemtime($path . $arquivo);

                if ($timeNow - $timeArquivo > 3600 * TIMEOUT_TEMPORARIOS_EM_HORAS)
                {
                    unlink($path . $arquivo);
                }
            }
        }
    }

    public static function getTodosOsArquivosDoDiretorio($directory, $recursive = false)
    {
        if (!file_exists($directory))
        {
            return;
        }
        $result = array();
        $handle = opendir($directory);
        while ($datei = readdir($handle))
        {
            if (($datei != '.') && ($datei != '..') && ($datei != '.htaccess'))
            {
                $file = $directory . $datei;
                if (is_dir($file))
                {
                    if ($recursive)
                    {
                        $result = array_merge($result, self::getTodosOsArquivosDoDiretorio($file . '/'));
                    }
                }
                else
                {
                    $result[] = $file;
                }
            }
        }

        closedir($handle);

        return $result;
    }

    public static function getCaminhoArquivo($pathCompleto)
    {
        $posicao1 = strrpos($pathCompleto, "/");

        return substr($pathCompleto, 0, $posicao1);
    }

    public static function getDiaDaSemanaPorExtenso($diaDaSemanaISO)
    {
        if (is_numeric($diaDaSemanaISO) && $diaDaSemanaISO >= 1 && $diaDaSemanaISO <= 7)
        {
            switch ($diaDaSemanaISO)
            {
                case "1":
                    return "Segunda-Feira";
                    break;
                case "2":
                    return "Ter?a-Feira";
                    break;
                case "3":
                    return "Quarta-Feira";
                    break;
                case "4":
                    return "Quinta-Feira";
                    break;
                case "5":
                    return "Sexta-Feira";
                    break;
                case "6":
                    return "S?bado";
                    break;
                case "7":
                    return "Domingo";
                    break;
            }
        }
    }

    public static function getDiferencaEntreDatasEmDias($timestamp1, $timestamp2)
    {
        $maiorTimestamp = 0;
        $menorTimestamp = 0;

        if ($timestamp1 >= $timestamp2)
        {
            $maiorTimestamp = $timestamp1;
            $menorTimestamp = $timestamp2;
        }

        if ($timestamp1 < $timestamp2)
        {
            $menorTimestamp = $timestamp1;
            $maiorTimestamp = $timestamp2;
        }

        $diferenca = ($maiorTimestamp - $menorTimestamp) / SEGUNDOS_EM_UM_DIA;

        return $diferenca;
    }

    public static function getUnixTimestampDeHora($hora)
    {
        $hora = str_replace("'", "", $hora);

        if (substr_count($hora, ":") > 0)
        {
            $partes = explode(":", $hora);

            if (count($partes) == 2)
            {
                $retorno = mktime($partes[0], $partes[1], 0, 1, 1, 2000);
            }
            elseif (count($partes) == 3)
            {
                $retorno = mktime($partes[0], $partes[1], $partes[2], 1, 1, 2000);
            }
        }

        return $retorno;
    }

    public static function addPostGet($key, $val)
    {
        if (Helper::$vetorPostGet == null)
        {
            Helper::$vetorPostGet = array();
        }
        Helper::$vetorPostGet[$key] = $val;
    }

    public static function setPostGet($matriz)
    {
        Helper::$vetorPostGet = $matriz;
    }

    public static function COOKIEPOSTGET($variavel)
    {
        $val = Helper::COOKIE($variavel);
        if ($val == null)
        {
            return Helper::POSTGET($variavel);
        }
        else
        {
            return $val;
        }
    }

    public static function COOKIE($variavel)
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                if (isset(Helper::$vetorPostGet[$variavel]))
                {
                    return Helper::$vetorPostGet[$variavel];
                }
                else
                {
                    return null;
                }
            }
        }
        else
        {
            if (isset($_COOKIE[$variavel]))
            {
                return ($_COOKIE[$variavel]);
            }
            else
            {
                return null;
            }
        }
    }

    public static function POSTGET($variavel, $urlDecode = false)
    {
        if (Helper::POST($variavel) != null)
        {
            return Helper::POST($variavel);
        }
        else
        {
            $get = Helper::GET($variavel);
            if (strlen($get) && $urlDecode)
            {
                $get = urldecode($get);
            }

            return $get;
        }
    }

    public static function POST($variavel)
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                if (isset(Helper::$vetorPostGet[$variavel]))
                {
                    return Helper::$vetorPostGet[$variavel];
                }
                else
                {
                    return null;
                }
            }
        }
        else
        {
            if (isset($_POST[$variavel]))
            {
                return ($_POST[$variavel]);
            }
            else
            {
                return null;
            }
        }
    }

    public static function getKeysPost()
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                return array_keys(Helper::$vetorPostGet);
            }
        }
        else
        {
            return array_keys($_POST);
        }
    }

    public static function getKeysGet()
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                return array_keys(Helper::$vetorPostGet);
            }
        }
        else
        {
            return array_keys($_GET);
        }
    }

    public static function removerQuebrasDeLinha($string, $substituirPorEspaco = false)
    {
        $arrSearch = array("\r\n", "\n");

        if ($substituirPorEspaco)
        {
            $arrReplace = array(" ", " ");
        }
        else
        {
            $arrReplace = array("", "");
        }

        return str_replace($arrSearch, $arrReplace, $string);
    }

    public static function gerarMenuPopup($idDiferenciacaoDiv, $arrayLinksMenu, $organograma = false)
    {
        $retorno = array("id" => "menu_popup_{$idDiferenciacaoDiv}",
            "conteudo" => "");

        $alturaDiv = count($arrayLinksMenu) * 13;

        $objLink = new Link();
        $strRetorno = "";
        $maiorTexto = 0;
        $strRetorno = "";
        foreach ($arrayLinksMenu as $objLink)
        {
            $objLink->organograma = $organograma;

            $strRetorno .= "&bull;&nbsp;{$objLink->montarLink()}<br />";

            $larguraTexto = strlen($objLink->label);

            if ($larguraTexto > $maiorTexto)
            {
                $maiorTexto = $larguraTexto;
            }
        }

        $larguraDiv = 30 + ceil($maiorTexto * 6.5);

        $strRetorno = "<div id=\"menu_popup_{$idDiferenciacaoDiv}\" onclick=\"javascript:this.style.display='none';\" class=\"div_menu_popup\" style=\"display: none; height:{$alturaDiv}px; width:{$larguraDiv}px;\">" . $strRetorno;

        $strRetorno .= "</div>";

        $retorno["conteudo"] = $strRetorno;

        return $retorno;
    }

    public static function gerarPopupDeInformacao($id, $informacao, $classeCss = "div_menu_informacoes", $arrEstiloAdicional = false, $larguraDinamica = false)
    {
        $objLink = new Link();

        if ($larguraDinamica)
        {
            $larguraDiv = 30 + ceil($informacao * 6.5);
            $arrEstiloAdicional[] = "width: {$larguraDiv}px";
        }
        $strEstiloAdicional = "";
        if (count($arrEstiloAdicional))
        {
            foreach ($arrEstiloAdicional as $estiloAdicional)
            {
                $strEstiloAdicional .= "$estiloAdicional; ";
            }
        }

        $strRetorno = "<div id=\"{$id}\" onclick=\"javascript:this.style.display='none';\" class=\"{$classeCss}\" style=\"display: none; {$arrEstiloAdicional}\">{$informacao}";

        $strRetorno .= "</div>";

        return $strRetorno;
    }

    public static function getComandoJavascriptComParametro($stringComando, $parametros = null)
    {
        $strP = '';
        if ($parametros != null && is_array($parametros))
        {
            foreach ($parametros as $p)
            {
                if (!empty($strP))
                {
                    $strP .= ",";
                }
                $strP .= json_encode($p);
            }
        }

        $string = "

            <script type=\"text/javascript\">
            $(document).ready(function(){
                try{
                    {$stringComando}($strP);
                }catch(ex){
                    ReportarErroExcecao(ex);
                }
            });
        </script>";

        return $string;
    }

    public static function imprimirComandoJavascriptAoCarregarDocumento($stringComando, $timerEmSegundos = false, $infinitamente = false)
    {
        if ($timerEmSegundos)
        {
            $comandoInterno = self::getComandoJavascriptComTimer($stringComando, $timerEmSegundos, $infinitamente, false);
        }
        else
        {
            $comandoInterno = $stringComando;
        }

        $strComando = "

			<script type=\"text/javascript\">

        		$('document').ready(function(){{$comandoInterno}});

            </script>";

        echo $strComando;
    }

    public static function getPathComBarrasUnix($path)
    {
        return str_replace("\\", "/", $path);
    }

    public static function getExtensaoDoArquivo($pathOuNomeDoArquivo)
    {
        if (substr_count($pathOuNomeDoArquivo, "tar.gz") > 0)
        {
            $extensao = "tar.gz";
        }
        else
        {
            $pos1 = strrpos($pathOuNomeDoArquivo, '.');
            $extensao = substr($pathOuNomeDoArquivo, $pos1 + 1);
        }

        return $extensao;
    }

    public static function corrigirBarrasPath($path)
    {
        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS)
        {
            $path = str_replace("/", "\\", $path);

            return $path;
        }
        else
        {
            return $path;
        }
    }

    public static function getBarraDaNextAction($arr)
    {
        $strRetorno = "\t<table class=\"table_next_action\">";
        $strRetorno .= "\t\t<tr class=\"tr_next_action\">";

        $i = 0;

        foreach ($arr as $chave => $valor)
        {
            if (count($arr) == 1)
            {
                $strRetorno .= "\t\t\t<td class=\"td_next_action\"><input type=\"hidden\" id=\"next_action\" name=\"next_action\" value=\"{$chave}\" ></td>";
                break;
            }

            if (is_numeric(self::GET("id1")))
            {
                $checked = (strpos($chave, "list_") == 0) ? "checked=\"checked\"" : "";
            }
            else
            {
                $id = self::GET("id1");
                $checked = $id == 0 ? "checked=\"checked\"" : "";
            }

            $titulo = ($i == 0) ? "Ap?s esta opera??o:&nbsp;" : "";

            $strRetorno .= "\t\t\t<td class=\"td_next_action\">{$titulo}<input id=\"next_action_{$i}\" type=\"radio\" name=\"next_action\" {$checked} value=\"{$chave}\" /><label for=\"next_action_{$i}\">{$valor}</label></td>";

            $i++;
        }

        $strRetorno .= "\t\t</tr>";
        $strRetorno .= "\t</table>";

        return $strRetorno;
    }

    public static function conferirPermissaoSobreEscrita($arquivo)
    {
        if (!file_exists($arquivo))
        {
            return true;
        }

        $handleArquivo = fopen($arquivo, "r");

        $tamanhoLido = 1024;

        if (filesize($arquivo) < $tamanhoLido)
        {
            $tamanhoLido = filesize($arquivo);
        }

        if ($tamanhoLido > 0)
        {
            $buffer = fread($handleArquivo, $tamanhoLido);

            if (substr_count($buffer, "@@NAO_MODIFICAR") > 0)
            {
                return false;
            } //para desabilitar verificacao, coloque 'return true';

        }

        return true;
    }

    public static function getBarraDeBotoesDoFormulario($botaoLimpar = true, $botaoCadastrar = true, $isEdicao = false, $labelAlternativo = "", $botaoVoltar = true, $actionVoltar = false, $mostrarNoRodapeDaPagina = true)
    {
        $idDiv = "";

        if ($mostrarNoRodapeDaPagina)
        {
            $idDiv = "div_barra_de_acoes";
        }

        $strRetorno = "<div id=\"{$idDiv}\">\n";
        $strRetorno .= "\t<table class=\"table_botoes_form\">\n";
        $strRetorno .= "\t\t<tr class=\"tr_botoes_form\">\n";
        $strRetorno .= "\t\t\t<td class=\"td_botoes_form\">\n";

        if ($botaoVoltar)
        {
            if (!$actionVoltar)
            {
                $actionVoltar = "history.go(-1);";
            }
            else
            {
                $actionVoltar = "document.location.href='$actionVoltar'";
            }

            $strRetorno .= "\t\t\t\t<input class=\"botoes_form\" type=\"button\" value=\"Voltar\" id=\"botao_submit\" onclick=\"javascript:{$actionVoltar}\">\n";
        }

        if ($botaoLimpar)
        {
            if (strlen($strRetorno))
            {
                $strRetorno .= "&nbsp;";
            }

            $strRetorno .= "\t\t\t\t<input class=\"botoes_form\" type=\"reset\" value=\"Limpar\">\n";
        }

        if ($botaoCadastrar && self::GET("visualizacao") != "true")
        {
            if (strlen($strRetorno))
            {
                $strRetorno .= "&nbsp;";
            }

            $label = "Salvar Dados";

            if ($isEdicao)
            {
                $label = "Salvar Altera??es";
            }

            if ($labelAlternativo != "")
            {
                $label = $labelAlternativo;
            }

            $strRetorno .= "\t\t\t\t<input class=\"botoes_form\" type=\"submit\" value=\"$label\">\n";
        }

        $strRetorno .= "\t\t\t</td>\n";
        $strRetorno .= "\t\t</tr>\n";
        $strRetorno .= "\t</table>\n";
        $strRetorno .= "</div>\n";

        return $strRetorno;
    }

    public static function imprimirBarraBotoes($botoes)
    {
        $strRetorno = "\t<table class=\"table_botoes_form\">\n";
        $strRetorno .= "\t\t<tr class=\"tr_botoes_form\">\n";
        $strRetorno .= "\t\t\t<td class=\"td_botoes_form\">\n";

        for ($i = 0; $i < count($botoes); $i++)
        {
            $tipo = $botoes[$i]->tipo;
            $label = $botoes[$i]->label;
            $action = $botoes[$i]->action;

            if (strlen($action) > 0)
            {
                $strAction = "onclick=\"$action\";";
            }

            $strRetorno .= "\t\t\t\t<input class=\"botoes_form\" type=\"$tipo\" {$strAction} value=\"$label\">\n";
        }

        $strRetorno .= "\t\t\t</td>\n";
        $strRetorno .= "\t\t</tr>\n";
        $strRetorno .= "\t</table>\n";

        return $strRetorno;
    }

    public static function imprimirBotoesAjax($botaoLimpar = true, $botaoCadastrar = true, $isEdicao = false)
    {
        $strRetorno = "\t<table class=\"table_botoes_form\">\n";
        $strRetorno .= "\t\t<tr class=\"tr_botoes_form\">\n";
        $strRetorno .= "\t\t\t<td class=\"td_botoes_form\">\n";

        if ($botaoLimpar)
        {
            $strRetorno .= "\t\t\t\t<input class=\"botoes_form\" type=\"reset\" value=\"Limpar\">\n";
        }

        if ($botaoLimpar && $botaoCadastrar)
        {
            $strRetorno .= "&nbsp;";
        }

        if ($botaoCadastrar)
        {
            $label = "Carregar Lista";

            if ($isEdicao)
            {
                $label = "Editar";
            }

            $strRetorno .= "\t\t\t\t<input id=\"botao_ok\" class=\"botoes_form\" type=\"button\" value=\"$label\" onclick=\"javascript:carregarAjaxAtual(false);\">\n";
        }

        $strRetorno .= "\t\t\t</td>\n";
        $strRetorno .= "\t\t</tr>\n";
        $strRetorno .= "\t</table>\n";

        return $strRetorno;
    }

    public static function imprimirBotoesList($botaoLimpar = true, $botaoPesquisar = true, $labelAlternativo = false)
    {
        $strRetorno = "\t<table class=\"table_botoes_filter\">";
        $strRetorno .= "\t\t<tr class=\"tr_botoes_form\">";
        $strRetorno .= "\t\t\t<td class=\"td_botoes_form\">";

        if ($botaoLimpar)
        {
            $textoLimpar = I18N::getExpression("Limpar");
            $strRetorno .= "<input class=\"botoes_form\" type=\"reset\" value=\"{$textoLimpar}\">";
        }

        if ($botaoLimpar && $botaoPesquisar)
        {
            $strRetorno .= "&nbsp;";
        }

        if ($botaoPesquisar)
        {
            if ($labelAlternativo)
            {
                $label = $labelAlternativo;
            }
            else
            {
                $label = I18N::getExpression("Pesquisar");
            }

            $strRetorno .= "<input class=\"botoes_form\" type=\"submit\" value=\"{$label}\">";
        }

        $strRetorno .= "\t\t\t</td>";
        $strRetorno .= "\t\t</tr>";
        $strRetorno .= "\t</table>";

        return $strRetorno;
    }

    public static function getUrlAction($actionPar, $id = "")
    {
        $posInicial = 0;

        if (substr_count($actionPar, "ajax"))
        {
            $posInicial = strpos($actionPar, "ajax") + 5;

            $action = substr($actionPar, 0, strpos($actionPar, "ajax") + 4);
            $page = substr($actionPar, $posInicial);
        }
        else
        {
            $action = substr($actionPar, 0, strpos($actionPar, "_"));
            $page = substr($actionPar, $posInicial + strpos($actionPar, "_") + 1);
        }

        if ($action == "add")
        {
            $strRet = "index.php?tipo=forms&page=$page";
        }
        elseif ($action == "add_ajax")
        {
            $strRet = "index.php?tipo=ajax_pages&page=$page";
        }
        elseif ($action == "edit")
        {
            $strRet = "index.php?tipo=forms&page=$page&id1=$id";
        }
        elseif ($action == "edit_ajax")
        {
            $strRet = "index.php?tipo=ajax_pages&page=$page&id1=$id";
        }
        elseif ($action == "list")
        {
            if (isset($_POST["url_origem"]))
            {
                $strRet = self::getUrlDecodificada(self::POST("url_origem"));
            }
            else
            {
                $strRet = "index.php?tipo=lists&page=$page";
            }
        }
        else
        {
            $strRet = "index.php?tipo=$action&page=$page";
        }

        return $strRet;
    }

    public static function getUrlDecodificada($urlCodificada)
    {
        return urldecode($urlCodificada);
    }

    public static function apagarArquivo($pathApartirDaRaiz)
    {
        if (file_exists($pathApartirDaRaiz))
        {
            unlink($pathApartirDaRaiz);
        }
    }

    public static function verificarUploadArquivo($nomeCampo)
    {
        if ($_FILES[$nomeCampo]["tmp_name"] != "")
        {
            return true;
        }

        return false;
    }

    public static function getTipoCampo($campo)
    {
        if (strpos($campo, "_DATETIME") !== false)
        {
            return "DATETIME";
        }
        elseif (strpos($campo, "_DATE") !== false)
        {
            return "DATE";
        }
        elseif (strpos($campo, "_FLOAT") !== false)
        {
            return "FLOAT";
        }
        elseif (strpos($campo, "_INT") !== false)
        {
            return "INT";
        }
        elseif (strpos($campo, "_BOOLEAN") !== false)
        {
            return "BOOLEAN";
        }
        elseif (strpos($campo, "_IMAGEM") !== false)
        {
            return "IMAGEM";
        }
        elseif (strpos($campo, "_ARQUIVO") !== false)
        {
            return "ARQUIVO";
        }
        elseif (strpos($campo, "cpf") !== false)
        {
            return "CPF";
        }
        elseif (strpos($campo, "cnpj") !== false)
        {
            return "CNPJ";
        }
        elseif (strpos($campo, "cep") !== false)
        {
            return "CEP";
        }
        elseif (strpos($campo, "email") !== false)
        {
            return "EMAIL";
        }
        elseif (strpos(strtolower($campo), "telefone") !== false || strpos(strtolower($campo), "celular"
                                                                                             || substr(strtolower($campo), strlen($campo) - 3, strlen($campo)) == "tel")
        )
        {
            return "TELEFONE";
        }
        else
        {
            return "TEXTO";
        }
    }

    public static function getLimitesRegsPaginacao($regsPagina)
    {
        $pagAtual = self::getPaginaAtual();
        $primeiroReg = ($pagAtual - 1) * $regsPagina;

        return array($primeiroReg, $regsPagina);
    }

    public static function getPaginaAtual()
    {
        if (self::GET("pagina"))
        {
            $pagina = self::GET("pagina");
        }
        else
        {
            $pagina = 1;
        }

        return $pagina;
    }

    public static function getNumeroPaginas($regsPagina, $regsTotal)
    {
        return ceil($regsTotal / $regsPagina);
    }

    public static function isNull($valor)
    {
        if ($valor == "null" || trim($valor) == "" || $valor == null || $valor == "'null'")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function getArrayComIntervaloNumerico($inicio = 1, $fim = 100)
    {
        $arrRetorno = array();

        for ($i = $inicio; $i <= $fim; $i++)
        {
            $arrRetorno[$i] = $i;
        }

        return $arrRetorno;
    }

    public static function getOptionsIntervaloNumerico($inicio = 1, $fim = 100, $selecionado = false)
    {
        $strRetorno = "<option value=\"\"></option>\n";

        for ($i = $inicio; $i <= $fim; $i++)
        {
            $selected = ($selecionado != false && $i == $selecionado) ? "selected=\"selected\"" : "";

            $strRetorno .= "<option $selected value=\"$i\">$i</option>\n";
        }

        return $strRetorno;
    }

    public static function getSubconjuntoNaoNuloDoArray($array)
    {
        $ultimoIndex = self::getUltimoIndiceComValorNaoNulo($array);

        return array_slice($array, 0, $ultimoIndex + 1, true);
    }

    public static function getUltimoIndiceComValorNaoNulo($array)
    {
        $i = 0;

        for ($i = count($array) - 1; $i >= 0; $i--)
        {
            if ($array[$i] != 0)
            {
                break;
            }
        }

        return $i;
    }

    public static function formatarDataGraficoParaExibicao($data)
    {
        $partes = explode(" ", $data);

        $ano = $partes[0];
        $mes = $partes[1];

        $mes = self::getAbreviacaoMes($mes);

        return "{$mes}/{$ano}";
    }

    public static function getAbreviacaoMes($mesNumerico)
    {
        if (!is_numeric($mesNumerico))
        {
            return "";
        }

        $mes = $mesNumerico + 0;
        $mesRetorno = "";

        switch ($mes)
        {
            case 1:
                $mesRetorno = "Jan";
                break;
            case 2:
                $mesRetorno = "Fev";
                break;
            case 3:
                $mesRetorno = "Mar";
                break;
            case 4:
                $mesRetorno = "Abr";
                break;
            case 5:
                $mesRetorno = "Mai";
                break;
            case 6:
                $mesRetorno = "Jun";
                break;
            case 7:
                $mesRetorno = "Jul";
                break;
            case 8:
                $mesRetorno = "Ago";
                break;
            case 9:
                $mesRetorno = "Set";
                break;
            case 10:
                $mesRetorno = "Out";
                break;
            case 11:
                $mesRetorno = "Nov";
                break;
            case 12:
                $mesRetorno = "Dez";
                break;
            default:
                $mesRetorno = "";
                break;
        }

        return $mesRetorno;
    }

    public static function getStringComPrimeirasLetrasDasPalavrasUC($string)
    {
        $arrExcessoes = array(" e ", " de ", " da ", " do ", " das ", " dos ", "III", "II");

        $arrUCExcessoes = array(" E ", " De ", " Da ", " Do ", " Das ", " Dos ", "Iii", "Ii");

        $string = ucwords(mb_strtolower($string));

        $string = str_replace($arrUCExcessoes, $arrExcessoes, $string);

        return $string;
    }

    public static function getActionParaExibicao($action)
    {
        switch ($action)
        {
            case "add":
                $retorno = "Adi��o de Registro";
                break;
            case "add_ajax":
                $retorno = "Adi��o de Multiplos Registros";
                break;
            case "edit":
                $retorno = "Edi��o de Registro";
                break;
            case "edit_ajax":
                $retorno = "Edi��o de Multiplos Registros";
                break;
            case "remove":
                $retorno = "Remo��o de Registro";
                break;
            case "login":
                $retorno = "Login do Sistema";
                break;
            case "logout":
                $retorno = "Logout do Sistema";
                break;
            default:
                $retorno = $action;
        }

        return $retorno;
    }

    public static function existsResultSet($resultSet, $colunaIndiceDoResultSet)
    {
        $matriz = self::getResultSetToMatriz($resultSet, 0, 1, $colunaIndiceDoResultSet);

        $listaRetorno = array();

        for ($i = 0; $i < count($matriz); $i++)
        {
            $listaRetorno[$matriz[$i][0]] = $matriz[$i][0];
        }

        return $listaRetorno;
    }

    public static function getResultSetToMatriz($resultSet, $assoc = 1, $num = 1, $colunaIndiceDoResultSet = false)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }
        if ($assoc && !$num)
        {
            $constante = MYSQL_ASSOC;
        }

        elseif (!$assoc && $num)
        {
            $constante = MYSQL_NUM;
        }

        elseif ($assoc && $num)
        {
            $constante = MYSQL_BOTH;
        }

        $arrayRetorno = array();
        for ($i = 0; $registro = mysqli_fetch_array($resultSet, $constante); $i++)
        {
            if ($colunaIndiceDoResultSet)
            {
                $indice = $registro[$colunaIndiceDoResultSet];
            }
            else
            {
                $indice = $i;
            }

            $arrayRetorno[$indice] = $registro;
        }

        return $arrayRetorno;
    }

    public static function getDataHoraParaCalendario($dataHoraAtual = false, $ano = false, $mes = false, $dia = false, $hora = false, $minuto = false)
    {
        if ($dataHoraAtual === true)
        {
            $ano = date("Y");
            $mes = date("m") - 1;
            $dia = date("d");
            $hora = date("H");
            $minuto = date("i");
        }
        else
        {
            $mes = $mes - 1;
        }

        $stringDataJS = "{$ano}, {$mes}, {$dia}, {$hora}, {$minuto}";

        return $stringDataJS;
    }

    public static function getResultSetToPrimeiroResultado($resultSet)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        for ($i = 0; $registro = mysqli_fetch_array($resultSet); $i++)
        {
            return $registro[0];
        }

        return null;
    }

    public static function getResultSetToArrays($resultSet, $colunaIndiceDoResultSet = false)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        $constante = MYSQL_NUM;

        $arrayRetorno = array();

        for ($i = 0; $registro = mysqli_fetch_array($resultSet, $constante); $i++)
        {
            if ($i == 0)
            {
                for ($j = 0; $j < count($registro); $j++)
                {
                    $arrayRetorno[$j] = array();
                }
            }
            for ($j = 0; $j < count($registro); $j++)
            {
                $arrayRetorno[count($arrayRetorno[$j])] = $registro[$j];
            }
        }
        if (!count($arrayRetorno))
        {
            return null;
        }
        else
        {
            return $arrayRetorno;
        }
    }

    public static function getResultFirstObject($resultSet, $assoc = 1, $num = 1, $printNull = false)
    {
        $m = Helper::getResultSetToMatriz($resultSet, $assoc, $num, false, $printNull);

        if (!empty($m))
        {
            return $m[0];
        }
        else
        {
            return null;
        }
    }

    public static function getPrimeiroObjeto($resultSet, $assoc = 1, $num = 1, $colunaIndiceDoResultSet = false)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }
        if ($assoc && !$num)
        {
            $constante = MYSQL_ASSOC;
        }
        elseif (!$assoc && $num)
        {
            $constante = MYSQL_NUM;
        }
        elseif ($assoc && $num)
        {
            $constante = MYSQL_BOTH;
        }

        for ($i = 0; $registro = mysqli_fetch_array($resultSet, $constante); $i++)
        {
            return $registro;
        }

        return null;
    }

    public static function getMatrizLinearToArray($matriz)
    {
        $retorno = array();

        for ($i = 0; $i < count($matriz); $i++)
        {
            $retorno[] = $matriz[$i][0];
        }

        return $retorno;
    }

    public static function somarValoresComAMesmaChaveDeDoisArrays($array1, $array2)
    {
        $arrResult = array_merge($array1, $array2);

        for ($i = 0; $i < count($arrResult); $i++)
        {
            $arrResult[$i] = 0;
        }

        foreach ($arrResult as $chave => $valor)
        {
            $valor1 = 0;
            $valor2 = 0;

            if (isset($array1[$chave]))
            {
                $valor1 = $array1[$chave];
            }

            if (isset($array2[$chave]))
            {
                $valor2 = $array2[$chave];
            }

            $arrResult[$chave] = $valor1 + $valor2;
        }
    }

    public static function getArrayComTodosOsValoresIguais($numeroPosicoes, $valor)
    {
        $arrRetorno = array();

        for ($i = 0; $i < $numeroPosicoes; $i++)
        {
            $arrRetorno[] = $valor;
        }

        return $arrRetorno;
    }

    public static function appendResultSetToArray($array, $resultSet, $assoc = 1, $num = 1)
    {
        if ($assoc && !$num)
        {
            $constante = MYSQL_ASSOC;
        }

        elseif (!$assoc && $num)
        {
            $constante = MYSQL_NUM;
        }

        elseif ($assoc && $num)
        {
            $constante = MYSQL_BOTH;
        }

        $arrayRetorno = $array;

        for ($i = count($array); $registro = mysqli_fetch_array($resultSet, $constante); $i++)
        {
            $arrayRetorno[$i] = $registro;
        }

        return $arrayRetorno;
    }

    public static function appendArrayToArray($arrayMenor, $arrayRetorno)
    {
        $j = 0;
        if (count($arrayMenor) == 0)
        {
            return $arrayRetorno;
        }

        $condicao = count($arrayRetorno) + count($arrayMenor);

        for ($i = count($arrayRetorno); $i < $condicao; $i++)
        {
            $arrayRetorno[$i] = $arrayMenor[$j];
            $j++;
        }

        return $arrayRetorno;
    }

    public static function removerOsPrimeirosCaracteresDaString($string, $numeroCaracteres = 1)
    {
        return substr($string, $numeroCaracteres, strlen($string) - ($numeroCaracteres));
    }

    public static function formatarCampoTextoHTMLParaSQL($string)
    {
        return htmlentities($string, ENT_QUOTES);
    }

    public static function formatarIntegerParaComandoSQL($numero)
    {
        if ($numero == "" || $numero == null)
        {
            return "null";
        }
        else
        {
            return $numero;
        }
    }

    public static function formatarFloatParaComandoSQL($numero)
    {
        $numero = trim($numero);

        if ($numero == "" || $numero == null)
        {
            return "null";
        }

        if (substr_count($numero, ",") > 0)
        {
            $retorno = str_replace(".", "", $numero);
            $retorno = str_replace(",", ".", $retorno);
        }
        else
        {
            if (substr_count($numero, ".") == 0 && is_numeric($numero))
            {
                $numero = "{$numero}.00";
            }

            $retorno = $numero;
        }

        return $retorno;
    }

    public static function aplicaMascaraString($str)
    {
        $str = I18N::getExpression($str);
        $str = strtoupper($str);

        return $str;
    }

    public static function formatarStringParaComandoSQL($numero, Database $db = null)
    {
        $format = trim($numero);

        if ($format == "" || $format == null)
        {
            return "null";
        }
        if ($db != null)
        {
            return "'" . $db->realEscapeString($numero) . "'";
        }
        else
        {
            return "'$numero'";
        }
    }

    public static function formatarFloatParaExibicao($numero, $casasDecimais = 2)
    {
        if (trim($numero) == "" || $numero == null)
        {
            return "";
        }

        if (substr_count($numero, ",") > 0)
        {
            return $numero;
        }
        else
        {
            return number_format($numero, $casasDecimais, ",", ".");
        }
    }

    public static function corrigirQuebrasDeLinha($pathArquivo)
    {
        if (is_file($pathArquivo))
        {
            $fileHandler = fopen($pathArquivo, 'r');

            $size = filesize($pathArquivo);
            if ($size)
            {
                $conteudo = fread($fileHandler, $size);
            }
            else
            {
                $conteudo = "";
            }
            fclose($fileHandler);
            $pos = strpos($conteudo, "\r\n");
            if (is_numeric($pos) && $pos >= 0)
            {
                $pathArquivoNew = "{$pathArquivo}_new";
                if (file_exists($pathArquivoNew))
                {
                    unlink($pathArquivoNew);
                }
                $fileHandlerWrite = fopen($pathArquivoNew, 'w');
                $novoConteudo = str_replace("\r\n", "\n", $conteudo);
                fwrite($fileHandlerWrite, $novoConteudo);
                fclose($fileHandlerWrite);

                unlink($pathArquivo);
                rename($pathArquivoNew, $pathArquivo);
            }
        }
    }

    public static function getResultSetToArrayDeUmCampo($resultSet)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        $arrayRetorno = array();

        for ($i = 0; $registro = mysqli_fetch_array($resultSet); $i++)
        {
            $indice = $i;

            $arrayRetorno[$indice] = $registro[0];
        }

        return $arrayRetorno;
    }

    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0)
        {
            return true;
        }

        $start = $length * -1; //negative

        return (substr($haystack, $start) === $needle);
    }

    public static function getPhpInputObject()
    {
        $phpInputObject = json_decode(file_get_contents('php://input'));
        if (is_null($phpInputObject))
        {
            $phpInputObject = new stdClass();
        }

        return $phpInputObject;
    }

    public static function getNomeDeEntidadeAPartirDoEmail($enderecoDeEmail)
    {
        if (static::isNullOrEmpty($enderecoDeEmail))
        {
            return null;
        }
        else
        {
            $posicaoArroba = strpos($enderecoDeEmail, "@");
            if ($posicaoArroba !== false)
            {
                return static::strtoupperlatin1(substr($enderecoDeEmail, 0, $posicaoArroba));
            }
            else
            {
                return static::strtoupperlatin1($enderecoDeEmail);
            }
        }
    }

    public static function strtoupperlatin1($pToken)
    {
        if ($pToken == null)
        {
            return null;
        }
        else
        {
            if (strlen($pToken))
            {
                $pToken = strtoupper(strtr($pToken, "�����������������������������", "�����������������������������"));

                return strtr($pToken, array("?" => "SS"));
            }
            else
            {
                return null;
            }
        }
    }

    public static function strtolowerlatin1($pToken)
    {
        if ($pToken == null)
        {
            return null;
        }
        else
        {
            if (strlen($pToken))
            {
                $pToken = strtolower(strtr($pToken, "�����������������������������", "�����������������������������"));

                return $pToken;
            }
            else
            {
                return null;
            }
        }
    }

    public static function retiraAcentoParaComandoSQL($pToken)
    {
        if ($pToken == null)
        {
            return null;
        }
        else
        {
            if ($pToken == "null")
            {
                return null;
            }
            else
            {
                if (!strlen($pToken))
                {
                    return null;
                }
                else
                {
                    $vValor = substr($pToken, 1, -1);
                    $vValorSemAcento = Helper::retiraAcento($vValor);

                    return $vValorSemAcento;
                }
            }
        }
    }

    public static function retiraAcento($pToken)
    {
        if ($pToken == null)
        {
            return null;
        }
        else
        {
            if (strlen($pToken))
            {
//            $pToken = preg_replace("/[^a-zA-Z0-9[-][_] ]/", "", strtr($pToken, "????????????????????????????", "aaaaeeiooouucnAAAAEEIOOOUUCN"));
                $pToken = strtr($pToken, "����������������������������", "aaaaeeiooouucnAAAAEEIOOOUUCN");

                return $pToken;
            }
            else
            {
                return null;
            }
        }
    }

    public static function formatarDataParaExibicao($valor)
    {
        if ($valor == "null" || trim($valor) == "")
        {
            return "";
        }
        else
        {
            if (substr_count($valor, "-") > 0)
            {
                $partes = explode("-", $valor);
                $retorno = $partes[2] . "/" . $partes[1] . "/" . $partes[0];

                return $retorno;
            }
        }

        return "";
    }

    public static function formatarHoraParaComandoSQL($valor)
    {
        if ($valor == "null" || trim($valor) == "")
        {
            return "null";
        }
        elseif (strlen($valor) == 5)
        {
            return "'{$valor}:00'";
        }
        elseif (strlen($valor) == 8)
        {
            return "'$valor'";
        }
        else
        {
            return "null";
        }
    }

    public static function formatarHoraParaExibicao($valor)
    {
        if ($valor == "null" || trim($valor) == "")
        {
            return "";
        }
        elseif (strlen($valor) == 8)
        {
            $valor = self::removerOsUltimosCaracteresDaString($valor, 3);

            return $valor;
        }
        elseif (strlen($valor) == 5)
        {
            return $valor;
        }
        else
        {
            return "";
        }
    }

    public static function formatarDataTimeParaExibicao($valor)
    {
        if ($valor == "null" || trim($valor) == "")
        {
            return "";
        }
        elseif (substr_count($valor, "-") > 0)
        {
            $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

            return $retorno;
        }

        return "";
    }

    public static function formatarBooleanParaExibicao($valor, $valorTrue = "Sim", $valorFalse = "N�o")
    {
        if (is_bool($valor))
        {
            return ($valor === true) ? $valorTrue : $valorFalse;
        }
        elseif (is_numeric($valor))
        {
            return ($valor >= 1) ? $valorTrue : $valorFalse;
        }
        elseif ($valor === "0")
        {
            return $valorFalse;
        }
        elseif ($valor === "1")
        {
            return $valorTrue;
        }

        return null;
    }

    public static function getBoolean($valor)
    {
        if (is_bool($valor))
        {
            return $valor;
        }
        elseif (is_numeric($valor))
        {
            return ($valor >= 1) ? true : false;
        }

        return null;
    }

    public static function imprimirMensagemNaoFormatada($mensagem)
    {
        print str_replace("\n", "<br />", $mensagem) . "<br />";
    }

    public static function getRadioList($nome, $arrayDeChavesEValores, $valorSelecionado, $arrayInativos = false)
    {
        $strRetorno = "";
        if (empty($arrayDeChavesEValores))
        {
            return null;
        }
        foreach ($arrayDeChavesEValores as $chave => $valor)
        {
            if (is_array($arrayInativos) && in_array($chave, $arrayInativos))
            {
                $complemento = "disabled=\"disabled\"";
            }
            else
            {
                $complemento = "";
            }

            $strRetorno .= "\t\t\t<input type=\"radio\" name=\"{$nome}\" id=\"{$nome}\" value=\"{$chave}\" " . (($chave == $valorSelecionado) ? "checked" : "") . " {$complemento}/>{$valor}\n&nbsp;&nbsp;";
        }

        return $strRetorno;
    }

    public static function getLinguaPadrao($deflang = LINGUA_PADRAO)
    {
        if ($deflang)
        {
            return $deflang;
        }

        if (Helper::isConsole())
        {
            return "pt-br";
        }

        $http_accept = $_SERVER["HTTP_ACCEPT_LANGUAGE"];

        if (isset($http_accept) && strlen($http_accept) > 1)
        {
            $x = explode(",", $http_accept);
            if (!empty($x))
            {
                foreach ($x as $val)
                {
                    if (preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i", $val, $matches))
                    {
                        $lang[$matches[1]] = (float)$matches[2];
                    }
                    else
                    {
                        $lang[$val] = 1.0;
                    }
                }
            }

            $qval = 0.0;
            if (!empty($lang))
            {
                foreach ($lang as $key => $value)
                {
                    if ($value > $qval)
                    {
                        $qval = (float)$value;
                        $deflang = $key;
                    }
                }
            }
        }

        return strtolower($deflang);
    }

    public static function getCorDoArrayGlobal()
    {
        if (self::$contadorNoArrayDeCores >= count(self::$arrayDeCores))
        {
            self::$contadorNoArrayDeCores = 0;
        }

        $retorno = self::$arrayDeCores[self::$contadorNoArrayDeCores];
        self::$contadorNoArrayDeCores++;

        return $retorno;
    }

    public static function getCorAleatoria()
    {
        mt_srand((double)microtime() * 1000000);
        $c = '';

        while (strlen($c) < 6)
        {
            $c .= sprintf("%02X", mt_rand(0, 255));
        }

        return $c;
    }

    public static function getEnderecoIPDoCliente()
    {
        return $_SERVER[REMOTE_ADDR];
    }

    public static function imprimirArquivoFonteFormatado($arquivoFonte, $incluirFieldset = true)
    {
        if ($incluirFieldset)
        {
            $identificador = rand(1, 1000);
            print self::getCabecalhoDeFieldsetDeExpansao("error_{$identificador}", "{$identificador}", "C?digo Fonte do Arquivo {$arquivoFonte}", INVISIVEL);
        }

        highlight_file($arquivoFonte);

        if ($incluirFieldset)
        {
            print self::getRodapeDeFieldsetDeExpansao();
        }
    }

    public static function getCabecalhoDeFieldsetDeExpansao($idComponente, $idNivel, $titulo, $estadoInicial = INVISIVEL, $funcaoExpansao = "")
    {
        $imagemCliqueParaExpandir = "imgs/padrao/expandir.png";
        $imagemCliqueParaEsconder = "imgs/padrao/esconder.png";

        if ($estadoInicial == VISIVEL)
        {
            $classPadrao = "div_expansao_visivel";
            $imagemPadrao = $imagemCliqueParaEsconder;
            $borderPadrao = "1px;";
        }
        else
        {
            $classPadrao = "div_expansao_invisivel";
            $imagemPadrao = $imagemCliqueParaExpandir;
            $borderPadrao = "0px;";
        }

        $strRetorno = "<fieldset id=\"fieldset_{$idComponente}\" class=\"fieldset_expansao\" style=\"border-width: {$borderPadrao}\">
        					<legend class=\"legend_expansao\">

        						<a href=\"javascript:void(0)\" class=\"link_vermelho\" id=\"link_$idComponente\"
        						   onclick=\"javascript:alterarVisibilidadeDiv(new Array('{$idComponente}'), 'div_expansao_visivel', 'div_expansao_invisivel', 'imagem_{$idComponente}', '{$imagemCliqueParaExpandir}', '{$imagemCliqueParaEsconder}', 'fieldset_{$idComponente}');
        						   if(document.getElementById('ajax_carregado_{$idComponente}').value == '0'){ $funcaoExpansao } \">
            					<img src=\"{$imagemPadrao}\" border=\"0\" id=\"imagem_{$idComponente}\"
            				    style=\"cursor: pointer;\" />
                                {$titulo}
                                </a>

                            </legend>
        				";

        $strRetorno .= "<input type=\"hidden\" id=\"ajax_carregado_{$idComponente}\" value=\"0\">";

        $strRetorno .= "<div id=\"{$idComponente}\" class=\"{$classPadrao}\">";

        return $strRetorno;
    }

    public static function getRodapeDeFieldsetDeExpansao()
    {
        $strRetorno = "";
        $strRetorno .= "</div>";
        $strRetorno .= "</fieldset>";

        return $strRetorno;
    }

    public static function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    public static function getServerIp()
    {
        return getHostByName(getHostName());
    }

    public static function getStringBacktrace($stringBacktrace)
    {
//        if (MODO_DE_DEPURACAO && in_array(self::getEnderecoIPDoCliente(), Seguranca::getIPsAutorizadosNaDepuracao())) {

        return $stringBacktrace;
//        } else {
//
//            return "N?o dispon?vel";
//        }
    }

    public static function getMDCdoArray($array)
    {
        if (count($array) < 1)
        {
            return false;
        }
        if (count($array) == 1)
        {
            return $array[0];
        }
        if ($array[0] == 1)
        {
            return array(1);
        }
        array_unshift($array, self::getMDC(array_shift($array), array_shift($array)));

        return self::getMDCdoArray($array);
    }

    public static function getMDC($a, $b)
    {
        if ($b > $a)
        {
            return self::getMDC($b, $a);
        }
        if ($b == 0)
        {
            return $a;
        }

        return self::getMDC($b, $a % $b);
    }

    public static function getBarraDeCoresPreferidas($idDoInput, $arrayDeCoresPreferidas)
    {
        if (empty($arrayDeCoresPreferidas))
        {
            return null;
        }
        $strRetorno = "";
        foreach ($arrayDeCoresPreferidas as $valor)
        {
            $strRetorno .= "<input value=\"\" cor=\"{$valor}\" onclick=\"javascript: mudarCorDaPaleta('$idDoInput', this);\" type=\"button\" class=\"botao_de_selecao_de_cor\" style=\"background-color: #{$valor};\" />";
        }

        return $strRetorno;
    }

    public static function getVazio($valor)
    {
        return null;
    }

    public static function getUrlDeReferenciaSemVariaveisDeMensagem()
    {
        $urlReferencia = $_SERVER["HTTP_REFERER"];

        return self::removerVariaveisDaUrl($urlReferencia, array("msgErro", "msgSucesso"));
    }

    public function removerVariaveisDaUrl($url, $arrNomesVariaveis)
    {
        if (empty($arrNomesVariaveis))
        {
            return $url;
        }
        foreach ($arrNomesVariaveis as $nomeVariavel)
        {
            $url = self::removerVariavelDaUrl($url, $nomeVariavel);
        }

        return $url;
    }

    public static function removerVariavelDaUrl($url, $nomeVariavel)
    {
        $posicao = strrpos($url, "{$nomeVariavel}=");

        if ($posicao !== false)
        {
            $posicaoUltimaVariavel = strrpos($url, "&");

            if ($posicaoUltimaVariavel > $posicao)
            {
                $strRetorno = substr($url, 0, $posicao - 1) . substr($url, $posicaoUltimaVariavel);
            }
            else
            {
                $strRetorno = substr($url, 0, $posicao - 1);
            }

            return $strRetorno;
        }
        else
        {
            return $url;
        }
    }

    public static function getScriptName()
    {
        return $_SERVER["SCRIPT_NAME"];
    }

    public static function isWebserviceFile()
    {
        $phpFile = Helper::getPhpFileName();

        return $phpFile == "webservice.php";
    }

    public static function getPhpFileName()
    {
        $str = $_SERVER["SCRIPT_NAME"];
        $index = strrpos($str, "/");

        if ($index != false)
        {
            return substr($str, $index + 1);
        }

        return null;
    }

    public static function verificarEmail($email)
    {
        if (empty($email))
        {
            return 0;
        }
        $email = strtolower($email);

        $mail_correcto = 0;
        //verifico umas coisas
        if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email, 0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@"))
        {
            if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) && (!strstr($email, "\$")) && (!strstr($email, " ")))
            {
                //vejo se tem caracter .
                if (substr_count($email, ".") >= 1)
                {
                    return 1;
                }
            }
        }

        if ($mail_correcto)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public static function removerMascaraDeTelefone($telefone)
    {
        $strRetorno = "";

        for ($i = 0; $i < strlen($telefone); $i++)
        {
            $posicao = substr($telefone, $i, 1);

            if (is_numeric($posicao))
            {
                $strRetorno .= $posicao;
            }
        }

        if (strlen($strRetorno) == 8)
        {
            $strRetorno = PREFIXO_PADRAO_TELEFONE . $strRetorno;
        }
        elseif (strlen($strRetorno) != 10)
        {
            $strRetorno = "";
        }

        return $strRetorno;
    }

    public static function removerCaracteresEspeciais($valor)
    {
        return strtr($valor, static::$caracteresAcentuados);
    }

    public static function getArrayComChaveEValorDasVariaveisDaURL($url)
    {
        if (strlen($url))
        {
            $dadosURL = parse_url($url);
            if (isset($dadosURL["query"]) && strlen($dadosURL["query"]))
            {
                parse_str($dadosURL["query"], $arrSaida);

                return $arrSaida;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function getDateNomeArquivo()
    {
        return date("d_m_Y");
    }

    public static function getDataHoraNomeArquivo()
    {
        return date("d_m_Y");
    }

    public static function getDiaNomeArquivo()
    {
        return date("d_m_Y");
    }

    public static function carregarCssLoginFlatty()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = Helper::carregarArquivoCss(0, "{$path}/css/flatty/", "login");

        return $str;
    }

    public static function carregarCssPagamentoFlatty()
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS;
        $str = Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets/bootstrap", "bootstrap");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets", "light-theme");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets", "theme-colors");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets/jquery/", "jquery_ui");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets/plugins/tabdrop/", "tabdrop");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets/plugins/jgrowl/", "jquery.jgrowl.min");
        $str .= Helper::carregarArquivoCss(0, "{$path}/css/flatty/assets/stylesheets/plugins/lightbox/", "lightbox");

        $str .= Helper::carregarArquivoCssDoProjeto("css/", "_custom");

        //$str .= Helper::carregarArquivoCss(0, "$path/css/flatty/assets/stylesheets", "demo") ;
        return $str;
    }

    public static function carregarArquivoCssDoProjeto($urlCssRelativoAoRootDoProjeto, $arquivoSemExtensao)
    {
        $path = Javascript::$pathNiveisAteARaizDoProjeto . PATH_RELATIVO_PROJETO . "/" . $urlCssRelativoAoRootDoProjeto;

        return self::carregarArquivoCssDaURL($path . $arquivoSemExtensao . ".css");
    }

    public static function carregarArquivoCssDaURL($url)
    {
        $strPrint = "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$url}\" />\n";

        return $strPrint;
    }

    public static function encodeURLGet($url)
    {
        $token = base64_encode($url);
        $token = urlencode($token);

        return $token;
    }

    /// public 'a' => string 'this should be set to property' (length=30)
    ///public 'b' => string 'and this also' (length=13)
    /// $a->set(array("a" => "this should be set to property", "b" => "and this also"));

    public static function decodeURLGet($url)
    {
        $token = urldecode($url);
        $token = base64_decode($token);

        return $token;
    }

    public static function getListaArquivoDoDiretorio($path)
    {
        $lista = array();
        if (is_dir($path))
        {
            if ($handle = opendir(Helper::getPathSemBarra($path)))
            {
                while (false !== ($file = readdir($handle)))
                {
                    if ($file != "." && $file != "..")
                    {
                        $lista[count($lista)] = Helper::getPathComBarra($path) . $file;
                    }
                }
                closedir($handle);
            }
        }

        return $lista;
    }

    public static function getPathSemBarra($path)
    {
        if ((substr($path, strlen($path) - 1, 1) == "/"))
        {
            return substr($path, 0, strlen($path) - 1);
        }

        return $path;
    }

    public static function vetorPhpToJavascript($vetor)
    {
        if (!empty($vetor))
        {
            $html = "";

            foreach ($vetor as $valor)
            {
                if (strlen($html))
                {
                    $html .= "'$valor'";
                }
                else
                {
                    $html .= "[ '$valor'";
                }
            }
            $html .= "];";

            return $html;
        }
    }

    public static function constroiGET($strGET, $divisor = "&")
    {
        $ret = array();
        if (strlen($strGET))
        {
            $vetor = explode($divisor, $strGET);
            if (!empty($vetor))
            {
                foreach ($vetor as $valor)
                {
                    $termo = explode("=", $valor);
                    if (count($termo) == 2)
                    {
                        $ret[$termo[0]] = $termo[1];
                    }
                }
            }
        }

        return $ret;
    }

    public static function getMensagem($mensagem, $tipoMensagem = MENSAGEM_OK)
    {
        //substitui \n por <br />
        $mensagem = nl2br($mensagem);

        if ($tipoMensagem == MENSAGEM_OK)
        {
            return "<div class='div_mensagem_ok'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_ok'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        if ($tipoMensagem == MENSAGEM_INFO)
        {
            return "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        elseif ($tipoMensagem == MENSAGEM_WARNING)
        {
            return "<div class='div_mensagem_warning'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_warning'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        elseif ($tipoMensagem == MENSAGEM_ERRO)
        {
            return "<div class='div_mensagem_erro'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_erro'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        elseif ($tipoMensagem == MENSAGEM_ASK)
        {
            return "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        return "";
    }

    public static function formatarCampoConsultaSQLParaSQL($string)
    {
        $vRet = str_replace("'", "\'", $string);

        return $vRet;
    }

    public static function getMensagemExcecao($exc)
    {
        if ($exc != null)
        {
            return "Stack: " . $exc->getTraceAsString() . ". Message: " . $exc->getMessage();
        }
        else
        {
            return "Excecao nula.";
        }
    }

    public static function vetorToString($pVetor, $pDelimitador)
    {
        if (empty($pVetor))
        {
            return null;
        }

        $str = "";
        foreach ($pVetor as $token)
        {
            if (strlen($str))
            {
                $str .= $pDelimitador . $token;
            }
            else
            {
                $str .= $token;
            }
        }

        return $str;
    }

    public static function formataGET($vetorChave, $vetorValor)
    {
        if (count($vetorChave) != count($vetorValor))
        {
            return false;
        }
        else
        {
            $str = "";
            for ($i = 0; $i < count($vetorChave); $i++)
            {
                $str .= "&" . $vetorChave[$i] . "=" . $vetorValor[$i];
            }

            return $str . "&";
        }
    }

    public static function getNomeAleatorio($pSufixo = "", $pExtensao = "")
    {
        if (strlen($pSufixo) > 5)
        {
            $pSufixo = substr($pSufixo, 0, 15);
        }

        $rand = rand(0, 1000000);

        return $pSufixo . "_" . $rand . $pExtensao;
    }

    public static function somaSegundosAData($data, $totalSegundos)
    {
        $dataModificada = date('Y-m-d H:i:s', strtotime($data) + $totalSegundos);

        return $dataModificada;
    }

    public static function arrayToSQL($array, Database $db = null)
    {
        if (!count($array))
        {
            return false;
        }
        $str = "";
        for ($i = 0; $i < count($array); $i++)
        {
            if (strlen($str))
            {
                $str .= ", ";
            }
            if ($array[$i] == null)
            {
                $str .= "null";
            }
            else
            {
                $conteudo = $array[$i];
                if ($db == null)
                {
                    $conteudo = str_replace("", "\'", $conteudo);
                }
                else
                {
                    $conteudo = $db->realEscapeString($conteudo);
                }
                $str .= "'$conteudo'";
            }
        }

        return $str;
    }

    public static function arrayToJson($array)
    {
        if (!count($array))
        {
            return false;
        }
        $str = "";
        for ($i = 0; $i < count($array); $i++)
        {
            if (strlen($str))
            {
                $str .= ",";
            }
            $str .= "\"" . $array[$i] . "\"";
        }

        return $str;
    }

    public static function getIdHTML($nome)
    {
        return str_replace(' ', '_', $nome);
    }

    public static function getFetchObject($resultSet, $assoc = 1, $num = 1, $colunaIndiceDoResultSet = false)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }

        if ($assoc && !$num)
        {
            $constante = MYSQL_ASSOC;
        }

        elseif (!$assoc && $num)
        {
            $constante = MYSQL_NUM;
        }

        elseif ($assoc && $num)
        {
            $constante = MYSQL_BOTH;
        }

        $arrayRetorno = array();

        for ($i = 0; $registro = mysqli_fetch_array($resultSet, $constante); $i++)
        {
            if ($colunaIndiceDoResultSet)
            {
                $indice = $registro[$colunaIndiceDoResultSet];
            }
            else
            {
                $indice = $i;
            }

            $arrayRetorno[$indice] = $registro;
        }

        return $arrayRetorno;
    }

    public static function postCurl($pURL, $pMatrizChavePorValorPost, $pNumeroTentativa = 1)
    {
        for ($i = 0; $i < $pNumeroTentativa; $i++)
        {
            try
            {
                if (MODO_CONSOLE && defined('PATH_CURL_CMD'))
                {
                    $cmd = "";
                    if (Helper::getSistemaOperacionalDoServidor() == WINDOWS)
                    {
                        $cmd = "\"" . Helper::getPathComBarra(PATH_CURL_CMD) . "curl.exe\"  -H \"Content-Type: application/json\" -X POST -d '"
                            . json_encode($pMatrizChavePorValorPost)
                            . "'  $pURL";
                    }
                    else
                    {
                        $pathComBarra = Helper::getPathComBarra(PATH_CURL_CMD);
                        $cmd = $pathComBarra . "curl  -H \"Content-Type: application/json\" -X POST -d '"
                            . json_encode($pMatrizChavePorValorPost)
                            . "'  $pURL";
                    }
                    $ret = Helper::exec($cmd);
                }
                else
                {
                    $handler = curl_init();
                    curl_setopt($handler, CURLOPT_TIMEOUT, 10);
                    curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($handler, CURLOPT_POSTFIELDS, $pMatrizChavePorValorPost);

                    curl_setopt($handler, CURLOPT_URL, $pURL);
                    curl_setopt($handler, CURLOPT_HEADER, false);

                    $vUserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
                    curl_setopt($handler, CURLOPT_USERAGENT, $vUserAgent);
                    curl_setopt($handler, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($handler, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($handler, CURLOPT_HTTPHEADER, array('Accept: text/plain,text/html', 'Accept-Charset: iso-8859-1, utf-8', 'Accept-Language: en-us, en-gb, pt-br'));
                    curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
                    $ret = curl_exec($handler);
                    curl_close($handler);
                }
                HelperLog::logCurl($pURL . ". Result: " . $ret);

                return $ret;
            }
            catch (Exception $ex)
            {
                if ($i < $pNumeroTentativa)
                {
                    sleep(rand(0, 5));
                }
                else
                {
                    throw $ex;
                }
            }
        }

        return null;
    }

    public static function isTokenInVector($pVetor, $pToken)
    {
        if (!empty($pVetor) && strlen($pToken) > 0)
        {
            $pAttrLower = strtolower($pToken);
            $i = 0;
            foreach ($pVetor as $vAttr)
            {
                if (strcmp(strtolower($vAttr), $pAttrLower) == 0)
                {
                    return $i;
                }
                $i += 1;
            }
        }

        return -1;
    }

    public static function formatarBooleanParaComandoSQL($str)
    {
        if (is_bool($str))
        {
            return ($str === true) ? "1" : "0";
        }
        elseif ($str === null || !strlen($str))
        {
            return "null";
        }
        else
        {
            if ($str == true || $str == 1 || $str == "1")
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
    }

    public static function resultSetExists($resultSet)
    {
        if (!function_exists("mysqli_connect"))
        {
            if (mysql_num_rows($resultSet) > 0)
            {
                mysql_data_seek($resultSet, 0);
            }
        }
        else
        {
            if (mysqli_num_rows($resultSet) > 0)
            {
                mysqli_data_seek($resultSet, 0);
            }
        }
        if (!function_exists("mysqli_connect"))
        {
            if (mysql_fetch_array($resultSet))
            {
                return true;
            }
        }
        else
        {
            if (mysqli_fetch_array($resultSet))
            {
                return true;
            }
        }

        return false;
    }

    public static function getDiaEHoraEMilisegundoAtual($utimestamp = null)
    {
        $format = 'Y-m-d H:i:s.u';
        if (is_null($utimestamp))
        {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }

    public static function sessionStart($prefixo = "")
    {
        if ($prefixo == "")
        {
            if (defined('IDENTIFICADOR_SESSAO'))
            {
                $prefixo = IDENTIFICADOR_SESSAO;
            }
            else
            {
                $configuracaoSite = Registry::get('ConfiguracaoSite');
                if ($configuracaoSite != null)
                {
                    $prefixo = $configuracaoSite->IDENTIFICADOR_SESSAO;
                }
            }
        }
        if (MODO_SESSION == 'REDIS')
        {
            SessionRedis::getSingleton($prefixo);
        }
        else
        {
            session_name($prefixo);
            session_start();
        }
    }

    public static function killSession($prefixo)
    {
        if (MODO_SESSION == 'REDIS')
        {
            SessionRedis::getSingleton($prefixo);
        }
        else
        {
            session_destroy();
        }
    }

    public static function clearSession($key)
    {
        if (MODO_SESSION == 'REDIS')
        {
            $s = SessionRedis::getSingleton();
            if ($s != null)
            {
                $s->clear($key);
            }
        }
        else
        {
            unset($_SESSION[$key]);
        }
    }

    public static function getSession($key, $isArrayOuObjeto = false)
    {
        try
        {
            if (MODO_SESSION == 'REDIS')
            {
                $s = SessionRedis::getSingleton();
                if ($s != null)
                {
                    $obj = $s->get($key);
                    return $obj;
                }

                return null;
            }
            else
            {
                return $_SESSION[$key];
            }
        }
        catch (Exception $ex)
        {
            throw new Exception("getSession -$key, $isArrayOuObjeto", 0, $ex);
        }
    }

    public static function formatHtmlSpecialChars(&$array)
    {
        if (empty($array))
        {
            return;
        }
        foreach ($array as $key => &$value)
        {
            if (is_string($key))
            {
                $newkey = Helper::formatStringEntites($key);

                if (strlen($newkey) != strlen($key))
                {
                    $array[$newkey] = $value;
                    unset($array[$key]);
                    $key = $newkey;
                }
            }

            if (is_string($value))
            {
                $value = Helper::formatStringEntites($value);
            }
            else
            {
                if (is_array($value))
                {
                    Helper::formatHtmlSpecialChars($value);
                }
            }
        }
    }

    public static function formatStringEntites($t)
    {
        $e = ENT_COMPAT;
        if (!$e && defined('ENT_HTML401'))
        {
            $e = ENT_HTML401;
        }

        $t = htmlspecialchars(htmlentities($t, $e, "ISO-8859-1"));

        return $t;
    }

    public static function setSession($key, $value)
    {
        if (MODO_SESSION == 'REDIS')
        {
            $s = SessionRedis::getSingleton();
            if ($s != null)
            {
                $s->set($key, $value);
            }
        }
        else
        {
            $_SESSION[$key] = $value;
        }
    }

    public static function clearCookie($key)
    {
        if (Helper::isSetCookie($key))
        {
            $result = setcookie($key, "", time() - 3600, Helper::acharRaiz());
            HelperLog::verbose("[wertg4234]clearCookie ($key): $result ");
        }
        else
        {
            HelperLog::verbose("[wertg4234] cookie was already cleaned !! ($key) ");
        }
    }

    public static function isSetCookie($variavel)
    {
        if (MODO_CONSOLE)
        {
            if (Helper::$vetorPostGet == null)
            {
                return null;
            }
            else
            {
                return Helper::$vetorPostGet[$variavel];
            }
        }
        else
        {
            return isset($_COOKIE[$variavel]);
        }
    }

    public static function setCookie($key, $value, $time = 0)
    {
        $raiz = Helper::acharRaiz();
        HelperLog::verbose("234fsdf setCookie($key) - $value [$time]. $raiz");

        setcookie(
            $key,
            $value,
            $time,
            $raiz);
    }

    public static function getDescricaoException($exc)
    {
        return "Message: " . $exc->getMessage() . ". Stacktrace: " . $exc->getTraceAsString();
    }

    public static function formatarStringJs($token)
    {
        $token = str_replace("'", "\'", $token);
//        echo "1: $token\n";
        $token = preg_replace("/\r|\n/", " ", $token);
//        echo "2: $token\n";
        $token = Helper::formatStringEntites($token);
//        echo "3: $token\n";
//        exit();
        return $token;
    }

    public static function formataPathDiretorio($pathRaiz, $diretorio = null)
    {
        if (strlen($diretorio))
        {
            return Helper::getPathComBarra($pathRaiz) . Helper::getPathComBarra($diretorio);
        }
        else
        {
            return Helper::getPathComBarra($pathRaiz);
        }
    }

    public static function getPathComBarraDiretorio($path)
    {
        return Helper::getPathComBarra($path);
    }

    public static function includePHPDoProjeto($pathArquivoRelativo)
    {
        if (!empty($pathArquivoRelativo))
        {
            $path = Javascript::$pathNiveisAteARaizDoProjeto . $pathArquivoRelativo;
            Helper::includePHPDoPath($path);
        }
    }

    public static function includePHPDoPath($pathArquivo)
    {
//        if(!file_exists($pathArquivo))
//            echo $pathArquivo;
        include($pathArquivo);

        return true;
    }

    public static function includePHPDaBibliotecaCompartilhada($pathArquivoRelativo)
    {
        $path = Javascript::$pathNiveisAteARaizDoWorkspace . __PATH_BIBLIOTECAS_COMPARTILHADAS . $pathArquivoRelativo;
        Helper::includePHPDoPath($path);
    }

    public static function carregarArquivoCssDaBibliotecaCompartilhada(
        $pathArquivoRelativo, $arquivoSemExtensao)
    {
        $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/" . $pathArquivoRelativo;

        return self::carregarArquivoCssDaURL($path . "/" . $arquivoSemExtensao . ".css");
    }

    public static function getUrlRaizDoProjeto($urlRelativoAInterfaceDoProjeto = "adm/")
    {
        $path = Javascript::$pathNiveisAteARaizDoProjeto . PATH_RELATIVO_PROJETO . "/" . $urlRelativoAInterfaceDoProjeto;

        return $path;
    }

    public static function carregarArquivoJavascriptDaUrl($url)
    {
        $strPrint = "<script type=\"text/javascript\" src=\"{$url}\" ></script>\n";

        return $strPrint;
    }

    public static function getDebugStackTrace()
    {
        ob_start();
        debug_print_backtrace();
        $trace = ob_get_contents();
        ob_end_clean();

        return $trace;
    }

    public static function explode($delimiter, $token, $limit = null)
    {
        if (!strlen($token))
        {
            return;
        }
        $vetor = preg_split($delimiter, $token);

        if (count($vetor) == 1)
        {
            return null;
        }
        else
        {
            return $vetor;
        }
    }

    public static function shellExec($cmd)
    {
        HelperLog::logExec("Cmd: " . $cmd . " ...");

        $ret = shell_exec($cmd);
        HelperLog::logExec("Retorno: $ret.");

        return $ret;
    }

    public static function getArrayGETParaJS()
    {
        self::corrigirObjetoParaJsonEncode($_GET);
        $json = json_encode($_GET);

        return "JSON.parse({$json});";
    }

    public static function jsonEncode($obj)
    {
        self::corrigirObjetoParaJsonEncode($obj);
        return json_encode($obj);
    }

    public static function corrigirObjetoParaJsonEncode(&$object)
    {
        if (is_array($object))
        {
            foreach ($object as $key => $value)
            {
                self::corrigirObjetoParaJsonEncode($object[$key]);
            }
        }
        elseif (is_object($object))
        {
            foreach ($object as $key => $value)
            {
                self::corrigirObjetoParaJsonEncode($object->$key);
            }
        }
        elseif (is_string($object))
        {
            $object = utf8_encode($object);
        }
    }

    public static function checarSeRequisicaoIsAjaxAngularJS()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function gerarObjetoJSDeVariaveisGET()
    {
        $objRetorno = new stdClass();
        foreach ($_GET as $chave => $valor)
        {
            $objRetorno->$chave = $valor;
        }

        return "var __GET = JSON.parse(\"" . str_replace('"', '\"', json_encode($objRetorno)) . "\");";
    }

    public static function incluirControlllerCustomizadoAngularJS($diretorioController)
    {
        $strRetorno = "";
        if (!is_null($diretorioController))
        {
            $strPathAdm = self::getRaizDoAdm();
            $diretorioControllers = "{$strPathAdm}/{$diretorioController}";
            $strRetorno .= "<script src=\"{$diretorioControllers}/controller.js\" type=\"text/javascript\"></script>\r\n\r\n";
        }
        return $strRetorno;
    }

    public static function incluirControllerAngularJS($incluirControllerEntidadeCorrente = true, $arrEntidadesControllers = null, $versionarCarregamentoCliente = true)
    {
        $strRetorno = "";
        $strVersionamento = "";

        if ($versionarCarregamentoCliente)
        {
            $strVersionamento = "?v=1.0";
        }

        if ($incluirControllerEntidadeCorrente)
        {
            $diretorioControllers = self::getDiretorioDaViewCorrente();
            $strRetorno .= "<script src=\"{$diretorioControllers}/controller.generated.js{$strVersionamento}\" type=\"text/javascript\"></script>\r\n";
            $strRetorno .= "<script src=\"{$diretorioControllers}/controller.js{$strVersionamento}\" type=\"text/javascript\"></script>\r\n";
        }

        if (!is_null($arrEntidadesControllers))
        {
            $strPathAdm = self::getRaizDoAdm();
            foreach ($arrEntidadesControllers as $entidadeContoller)
            {
                $diretorioControllers = "{$strPathAdm}/{$entidadeContoller}";
                $strRetorno .= "<script src=\"{$diretorioControllers}/controller.generated.js{$strVersionamento}\" type=\"text/javascript\"></script>\r\n";
                $strRetorno .= "<script src=\"{$diretorioControllers}/controller.js{$strVersionamento}\" type=\"text/javascript\"></script>\r\n\r\n";
            }
        }

        return $strRetorno;
    }

    public static function getDiretorioDaViewCorrente()
    {
        $tipoPagina = Helper::GET("tipo");
        $pagina = Helper::GET("page");

        return self::getDiretorioDaView($tipoPagina, $pagina);
    }

    public static function getDiretorioDaView($tipoPagina, $pagina)
    {
        $diretorioAdm = self::getRaizDoAdm();
        $diretorioView = "{$diretorioAdm}/{$tipoPagina}/{$pagina}";

        return $diretorioView;
    }

    public static function getRaizDoAdm()
    {
        $strRaiz = self::getStringNiveisAteRaiz();

        return "{$strRaiz}adm";
    }

    public static function isAngularModal()
    {
        return strtolower(basename($_SERVER["SCRIPT_FILENAME"])) == "angularjs_modal.php";
    }

    public static function incluirConteudoDataServices($importarLocalAutomaticamente = true, $arrDataServicesImportacao = null)
    {
        $strJavascript = "omegaApp.factory('dataService',
                    [\"\$http\",
                        function (\$http) {

                            //utilizar esta vari�vel para definir fun��es
                            returnObject = {};

                        ";

        if ($importarLocalAutomaticamente)
        {
            $diretorioDataService = self::getDiretorioDaViewCorrente();
            $strJavascript .= file_get_contents("{$diretorioDataService}/data.service.js");
        }

        if (is_array($arrDataServicesImportacao))
        {
            $strPathAdm = self::getRaizDoAdm();
            foreach ($arrDataServicesImportacao as $path)
            {
                $strJavascript .= file_get_contents("{$strPathAdm}/{$path}/data.service.js");
            }
        }

        $strJavascript .= "
            
            window.dataServiceReturnObject = returnObject;
            return returnObject; }]);";

        return $strJavascript;
    }

    public static function includeSubView($subviewName)
    {
        Helper::includePHP(false, "adm/" . Helper::GET("tipo") . "/" . Helper::GET("page") . "/{$subviewName}.php");
    }



    public static function includePHP(
        $niveisRaiz = false,
        $caminhoApartirRaiz,
        $includeOnce = true,
        $printError = false)
    {
        if ($niveisRaiz === false)
        {
            $strRaiz = self::acharRaiz();
        }
        else
        {
            $strRaiz = self::getStringNiveisAteRaiz($niveisRaiz);
        }

        if (is_file($strRaiz . $caminhoApartirRaiz))
        {
            if ($includeOnce)
            {
                include_once($strRaiz . $caminhoApartirRaiz);
            }
            else
            {
                include($strRaiz . $caminhoApartirRaiz);
            }

            return true;
        }
        else
        {
            if (self::isDebugModeActive())
            {
                HelperLog::logWarning("$caminhoApartirRaiz - n�o foi encontrado.");
            }

            return false;
        }
    }

    public static function isDebugModeActive()
    {
        return (Helper::GET("debug") === "true") ? true : false;
    }

    public static function isNullOrEmpty($valor)
    {
        if ((is_string($valor) && ($valor == "null" || trim($valor) == "")) || $valor == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function objectToArray($databaseFetchObject)
    {
        $arrRetorno = array();
        foreach ($databaseFetchObject as $property => $value)
        {
            $arrRetorno[$property] = $value;
        }

        return $arrRetorno;
    }

    public static function getResultSetToLista($resultSet, $colunaIndiceDoResultSet)
    {
        $matriz = self::getResultSetToMatriz($resultSet, 0, 1, $colunaIndiceDoResultSet);

        $listaRetorno = array();

        for ($i = 0; $i < count($matriz); $i++)
        {
            $listaRetorno[$matriz[$i][0]] = $matriz[$i][0];
        }

        return $listaRetorno;
    }

    public static function removerCaracteresNaoValidosEmVariaveis()
    {
        $arrCaracteres = array("/", "{", "(", "+", "-", ".", ";", ",");
    }

    public static function pathCombine()
    {
        $args = func_get_args();
        $paths = array();
        foreach ($args as $arg)
        {
            $paths = array_merge($paths, (array)$arg);
        }

        $paths = array_map(create_function('$p', 'return trim($p, "/");'), $paths);
        $paths = array_filter($paths);

        return join('/', $paths);
    }

    public static function getStringNormalizada($string)
    {
        return Helper::retiraAcento($string);
    }

    public static function isPhpSessionStarted()
    {
        if (php_sapi_name() !== 'cli')
        {
            if (version_compare(phpversion(), '5.4.0', '>='))
            {
                return session_status() === PHP_SESSION_ACTIVE ? true : false;
            }
            else
            {
                return session_id() === '' ? false : true;
            }
        }

        return false;
    }

    public static function getDiaEHoraFuturaSQL($somarMinutos)
    {
        if ($somarMinutos > 0)
        {
            return date("Y-m-d H:i:s", strtotime("+{$somarMinutos} minutes"));
        }
        else
        {
            return date("Y-m-d H:i:s", strtotime("{$somarMinutos} minutes"));
        }
    }

    public static function execInBackground($cmd)
    {
        try
        {
            if (substr(php_uname(), 0, 7) == "Windows")
            {
                $q = "start /B " . $cmd;
                HelperLog::logExec($q . " ...");

                $out = "";
                $h = popen($q, "w");
                //$h = new COM('WScript.Shell');
                //$h->Run($q, 0, false);
                HelperLog::logExec("R.: " . print_r($h, true) . ". Type: " . gettype($h) . "\n");
                pclose($h);
            }
            else
            {
                $q = $cmd . " > /dev/null &";
                Helper::exec($q);
            }
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, $cmd);
        }
        catch (Exception $ex)
        {
            HelperLog::logErro($ex, null, $q);
            return new Mensagem(null, null, $ex);
        }
    }

    public static function getUTCNowSec()
    {
        $time = strtotime(gmdate("M d Y H:i:s"));
        return $time;
    }

    public static function getTimezoneOffsetSec()
    {
        if (self::$__currentOffset == null)
        {
            self::$__currentOffset = date('Z');
        }

        return self::$__currentOffset;
    }

    public static function killAll($pids)
    {
        $os = stripos(php_uname('s'), 'win') > -1;
        ($_ = implode($os ? ' /PID ' : ' ', $pids)) or ($_ = $pids);

        return preg_match('/success|close/',
                          $os ? self::exec("taskkill /F /PID $_") : self::exec("kill -9 $_"));
    }

    public static function appendOrCreateFile($filepath, $txt)
    {
        return file_put_contents($filepath, $txt . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

    function getStringJavascript($s)
    {
        return '"' . addcslashes($s, "\0..\37\"\\") . '"';
    }

    function getStringArrayJavascript($array)
    {
        $temp = array_map('js_str', $array);
        return '[' . implode(',', $temp) . ']';
    }

    public function imprimirMensagemExcecao($exc)
    {
        $msg = "MESSAGE=" . $exc->getMessage() . "</br>STACK:" . $exc->getTraceAsString();
        Helper::imprimirMensagem($msg, MENSAGEM_ERRO);
    }

    public static function imprimirMensagem($mensagem, $tipoMensagem = MENSAGEM_OK, $linkImagem = false)
    {
        $mensagemOriginal = $mensagem;
        if (strlen($linkImagem))
        {
            $strLink = "onclick=\"document.location.href='{$linkImagem}'\"";
        }
        else
        {
            $strLink = "";
        }

        //substitui \n por <br />
        $mensagem = nl2br($mensagem);

        if ($tipoMensagem == MENSAGEM_OK)
        {
            echo "<div class='div_mensagem_ok'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_ok'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }

        if ($tipoMensagem == MENSAGEM_INFO)
        {
            echo "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }
        elseif ($tipoMensagem == MENSAGEM_WARNING)
        {
            echo "<div class='div_mensagem_warning'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_warning'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }
        elseif ($tipoMensagem == MENSAGEM_ERRO)
        {
            echo "<div class='div_mensagem_erro'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_erro'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }
        elseif ($tipoMensagem == MENSAGEM_ASK)
        {
            echo "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>{$mensagem}</td></tr></table></div>";
        }
//            elseif ($tipoMensagem == MENSAGEM_ANDROID)
//            {
//                echo "<div class='div_mensagem_android'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_android' {$linkImagem}>&nbsp;</td><td class='div_conteudo_mensagem_android'>{$mensagem}</td></tr></table></div>";
//            }

        echo "\n";

        return "";
    }

    public function imprimirArrayPhpToJavascript($php_array, $nomeVarJs, $imprimir = true)
    {
        $js_array = json_encode($php_array);

        $str = Helper::getComandoJavascript("var $nomeVarJs = " . $js_array . ";\n");
        if ($imprimir)
        {
            echo $str;
        }
        else
        {
            return $str;
        }
    }

    function randomString($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
    ////killall: without using array_map and a boolean return value
    /// if( $killall([19008,23012,1802,930]) and $killall(19280)){

    function set($obj, array $array)
    {
        if (empty($array))
        {
            return;
        }
        $reflektion = new ReflectionClass($obj);
        foreach ($array AS $property => $value)
        {
            $property = $reflektion->getProperty($property);
            if ($property instanceof ReflectionProperty)
            {
                $property->setValue($obj, $value);
            }
        }
    }

    public function escapeStringSQL($token)
    {
        if (!strlen($token))
        {
            return $token;
        }

        return str_replace("_", "\_", $token);
    }

    public static function factory()
    {
        return new Helper();
    }

    public static function teste()
    {
        $colunas = array("senha", "x");
        $temp = array();
        foreach ($colunas as $col)
        {
            if ($col == "senha")
            {
                continue;
            }
            $temp[count($temp)] = $col;
        }
        $colunas = $temp;
        //  unset($colunas[$indexSenha]);
        $select = Helper::arrayToSQL($colunas, new Database());
        //}

        $jsonColunas = json_encode($colunas);
        echo $jsonColunas;
    }

    public static function mkdir($path, $mode = 0777, $recursive = true)
    {
        HelperLog::logMkdir("mkdir($path, $mode, $recursive) ...");
        $ret = mkdir($path, $mode, $recursive);
        HelperLog::logMkdir("mkdir($path, $mode, $recursive) - $ret");
        return $ret;
    }

    public static function getPidsRunning($pids)
    {
        // $pids = array(3528,3529, 15165465 );
        if (Helper::getSistemaOperacionalDoServidor() == WINDOWS)
        {
            throw new Exception ("N�o implementado!");
        }
        $strIds = Helper::arrayToString($pids, ",");
        $cmd = "ps -p $strIds -o pid";

        $out = array();
        self::exec($cmd, $out);

        if ($out != null && !empty($out))
        {
            $idsRet = array();
            for ($i = 1; $i < count($out); $i++)
            {
                $idsRet[count($idsRet)] = $out[$i];
            }

            if (empty($idsRet))
            {
                return null;
            }
            return $idsRet;
        }

        return null;
    }

    public static function getPidsNotRunning($pids)
    {
//            $pids = array("3528","3529", "15165465" );

        $idsRunning = self::getPidsRunning($pids);
        return Helper::arrayNotIn($pids, $idsRunning);
    }

    public static function arrayNotIn($arrayTot, $arrayParcial)
    {
        $idsNotRunning = array();
        for ($i = 0; $i < count($arrayTot); $i++)
        {
            $index = array_search($arrayTot[$i], $arrayParcial);
            if ($index === false)
            {
                $idsNotRunning [count($idsNotRunning)] = $arrayTot[$i];
            }
        }

        if (empty($idsNotRunning))
        {
            return null;
        }
        else
        {
            return $idsNotRunning;
        }
    }

    public static function formatarParametrosStrGet($strGet)
    {
        $strGet = urldecode($strGet);
        $parametros = explode("&", $strGet);
        $chaves = array();
        $valores = array();
        $hash = array();
        for ($i = 0; $i < count($parametros); $i++)
        {
            if (strlen($parametros[$i]))
            {
                $chaveValor = explode("=", $parametros[$i]);
//                        echo "CHAVE VALOR: ";
//                        print_r($chaveValor);
                if (count($chaveValor) == 2)
                {
                    $chaveParametro = $chaveValor[0];

                    if (strlen($chaveParametro))
                    {
                        $chaves[count($chaves)] = $chaveParametro;
                        $valores[$chaveParametro] = $chaveValor[1];
                        $hash[$chaveParametro] = $chaveValor[1];
                    }
                }
                else
                {
                    if (count($chaveValor) == 1)
                    {
                        $chaveParametro = $chaveValor[0];

                        if (strlen($chaveParametro))
                        {
                            $chaves[count($chaves)] = $chaveParametro;
                            $valores[$chaveParametro] = null;
                            $hash[$chaveParametro] = null;
                        }
                    }
                }
            }
        }

        $retorno['chaves'] = $chaves;
        $retorno['valores'] = $valores;
        $retorno['hash'] = $hash;
        return $retorno;
    }

    public static function getHoraAtualDoTimezone($timezone = "America/Brasilia")
    {
        $date = new DateTime("now", new DateTimeZone($timezone));
        //echo $date->format('Y-m-d H:i:sP');
        $hora = $date->format('H');
        return $hora;
    }

    public static function validaHorarioFuncionamento($horaMinima, $horaMaxima, $timezone = "America/Sao_Paulo")
    {
        $hora = Helper::getHoraAtualDoTimezone($timezone);
        if ($horaMinima <= $hora && $horaMaxima >= $hora)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function toStringDatetime(DateTime $time)
    {
        $strFormat = I18N::getExpression('d/m/Y H:i');
        $stamp = $time->format($strFormat);
        return $stamp;
    }

    public static function toStringDate(DateTime $time)
    {
        $strFormat = I18N::getExpression('d/m/Y');
        $stamp = $time->format($strFormat);
        return $stamp;
    }

    public static function toStringDatetimeAmerican(DateTime $time)
    {
        $strFormat = I18N::getExpression('Y-m-d H:i');
        $stamp = $time->format($strFormat);
        return $stamp;
    }

    public static function toStringDateAmerican(DateTime $time)
    {
        $strFormat = I18N::getExpression('Y-m-d');
        $stamp = $time->format($strFormat);
        return $stamp;
    }

    public static function utf8ToIso88591(&$valores)
    {
        if (count($valores))
        {
            foreach ($valores as $chave => $valor)
            {
                //$valores[$chave] = utf8_decode($valor);
                $valores[$chave] = iconv('UTF-8', 'ISO-8859-1', $valor);
            }
        }
    }


    public static function ucFirstLatin1($pToken)
    {
        if ($pToken == null)
        {
            return null;
        }
        else
        {
            if (strlen($pToken))
            {
                return static::strtoupperlatin1($pToken[0]) . static::removerOsPrimeirosCaracteresDaString($pToken, 1);
            }
            else
            {
                return null;
            }
        }
    }

    public static function somenteLetrasENumeros($string){
        return preg_replace("/[^A-Za-z0-9 ]/", '', $string);
    }
    public static function somenteNumerosEPonto($string){
        return preg_replace("/[^0-9\.]/", '', $string);
    }
    public static function somenteNumeros($string){
        return preg_replace("/[^0-9]/", '', $string);
    }

    public static function getResultSetToMatrizObj($resultSet, $colunaIndiceDoResultSet = false)
    {
        if (mysqli_num_rows($resultSet) > 0)
        {
            mysqli_data_seek($resultSet, 0);
        }
        

        $arrayRetorno = array();
        for ($i = 0; $registro = mysqli_fetch_object($resultSet); $i++)
        {
            if ($colunaIndiceDoResultSet)
            {
                $indice = $registro[$colunaIndiceDoResultSet];
            }
            else
            {
                $indice = $i;
            }

            $arrayRetorno[$indice] = $registro;
        }

        return $arrayRetorno;
    }

    public static function encodeBase62($r){
        $base62 =  gmp_strval(gmp_init($r, 10), 62);
        return $base62;
    }
    function splitName($name) {
        $parts = explode(" ", $name);
        $lastname = array_pop($parts);
        while(count($parts) > 1)
        {
            array_pop($parts);
        }
        $firstname = implode(" ", $parts);

        $name = array(
            'first' => $firstname,
            'last' => $lastname,
        );

        return $name;
    }

    public static function curlFacebook($url, $jsonData){
        $x = $url;
        $ch = curl_init($url);
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $ret = curl_exec($ch);

        curl_close($ch);

        HelperLog::logCurl($url . " - Result: '" . print_r($ret,true) . "'");
    }
    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public static function getDiaDaSemana(){
        return date('w');
    }
}

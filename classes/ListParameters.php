<?php

    class ListParameters
    {
        public $listMappingType;
        public $filterParameters;
        public $sortingParameters;
        public $paginationParameters;

        private $maxOrderValue;

        public function __construct($listMappingType)
        {
            $this->listMappingType = $listMappingType;
            $this->filterParameters = array();
            $this->sortingParameters = array();
            $this->paginationParameters = array();

            $this->maxOrderValue = 0;
        }

        public function addFilterParameters($attribute, $value, $operator = Generic_DAO::OPERATOR_EQUALS)
        {
            $objParameter = new stdClass();
            $objParameter->attribute = $attribute;
            $objParameter->value = $value;
            $objParameter->operator = $operator;

            $this->filterParameters[] = $objParameter;
        }

        public function addMultipleFilterParametersByObject($object, $defaultOperator = Generic_DAO::OPERATOR_EQUALS)
        {
            foreach ($object as $key => $value)
            {
                $objParameter = new stdClass();
                $objParameter->attribute = $key;
                $objParameter->value = $value;
                $objParameter->operator = $defaultOperator;

                $this->filterParameters[] = $objParameter;
            }
        }

        public function addSortingParameters($columnAlias, $sortingType = Generic_DAO::SORTING_TYPE_ASC, $order = null)
        {
            if (is_null($order))
            {
                $order = ++$this->maxOrderValue;
            }
            else
            {
                if ($order > $this->maxOrderValue)
                {
                    $this->maxOrderValue = $order;
                }
            }

            $objSorting = new stdClass();
            $objSorting->columnAlias = $columnAlias;
            $objSorting->sortingType = $sortingType;
            $objSorting->order = $order;

            $this->sortingParameters = $objSorting;
        }

        public function addPaginationParameters($offset, $numberOfRecords)
        {
            $objPagination = new stdClass();
            $objPagination->offset = $offset;
            $objPagination->numberOfRecords = $numberOfRecords;

            $this->paginationParameters = $objPagination;
        }

        public static function addFilterParameterToObject(&$object, $attribute, $value, $operator = Generic_DAO::OPERATOR_EQUALS)
        {
            if (is_array($object))
            {
                $objParameter = new stdClass();
                $objParameter->attribute = $attribute;
                $objParameter->value = $value;
                $objParameter->operator = $operator;

                $object[] = $objParameter;
            }
            else
            {
                $object->$attribute = $value;
            }
        }

        public static function getFilterParameterValueFromObject(&$object, $attribute)
        {
            if (is_array($object))
            {
                for ($i = 0; $i < count($object); $i++)
                {
                    if (isset($object[ $i ]->attribute) && $object[ $i ]->attribute == $attribute)
                    {
                        return $object[ $i ]->value;
                    }
                }
            }
            else
            {
                return $object->$attribute;
            }
        }
    }

?>
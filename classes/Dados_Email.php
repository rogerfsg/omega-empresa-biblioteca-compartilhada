<?php

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of DadosExternos
     *
     * @author Eduardo Alves
     */
    define('SERVICO_HOTMAIL', "Hotmail");
    define('SERVICO_GMAIL', "Gmail");
    define('SERVICO_YAHOO', "Yahoo");
    define('SERVIDOR_OMEGA_SOFTWARE', "OmegaSoftware");

    define('TIPO_SERVICO_IMAP', "imap");
    define('TIPO_SERVICO_POP', "pop");

    class Dados_Email
    {

        //put your code here
        private $urlServico;
        private $portaServico;
        private $tipoDeServico;
        private $usuario;
        private $senha;
        private $handlerDeConexao;
        private $opcoesDeConexao;
        private $caixasDeMensagens;
        private $parametrosAdicionais;
        private $arrCaixasAPesquisar;
        private $arrIndicesCaixas;
        private $arrIndicesMensagens;
        private $arrRemetentesDeInteresse;
        private $arrParametrosConexao;

        public function __construct()
        {
            $this->arrIndicesMensagens = array();
            $this->arrRemetentesDeInteresse = array();
            $this->arrParametrosConexao = null;
        }

        public function iniciarConexao()
        {
            $servico = Helper::POST("servico");
            $usuario = Helper::POST("usuario");
            $senha = Helper::POST("senha");

            $this->arrRemetentesDeInteresse = Helper::POST("remetente");

            if (strlen($servico))
            {
                $metodo = "setDados{$servico}";

                if (method_exists("Dados_Email", $metodo))
                {
                    call_user_method_array($metodo, $this, array($usuario, $senha));
                }

                if ($this->abrirConexao())
                {
                    $this->getEmails();
                }
            }
        }

        public function iniciarConexaoParaPegarEmailsInvalidos()
        {
            $objConfiguracaoEmail = new EXTDAO_Configuracao_email();

            $usuario = $objConfiguracaoEmail->getUsuario_return_path();
            $senha = $objConfiguracaoEmail->getSenha_return_path();

            if (Helper::GET("imprimir_informacoes") == "true")
            {
                $imprimirInformacoes = true;
                Helper::imprimirCabecalhoParaFormatarAction();
            }
            else
            {
                $imprimirInformacoes = false;
            }

            if (strlen($usuario) && strlen($senha))
            {
                $this->setDadosOmegaSoftware($usuario, $senha);

                if ($this->abrirConexao())
                {
                    $numeroEmailsRetornados = $this->getEmailsRetornados($imprimirInformacoes);
                    $this->fecharConexao();
                }
            }

            if ($numeroEmailsRetornados == 0)
            {
                $mensagem = "Nenhum destinatário foi identificado com problemas.";
            }
            elseif ($numeroEmailsRetornados == 1)
            {
                $mensagem = "Um destinatário foi identificado com problemas.";
            }
            elseif ($numeroEmailsRetornados == 2)
            {
                $mensagem = "{$numeroEmailsRetornados}destinatários foram identificados com problemas.";
            }

            if (!$imprimirInformacoes)
            {
                return array("index.php5?page=pagina_inicial&tipo=pages&msgSucesso={$mensagem}");
            }
            else
            {
                Helper::imprimirMensagem("Limpando caixa de email....", MENSAGEM_INFO);

                if ($this->abrirConexao())
                {
                    $this->__apagarMensagensJaLidas();
                    $this->fecharConexao();
                }

                Helper::imprimirMensagem("Verificação Concluída, redirecionando...", MENSAGEM_OK);
                Helper::imprimirComandoJavascriptComTimer("document.location.href='index.php5?page=pagina_inicial&tipo=pages&msgSucesso={$mensagem}'", 5);
            }
        }

        private function fecharConexao()
        {
            imap_close($this->handlerDeConexao);
        }

        private function getStringDeConexao($stringSimples = false)
        {
            $strRetorno = "{$this->urlServico}:{$this->portaServico}";

            if ($stringSimples)
            {
                return "{{$strRetorno}}";
            }

            if ($this->tipoDeServico == TIPO_SERVICO_IMAP)
            {
                $strRetorno .= "/imap";
            }
            elseif ($this->tipoDeServico == TIPO_SERVICO_POP)
            {
                $strRetorno .= "/pop3";
            }

            $strRetorno .= $this->parametrosAdicionais;

            $strRetorno = "{{$strRetorno}}INBOX";

            return $strRetorno;
        }

        private function setDadosGmail($usuario, $senha)
        {
            $this->urlServico = "imap.googlemail.com";
            $this->portaServico = "993";
            $this->parametrosAdicionais = "/ssl/novalidate-cert";
            $this->opcoesDeConexao = OP_READONLY & OP_SECURE;
            $this->tipoDeServico = TIPO_SERVICO_IMAP;
            $this->usuario = $usuario;
            $this->senha = $senha;

            if (substr_count($this->usuario, "@") == 0)
            {
                $this->usuario .= "@gmail.com";
            }
        }

        private function setDadosOmegaSoftware($usuario, $senha)
        {
            $this->urlServico = "nyx.omegasoftware.com.br";
            $this->portaServico = "143";
            $this->parametrosAdicionais = "/tls/novalidate-cert/norsh";
            $this->opcoesDeConexao = NIL;
            $this->tipoDeServico = TIPO_SERVICO_IMAP;
            $this->usuario = $usuario;
            $this->senha = $senha;

            if (substr_count($this->usuario, "@") == 0)
            {
                $this->usuario .= "@nyx.omegasoftware.com.br";
            }
        }

        private function setDadosYahoo($usuario, $senha)
        {
            $this->urlServico = "imap.mail.yahoo.com";
            $this->portaServico = "993";
            $this->parametrosAdicionais = "/ssl/novalidate-cert";
            $this->opcoesDeConexao = OP_READONLY & OP_SECURE;
            $this->tipoDeServico = TIPO_SERVICO_IMAP;
            $this->usuario = $usuario;
            $this->senha = $senha;

            if (substr_count($this->usuario, "@") == 0)
            {
                $this->usuario .= "@yahoo.com.br";
            }
        }

        private function setDadosHotmail($usuario, $senha)
        {
            $this->urlServico = "pop3.live.com";
            $this->portaServico = "995";
            $this->parametrosAdicionais = "/ssl/novalidate-cert";
            $this->opcoesDeConexao = NIL;
            $this->tipoDeServico = TIPO_SERVICO_POP;
            $this->usuario = $usuario;
            $this->senha = $senha;
            $this->arrParametrosConexao = array('DISABLE_AUTHENTICATOR' => 'GSSAPI');

            if (substr_count($this->usuario, "@") == 0)
            {
                $this->usuario .= "@hotmail.com";
            }
        }

        public function abrirConexao()
        {
            $this->handlerDeConexao = imap_open("{$this->getStringDeConexao()}", $this->usuario, $this->senha, $this->opcoesDeConexao, 1);

            if ($this->handlerDeConexao !== false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function getCaixasDeMensagens()
        {
            $this->caixasDeMensagens = imap_list($this->handlerDeConexao, "{$this->getStringDeConexao(true)}", "*");

            foreach ($this->arrCaixasAPesquisar as $caixaDeEmail)
            {
                $indiceCaixa = array_search("{$this->getStringDeConexao(true)}{$caixaDeEmail}", $this->caixasDeMensagens);

                if ($indiceCaixa !== false)
                {
                    $this->arrIndicesCaixas[] = $indiceCaixa;
                }
            }
        }

        public function getEmails()
        {
            $this->arrRemetentesDeInteresse = array_filter($this->arrRemetentesDeInteresse, "strlen");

            foreach ($this->arrRemetentesDeInteresse as $remetente)
            {
                $this->arrIndicesMensagens = array_merge($this->arrIndicesMensagens, imap_search($this->handlerDeConexao, "FROM \"{$remetente}\""));
            }

            $this->arrIndicesMensagens = array_unique($this->arrIndicesMensagens);

            $arrEmails = array();
            $arrNomes = array();

            foreach ($this->arrIndicesMensagens as $indiceMensagem)
            {
                $objRetorno = imap_headerinfo($this->handlerDeConexao, $indiceMensagem);

                if (is_array($objRetorno->to))
                {
                    if (strlen($objetoCC->mailbox) && strlen($objetoCC->host))
                    {
                        foreach ($objRetorno->to as $objetoTo)
                        {
                            $arrEmails[] = $objetoTo->mailbox . "@" . $objetoTo->host;
                            $arrNomes[] = $this->__getStringDoCabecalhoDescodificada($objetoTo->personal);
                        }
                    }
                }

                if (is_array($objRetorno->cc))
                {
                    foreach ($objRetorno->cc as $objetoCC)
                    {
                        if (strlen($objetoCC->mailbox) && strlen($objetoCC->host))
                        {
                            $arrEmails[] = $objetoCC->mailbox . "@" . $objetoCC->host;
                            $arrNomes[] = $this->__getStringDoCabecalhoDescodificada($objetoCC->personal);
                        }
                    }
                }
            }

            $arrEmails = array_unique($arrEmails);

            Helper::imprimirCabecalhoParaFormatarAction();

            for ($i = 0; $i < count($arrEmails); $i++)
            {
                $email = $arrEmails[ $i ];
                $nome = $arrNomes[ $i ];

                Helper::imprimirMensagem("Contato encontrado: {$nome} &lt;{$email}&gt;", MENSAGEM_OK);
            }
        }

        private function getEmailsRetornados($imprimirInformacoes = false)
        {
            $this->arrIndicesMensagens = imap_search($this->handlerDeConexao, "UNSEEN");

            $arrEmailsRetornados = array();

            if (is_array($this->arrIndicesMensagens))
            {
                foreach ($this->arrIndicesMensagens as $indiceMensagem)
                {
                    $cabecalho = imap_fetchheader($this->handlerDeConexao, $indiceMensagem);

                    $email = $this->__getFailedRecipients($cabecalho);

                    if ($email !== false)
                    {
                        $arrEmailsRetornados[] = trim(ereg_replace("/\n\r|\r\n|\n/", "", $email));
                    }
                }
            }

            $objBanco2 = new Database();

            $objBanco2->iniciarTransacao();

            foreach ($arrEmailsRetornados as $email)
            {
                $objBanco2->query("SELECT id FROM pessoa WHERE email='{$email}'");

                if ($objBanco2->rows > 0)
                {
                    $idPessoa = $objBanco2->getPrimeiraTuplaDoResultSet(0);
                    $objBanco2->query("UPDATE pessoa SET email_com_problemas_BOOLEAN=1, data_deteccao_problema_DATETIME=NOW() WHERE id={$idPessoa}");

                    if ($imprimirInformacoes)
                    {
                        Helper::imprimirMensagem(" Novo email cadastrado como inválido: {$email}", MENSAGEM_INFO);
                    }
                }
            }

            $objBanco2->commitTransacao();

            if (is_array($this->arrIndicesMensagens))
            {
                foreach ($this->arrIndicesMensagens as $indiceMensagem)
                {
                    $status = imap_setflag_full($this->handlerDeConexao, imap_uid($this->handlerDeConexao, $indiceMensagem), "\\SEEN", ST_UID);
                }
            }

            return count($arrEmailsRetornados);
        }

        public function __apagarMensagensJaLidas()
        {
            $arrMensagens = imap_search($this->handlerDeConexao, "SEEN");

            $arrDelecoes = array();

            foreach ($arrMensagens as $idMensagem)
            {
                $arrDelecoes = imap_delete($this->handlerDeConexao, $idMensagem);
            }

            $contador = array_count_values($arrDelecoes);

            if ($contador[ false ] > 0)
            {
                Helper::imprimirMensagem("Falha ao apagar mensagem(ns).", MENSAGEM_ERRO);
            }

            imap_expunge($this->handlerDeConexao);
        }

        public function __getFailedRecipients($cabecalho)
        {
            $parteChave = "\nX-Failed-Recipients: ";
            $strPos1 = strpos($cabecalho, $parteChave);

            if ($strPos1 === false)
            {
                return false;
            }
            else
            {
                $strPos1 += strlen($parteChave);

                $strPos2 = strpos($cabecalho, "\n", $strPos1);

                $offset = $strPos2 - $strPos1;
                $email = substr($cabecalho, $strPos1, $offset);

                return $email;
            }
        }

        public function __getStringDoCabecalhoDescodificada($string)
        {
            $objs = imap_mime_header_decode($string);

            return $objs[0]->text;
        }

        public function factory()
        {
            return new Dados_Email();
        }

    }

?>

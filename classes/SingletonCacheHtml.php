<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of CacheWebservice
     *
     * @author home
     */
    include_once __DIR__ . '/HelperLog.php';

    class SingletonCacheHtml
    {
        //put your code here

        private $urls = array();
        static $singleton = null;
        private $redisDbName = null;
        private $startedCaching = false;
        private $tipos = array();
        private $paginas = array();
        private $urlsIsoladas = array();

        public static function getSingleton()
        {
            if (SingletonCacheHtml::$singleton == null)
            {
                SingletonCacheHtml::$singleton = new SingletonCacheHtml();
            }

            return SingletonCacheHtml::$singleton;
        }

        public function __construct()
        {
        }

        public function getUrl()
        {
            $index = strrpos($_SERVER["SCRIPT_NAME"], "/");
            if ($index >= 0)
            {
                $index += 1;
                $pagina = substr($_SERVER["SCRIPT_NAME"], $index);

                return $pagina;
            }

            return null;
        }

        public function setUrls($redisDbName, $urls, $tipos, $paginas, $urlsIsoladas)
        {
            HelperLog::logRedis("SingletonCacheHtml - setWebservices - $redisDbName. " . print_r($urls, true));
            $this->webservices = $urls;
            $this->redisDbName = $redisDbName;
            $this->urls = $urls;
            $this->paginas = $paginas;
            $this->tipos = $tipos;
            $this->urlsIsoladas = $urlsIsoladas;
        }

        public function hasToCache()
        {
            $url = $this->getUrl();
            if (in_array($url, $this->urlsIsoladas))
            {
                return true;
            }
            if ($this->tipos == null)
            {
                return false;
            }
            $tipo = Helper::POSTGET("tipo");

            $index = array_search($this->tipos, $tipo);

            if ($index >= 0)
            {
                $pagina = Helper::POSTGET("pagina");
                HelperLog::logRedis("SingletonCacheHtml - hasToCache - $tipo::$pagina");

                return $this->paginas[ $index ] == $pagina && $this->urls[ $index ] == $url;
            }

            return false;
        }

        public function getCurrentKey()
        {
            $url = $this->getUrl();
            $tipo = Helper::POSTGET("tipo");
            $pagina = Helper::POSTGET("pagina");
            if (strlen($tipo))
            {
                return "$url::$tipo::$pagina";
            }
            else
            {
                return $url;
            }
        }

        public function exitIfCached()
        {
            //O vetor dos webservices devem ser repassados

            if ($this->hasToCache())
            {
                $key = $this->getCurrentKey();

                $redis = HelperRedis::getSingleton();

                $html = $redis->get($key, false);
                HelperLog::logRedis("SingletonCacheHtml - exitIfCached - [$key]");
                if ($html != null)
                {
                    echo $html;
                    exit();
                }
            }
        }

        public function startCachingIfNecessary()
        {
            if ($this->hasToCache()
                && !$this->startedCaching)
            {
                ob_start();

                $out2 = ob_get_contents();

                ob_end_clean();
                echo $out2;
                $this->startedCaching = true;
            }
        }

        public function stopCaching()
        {
            if ($this->startedCaching)
            {
                $webservice = $this->getCurrentKey();

                $out2 = ob_get_contents();

                ob_end_clean();
                echo $out2;
                $redis = HelperRedis::getSingleton();
                $redis->set($webservice, $out2);
                $this->startedCaching = false;
            }
        }
    }

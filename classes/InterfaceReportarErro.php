<?php

abstract class InterfaceReportarErro extends InterfaceScript
{
    const ID_DIV_ERRO = "divErro1234";

    public static function reportarMensagem($msg, $mensagemUsuario = null)
    {
        $h = Registry::get('HelperLog');
        $json = $h->gravarLogEmCasoErro($msg);
        if ($msg != null && $msg->erro())
        {
            Helper::getComandoJavascriptComParametro("ReportarErroExcecao", array($msgExc, $mensagemUsuario));
        }
        else
        {
            Helper::getComandoJavascriptComParametro("ReportarErroTempo", array($msgExc, $mensagemUsuario));
        }

        return $json;
    }

    public static function reportarExcecao($exc, $mensagemUsuario = null)
    {
        $msg = null;
        if ($exc != null)
        {
            $msg = new Mensagem(null, null, $exc);
            $h = Registry::get('HelperLog');
            $json = $h->gravarLogEmCasoErro($msg);
        }

        echo Helper::getComandoJavascriptComParametro("ReportarErroExcecao", array($msg, $mensagemUsuario));

        return $json;
    }

}

if (Helper::getPhpFileName() != "webservice.php")
{
?>

<script type="text/javascript">
    function Alerta(mensagem)
    {
        bootbox.alert({
            message: mensagem
        });
    }

    function LogException(exc)
    {
        console.log(exc.message);
        throw exc;
    }

    function ReportarErroExcecao(msg, msgUsuario)
    {
        try
        {
            if (msg != null && msg instanceof Object && msg.message != null)
            { // true)
                msg = msg.message;
            }
            if (msg != null)
            {
                console.log(msg);
            }
            var $divErro = $('#<?=  InterfaceReportarErro::ID_DIV_ERRO?>');
            $divErro.find('.inErro').val(msg);
            $divErro.find('.erro-tempo').css('display', 'none');
            $divErro.find('.erro-excecao').css('display', 'block');
            if (msgUsuario == null)
            {
                msgUsuario = "Ocorreu um erro durante o processamento, estamos trabalhando para corrigir o problema.";
            }
            $divErro.find('.mensagem-usuario').html(msgUsuario);
            AbrirDialogErro();
        }
        catch (ex)
        {
            console.log(ex.message);
        }
    };

    function ReportarErroTempo(msg, msgUsuario)
    {
        try
        {
            var $divErro = $('#<?=  InterfaceReportarErro::ID_DIV_ERRO?>');
            $divErro.find('.inErro').val(msg);
            $divErro.find('.erro-tempo').css('display', 'block');
            $divErro.find('.erro-excecao').css('display', 'none');
            if (msgUsuario == null)
            {
                msgUsuario = "O servidor est� muito ocupado, tente novamente mais tarde";
            }
            $divErro.find('.mensagem-usuario').html(msgUsuario);
            AbrirDialogErro();
        }
        catch (ex)
        {
            console.log(ex.message);
        }
    };

    function AbrirDialogErro()
    {
        try
        {
            var $divErro = $('#<?=  InterfaceReportarErro::ID_DIV_ERRO?>');
            $divErro.find('.botao-acao').click();
        }
        catch (ex)
        {
            console.log(ex.message);
        }
    }


</script>
    <?php
    }
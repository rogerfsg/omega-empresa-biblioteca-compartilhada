<?php

    /**
     * LRU is a system which cache data used last recently.
     * see more: http://www.slideshare.net/t_wada/tddbc-exercise
     */
    class CacheLRU
    {

        protected $_cache = array();
        protected $_cacheSize;

        public function __construct($size)
        {
            $this->_cacheSize = $size;
        }

        public function set($key, $value)
        {
            $this->_cache[ $key ] = $value;
            if (count($this->_cache) > $this->_cacheSize)
            {
                array_shift($this->_cache);
            }
        }

        public function get($key)
        {
            if (!isset($this->_cache[ $key ]))
            {
                return null;
            }
            // Put the value gotten to last.
            $tmpValue = $this->_cache[ $key ];
            unset($this->_cache[ $key ]);
            $this->_cache[ $key ] = $tmpValue;

            return $this->_cache[ $key ];
        }

    }

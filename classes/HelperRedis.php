<?php

    require __DIR__ . '/../libs/predis/autoload.php';
    class HelperRedis
    {
        const PREFIXO_IDENTIFICADOR_SESSAO = "-1";
        public static function getSingleton($prefixo = HelperRedis::PREFIXO_IDENTIFICADOR_SESSAO)
        {
            HelperLog::logRedis("getSingleton::$prefixo");
            if ($prefixo == HelperRedis::PREFIXO_IDENTIFICADOR_SESSAO)
            {
                if (defined('IDENTIFICADOR_SESSAO'))
                {
                    $prefixo = IDENTIFICADOR_SESSAO;
                }
                else
                {
                    $configuracaoSite = Registry::get('ConfiguracaoSite', null);
                    if ($configuracaoSite != null)
                    {
                        $prefixo = $configuracaoSite->IDENTIFICADOR_SESSAO;
                    }
                }
            }
            $singleton = Registry::get("HelperRedis{$prefixo}", false);
            if ($singleton == null)
            {
                $singleton = new HelperRedis($prefixo);
                Registry::add($singleton, "HelperRedis{$prefixo}");

                return $singleton;
            }
            else
            {
                return $singleton;
            }
        }

        const PREFIXO_REDIS_COMUM = "_";
        private static $redis = array();
        private $prefixo = null;
        private $database = 0;

        public function __construct($prefixo = HelperRedis::PREFIXO_IDENTIFICADOR_SESSAO)
        {
            // since we connect to default setting localhost
            // and 6379 port there is no need for extra
            // configuration. If not then you can specify the
            // scheme, host and port to connect as an array
            // to the constructor.

            $this->prefixo = $prefixo;
            //cluster s� aceita um database
            $this->database = 0;
        }

        private function getDatabase()
        {
            if (!isset(HelperRedis::$redis[ $this->database ]))
            {
                HelperRedis::$redis[ $this->database ] = new Predis\Client(array(
                                                                               "scheme" => REDIS_SCHEME,
                                                                               "host" => REDIS_HOST,
                                                                               "port" => REDIS_PORT,
                                                                               "database" => $this->database
                                                                           ));
            }

            return HelperRedis::$redis[ $this->database ];
        }

        public static function closeAll()
        {
            foreach (HelperRedis::$redis as $key => $db)
            {
                if ($db != null)
                {
                    $db->disconnect();
                }
                unset(HelperRedis::$redis[ $key ]);
            }
            HelperRedis::$redis = array();
        }

        public function set($key, $value, $expireResolution = null, $expireTTL = null, $flag = null)
        {
            if($expireTTL != null && $expireTTL == 0)
                $expireResolution = null;
            HelperLog::logRedis("HelperRedis set - $key - $value. $expireResolution, $expireTTL, $flag");

            if ($expireResolution == null && $expireTTL == null && $flag == null)
            {
                $this->getDatabase()->set($this->prefixo . $key, $value);
            }
            else
            {
                if ($expireTTL == null && $flag == null)
                {
                    $this->getDatabase()->set($this->prefixo . $key, $value, $expireResolution);
                }
                else
                {
                    if ($flag == null)
                    {
                        $this->getDatabase()->set($this->prefixo . $key, $value, $expireResolution, $expireTTL);
                    }
                    else
                    {
                        $this->getDatabase()->set($this->prefixo . $key, $value, $expireResolution, $expireTTL, $flag);
                    }
                }
            }
        }

        public function setNx($key, $value)
        {
            HelperLog::logRedis("HelperRedis setNx - $key - $value");

            return $this->getDatabase()->setNx($this->prefixo . $key, $value);
        }

        public function get($key, $logConteudo = true)
        {
            $ret = $this->getDatabase()->get($this->prefixo . $key);
            if ($logConteudo)
            {
                HelperLog::logRedis("HelperRedis get - $key - $ret");
            }
            else
            {
                HelperLog::logRedis("HelperRedis - $key");
            }

            return $ret;
        }

        public function exists($key)
        {
            $has = $this->getDatabase()->exists($this->prefixo . $key);
            HelperLog::logRedis("HelperRedis exists - $key - $has");

            return $has;
        }

        public function del($key)
        {
            $has = $this->getDatabase()->del($this->prefixo . $key);
            HelperLog::logRedis("HelperRedis del - $key - $has");

            return $has;
        }

        public function hset($hash, $key, $value)
        {
            $h = $this->getDatabase()->hset($this->prefixo . $hash, $key, $value);
            HelperLog::logRedis("HelperRedis hset - " . $this->prefixo . ".$hash, {$key}, {$value}");

            return $h;
        }

        public function hdel($hash, $key)
        {
            $h = $this->getDatabase()->hdel($this->prefixo . $hash, $key);
            HelperLog::logRedis("HelperRedis hdel - " . $this->prefixo . ".$hash, {$key}");

            return $h;
        }

        public function hget($hash, $key)
        {
            $h = $this->getDatabase()->hget($this->prefixo . $hash, $key);
            HelperLog::logRedis("HelperRedis hget - " . $this->prefixo . "$hash, {$key} => $h");

            return $h;
        }

        public function hmset($hash, $matrixHash)
        {
            $this->getDatabase()->hmset($this->prefixo . $hash, $matrixHash);
            HelperLog::logRedis("hmset (" . $this->prefixo . "$hash, " . print_r($matrixHash, true));
        }

        public function expireAt($key, $tempo)
        {
            $this->getDatabase()->expireAt(
                $this->prefixo . $key,
                $tempo);
            HelperLog::logRedis("expireAt(" . $this->prefixo . "$key, $tempo");
        }

        public function rpush($key, $value)
        {
            //$value = 'S';
            $length = $this->getDatabase()->rpush(
                $this->prefixo . $key,
                $value);
            HelperLog::logRedis("rpush(" . $this->prefixo . "$key, $value) - $length");

            return $length;
        }

        public function rpushx($key, $value)
        {
            //$value = 'S';
            $length = $this->getDatabase()->rpushx(
                $this->prefixo . $key,
                $value);
            HelperLog::logRedis("rpushx(" . $this->prefixo . "$key, $value) - $length");

            return $length;
        }

        public function blpop($key, $timeout = 10000)
        {
            $value = $this->getDatabase()->blpop(
                $this->prefixo . $key, $timeout);
            HelperLog::logRedis("blpop(" . $this->prefixo . "$key, $timeout");

            return $value;
        }


        public function lpop($key)
        {
            $value = $this->getDatabase()->lpop(
                $this->prefixo . $key);
            HelperLog::logRedis("lpop(" . $this->prefixo . "$key");

            return $value;
        }

        public function lrem($key, $count, $value)
        {
            $value = $this->getDatabase()->lrem($this->prefixo . $key, $count, $value);

            HelperLog::logRedis("lrem(" . $this->prefixo . "$key, $count, $value");

            return $value;
        }

        public function incr($key)
        {
            $value = $this->getDatabase()->incr($key);

            HelperLog::logRedis("incr($key) = $value");

            return $value;
        }

        public function decr($key)
        {
            $value = $this->getDatabase()->decr($key);

            HelperLog::logRedis("decr($key) = $value");

            return $value;
        }

        public function multi()
        {
            $value = $this->getDatabase()->multi();
            HelperLog::logRedis("multi() - $value ");

            return $value;
        }

        public function discard()
        {
            $value = $this->getDatabase()->discard();
            HelperLog::logRedis("discard() - $value ");

            return $value;
        }

        public function rpop($key)
        {
            $value = $this->getDatabase()->rpop($this->prefixo . $key);
            HelperLog::logRedis("rpop($this->prefixo.$key) - $value ");

            return $value;
        }

    }
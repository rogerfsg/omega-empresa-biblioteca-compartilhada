
omegaApp.filter('propsFilter', function()
{
    return function(items, props)
    {
        var out = [];
        if (angular.isArray(items))
        {
            var keys = Object.keys(props);

            items.forEach(function(item)
            {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++)
                {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (typeof item[prop] != 'undefined' && item[prop] != null
                            && item[prop].toString().toLowerCase().indexOf(text) !== -1)
                    {
                        itemMatches = true;
                        break;
                    }

                }

                if (itemMatches)
                {
                    out.push(item);
                }

            });

        }
        else
        {
            out = items;
        }

        return out;
    };

});
function Message(title, message, callback)
{
    this.title = title || '';
    this.message = message || '';
    this.callback = callback || null;
}

Message.prototype.getTitle = function()
{
    return this.title;
}

Message.prototype.getMessage = function()
{
    return this.message;
}

Message.prototype.getCallback = function()
{
    return this.callback;
}
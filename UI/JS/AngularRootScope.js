
omegaApp.run(
    ["$http", "$rootScope", "$uibModal", "$parse", "dialogs", "$timeout", function($http, $rootScope, $uibModal, $parse, dialogs, $timeout) {

        $rootScope.isNullOrEmpty = function(object)
        {
            return GeneralUtil.isNullOrEmpty(object);
        };

        $rootScope.isNullOrEmptyArray = function(object)
        {
            return ArrayUtil.isNullOrEmpty(object);
        };

        $rootScope.getPaginationArray = function(gridData)
        {

        };

        $rootScope.instantErrorMessages = [];
        $rootScope.getFormControlCssClass = function(angularElement)
        {
            if(GeneralUtil.isNullOrEmpty(angularElement))
                return;

            var cssClasses = [];
            if(angularElement.$dirty && angularElement.$invalid)
            {
                cssClasses.push("has-error");
            }

            return cssClasses.join(" ");

        };

        var validationMessages = {};
        var validationBind = [];
        $rootScope.showValidationMessage = function(angularElement, angularForm, validations)
        {
            if(typeof angularElement == "string")
            {
                angularElement = angularForm[angularElement];
            }

            if(GeneralUtil.isNullOrEmpty(angularElement) || !angularElement.$dirty)
            {
                return false;
            }

            if(validationBind.indexOf(angularElement.$name) == -1)
            {
                validationBind.push(angularElement.$name);
                $rootScope.$watch(function () { return angularElement.$error.length }, function (){  }, true);
            }

            var arrMessages = [];
            for(var validation in validations)
            {
                if(angularElement.$error[validation])
                {
                    arrMessages.push(validations[validation]);
                }
            }

            validationMessages[angularElement.$name] = arrMessages;
            return (arrMessages.length > 0);

        };

        $rootScope.showNotifyMessage = function(successMessage, title, options)
        {
            title = title || null;
            options = options || ApplicationUtil.getDefaultSuccessMessageDialogOption();

            return dialogs.notify(title, successMessage, options);
        };

        $rootScope.showErrorMessage = function(errorMessage, title, options)
        {
            title = title || null;
            options = options || ApplicationUtil.getDefaultMessageDialogOption();

            return dialogs.error(title, errorMessage, options);
        };

        $rootScope.getValidationMessages = function(angularElement, angularForm)
        {
            if(typeof angularElement == "string")
            {
                angularElement = angularForm[angularElement];
            }

            if(typeof angularElement !== 'undefined')
            {
                return validationMessages[angularElement.$name];
            }
        };

        $rootScope.validateMinNumberOfLines = function(minLines, text)
        {
            return (text != null && typeof text != 'undefined' && text.split("\r").length-1 >= minLines);
        };

        $rootScope.setDateRangeMinDate = function (startDate, endDateParameter)
        {
            endDateParameter.datePickerParams.minDate = startDate;
        };

        $rootScope.setDateRangeMaxDate = function (endDate, startDateParameter)
        {
            startDateParameter.datePickerParams.maxDate = endDate;
        };

        $rootScope.validateMinChoices = function(minChoices, modelValue)
        {
            return (!GeneralUtil.isNullOrEmpty(modelValue) && modelValue.length >= minChoices);
        };

        $rootScope.validateMaxChoices = function(maxChoices, modelValue)
        {
            return (!GeneralUtil.isNullOrEmpty(modelValue) && modelValue.length >= maxChoices);
        };

        $rootScope.getNextModalIndex = function()
        {
            if(!($rootScope.uibModalInstance instanceof Array))
            {
                return 0;
            }
            else
            {
                return $rootScope.uibModalInstance.length;
            }
        };

        $rootScope.registerModalInstance = function(modalInstance)
        {
            if(!($rootScope.uibModalInstance instanceof Array))
            {
                $rootScope.uibModalInstance = [modalInstance];
            }
            else
            {
                $rootScope.uibModalInstance.push(modalInstance);
            }

            return $rootScope.uibModalInstance.length-1;
        };

        $rootScope.closeLastModalInstance = function()
        {
            if(($rootScope.uibModalInstance instanceof Array))
            {
                for(var i=$rootScope.uibModalInstance.length-1; i >= 0; i--)
                {
                    if($rootScope.uibModalInstance[i] != null)
                    {
                        $rootScope.closeCurrentModalInstance(i);
                        break;
                    }
                }
            }
        };

        $rootScope.closeCurrentModalInstance = function(modalIndex)
        {
            $rootScope.uibModalInstance[modalIndex].close();
            $rootScope.uibModalInstance[modalIndex] = null;
        };

        $rootScope.showDialog = function(containerVM, templateId, options)
        {
            options = options || 'lg';
            return dialogs.create(templateId, 'genericDialogController', { vm: containerVM }, options);
        };

        $rootScope.range = function(n)
        {
            return new Array(n);
        };

        $rootScope.defaultAfterInsertAction = function(vm, modalTemplate, successMessage)
        {
            if(!GeneralUtil.isNullOrEmpty(vm.isModalView) && vm.isModalView)
            {
                if(!GeneralUtil.isNullOrEmpty(successMessage))
                {
                    ApplicationUtil.showSuccessMessage(successMessage, null, vm);
                }

                $timeout(function()
                {
                    $rootScope.closeLastModalInstance();
                }, 2000);
            }
            else
            {
                $rootScope.showDialog(vm, modalTemplate);
            }
        };

        $rootScope.defaultAfterUpdateAction = function(vm, modalTemplate, successMessage)
        {
            if(!GeneralUtil.isNullOrEmpty(vm.isModalView) && vm.isModalView)
            {
                if(!GeneralUtil.isNullOrEmpty(successMessage))
                {
                    ApplicationUtil.showSuccessMessage(successMessage, null, vm);
                }

                $timeout(function()
                {
                    $rootScope.closeLastModalInstance();
                }, 2000);

            }
            else
            {
                $rootScope.showDialog(vm, modalTemplate);
            }
        };

        $rootScope.$watch(function (rootScope) { return rootScope.instantErrorMessages.length }, function (newValue, oldValue)
        {
            if (newValue > 0)
            {
                var messages = $rootScope.instantErrorMessages;
                var completeMessage = '';
                var title = '';
                var callback = null;

                for(var i=0; i < messages.length; i++)
                {
                    if(typeof messages[i] == 'string')
                    {
                        completeMessage += messages[i] + "<br />";
                    }
                    else if(messages[i] instanceof Message)
                    {
                        completeMessage += messages[i].getMessage() + "<br />";

                        if(title == '')
                            title = messages[i].getTitle();

                        if(callback == null)
                            callback = messages[i].getCallback();

                    }

                }

                $rootScope.instantErrorMessages = [];
                callback = callback || function(){};

                var dlg = dialogs.error(title, completeMessage);
                dlg.result.then(callback);

            }

        }, true);

        $rootScope.selectAllRecords = false;
        $rootScope.checkAllRecords = function(controllerAs)
        {
            if(controllerAs.selectAllRecords)
            {
                controllerAs.selectedRecords = [];
                angular.forEach(controllerAs.dataSet, function(record)
                {
                    controllerAs.selectedRecords.push(record.primaryKey);
                });
            }
            else
            {
                controllerAs.selectedRecords = [];
            }
        };

        $rootScope.showModalDialog = function(templateType, templateName, modalIndex, controllerParams, modalParams)
        {
            var modalObject = {};
            modalObject.templateUrl = "angularjs_modal.php?tipo=" + templateType + "&page=" + templateName + "&modalIndex=" + modalIndex;

            if(GeneralUtil.isNullOrEmpty(modalParams)) modalParams = {};

            modalObject.backdrop = modalParams.backdrop || 'static';
            modalObject.windowClass = modalParams.windowClass || 'modal-window';
            modalObject.keyboard = GeneralUtil.isNullOrEmpty(modalParams.keyboard) ? modalParams.keyboard : true;

            if(typeof modalParams.size !== 'undefined')
            {
                modalObject.size = modalParams.size;
            }

            if(!GeneralUtil.isNullOrEmpty(controllerParams))
            {
                for(var item in controllerParams)
                {
                    window.dataServiceReturnObject[item] = controllerParams[item];
                }
            }

            return $uibModal.open(modalObject);

        };

        $rootScope.getExpandOrShinkPanelsButtonClass = function(formInstance)
        {
            return $('form[name="' + formInstance.$name + '"] .omega-form-section-box.box-collapsed').length > 0 ? 'icon-resize-full' : 'icon-resize-small';
        };

        $rootScope.expandOrShrinkPanels = function(formInstance)
        {
            var $boxes = $('form[name="' + formInstance.$name + '"] .omega-form-section-box');
            if($boxes.hasClass('box-collapsed'))
            {
                $boxes.removeClass('box-collapsed');
            }
            else
            {
                $('form[name="' + formInstance.$name + '"] .omega-form-section-box:not(.box-collapsed)').addClass('box-collapsed');
            }
        };

        $rootScope.validateForm = function(formInstance, vm, showMessage)
        {
            showMessage = GeneralUtil.isNullOrEmpty(showMessage) ? true : showMessage;
            var formName = formInstance.$name;
            var errors = formInstance.$error;

            var arrDistinctFields = [];
            for(var error in errors)
            {
                var errorArray = errors[error];
                for(var i=0; i < errorArray.length; i++)
                {
                    var fieldName = errorArray[i].$name;
                    if(arrDistinctFields.indexOf(fieldName) == -1)
                    {
                        arrDistinctFields.push(fieldName);
                    }

                    errorArray[i].$dirty = true;
                }

            }

            if (arrDistinctFields.length > 0)
            {
                if(showMessage)
                {
                    var title = I18NUtil.getExpression("Aten��o", "Warning");
                    var message = I18NUtil.getExpression("Existem {0} campos no formul�rio que precisam da sua aten��o.", "There are {0} fields on the form that need your attention.");
                    if (arrDistinctFields.length == 1)
                    {
                        message = I18NUtil.getExpression("Existe {0} campo no formul�rio que precisa da sua aten��o.", "There is {0} field on the form that need your attention.");
                    }

                    if(!ApplicationUtil.isModalView(vm))
                    {
                        vm.messages.error = [];
                        var messageObject = new Message(title, String.format(message, arrDistinctFields.length));
                        $rootScope.instantErrorMessages.push(messageObject);
                    }
                    else
                    {
                        vm.messages.error.push(String.format(message, arrDistinctFields.length));
                    }

                }

                if(!ApplicationUtil.isModalView(vm))
                {
                    if(!GeneralUtil.isNullOrEmpty(formInstance) && !GeneralUtil.isNullOrEmpty(formInstance.$name))
                    {
                        $('form[name="' + formInstance.$name + '"]').find('.box').removeClass('box-collapsed');
                    }

                    var selector = 'form[name="' + formName + '"] [name="' + arrDistinctFields[0] + '"]';

                    $('html, body').animate({
                        scrollTop: $(selector).offset().top - 10
                    }, 1000);

                    $(selector).focus();
                }
                else
                {
                    $('form[name="' + formInstance.$name + '"]').closest('.modal-body');
                }


            }

            return (arrDistinctFields.length == 0);

        };

        $rootScope.switchListSearch = function(controllerAs)
        {
            controllerAs.activeListSearch = !controllerAs.activeListSearch;
            controllerAs.activeListHighlight = false;
            controllerAs.listSearchIconClass = controllerAs.activeListSearch ? 'yellow-icon' : '';
            controllerAs.listHighlightIconClass = '';
        };

        $rootScope.switchListHighlight = function(controllerAs)
        {
            controllerAs.activeListHighlight = !controllerAs.activeListHighlight;
            controllerAs.activeListSearch = false;
            controllerAs.listHighlightIconClass = controllerAs.activeListHighlight ? 'yellow-icon' : '';
            controllerAs.listSearchIconClass = '';
        };

        $rootScope.filterListWatch = function(controllerAs)
        {
            $rootScope.$watch(function() { return controllerAs.listFilter; },

                function (newVal, oldVal)
                {
                    if(newVal != oldVal)
                    {
                        if(GeneralUtil.isNullOrEmpty(newVal))
                        {
                            controllerAs.dataSet = angular.copy(controllerAs.originalDataSet);
                        }
                        else
                        {
                            controllerAs.dataSet = [];
                            for(var i=0; i < controllerAs.originalDataSet.length; i++)
                            {
                                for(var field in controllerAs.originalDataSet[i])
                                {
                                    var fieldValue = controllerAs.originalDataSet[i][field];
                                    if(('' + fieldValue + '').toLowerCase().indexOf(newVal.toLowerCase()) > -1)
                                    {
                                        controllerAs.dataSet.push(controllerAs.originalDataSet[i]);
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

            );

        };

        $rootScope.loadParametersFromUrl = function(controllerAs)
        {
            controllerAs.getRemoteListDataFromQueryString();
        };

        $rootScope.filterDataSet = function(controllerAs, formInstance)
        {
            if($rootScope.validateForm(formInstance))
            {
                controllerAs.preservedFilterData = angular.copy(controllerAs.filterData);
                controllerAs.getRemoteListData();
            }

        };

        $rootScope.sortColumn = function(controllerAs, column, columnAlias)
        {
            var sortingParametersIndex = -1;
            var maxOrderValue = 0;
            for(var i=0; i < controllerAs.sortingParameters.length; i++)
            {
                if(controllerAs.sortingParameters[i].columnName == column)
                {
                    sortingParametersIndex = i;
                    break;
                }

                var orderValue = controllerAs.sortingParameters[i].order;
                if(maxOrderValue < orderValue)
                {
                    maxOrderValue = orderValue;
                }

            }

            for(var columnVar in controllerAs.sortingClass)
            {
                if(columnVar == column)
                {
                    var currentClass = controllerAs.sortingClass[columnVar];
                    if(currentClass == Constants.ListSortingClass.IDLE)
                    {
                        controllerAs.sortingClass[columnVar] = Constants.ListSortingClass.ASC;

                        if(sortingParametersIndex != -1)
                        {
                            controllerAs.sortingParameters[sortingParametersIndex].sortingType = 'ASC';
                        }
                        else
                        {
                            controllerAs.sortingParameters.push({ columnName: column, columnAlias: columnAlias, sortingType: 'ASC', order: ++maxOrderValue });
                        }

                    }
                    else if(currentClass == Constants.ListSortingClass.DESC)
                    {
                        controllerAs.sortingClass[columnVar] = Constants.ListSortingClass.IDLE;

                        if(sortingParametersIndex != -1)
                        {
                            controllerAs.sortingParameters.splice(sortingParametersIndex, 1);
                        }

                    }
                    else if(currentClass == Constants.ListSortingClass.ASC)
                    {
                        controllerAs.sortingClass[columnVar] = Constants.ListSortingClass.DESC;

                        if(sortingParametersIndex != -1)
                        {
                            controllerAs.sortingParameters[sortingParametersIndex].sortingType = 'DESC';
                        }
                        else
                        {
                            controllerAs.sortingParameters.push({ columnName: column, columnAlias: columnAlias, sortingType: 'DESC', order: ++maxOrderValue });
                        }

                    }

                    controllerAs.preservedFilterData = angular.copy(controllerAs.filterData);
                    controllerAs.getRemoteListData();
                }

            }

        };

        $rootScope.previousPageCssClass = function(controllerAs)
        {
            return (GeneralUtil.isNullOrEmpty(controllerAs.paginationParameters) || controllerAs.paginationParameters.currentPage == 1) ? 'disabled' : '';
        };

        $rootScope.nextPageCssClass = function(controllerAs)
        {
            return (GeneralUtil.isNullOrEmpty(controllerAs.paginationParameters) || controllerAs.paginationParameters.currentPage == controllerAs.paginationParameters.lastPage) ? 'disabled' : '';
        };

        $rootScope.changeToPage = function(controllerAs, page)
        {
            if(page == 'next' && $rootScope.nextPageCssClass() == 'disabled')
            {
                return;
            }
            else if(page == 'prev' && $rootScope.previousPageCssClass() == 'disabled')
            {
                return;
            }
            else if(NumberUtil.isInteger(page) && controllerAs.paginationParameters.currentPage == page)
            {
                return;
            }

            if(page == 'prev')
            {
                controllerAs.paginationParameters.currentPage--;
            }
            else if(page == 'next')
            {
                controllerAs.paginationParameters.currentPage++;
            }
            else
            {
                controllerAs.paginationParameters.currentPage = page;
            }

            //faz chamada remota ap�s atualizacao dos parametros
            controllerAs.getRemoteListData();
        };

        $rootScope.defaultDateFormat = I18NUtil.getUserDateFormat();
        $rootScope.$on('loading:progress', function () {

            HtmlUtil.mostrarLoading();

        });

        $rootScope.$on('loading:finish', function () {

            HtmlUtil.esconderLoading();

        });

        ApplicationUtil.setRootScope($rootScope);

    }]);

    omegaApp.controller('genericDialogController',function($scope, $rootScope, $uibModalInstance, data)
    {
        $scope.vm = data.vm;

        $rootScope.closeMessageDialog = function()
        {
            $uibModalInstance.dismiss("Canceled");
        };

    });

    omegaApp.directive('a', function()
    {
        return {
            restrict: 'E',
            link: function(scope, elem, attrs)
            {
                if(attrs.ngClick || attrs.href === '' || attrs.href === '#')
                {
                    elem.on('click', function(e)
                    {
                        e.preventDefault();
                    });
                }
            }
        };
    });


    angular.module('omegaApp').factory('httpInterceptor', ['$q', '$rootScope',
        function ($q, $rootScope) {

            var loadingCount = 0;
            return {
                request: function (config)
                {
                    if(config.url.indexOf('webservice.php') > -1)
                    {
                        config.headers['Accept'] = 'application/json; charset=iso-8859-1';
                    }

                    //define header espec�fico
                    config.headers['X-Application-Type'] = 'AngularJS';

                    if (config && ((config.params && config.params.hideLoading) || (config.data && config.data.hideLoading)))
                    {
                        return config || $q.when(config);
                    }

                    if (++loadingCount >= 1)
                    {
                        $rootScope.$broadcast('loading:progress');
                    }

                    return config || $q.when(config);
                },

                response: function (response)
                {
                    if (response && response.config && ((response.config.params && response.config.params.hideLoading) || (response.config.data && response.config.data.hideLoading)))
                    {
                        return response || $q.when(response);
                    }

                    if (--loadingCount === 0)
                    {
                        $rootScope.$broadcast('loading:finish');
                    }

                    return response || $q.when(response);
                },

                responseError: function (response) {

                    if (response && response.config && ((response.config.params && response.config.params.hideLoading) || (response.config.data && response.config.data.hideLoading)))
                    {
                        return response || $q.when(response);
                    }

                    if (--loadingCount === 0)
                    {
                        $rootScope.$broadcast('loading:finish');
                    }

                    return $q.reject(response);
                }

            };

        }

    ]).config(['$httpProvider', function ($httpProvider)
    {
        $httpProvider.interceptors.push('httpInterceptor');
    }]);
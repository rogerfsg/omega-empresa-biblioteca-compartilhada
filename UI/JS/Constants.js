var Constants = {

    MAX_VISIBLE_PAGINATION_BUTTONS: 10,

    RemoteReturnCodes:
    {
        OPERACAO_REALIZADA_COM_SUCESSO: -1,
        ERRO_SEM_SER_EXCECAO: 7,
        ERRO_COM_SERVIDOR: 1,
        ERRO_PARAMETRO_INVALIDO: 12,
        RESULTADO_VAZIO: 11
    },

    MobileOperationStatus:
    {
        WAITING_DEVICE_TO_START_PROCESS: 1,
        PROCESS_RECEIVED_BY_DEVICE: 2,
        FILE_UPLOADED_TO_SERVER: 3,
        PROCESS_CANCELED: 4,
        PROCESS_FINISHED_WITH_ERROR: 5
    },

    ListSortingClass:
    {
        IDLE: 'sorting',
        ASC: 'sorting_asc',
        DESC: 'sorting_desc'
    },

    ListMappingType:
    {
        GRID: 'GRID',
        COMBOBOX: 'COMBOBOX',
        RELATED_ENTITY: 'RELATED_ENTITY',
        GENERIC: 'GENERIC'
    },

    FieldType:
    {
        FK: 'FK',
        INT: 'INT',
        FLOAT: 'FLOAT',
        DATE: 'DATE',
        TIME: 'TIME',
        DATETIME: 'DATETIME',
        BOOLEAN: 'BOOLEAN',
        TEXT: 'TEXT',
        TIME_IN_SECONDS: 'TIME_IN_SECONDS',
        TIMEZONE_OFFSET_IN_SECONDS: 'TIMEZONE_OFFSET_IN_SECONDS'
    },

    FilterOperator:
    {
        EQUALS: 'EQUALS',
        STARTS_WITH: 'STARTS_WITH',
        ENDS_WITH: 'ENDS_WITH',
        CONTAINS: 'CONTAINS',
        GREATER_THAN: 'GREATER_THAN',
        LESS_THAN: 'LESS_THAN',
        GREATER_THAN_OR_EQUALS: 'GREATER_THAN_OR_EQUALS',
        LESS_THAN_OR_EQUALS: 'LESS_THAN_OR_EQUALS',
    },

    DeviceType:
    {
        PHONE: 'PHONE',
        TABLET: 'TABLET',
        COMPUTER: 'COMPUTER'
    },

    LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE: 10

};
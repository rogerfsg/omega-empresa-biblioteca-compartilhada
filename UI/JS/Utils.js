Number.prototype.padLeft = function (base, chr)
{
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + String(this) : String(this);
};

Number.prototype.padRight = function (base, chr)
{
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? String(this) + new Array(len).join(chr || '0') : String(this);
};

if (!String.format)
{
    String.format = function (format)
    {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number)
        {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
};

var StringUtil = {

    countOccurrences: function (string, substring)
    {
        var substrings = string.split(substring);
        return substrings.length - 1;
    },

    isString: function (string)
    {
        return (string instanceof String);
    },

    isNullOrEmpty: function (string)
    {
        return GeneralUtil.isNullOrEmpty(string);
    }
};

var AngularControllerUtil = {

    isEditionMode: function(vm)
    {
        return !GeneralUtil.isNullOrEmpty(vm.isEdition) && vm.isEdition === true && !GeneralUtil.isNullOrEmpty(vm.entityIdForEdition) && NumberUtil.isInteger(vm.entityIdForEdition);
    },

    getEntityId: function(vm)
    {
        return vm.entityIdForEdition;
    },

    getFilterParametersForEdition: function(vm)
    {
        var filterData = {id: vm.entityIdForEdition};
        var filterOperators = {id: Constants.FilterOperator.EQUALS};
        return GeneralUtil.getFilterParameters(filterData, filterOperators);
    }

};

var RemoteDataUtil = {

    //protocolo_sistema.js - fica no comum_omegasoftware
    validateRemoteResponse: function (responseContent, arrValidResonseCodes)
    {
        var me = this;
        arrValidResonseCodes = arrValidResonseCodes || null;

        if(!GeneralUtil.isNullOrEmpty(responseContent)
            && !GeneralUtil.isNullOrEmpty(responseContent.data))
        {
            if(GeneralUtil.isNullOrEmptyArray(arrValidResonseCodes))
            {
                return me.isResponseCodeOK(responseContent.data.mCodRetorno);
            }
            else
            {
                return arrValidResonseCodes.indexOf(responseContent.data.mCodRetorno) > -1;
            }
        }
        else
        {
            return false;
        }

    },

    validateRemoteResponseForList: function(remoteData)
    {
        var me = this;
        var validation = me.validateRemoteResponse(remoteData);

        if (validation && remoteData.data.mCodRetorno == PROTOCOLO_SISTEMA.RESULTADO_VAZIO)
        {
            remoteData.data.mObj = { dataSet: []};
        }
        return validation;
    },

    isResponseCodeOK: function (mCodRetorno)
    {
        return (mCodRetorno === null || mCodRetorno == PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO || mCodRetorno == PROTOCOLO_SISTEMA.RESULTADO_VAZIO);
    },

    isResponseCodeErroFatal: function (mCodRetorno)
    {
        return (mCodRetorno == PROTOCOLO_SISTEMA.ERRO_COM_SERVIDOR);
    },

    getResponseCode: function(responseContent)
    {
        if(!GeneralUtil.isNullOrEmpty(responseContent)
                && !GeneralUtil.isNullOrEmpty(responseContent.data)
                    && !GeneralUtil.isNullOrEmpty(responseContent.data.mCodRetorno)){

            return responseContent.data.mCodRetorno;

        }
        else
        {
            return null;
        }
    },

    getResponseMessageString: function(responseContent)
    {
        if(!GeneralUtil.isNullOrEmpty(responseContent) && !GeneralUtil.isNullOrEmpty(responseContent.data)){
            return responseContent.data.mMensagem;
        }
        else if(!GeneralUtil.isNullOrEmpty(responseContent) && !GeneralUtil.isNullOrEmpty(responseContent.mMensagem)) {
            return responseContent.mMensagem;
        }
        return null;
    },

    getResponseMessageStringSafe: function(responseContent)
    {
        var ret = (!GeneralUtil.isNullOrEmpty(responseContent) && !GeneralUtil.isNullOrEmpty(responseContent.data)) ? responseContent.data.mMensagem : null;
        if(ret === null){
            ret = (!GeneralUtil.isNullOrEmpty(responseContent) && !GeneralUtil.isNullOrEmpty(responseContent.mMensagem)) ? responseContent.mMensagem : null;
        }
        return ret;
    },

    getDefaultResponseMessageString: function(responseCode)
    {
        switch(responseCode)
        {
            case PROTOCOLO_SISTEMA.CONSULTA_INVALIDA:

                return I18NUtil.getExpression("Erro no banco de dados.", "Database error.");

            default:

                return I18NUtil.getExpression("Ocorreu um erro ao recuperar os dados.", "An error has occured.");

        }
    },

    getResponseObject: function(responseObject)
    {
        return responseObject.data.mObj;
    },

    getResponseObjectDataSetOrEmptyArray: function(responseObject)
    {
        return (!GeneralUtil.isNullOrEmpty(responseObject.data.mObj) && !GeneralUtil.isNullOrEmpty(responseObject.data.mObj.dataSet)) ? responseObject.data.mObj.dataSet : [];
    },

    getResponseObjectSafe: function(responseObject)
    {
        return (!GeneralUtil.isNullOrEmpty(responseObject) && !GeneralUtil.isNullOrEmpty(responseObject.data)) ? responseObject.data.mObj : null;
    },

    getOffsecValueIfExists: function (secAttribute, remoteResponseObject)
    {
        var prefix = str.replace(/Sec/gi, "");
        var offSecAttributeName = prefix + "Offsec";
        var offSecValue = remoteResponseObject[offSecAttributeName];
        if (!Helper.isNullOrEmpty(offSecValue))
        {
            return offSecValue;
        }
        else
        {
            return null;
        }
    },

    replaceIntegerIdWithComboBoxModelObject: function (formDataObject, idField, comboBoxOptionsList)
    {
        var me = this;
        me.replaceIntegerIdsWithComboBoxModelObjects(formDataObject, [idField], [comboBoxOptionsList]);
    },

    replaceIntegerIdsWithComboBoxModelObjects: function (formDataObject, arrFkIdFields, arrFkComboListModel)
    {
        if (ArrayUtil.isArray(arrFkIdFields) && ArrayUtil.isArray(arrFkComboListModel) && arrFkIdFields.length == arrFkComboListModel.length)
        {
            for (var i = 0; i < arrFkIdFields.length; i++)
            {
                var id = formDataObject[arrFkIdFields[i]];
                var comboOptions = arrFkComboListModel[i];
                if (!GeneralUtil.isNullOrEmpty(id))
                {
                    for (var j = 0; j < comboOptions.length; j++)
                    {
                        if (comboOptions[j].id == id)
                        {
                            formDataObject[arrFkIdFields[i]] = comboOptions[j];
                            break;
                        }
                    }
                }
            }
        }
    },

    refactorInitialFilterLoadRemoteResponseData: function(vm, remoteResponseMessageObject, fieldNamesRelationshipsObject)
    {
        var viewModelFieldsParametersObject = vm.fieldsParameters;

        if (!GeneralUtil.isNullOrEmpty(remoteResponseMessageObject))
        {
            var comboBoxesRemoteData = remoteResponseMessageObject.comboBoxesData;
            if (!GeneralUtil.isNullOrEmpty(comboBoxesRemoteData))
            {
                for (var chave in fieldNamesRelationshipsObject)
                {
                    var fieldName = fieldNamesRelationshipsObject[chave];
                    var remoteData = comboBoxesRemoteData[fieldName];

                    if(!GeneralUtil.isNullOrEmpty(remoteData) && !GeneralUtil.isNullOrEmpty(remoteData.mObj))
                    {
                        vm.filterFieldsParameters[fieldName] = { remoteData: remoteData.mObj.dataSet };
                    }
                }
            }

        }
    },

    refactorInitialFormLoadRemoteResponseData: function (vm, remoteResponseMessageObject, fieldNamesRelationshipsObject)
    {
        var me = this;
        var viewModelFormDataObject = vm.formData;
        var viewModelFieldsParametersObject = vm.fieldsParameters;

        if (!GeneralUtil.isNullOrEmpty(remoteResponseMessageObject))
        {
            var remoteFormData = remoteResponseMessageObject.formData;
            var comboBoxesRemoteData = remoteResponseMessageObject.comboBoxesData;
            if (!GeneralUtil.isNullOrEmpty(comboBoxesRemoteData))
            {
                for (var chave in fieldNamesRelationshipsObject)
                {
                    var fieldName = fieldNamesRelationshipsObject[chave];
                    var remoteData = comboBoxesRemoteData[fieldName];

                    if(!GeneralUtil.isNullOrEmpty(remoteData) && !GeneralUtil.isNullOrEmpty(remoteData.mObj))
                    {
                        vm.fieldsParameters[fieldName] = { remoteData: remoteData.mObj.dataSet };
                    }
                }
            }

            if (!GeneralUtil.isNullOrEmpty(remoteFormData))
            {
                var formData = me.refactorRemoteFormData(remoteFormData);
                if (!GeneralUtil.isNullOrEmpty(comboBoxesRemoteData))
                {
                    for (var chave in fieldNamesRelationshipsObject)
                    {
                        var fieldName = fieldNamesRelationshipsObject[chave];
                        var optionsArray = vm.fieldsParameters[fieldName].remoteData;
                        if (!GeneralUtil.isNullOrEmpty(remoteFormData[chave]) && !ArrayUtil.isNullOrEmpty(optionsArray))
                        {
                            for (var i = 0; i < optionsArray.length; i++)
                            {
                                if (optionsArray[i].id == remoteFormData[chave])
                                {
                                    formData[chave] = optionsArray[i];
                                    break;
                                }
                            }
                        }
                    }
                }

                GeneralUtil.safeCopy(formData, vm.formData);
            }
            else if (!GeneralUtil.isNullOrEmpty(remoteResponseMessageObject.__types))
            {
                GeneralUtil.safeCopy(remoteResponseMessageObject.__types, vm.formData.__types);
            }
        }
    },

    refactorRemoteFormData: function (remoteResponseFormData)
    {
        var refactorObject = remoteResponseFormData;
        var returnObject = {};
        var typesAttribute = refactorObject.__types;
        if (!GeneralUtil.isNullOrEmpty(typesAttribute))
        {
            for (var nomeAtributo in refactorObject)
            {
                var type = typesAttribute[nomeAtributo];
                switch (type)
                {
                    case Constants.FieldType.BOOLEAN:
                        var booleanValue = GeneralUtil.parseIntToBoolean(refactorObject[nomeAtributo]);
                        if (!GeneralUtil.isNullOrEmpty(booleanValue))
                        {
                            returnObject[nomeAtributo] = booleanValue;
                        }
                        break;

                    case Constants.FieldType.DATE:
                        var dateObject = DateUtil.getDateObject(refactorObject[nomeAtributo]);
                        if (!GeneralUtil.isNullOrEmpty(dateObject))
                        {
                            returnObject[nomeAtributo] = DateUtil.getMysqlDate(dateObject);
                        }
                        break;

                    case Constants.FieldType.DATETIME:
                        var dateObject = DateUtil.getDateObject(refactorObject[nomeAtributo]);
                        if (!GeneralUtil.isNullOrEmpty(dateObject))
                        {
                            returnObject[nomeAtributo] = DateUtil.gdetMysqlDateTime(dateObject);
                        }
                        break;

                    case Constants.FieldType.TIME_IN_SECONDS:
                        var utcSecValue = refactorObject[nomeAtributo];
                        var userTimezoneOffset = DateUtil.getUserTimezoneOffsetInSeconds();
                        returnObject[nomeAtributo] = DateUtil.getDateObject(utcSecValue + userTimezoneOffset);
                        break;

                    case Constants.FieldType.TIMEZONE_OFFSET_IN_SECONDS:
                        returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                        break;

                    default:
                        returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                        break;

                }
            }

            returnObject.__types = typesAttribute;
        }
        else
        {
            returnObject = refactorObject;
        }

        return returnObject;
    },

    getRelatedEntitiesData: function (relatedEntitiesObject)
    {
        var returnObject = {};
        if (!GeneralUtil.isNullOrEmpty(relatedEntitiesObject))
        {
            for (var property in relatedEntitiesObject)
            {
                if (!GeneralUtil.isNullOrEmpty(relatedEntitiesObject[property].formData))
                {
                    returnObject[property] = relatedEntitiesObject[property].formData;
                }
            }
            return returnObject;
        }
        else
        {
            return {};
        }
    },

    refactorRemoteRequestForFilter: function(viewModelFormData, viewModelRelatedEntities)
    {
        var me = this;
        return me.refactorRemoteRequest(viewModelFormData, viewModelRelatedEntities, false);
    },

    refactorRemoteRequest: function (viewModelFormData, viewModelRelatedEntities, appendCurrentTimeAndTimezone)
    {
        if(typeof appendCurrentTimeAndTimezone === 'undefined')
        {
            appendCurrentTimeAndTimezone = true;
        }

        var me = this;
        var refactorObject = viewModelFormData;
        var returnObject = me.getRelatedEntitiesData(viewModelRelatedEntities);

        var typesAttribute = refactorObject.__types;
        var areTypesObjectDefined = !GeneralUtil.isNullOrEmpty(typesAttribute);
        for (var nomeAtributo in refactorObject)
        {
            if(!areTypesObjectDefined)
            {
                if(ArrayUtil.isArray(returnObject[nomeAtributo]))
                {
                    returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                }
                else if(DateUtil.isDateObject(refactorObject[nomeAtributo]))
                {
                    returnObject[nomeAtributo] = DateUtil.getUtcTimestampInSeconds(refactorObject[nomeAtributo]);
                }
                else if (GeneralUtil.isObject(refactorObject[nomeAtributo]))
                {
                    if(typeof refactorObject[nomeAtributo].id !== 'undefined')
                    {
                        returnObject[nomeAtributo] = refactorObject[nomeAtributo].id;
                    }
                    else if(typeof refactorObject[nomeAtributo].value !== 'undefined')
                    {
                        returnObject[nomeAtributo] = refactorObject[nomeAtributo].value;
                    }
                }
                else
                {
                    returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                }

                continue;
            }
            else
            {
                if(nomeAtributo == "__types")
                {
                    continue;
                }

                var type = typesAttribute[nomeAtributo];
                switch (type)
                {
                    case Constants.FieldType.FK:
                    case Constants.FieldType.INT:
                        var currentObject = refactorObject[nomeAtributo];
                        if (GeneralUtil.isObject(currentObject)
                            && (typeof currentObject.id !== 'undefined')
                        )
                        {
                            returnObject[nomeAtributo] = currentObject.id;
                        }
                        else if(NumberUtil.isInteger(currentObject))
                        {
                            returnObject[nomeAtributo] = currentObject;
                        }
                        break;

                    case Constants.FieldType.DATE:
                        var dateObject = DateUtil.getDateObject(refactorObject[nomeAtributo]);
                        if (!GeneralUtil.isNullOrEmpty(dateObject))
                        {
                            returnObject[nomeAtributo] = DateUtil.getMysqlDate(dateObject);
                        }
                        break;

                    case Constants.FieldType.DATETIME:
                        var dateObject = DateUtil.getDateObject(refactorObject[nomeAtributo]);
                        if (!GeneralUtil.isNullOrEmpty(dateObject))
                        {
                            returnObject[nomeAtributo] = DateUtil.getMysqlDateTime(dateObject);
                        }
                        break;

                    case Constants.FieldType.BOOLEAN:
                        if(typeof refactorObject[nomeAtributo] !== 'undefined')
                        {
                            returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                        }
                        else
                        {
                            returnObject[nomeAtributo] = returnObject[nomeAtributo];
                        }

                        break;

                    case Constants.FieldType.TIME_IN_SECONDS:
                        var dateObject = refactorObject[nomeAtributo];
                        if (DateUtil.isDateObject(dateObject))
                        {
                            var utcTimestamp = DateUtil.getUtcTimestampInSeconds(dateObject);
                            returnObject[nomeAtributo] = utcTimestamp;
                        }
                        break;

                    case Constants.FieldType.TIMEZONE_OFFSET_IN_SECONDS:
                        var userTimezoneOffset = DateUtil.getUserTimezoneOffsetInSeconds();
                        returnObject[nomeAtributo] = userTimezoneOffset;
                        break;

                    default:
                        returnObject[nomeAtributo] = refactorObject[nomeAtributo];
                        break;

                }
            }
        }

        if(appendCurrentTimeAndTimezone)
        {
            returnObject.__currentTimeInSeconds = DateUtil.getUtcTimestampInSeconds(new Date());
            returnObject.__currentTimezoneOffsetInSeconds = DateUtil.getUserTimezoneOffsetInSeconds();
        }

        return returnObject;
    },

    defaultRemoteErrorHandling: function(remoteContent, vm)
    {
        var me = this;
        var rootScope = ApplicationUtil.getRootScope();

        remoteContent = (!GeneralUtil.isNullOrEmpty(remoteContent.data)) ? remoteContent.data : remoteContent;
        if(!GeneralUtil.isNullOrEmpty(rootScope))
        {
            if(RemoteDataUtil.isResponseCodeOK(remoteContent.mCodRetorno))
            {
                ApplicationUtil.showSuccessMessage(remoteContent.mMensagem)
            }
            else if(RemoteDataUtil.isResponseCodeErroFatal(remoteContent.mCodRetorno))
            {
                var remoteMessage = me.getResponseMessageStringSafe(remoteContent);
                if(!GeneralUtil.isNullOrEmpty(remoteMessage))
                {
                    var titulo = I18NUtil.getExpression("Tente novamente mais tarde...", "Error returned from server");
                    ApplicationUtil.showErrorMessage("Caso o problema persista comunique a gente!", titulo, vm);
                }
                else
                {
                    var mensagem = I18NUtil.getExpression("Ocorreu um erro n�o esperado.", "An unexpected error has occured.");
                    ApplicationUtil.showErrorMessage("Caso o problema persista comunique a gente!", null, vm);
                }
            } else {
                ApplicationUtil.showWarningMessage(remoteContent.mMensagem)
            }
        }

        LogUtil.logRemoteResponseError(remoteContent);

    }

};

var ArrayUtil = {

    isArray: function (array)
    {
        return (array instanceof Array);
    },

    isNullOrEmpty: function (array)
    {
        return GeneralUtil.isNullOrEmptyArray(array);
    }

};

var GeneralUtil = {

    safeCopy: function(origin, destination)
    {
        var me = this;
        if(me.isObject(origin))
        {
            if(!me.isObject(destination))
            {
                destination = {};
            }

            for(var prop in origin)
            {
                destination[prop] = origin[prop];
            }
        }

        return destination;

    },

    unserializeCurrentQueryString: function()
    {
        var me = this;
        var currentQueryString = document.location.search;
        return me.unserializeQueryString(currentQueryString);
    },

    unserializeQueryString: function(str)
    {
        var me = this;
        if(!me.isNullOrEmpty(str))
        {
            if(str.startsWith("?"))
            {
                str = str.substring(1);
            }

            str = decodeURIComponent(str);
            var chunks = str.split('&');
            var obj = {};

            for(var c=0; c < chunks.length; c++)
            {
                var split = chunks[c].split('=', 2);
                obj[split[0]] = split[1];
            }

            return obj;
        }
        else
        {
            return {};
        }

    },

    getFilterParameters: function (filterData, filterOperators)
    {
        var me = this;
        var returnArray = [];
        for (var key in filterData)
        {
            if(!me.isNullOrEmpty(filterData[key]))
            {
                var value = filterData[key];
                returnArray.push({
                    value: value,
                    attribute: key,
                    operator: filterOperators[key]
                });
            }
        }
        return returnArray;
    },

    encodeObjectToUrlString: function(object)
    {
        var me = this;
        return encodeURIComponent(JSON.stringify(object))
    },

    decodeUrlStringToObject: function(urlString)
    {
        var me = this;
        if(me.isNullOrEmpty(urlString))
        {
            return {};
        }
        else
        {
            //deserializa o objeto atrav�s da string codificada
            var decodedObject = JSON.parse(decodeURIComponent(urlString));

            //faz ajustes no objeto (por refer�ncia)
            me.fixObjectAfterUnserialize(decodedObject);

            //retorna objeto corrigido
            return decodedObject;
        }
    },

    fixObjectAfterUnserialize: function(object)
    {
        var me = this;
        for (var property in object)
        {
            if (object.hasOwnProperty(property))
            {
                //caso seja objeto, faz chamada recursiva
                if (typeof object[property] == "object")
                {
                    me.fixObjectAfterUnserialize(object[property]);
                }
                //caso seja data serializada, converte em objeto Date
                else if(typeof object[property] == "string"
                            && /(\d{4})-(\d{2})-(\d{2})T((\d{2}):(\d{2}):(\d{2}))\.(\d{3})Z/.test(object[property] ))
                {
                    object[property] = new Date(object[property]);
                }
            }
        }
    },

    isNullOrEmptyObject: function(object)
    {
        var me = this;
        return me.isNullOrEmpty(object) || (me.isObject(object) && Object.keys(object).length === 0);
    },

    isNullOrEmpty: function (object)
    {
        return (typeof object == 'undefined' || object === null || object === '');
    },

    isArray: function(object)
    {
        var me = this;
        return me.isObject(object) && object instanceof Array;
    },

    isEmptyArray: function(object)
    {
        var me = this;
        return me.isArray(object) && object.length == 0;
    },

    isNullOrEmptyArray: function(object)
    {
        var me = this;
        return me.isNullOrEmpty(object) || me.isEmptyArray(object);
    },

    isBoolean: function(object)
    {
        return typeof(object) == typeof(true);
    },

    isNumberOrNumericString: function(object)
    {
        return typeof object == typeof 0 || parseInt(object) != NaN;
    },

    parseIntToBoolean: function(object)
    {
        var me = this;
        if(!me.isBoolean(object))
        {
            if(object === "1" || object === 1)
            {
                return true;
            }
            else if(object === "0" || object === 0)
            {
                return false;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return object;
        }
    },

    isObject: function(object)
    {
        return (object !== null && typeof object == typeof {});
    },

    isString: function(object)
    {
        return typeof object == typeof "";
    },

    // isDefined: function(object)
    // {
    //     return (typeof object !== 'undefined');
    // },

    isFunction: function(object)
    {
        var me = this;
        return (!me.isNullOrEmpty(object) && object instanceof Function);
    }

};

var FieldsParametersUtil = {

    getDefaultSelectForBooleanOptions: function(firstBlank)
    {
        if(GeneralUtil.isNullOrEmpty(firstBlank))
        {
            firstBlank = true;
        }

        var options = [
            {
                displayValue: I18NUtil.getExpression("Sim", "Yes"),
                value: true
            },
            {
                displayValue: I18NUtil.getExpression("N�o", "No"),
                value: false
            }
        ];

        if(firstBlank)
        {
            options.unshift({ displayValue: "", value: null });
        }

        return options;

    },

    getDefaultSelectForStatusOptions: function(firstBlank)
    {
        if(GeneralUtil.isNullOrEmpty(firstBlank))
        {
            firstBlank = true;
        }

        var options = [
            {
                displayValue: I18NUtil.getExpression("Ativo", "Active"),
                value: true
            },
            {
                displayValue: I18NUtil.getExpression("Inativo", "Inactive"),
                value: false
            }
        ];

        if(firstBlank)
        {
            options.unshift({ displayValue: "", value: null });
        }

        return options;

    },

    defaultDatePickerParams: {
        dateDisabled: false,
        formatYear: 'yyyy',
        maxDate: (new Date()).setDate(new Date() + 365),
        minDate: new Date(),
        showWeeks: false,
        startingDay: 1
    },

    defaultTimePickerParams: {
        hourStep: 1,
        minuteStep: 1,
        showMeridian: false
    },

    defaultUploadAreaParams: {
        keep: true,
        keepDistinct: true,
        dropAvailable: true,
        pattern: '*/*',
        acceptedTypes: '*/*',
        allowDirectory: false,
        allowMultipleFiles: false,
        dragOverCssClass: 'dragover',
        modelOptions: {debounce: 100},
        dragOverClass: {
            accept: 'dragover',
            reject: 'dragover-err',
            pattern: '*/*'
        },
        uploadLog: [],
        isUploading: false,
        uploadProgress: 0
    },

    defaultImageUploadAreaParams: {
        keep: true,
        keepDistinct: true,
        dropAvailable: true,
        pattern: 'image/*',
        acceptedTypes: 'image/*',
        allowDirectory: false,
        allowMultipleFiles: false,
        modelOptions: {debounce: 100},
        resize: {
            width: 1000,
            height: 1000,
            centerCrop: true
        },
        dragOverClass: {
            accept: 'dragover',
            reject: 'dragover-err',
            pattern: 'image/*'
        },
        uploadLog: [],
        isUploading: false,
        uploadProgress: 0
    }

};
var DateUtil = {

    differenceInDays: function (date1, date2)
    {
        var millisecondsPerDay = 1000 * 60 * 60 * 24;
        var millisBetween = date2.getTime() - date1.getTime();
        var days = millisBetween / millisecondsPerDay;
        return Math.floor(days);
    },

    convertJsonDateToObject: function (jsonDateString)
    {
        return new Date(parseInt(jsonDateString.replace("/Date(", "").replace(")/", ""), 10));
    },

    getMysqlDate: function (dateObjectOrString)
    {
        if (typeof dateObjectOrString == 'string' && dateObjectOrString.indexOf("/Date(") > -1)
        {
            dateObjectOrString = DateUtil.convertJsonDateToObject(dateObjectOrString);
        }
        else
        {
            if (NumberUtil.isInteger(dateObjectOrString))
            {
                dateObjectOrString = new Date(dateObjectOrString * 1000);
            }
        }

        if (typeof dateObjectOrString == 'object')
        {
            return [dateObjectOrString.getFullYear(), (dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getDate().padLeft()].join('-');
        }
    },

    getFormattedDate: function (dateObjectOrString)
    {
        if (typeof dateObjectOrString == 'string' && dateObjectOrString.indexOf("/Date(") > -1)
        {
            dateObjectOrString = DateUtil.convertJsonDateToObject(dateObjectOrString);
        }
        else
        {
            if (NumberUtil.isInteger(dateObjectOrString))
            {
                dateObjectOrString = new Date(dateObjectOrString * 1000);
            }
        }

        if (typeof dateObjectOrString == 'object')
        {
            var userLanguage = I18NUtil.getUserLanguage();
            switch (userLanguage)
            {
                case 'en':
                    return [(dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getDate().padLeft(), dateObjectOrString.getFullYear()].join('/');
                case 'pt':
                default:
                    return [dateObjectOrString.getDate().padLeft(), (dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getFullYear()].join('/');
            }
        }
    },

    isDateObject: function (dateObject)
    {
        return (typeof dateObject == 'object' && dateObject instanceof Date);
    },

    getDateObject: function (dateStringOrTimestamp)
    {
        var dateObject = null;
        if (this.isDateObject(dateObject))
        {
            dateObject = dateStringOrTimestamp;
        }
        if (typeof dateStringOrTimestamp == 'string')
        {
            if (dateStringOrTimestamp.indexOf("/Date(") > -1)
            {
                dateObject = this.convertJsonDateToObject(dateStringOrTimestamp);
            }
            //datetime MySQL
            else
            {
                if (StringUtil.countOccurrences(dateStringOrTimestamp, '-') == 2 && StringUtil.countOccurrences(dateStringOrTimestamp, ':' == 2))
                {
                    var partesData = dateStringOrTimestamp.split(/[- :]/);
                    if (partesData.length == 6)
                    {
                        dateObject = new Date(Date.UTC(partesData[0], partesData[1] - 1, partesData[2], partesData[3], partesData[4], partesData[5]));
                    }
                    else
                    {
                        return null;
                    }
                }
                //data MySQL
                else
                {
                    if (StringUtil.countOccurrences(dateStringOrTimestamp, '-') == 2)
                    {
                        var partesData = dateStringOrTimestamp.split(/[-]/);
                        if (partesData.length == 3)
                        {
                            dateObject = new Date(Date.UTC(partesData[0], partesData[1] - 1, partesData[2], 12, 0, 0));
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //data formatada
                    else
                    {
                        if (StringUtil.countOccurrences(dateStringOrTimestamp, '/') == 2)
                        {
                            var userLanguage = I18NUtil.getUserLanguage();
                            var partesData = dateStringOrTimestamp.split(/[/]/);
                            //datetime
                            if (StringUtil.countOccurrences(dateStringOrTimestamp, ':') == 2)
                            {
                                partesData = dateStringOrTimestamp.split(/[/ :]/);
                                if (partesData.length == 6)
                                {
                                    switch (userLanguage)
                                    {
                                        case 'en':
                                            dateObject = new Date(Date.UTC(partesData[2], partesData[0] - 1, partesData[1], partesData[3], partesData[4], partesData[5]));
                                            break;
                                        case 'pt':
                                        default:
                                            dateObject = new Date(Date.UTC(partesData[2], partesData[1] - 1, partesData[0], partesData[3], partesData[4], partesData[5]));
                                            break;
                                    }
                                }
                                else
                                {
                                    return null;
                                }
                            }
                            //date
                            else
                            {
                                switch (userLanguage)
                                {
                                    case 'en':
                                        dateObject = new Date(Date.UTC(partesData[2], partesData[0] - 1, partesData[1], 12, 0, 0));
                                        break;
                                    case 'pt':
                                    default:
                                        dateObject = new Date(Date.UTC(partesData[2], partesData[1] - 1, partesData[0], 12, 0, 0));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (NumberUtil.isInteger(dateStringOrTimestamp))
            {
                dateObject = new Date(dateStringOrTimestamp * 1000);
            }
        }
        return dateObject;
    },

    getMysqlDateTime: function (dateObjectOrString)
    {
        if (typeof dateObjectOrString != 'object')
        {
            dateObjectOrString = this.getDateObject(dateObjectOrString);
        }
        if (typeof dateObjectOrString == 'object')
        {
            return "" + [dateObjectOrString.getFullYear(), (dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getDate().padLeft()].join('-') + " " +
                +dateObjectOrString.getHours().padLeft() + ":" + dateObjectOrString.getMinutes().padLeft() + ":" + dateObjectOrString.getSeconds().padLeft() + "";
        }
        return null;
    },

    getFormattedDateTime: function (dateObjectOrString)
    {
        if (typeof dateObjectOrString != 'object')
        {
            dateObjectOrString = this.getDateObject(dateObjectOrString);
        }
        if (typeof dateObjectOrString == 'object')
        {
            var userLanguage = I18NUtil.getUserLanguage();
            switch (userLanguage)
            {
                case 'en':
                    return "" + [(dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getDate().padLeft(), dateObjectOrString.getFullYear()].join('/') + " " +
                        +dateObjectOrString.getHours().padLeft() + ":" + dateObjectOrString.getMinutes().padLeft() + ":" + dateObjectOrString.getSeconds().padLeft() + "";
                case 'pt':
                default:
                    return "" + [dateObjectOrString.getDate().padLeft(), (dateObjectOrString.getMonth() + 1).padLeft(), dateObjectOrString.getFullYear()].join('/') + " " +
                        +dateObjectOrString.getHours().padLeft() + ":" + dateObjectOrString.getMinutes().padLeft() + ":" + dateObjectOrString.getSeconds().padLeft() + "";
            }
        }
        return null;
    },

    getUserTimezoneOffsetInSeconds: function ()
    {
        var offset = -1 * (new Date().getTimezoneOffset()) * 60;
        return offset;
    },

    getUtcDate: function (date)
    {
        if (!(date instanceof Date))
        {
            date = this.getDateObject(date);
        }
        if (date instanceof Date)
        {
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        }
        else
        {
            return null;
        }
    },

    getUtcTimestampInSeconds: function (dateObject)
    {
        if (this.isDateObject(dateObject))
        {
            var utcDateObject = this.getUtcDate(dateObject);
            return Math.round(utcDateObject.getTime() / 1000);
        }
        else
        {
            return null;
        }
    }
};

var I18NUtil = {

    defaultLanguage: 'pt',
    getUserLanguage: function ()
    {
        try
        {
            var cookieLocale = CookieUtil.getCookie('idioma');
            var shortCookieLocale = this.getShortLocale(cookieLocale);
            if (!GeneralUtil.isNullOrEmpty(shortCookieLocale))
            {
                return shortCookieLocale;
            }
            else
            {
                var fullSystemLocale = Intl.DateTimeFormat().resolvedOptions().locale;
                var shortSystemLocale = this.getShortLocale(fullSystemLocale);
                if (!GeneralUtil.isNullOrEmpty(shortSystemLocale))
                {
                    return shortSystemLocale;
                }
                else
                {
                    return this.defaultLanguage;
                }
            }
        }
        catch (ex)
        {
            return this.defaultLanguage;
        }
    },

    getShortLocale: function (fullLocale)
    {
        if (!GeneralUtil.isNullOrEmpty(fullLocale) && fullLocale.length >= 2)
        {
            return fullLocale.substr(0, 2).toLowerCase();
        }
        else
        {
            return null;
        }
    },

    getUserDateFormat: function ()
    {
        var userLanguage = this.getUserLanguage();
        switch (userLanguage)
        {
            case 'en':
                return 'MM/dd/yyyy';
            case 'pt':
            default:
                return 'dd/MM/yyyy';
        }
    },

    getExpression: function (expressaoPortugues, expressaoIngles)
    {
        switch (this.getUserLanguage())
        {
            case 'en':
                return expressaoIngles;
            case 'pt':
            default:
                return expressaoPortugues;
        }
    }
};

var CookieUtil = {

    getCookie: function (cname)
    {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++)
        {
            var c = ca[i];
            while (c.charAt(0) == ' ')
            {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0)
            {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },

    setCookie: function (cname, cvalue, exdays)
    {
        exdays = exdays || 30;
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

};
var NumberUtil = {

    isInteger: function (variable)
    {
        return this.isNumber(variable) && (variable % 1 === 0);
    },

    isNumber: function (variable)
    {
        return !isNaN(variable);
    },

    getRandomNumber: function (min, max)
    {
        return Math.round(Math.random() * (max - min) + min);
    },

    getCurrencyFormattedString: function (valor, simboloMoeda)
    {
        simboloMoeda = simboloMoeda || 'R$';
        var valorString = '';
        if (typeof valor == 'number')
        {
            valorString = valor.toString();
        }
        else
        {
            if (typeof valor == 'string')
            {
                valorString = valor.replace(/\./g, '').replace(',', '.');
            }
        }
        if (valorString.indexOf('.') > -1)
        {
            valorString = valorString.replace(/\./g, ',');
        }
        else
        {
            valorString += ',00';
        }
        var partesNumero = valorString.split(',');

        if (partesNumero.length == 2)
        {
            var parteInteira = partesNumero[0];
            var parteDecimal = parseInt(partesNumero[1]).padRight();

            if((parteDecimal + "").length > 2)
            {
                parteDecimal = parseInt((parteDecimal + "").substring(0, 2));
            }

            var parteInteiraFormatada = '';
            for (i = parteInteira.length - 1; i >= 0; i -= 3)
            {
                if (i > 2)
                {
                    parteInteiraFormatada = '.' + parteInteira[i - 2] + parteInteira[i - 1] + parteInteira[i] + parteInteiraFormatada;
                }
                else
                {
                    if (i == 2)
                    {
                        parteInteiraFormatada = parteInteira[i - 2] + parteInteira[i - 1] + parteInteira[i] + parteInteiraFormatada;
                    }
                    else
                    {
                        if (i == 1)
                        {
                            parteInteiraFormatada = parteInteira[i - 1] + parteInteira[i] + parteInteiraFormatada;
                        }
                        else
                        {
                            if (i == 0)
                            {
                                parteInteiraFormatada = parteInteira[i] + parteInteiraFormatada;
                            }
                        }
                    }
                }
            }
            return simboloMoeda + ' ' + parteInteiraFormatada + ',' + parteDecimal;
        }
    }
};

var OmegaFormUtil = {

    shrinkSectionPanels: function(vm)
    {
        $(vm.rootControllerElement).find('.omega-form-section-box:not(.box-collapsed)').addClass('box-collapsed');
    },

    expandSectionPanels: function(vm)
    {
        $(vm.rootControllerElement).find('.omega-form-section-box').removeClass('box-collapsed');
    }

};

var OmegaListUtil = {

    doActionsAfterRemoteListDataReceived: function(vm, responseObject)
    {
        var me = this;
        PaginationUtil.setPaginationParameters(vm, responseObject);
        PaginationUtil.updateVisiblePaginationButtonsArray(vm);

        setTimeout(function()
        {
            var tableSelector = $(vm.rootControllerElement).find('.omega-list-table');
            TableUtil.fixTableHeaderWidth(tableSelector);
            TableUtil.makeTableColumnsResizable(tableSelector);
        }, 500);

        if(ApplicationUtil.detectDeviceType() == Constants.DeviceType.PHONE)
        {
            me.shrinkFilterPanel(vm);
        }

    },

    shrinkFilterPanel: function(vm)
    {
        $(vm.rootControllerElement).find('.omega-filter-box:not(.box-collapsed)').addClass('box-collapsed');
    },

    expandFilterPanels: function(vm)
    {
        $(vm.rootControllerElement).find('.omega-filter-box').removeClass('box-collapsed');
    },

    getListParametersFromBrowseUrlQueryString: function()
    {
        var currentQueryStringObject = GeneralUtil.unserializeCurrentQueryString();
        var filterParameters = currentQueryStringObject['filterParameters'] || '';
        var sortingParameters = currentQueryStringObject['sortingParameters'] || '';
        var paginationParameters = currentQueryStringObject['paginationParameters'] || '';
        var preservedFilterData = currentQueryStringObject['preservedFilterData'] || '';

        var filterParametersObject = GeneralUtil.decodeUrlStringToObject(filterParameters);
        var sortingParametersObject = GeneralUtil.decodeUrlStringToObject(sortingParameters);
        var paginationParametersObject = GeneralUtil.decodeUrlStringToObject(paginationParameters);
        var preservedFilterDataObject = GeneralUtil.decodeUrlStringToObject(preservedFilterData);

        return {
            filterParameters: filterParametersObject,
            sortingParameters: sortingParametersObject,
            paginationParameters: paginationParametersObject,
            preservedFilterData: preservedFilterDataObject
        };

    },

    addListParametersToBrowserUrlQueryString: function(parameters, preservedFilterData)
    {
        var newQueryString = "";
        var newBrowserBaseLocation = document.location.protocol + '//' + document.location.host + document.location.pathname;
        var currentQueryStringObject = GeneralUtil.unserializeCurrentQueryString();

        for(var property in parameters)
        {
            if(!GeneralUtil.isNullOrEmptyObject(parameters[property]))
            {
                currentQueryStringObject[property] = GeneralUtil.encodeObjectToUrlString(parameters[property]);
            }
        }

        if(!GeneralUtil.isNullOrEmptyObject(preservedFilterData))
        {
            currentQueryStringObject['preservedFilterData'] = GeneralUtil.encodeObjectToUrlString(preservedFilterData);
        }

        var newQueryString = $.param(currentQueryStringObject);
        window.history.replaceState(null, null, newBrowserBaseLocation + "?" + newQueryString);
    }

};

var PaginationUtil = {

    setPaginationParameters: function(vm, responseObject)
    {
        var me = this;
        if(!GeneralUtil.isNullOrEmpty(responseObject) && !GeneralUtil.isNullOrEmpty(responseObject.paginationParameters))
        {
            vm.paginationParameters = responseObject.paginationParameters;
        }
    },

    updateVisiblePaginationButtonsArray: function (vm, maxPaginationButtons)
    {
        var params = vm.paginationParameters;
        var minPossiblePage = 1;
        var maxPossiblePage = params.lastPage;

        vm.firstShowingRecord = params.recordsPerPage * (params.currentPage - 1) + 1;
        vm.lastShowingRecord = (params.recordsPerPage * (params.currentPage)) - (params.recordsPerPage - vm.originalDataSet.length);

        var maxButtons = maxPaginationButtons || Constants.MAX_VISIBLE_PAGINATION_BUTTONS;
        var minLimit = maxButtons / 2;
        var maxLimit = maxButtons / 2 + (maxButtons % 2);

        if (params.currentPage - minPossiblePage < minLimit)
        {
            maxLimit += minLimit - (params.currentPage - minPossiblePage);
        }

        if (maxPossiblePage - params.currentPage < maxLimit)
        {
            minLimit += maxLimit - (maxPossiblePage - params.currentPage);
        }

        var min = params.currentPage - minLimit > 1 ? params.currentPage - minLimit : 1;
        var max = params.currentPage + maxLimit > maxPossiblePage ? maxPossiblePage : params.currentPage + (maxLimit - 1);
        vm.paginationParameters.visibleButtonsArray = [];
        for (var i = min; i <= max; i++)
        {
            vm.paginationParameters.visibleButtonsArray.push(i);
        }
    }

};

var LogUtil = {

    logRemoteResponseError: function(returnContent)
    {
        var responseMessage = RemoteDataUtil.getResponseMessageStringSafe(returnContent);

        if(!GeneralUtil.isNullOrEmpty(responseMessage)
            && (typeof omegaLog !== 'undefined')
            && (typeof omegaLog.message !== 'undefined')
        )
        {
            if(typeof omegaLog !== 'undefined')
                omegaLog.error(responseMessage);
        }

    }

};

var TableUtil = {

    fixTableHeaderWidth: function(tableSelector)
    {
        $(tableSelector).find('th:not(.already-fixed):not(.omega-list-checkbox-th):not(.omega-list-record-options-th)').each(function()
        {
            var width = $(this).outerWidth();
            var newWidth = width + 30;

            $(this).addClass('already-fixed');
            $(this).css({ "min-width": newWidth + "px" });
        });

        $(tableSelector).find('th.omega-list-checkbox-th').css({ "max-width": "50px" });
        $(tableSelector).find('th.omega-list-record-options-th').css({ "max-width": "100px" });

    },

    makeTableColumnsResizable: function(tableSelector)
    {
        $(tableSelector).find("td").resizableTableColumns();
    },

    fixTrHoverWithRowspan: function (tableSelector)
    {
        function findBlocks(theTable)
        {
            if ($(theTable).data('hasblockrows') == null)
            {
                // we will loop through the rows but skip the ones not in a block
                var rows = null;
                if ($(theTable).is('tr'))
                {
                    rows = $(theTable);
                }
                else
                {
                    rows = $(theTable).find('tr');
                }
                for (var i = 0; i < rows.length;)
                {
                    var firstRow = rows[i];
                    // find max rowspan in this row - this represents the size of the block
                    var maxRowspan = 1;
                    $(firstRow).find('td').each(function ()
                    {
                        var attr = parseInt($(this).attr('rowspan') || '1', 10)
                        if (attr > maxRowspan)
                        {
                            maxRowspan = attr;
                        }
                    });
                    maxRowspan += i;
                    var blockRows = [];
                    for (; i < maxRowspan; i++)
                    {
                        $(rows[i]).data('blockrows', blockRows);
                        blockRows.push(rows[i]);
                    }
                }
                $(theTable).data('hasblockrows', 1);
            }
        }

        $(tableSelector).find("td").hover(
            function ()
            {
                $el = $(this);
                $.each($el.parent().data('blockrows'), function ()
                {
                    $(this).removeClass('no-hover').addClass('hover');
                    $(this).find('td').addClass('hover');
                });
            },
            function ()
            {
                $el = $(this);
                $.each($el.parent().data('blockrows'), function ()
                {
                    $(this).removeClass('hover').addClass('no-hover');
                    $(this).find('td').removeClass('hover');
                });
            });
        findBlocks(tableSelector);
    }
};

var HtmlUtil = {

    mostrarLoading: function ()
    {
        var rand = Math.floor((Math.random() * 1000) + 1);
        var mensagemCarregando = I18NUtil.getExpression('Carregando...', 'Loading...');
        $.blockUI({
            message: '<div id="carregando_' + rand + '" style="margin:20px;" ></div><p"><b>' + mensagemCarregando + '</b></p>',
            css: {
                'z-index': 2000,
                color: '#c5aa72',
                backgroundColor: '#000',
                width: '150px',
                top: ($(window).height() - 150) / 2 + 'px',
                left: ($(window).width() - 150) / 2 + 'px',
                border: 'none',
                padding: '15px',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                '-ie-border-radius': '5px',
                'border-radius': '5px',
                opacity: .7
            }
        });

        if ($('#carregando_' + rand).children().length == 0)
        {
            var carregando = new CanvasLoader("carregando_" + rand, {
                id: "canvasLoader",
                safeVML: true
            });
            carregando.setShape("spiral");
            carregando.setColor('#c5aa72');
            carregando.setDiameter(80);
            carregando.setDensity(100);
            carregando.setSpeed(3);
            carregando.setRange(1);
            carregando.show();
        }
    },

    esconderLoading: function ()
    {
        $.unblockUI();
    },

    corrigirHoverComRowspan: function (tableSelector)
    {
        function findBlocks(theTable)
        {
            if ($(theTable).data('hasblockrows') == null)
            {
                console.log('findBlocks'); // to prove we only run this once
                // we will loop through the rows but skip the ones not in a block
                var rows = null;
                if ($(theTable).is('tr'))
                {
                    rows = $(theTable)
                }
                else
                {
                    rows = $(theTable).find('tr');
                }
                for (var i = 0; i < rows.length;)
                {
                    var firstRow = rows[i];
                    // find max rowspan in this row - this represents the size of the block
                    var maxRowspan = 1;
                    $(firstRow).find('td').each(function ()
                    {
                        var attr = parseInt($(this).attr('rowspan') || '1', 10)
                        if (attr > maxRowspan)
                        {
                            maxRowspan = attr;
                        }
                    });
                    // set to the index in rows we want to go up to
                    maxRowspan += i;
                    // build up an array and store with each row in this block
                    // this is still memory-efficient, as we are just storing a pointer to the same array
                    // ... which is also nice becuase we can build the array up in the same loop
                    var blockRows = [];
                    for (; i < maxRowspan; i++)
                    {
                        $(rows[i]).data('blockrows', blockRows);
                        blockRows.push(rows[i]);
                    }
                    // i is now the start of the next block
                }
                // set data against table so we know it has been inited (for if we call it in the hover event)
                $(theTable).data('hasblockrows', 1);
            }
        }

        $(tableSelector).find("td").hover(
            function ()
            {
                $el = $(this);
                $.each($el.parent().data('blockrows'), function ()
                {
                    $(this).removeClass('no-hover').addClass('hover');
                    $(this).find('td').addClass('hover');
                });
            },
            function ()
            {
                $el = $(this);
                $.each($el.parent().data('blockrows'), function ()
                {
                    $(this).removeClass('hover').addClass('no-hover');
                    $(this).find('td').removeClass('hover');
                });
            });
        findBlocks(tableSelector);
    }
};

var EmailUtil = {

    validateEmailSafe: function (emailAddress)
    {

        //retorna false caso seja nulo ou vazio
        if (emailAddress == null || emailAddress == '' || typeof emailAddress == 'undefined')
        {
            return false;
        }
        //verifica se tem arroba, e se tem apenas um arroba
        var temUmAndApenasUmArroba = emailAddress.indexOf('@') > -1 && emailAddress.indexOf('@') == emailAddress.lastIndexOf('@');
        //retorna false caso n�o tenha arroba, ou tenha mais de um
        if (!temUmAndApenasUmArroba)
        {
            return false;
        }
        //separa a parte a esquerda e a direita do '@'
        var localPart = emailAddress.substring(0, emailAddress.indexOf('@'));
        var domain = emailAddress.substring(emailAddress.indexOf('@') + 1, emailAddress.length);
        //retorna false caso o comprimento de cada parte n�o esteja dentro do padr�o
        if (localPart.length < 1 || localPart.length > 64)
        {
            return false;
        }
        if (domain.length < 1 || domain.length > 255)
        {
            return false;
        }
        //retorna false caso n�o atenda as regras do 'localPart'
        for (var i = 0; i < localPart.length; i++)
        {
            if (i == 0 && localPart[i] == '.')
            {
                return false;
            }
            else
            {
                if (i == localPart.length - 1 && localPart[i] == '.')
                {
                    return false;
                }
                else
                {
                    if (i > 0 && localPart[i - 1] == '.' && localPart[i] == '.')
                    {
                        return false;
                    }
                    else
                    {
                        if (['!', '#', '$', '%', '&', '\'', '*', '+', '-', '/',
                             '=', '?', '^', '_', '`', '{', '|', '}', '~'].indexOf(localPart[i]) == -1
                            && !(  (localPart.charCodeAt(i) >= 48 && localPart.charCodeAt(i) <= 57)
                                || (localPart.charCodeAt(i) >= 65 && localPart.charCodeAt(i) <= 90)
                                || (localPart.charCodeAt(i) >= 97 && localPart.charCodeAt(i) <= 122)
                                || (localPart.charCodeAt(i) == 46)
                            )
                        )
                        {
                            return false;
                        }
                    }
                }
            }
        }
        //contorno: o d�minio para Internet deve conter pelo menos um '.'
        //demais regras presentes no 'for' abaixo
        if (domain.indexOf('.') == -1)
        {
            return false;
        }
        //retorna false caso n�o atenda as regras do 'domain'
        for (var i = 0; i < domain.length; i++)
        {
            if (i == 0 && domain[i] == '.')
            {
                return false;
            }
            else
            {
                if (i == domain.length - 1 && domain[i] == '.')
                {
                    return false;
                }
                else
                {
                    if (i == 0 && domain[i] == '-')
                    {
                        return false;
                    }
                    else
                    {
                        if (i == domain.length - 1 && domain[i] == '-')
                        {
                            return false;
                        }
                        else
                        {
                            if (i > 0 && domain[i - 1] == '.' && domain[i] == '.')
                            {
                                return false;
                            }
                            else
                            {
                                if (!((domain.charCodeAt(i) >= 48 && domain.charCodeAt(i) <= 57)
                                        || (domain.charCodeAt(i) >= 65 && domain.charCodeAt(i) <= 90)
                                        || (domain.charCodeAt(i) >= 97 && domain.charCodeAt(i) <= 122)
                                        || (domain.charCodeAt(i) == 46)
                                    )
                                )
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

};

var ApplicationUtil = {

    __rootScope: null,
    getRootScope: function()
    {
        return this.__rootScope
    },

    setRootScope: function(rootScope)
    {
        this.__rootScope = rootScope;
    },

    isModalView: function(controllerAs)
    {
        return controllerAs.isModalView === true;
    },

    runGeneratedTasks: function(generatedTasks)
    {
        if(GeneralUtil.isObject(generatedTasks) && typeof generatedTasks.initFunction == 'function')
        {
            generatedTasks.initFunction.call(generatedTasks.scope);
        }
    },

    clearModalMessages: function(vm)
    {
        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };
    },

    showSuccessMessage: function(message, title, vm, options)
    {
        var me = this;
        if(GeneralUtil.isObject(vm)
            && (typeof vm.messages !== 'undefined')
            && (typeof vm.messages.success !== 'undefined')
            && ApplicationUtil.isModalView(vm))
        {
            me.clearModalMessages(vm);
            vm.messages.success.push(
            (typeof title !== 'undefined') && !GeneralUtil.isNullOrEmpty(title) ? title + ": " + message : message);

        }
        else if(!GeneralUtil.isNullOrEmpty(this.__rootScope))
        {
            options = options || ApplicationUtil.getDefaultSuccessMessageDialogOption();
            return this.__rootScope.showNotifyMessage(message, title, options);
        }
    },

    showWarningMessage: function(message, title, vm, options)
    {
        var me = this;
        if(GeneralUtil.isObject(vm)
            && (typeof vm.messages !== 'undefined')
            && (typeof vm.messages.warning !== 'undefined')
            && ApplicationUtil.isModalView(vm))
        {
            me.clearModalMessages(vm);
            vm.messages.warning.push((typeof title !== 'undefined') && !GeneralUtil.isNullOrEmpty(title) ? title + ": " + message : message);

        }
        else if(!GeneralUtil.isNullOrEmpty(this.__rootScope))
        {
            options = options || ApplicationUtil.getDefaultSuccessMessageDialogOption();
            return this.__rootScope.showNotifyMessage(message, title, options);
        }
    },

    showInfoMessage: function(message, title, vm, options)
    {
        var me = this;
        if(GeneralUtil.isObject(vm)
            && (typeof vm.messages !== 'undefined')
            && (typeof vm.messages.info !== 'undefined')
            && ApplicationUtil.isModalView(vm))
        {
            me.clearModalMessages(vm);
            vm.messages.info.push((typeof title !== 'undefined') && !GeneralUtil.isNullOrEmpty(title) ? title + ": " + message : message);

        }
        else if(!GeneralUtil.isNullOrEmpty(this.__rootScope))
        {
            options = options || ApplicationUtil.getDefaultInfoMessageDialogOption();
            return this.__rootScope.showNotifyMessage(message, title, options);
        }
    },

    showErrorMessage: function(message, title, vm, options)
    {
        var me = this;
        if(GeneralUtil.isObject(vm)
            && (typeof vm.messages !== 'undefined')
            && (typeof vm.messages.error !== 'undefined')&& ApplicationUtil.isModalView(vm))
        {
            me.clearModalMessages(vm);
            vm.messages.error.push(
            (typeof title !== 'undefined') && !GeneralUtil.isNullOrEmpty(title) ? title + ": " + message : message);

        }
        else if(!GeneralUtil.isNullOrEmpty(this.__rootScope))
        {
            return this.__rootScope.showErrorMessage(message, title, options);
        }
    },

    getCustomMessageDialogOption: function(keyboardClose, clickOnBackDropClose, size, animation)
    {
        keyboardClose = GeneralUtil.isBoolean(keyboardClose) ? keyboardClose : false;
        clickOnBackDropClose = GeneralUtil.isBoolean(clickOnBackDropClose) ? clickOnBackDropClose : true;
        animation = GeneralUtil.isBoolean(animation) ? animation : true;
        size = GeneralUtil.isString(size) ? size : 'lg';

        return {
            keyboard: keyboardClose,
            size: size,
            animation: animation,
            backdrop: clickOnBackDropClose
        };

    },

    getDefaultMessageDialogOption: function()
    {
        return {
            keyboard: false,
            size: 'lg',
            animation: true,
            backdrop: true
        };

    },

    getDefaultSuccessMessageDialogOption: function()
    {
        return {
            keyboard: false,
            size: 'lg',
            animation: true,
            backdrop: true,
            windowClass: 'success-message'
        };

    },

    getDefaultWarningMessageDialogOption: function()
    {
        return {
            keyboard: false,
            size: 'lg',
            animation: true,
            backdrop: true,
            windowClass: 'warning-message'
        };

    },

    getDefaultInfoMessageDialogOption: function()
    {
        return {
            keyboard: false,
            size: 'lg',
            animation: true,
            backdrop: true,
            windowClass: 'info-message'
        };

    },

    detectDeviceType: function()
    {
        var isPhone = false;
        var isTablet = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) isPhone = true;})(navigator.userAgent||navigator.vendor||window.opera);

        if(!isPhone)
        {
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) isTablet = true;})(navigator.userAgent||navigator.vendor||window.opera);
        }

        return (isPhone ? Constants.DeviceType.PHONE : (isTablet ? Constants.DeviceType.TABLET : Constants.DeviceType.COMPUTER));

    },

    detectDeviceTypeAndAddCssClass: function()
    {
        var me = this;
        var deviceType = me.detectDeviceType();
        var cssClass = "";

        switch(deviceType)
        {
            case Constants.DeviceType.PHONE:
                cssClass = "x-mobile";
                break;

            case Constants.DeviceType.TABLET:
                cssClass = "x-tablet";
                break;

            case Constants.DeviceType.COMPUTER:
            default:
                cssClass = "x-computer";
                break;
        }

        if(!$('body').hasClass(cssClass))
        {
            $('body').addClass(cssClass);
        }
    },

    autoExpandMenuItens: function()
    {
        var me = this;
        var queryStringObj = GeneralUtil.unserializeCurrentQueryString();

        if(queryStringObj.tipo && queryStringObj.page)
        {
            $link = $('#main-nav a.menu-page[menu-link-page-type="' + queryStringObj.tipo + '"][menu-link-page-name="' + queryStringObj.page + '"]');
            $link.addClass('in menu-item-active');
            $link.parents('ul.nav').addClass('in');
        }

        if(me.detectDeviceType() == Constants.DeviceType.PHONE)
        {
            var $linksToExpand = $('#main-nav li > a.force-expand');
            var $listsToExpand = $('#main-nav li > ul.force-expand');

            if($linksToExpand.length > 0)
            {
                $linksToExpand.addClass('in');
                $linksToExpand.parents('ul.nav').addClass('in');
            }

            if($listsToExpand.length > 0)
            {
                $listsToExpand.addClass('in');
            }
        }
    },

    addParameterToQueryString: function(paramKey, paramValue)
    {
        if(!GeneralUtil.isNullOrEmpty(paramKey))
        {
            var newQueryString = "";
            var newBrowserBaseLocation = document.location.protocol + '//' + document.location.host + document.location.pathname;
            var currentQueryStringObject = GeneralUtil.unserializeCurrentQueryString();

            currentQueryStringObject[paramKey] = paramValue;

            var newQueryString = $.param(currentQueryStringObject);
            window.history.replaceState(null, null, newBrowserBaseLocation + "?" + newQueryString);
        }
    },

    setCookie: function(cookieKey, cookieValue)
    {

    }

};
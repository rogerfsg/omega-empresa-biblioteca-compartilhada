omegaApp.directive('uiGenericPhoneNumber', [

    function ()
    {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ctrl)
            {
                function parser(text)
                {
                    if (ctrl.$isEmpty(text))
                    {
                        return text;
                    }

                    var transformedInput = text.replace(/[^0-9(*)(+)(#)]/g, '');
                    if (transformedInput !== text)
                    {
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    return transformedInput;

                }

                ctrl.$parsers.push(parser);
            }
        };
    }
]);
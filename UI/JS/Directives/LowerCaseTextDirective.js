omegaApp.directive('uiLowercaseInput', [

    function ()
    {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl)
            {
                function parser(value)
                {
                    if (ctrl.$isEmpty(value))
                    {
                        return value;
                    }
                    var formattedValue = value.toLowerCase();
                    if (ctrl.$viewValue !== formattedValue)
                    {
                        ctrl.$setViewValue(formattedValue);
                        ctrl.$render();
                    }
                    return formattedValue;
                }

                function formatter(value)
                {
                    if (ctrl.$isEmpty(value))
                    {
                        return value;
                    }
                    return value.toLowerCase();
                }

                ctrl.$formatters.push(formatter);
                ctrl.$parsers.push(parser);
            }
        };
    }
]);
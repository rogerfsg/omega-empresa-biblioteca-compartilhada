omegaApp.directive('omegaControlType', ['$interval', 'dateFilter', function($interval, dateFilter)
{
    function link(scope, element, attrs)
    {
        element.on('$destroy', function()
        {

        });

        if(typeof scope.vm.fieldsTypes === 'undefined')
        {
            scope.vm.fieldsTypes = {};
        }

        if(!GeneralUtil.isNullOrEmpty(attrs.name))
        {
            scope.vm.fieldsTypes[attrs.name] = attrs.omegaControlType;
        }

    };

    return {
        restrict: 'A',
        link: link
    };

}]);
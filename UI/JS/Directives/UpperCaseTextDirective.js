omegaApp.directive('uiUppercaseInput', [

    function ()
    {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl)
            {
                function parser(value)
                {
                    if (ctrl.$isEmpty(value))
                    {
                        return value;
                    }
                    var formatedValue = value.toUpperCase();
                    if (ctrl.$viewValue !== formatedValue)
                    {
                        ctrl.$setViewValue(formatedValue);
                        ctrl.$render();
                    }
                    return formatedValue;
                }

                function formatter(value)
                {
                    if (ctrl.$isEmpty(value))
                    {
                        return value;
                    }
                    return value.toUpperCase();
                }

                ctrl.$formatters.push(formatter);
                ctrl.$parsers.push(parser);
            }
        };
    }
]);
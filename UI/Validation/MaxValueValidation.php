<?php

class MaxValueValidation extends Validation
{

    protected $maxValue;
    public function __construct($maxValue, $message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('maxvalue', $message, true, $level, false);

        $this->arrControlAttributes['max'] = $maxValue;
        $this->arrMessageParameters[] = $maxValue;
        $this->maxValue = $maxValue;
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return $this->maxValue;
    }

    public static function getDefaultInstance()
    {
        $maxValue = func_get_args();
        return new MaxValueValidation($maxValue[0], "O valor m�ximo permitido � {0}");
    }

}
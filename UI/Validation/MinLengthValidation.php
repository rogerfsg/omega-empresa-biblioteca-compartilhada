<?php

class MinLengthValidation extends Validation
{

    private $minLength;
    protected static $arrControlAttributesCheck = array('minlength');

    public function __construct($minLength, $message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('minlength', $message, true, $level, false);

        $this->minLength = $minLength;
        $this->arrControlAttributes['minlength'] = $minLength;
        $this->arrMessageParameters[] = $minLength;
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return $this->minLength == 1 ? "{$this->minLength} caractere" : "{$this->minLength} caracteres" ;
    }

    public static function getDefaultInstance()
    {
        $minLength = func_get_args();
        return new MinLengthValidation($minLength[0], "O comprimento m�nimo � de {f0}");
    }

}
<?php

class MinValueValidation extends Validation
{

    protected $minValue;
    public function __construct($minValue, $message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('minvalue', $message, true, $level, false);

        $this->arrControlAttributes['min'] = $minValue;
        $this->arrMessageParameters[] = $minValue;
        $this->minValue = $minValue;
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return $this->minValue;
    }

    public static function getDefaultInstance()
    {
        $minValue = func_get_args();
        return new MinValueValidation($minValue[0], "O valor m�nimo permitido � {0}");
    }

}

?>
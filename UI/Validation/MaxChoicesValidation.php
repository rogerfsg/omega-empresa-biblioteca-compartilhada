<?php

class MaxChoicesValidation extends CustomValidation
{

    public function __construct($maxChoices, $message, $level = self::LEVEL_ERROR, $isAsync = false)
    {
        parent::__construct('maxchoices', $message, false, $level, $isAsync);

        $value = self::INPUTVALUE_MODEL_VALUE;
        $this->validationScopeFunctionCall = "validateMaxChoices({$maxChoices}, {$value});";
    }

}

?>
<?php

class CpfValidation extends Validation
{

    protected static $arrControlAttributesCheck = array('ui-br-cpf-mask');

    public function __construct($message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('cpf', $message, true, $level, false);
        $this->arrMessageParameters = array();
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return new CpfValidation(I18N::getExpression("CPF Inv�lido"));
    }

}
<?php

class CnpjValidation extends Validation
{

    protected static $arrControlAttributesCheck = array('ui-br-cnpj-mask');

    public function __construct($message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('cnpj', $message, true, $level, false);
        $this->arrMessageParameters = array();
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return new CnpjValidation("CNPJ Inv�lido");
    }

}
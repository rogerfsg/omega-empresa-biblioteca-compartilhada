<?php

abstract class Validation
{

    const INPUTVALUE_RAW_VALUE = "\$value";
    const INPUTVALUE_MODEL_VALUE = "\$modelValue";

    const LEVEL_ERROR = 0;
    const LEVEL_WARNING = 1;

    protected $validationName;
    protected $validationMessage;
    protected $validationLevel;
    protected $isAsync;
    protected $arrMessageParameters;
    protected $arrControlAttributes;
    protected $isHtml5Validation;

    protected static $arrControlAttributesCheck;

    public function __construct($name, $message, $isHtml5Validation = false, $level = self::LEVEL_ERROR, $isAsync = false)
    {
        $this->validationName = $name;
        $this->isHtml5Validation = $isHtml5Validation;
        $this->validationMessage = $message;
        $this->validationLevel = $level;
        $this->isAsync = $isAsync;
    }

    public static function getArrControlAttributesCheck()
    {
        return static::$arrControlAttributesCheck;
    }

    public function getValidationAttribute()
    {
        return $this->isAsync ? "ui-validate" : "ui-validate-async";
    }

    /**
     * @return mixed
     */
    public function getValidationName()
    {
        return $this->validationName;
    }

    public function getArrControlAttributes()
    {
        return $this->arrControlAttributes;
    }

    public function addMessageParameters()
    {
        $parameters = func_get_args();
        foreach($parameters as $parameter)
        {
            $this->arrMessageParameters[] = $parameter;
        }
    }

    public function addMessageParameter($parameter)
    {
        $this->arrMessageParameters[] = $parameter;
    }

    /**
     * @return mixed
     */
    public function getValidationMessage()
    {
        $arrParameters = $this->arrMessageParameters;
        $expression = $this->validationMessage;

        if($arrParameters !== false && is_array($arrParameters))
        {

            for ($i = 0; substr_count($expression, "{{$i}}") > 0; $i++)
            {

                if (array_key_exists($i, $arrParameters))
                {
                    $value = $arrParameters[$i];
                }
                else
                {
                    $value = "";
                }

                $expression = str_replace("{{$i}}", $value, $expression);

            }

            for ($i = 0; substr_count($expression, "{f{$i}}") > 0; $i++)
            {

                if (array_key_exists($i, $arrParameters))
                {
                    $value = $this->getFormattedValue($arrParameters[$i]);
                }
                else
                {
                    $value = "";
                }

                $expression = str_replace("{{$i}}", $value, $expression);

            }

        }

        return $expression;

    }

    /**
     * @return int
     */
    public function getValidationLevel()
    {
        return $this->validationLevel;
    }

    /**
     * @return null
     */
    public abstract function getValidationScopeFunctionCall();

    public abstract function getFormattedValue();

    public abstract static function getDefaultInstance();

}
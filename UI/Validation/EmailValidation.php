<?php

class EmailValidation extends Validation
{
    protected $minValue;
    public function __construct($message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('email', $message, false, $level, false);
        $this->arrControlAttributes['ng-pattern'] = "/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/";
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return new EmailValidation("E-mail inv�lido");
    }

}

?>
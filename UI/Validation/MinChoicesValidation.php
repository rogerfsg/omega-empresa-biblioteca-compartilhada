<?php

class MinChoicesValidation extends CustomValidation
{

    public function __construct($minChoices, $message, $level = self::LEVEL_ERROR, $isAsync = false)
    {
        parent::__construct('minchoices', $message, false, $level, $isAsync);

        $value = self::INPUTVALUE_MODEL_VALUE;
        $this->validationScopeFunctionCall = "validateMinChoices({$minChoices}, {$value});";
    }

}

?>
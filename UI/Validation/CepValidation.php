<?php

class CepValidation extends Validation
{

    protected static $arrControlAttributesCheck = array('ui-br-cep-mask');

    public function __construct($message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('cep', $message, true, $level, false);
        $this->arrMessageParameters = array();
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return new CepValidation("CEP Inv�lido");
    }

}
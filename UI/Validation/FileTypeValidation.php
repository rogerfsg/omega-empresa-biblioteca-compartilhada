<?php

class FileTypeValidation extends Validation
{

    protected static $arrControlAttributesCheck = array('ngf-pattern');

    public function __construct($patternOrArray, $message, $level = self::LEVEL_ERROR, $isAsync = false)
    {
        parent::__construct('pattern', $message, true, $level, false);

        if(is_array($patternOrArray))
        {
            $pattern = implode(",", $patternOrArray);
        }
        else
        {
            $pattern = $patternOrArray;
        }

        $this->arrControlAttributes['ngf-pattern'] = $pattern;
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        $pattern = func_get_args();
        return new FileTypeValidation($pattern[0], I18N::getExpression("O tipo de arquivo � inv�lido"));
    }

}
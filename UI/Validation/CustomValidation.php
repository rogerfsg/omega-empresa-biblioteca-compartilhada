<?php

class CustomValidation extends Validation
{

    protected $validationScopeFunctionCall;

    public function __construct($name, $message, $scopeFunctionCall = null, $level = self::LEVEL_ERROR, $isAsync = false)
    {
        parent::__construct($name, $message, false, $level, $isAsync);

        $this->validationScopeFunctionCall = $scopeFunctionCall;
    }

    public function getValidationScopeFunctionCall()
    {
        return $this->validationScopeFunctionCall;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return null;
    }

}
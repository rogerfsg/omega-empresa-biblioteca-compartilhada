<?php

class MaxLengthValidation extends Validation
{

    private $maxLength;
    protected static $arrControlAttributesCheck = array('maxlength');

    public function __construct($maxLength, $message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('maxlength', $message, true, $level, false);

        $this->maxLength = $maxLength;
        $this->arrControlAttributes['maxlength'] = $maxLength;
        $this->arrMessageParameters[] = $maxLength;
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return $this->maxLength == 1 ? "{$this->maxLength} caractere" : "{$this->maxLength} caracteres" ;
    }

    public static function getDefaultInstance()
    {
        $maxLength = func_get_args();
        return new MaxLengthValidation($maxLength[0], "O comprimento m�ximo � de {f0}");
    }

}
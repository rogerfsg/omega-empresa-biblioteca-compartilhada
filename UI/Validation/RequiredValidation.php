<?php

class RequiredValidation extends Validation
{

    private $maxLength;
    protected static $arrControlAttributesCheck = array('ng-required');

    public function __construct($message, $level = self::LEVEL_ERROR)
    {
        parent::__construct('required', $message, true, $level, false);
    }

    public function getValidationScopeFunctionCall()
    {
        return null;
    }

    public function getFormattedValue()
    {
        return null;
    }

    public static function getDefaultInstance()
    {
        return new RequiredValidation("O campo � de preenchimento obrigat�rio.");
    }

}
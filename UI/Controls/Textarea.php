<?php

class Textarea extends FormControl
{

    const VALIDATION_MINLENGHT = "minlength";
    const VALIDATION_MAXLENGHT = "maxlength";

    /**
     * @var PropsFilter
     */
    protected $minLength;
    protected $maxLength;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->mainTag = "textarea";
    }

    /**
     * @param PropsFilter $minLength
     */
    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;
    }

    /**
     * @param mixed $maxLength
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    public function getAttributesString()
    {
        $attributesString = "";
        $attributesString .= parent::getAttributesString();

        if($this->minLength)
        {
            $attributesString .= "minlength=\"{$this->minLength}\" ";
        }

        if($this->maxLength)
        {
            $attributesString .= "maxlength=\"{$this->maxLength}\" ";
        }

        return $attributesString;

    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString} ></{$this->mainTag}>";

        return $returnString;

    }

}

?>
<?php

class SwitchControl extends FormControl
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);

        $this->mainTag = "input";
        $this->arrAttributes["bs-switch"] = null;
        $this->arrAttributes["type"] = "checkbox";
    }

    private function setSwitchAttribute($attributeKey, $attributeValue, $isAngularVariableOrFunction)
    {
        $this->arrAttributes[$attributeKey] = !$isAngularVariableOrFunction ? "{$attributeValue}" : "{{ {$this->getAngularControllerAsDotIfExists()}{$attributeValue} }}";
    }

    public function setOnText($onText, $isAngularVariable=false)
    {
        $this->setSwitchAttribute("switch-on-text", $onText, $isAngularVariable);
    }

    public function setOffText($offText, $isAngularVariable=false)
    {
        $this->setSwitchAttribute("switch-off-text", $offText, $isAngularVariable);
    }

    public function setTrueValue($trueValue, $isAngularVariable=false)
    {
        if(is_bool($trueValue))
        {
            $trueValue = ($trueValue ? "true" : "false");
        }
        elseif(!$isAngularVariable)
        {
            $trueValue = "'{$trueValue}'";
        }
        $this->setSwitchAttribute("ng-true-value", $trueValue, $isAngularVariable);
    }

    public function setFalseValue($falseValue, $isAngularVariable=false)
    {
        if(is_bool($falseValue))
        {
            $falseValue = ($falseValue ? "true" : "false");
        }
        elseif(!$isAngularVariable)
        {
            $falseValue = "'{$falseValue}'";
        }
        $this->setSwitchAttribute("ng-false-value", $falseValue, $isAngularVariable);
    }

    public function setIsReadOnly($isReadOnly, $isAngularVariable=false)
    {
        $this->setSwitchAttribute("switch-readonly", $isReadOnly, $isAngularVariable);
    }

    public function setIsActive($isActive, $isAngularVariable=false)
    {
        $this->setSwitchAttribute("switch-active", $isActive, $isAngularVariable);
    }

    public function setSwitchListener($angularListenerFunctionCall)
    {
        $this->setSwitchAttribute("switch-change", $angularListenerFunctionCall, true);
    }

    public function getAttributesString()
    {
        $attributesString = "";
        $attributesString .= parent::getAttributesString();

        return $attributesString;
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();

        $this->setCssClass("omega-switch");
        $this->setOnText("SIM", false);
        $this->setOffText("N�O", false);
        $this->setTrueValue(true, false);
        $this->setFalseValue(false, false);
    }

    public function renderValidationBlock()
    {
        return parent::renderValidationBlock();
    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString} />";

        return $returnString;

    }

}

?>
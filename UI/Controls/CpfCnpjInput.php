<?php

class CpfCnpjInput extends TextInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->arrAttributes["ui-br-cpfcnpj-mask"] = null;
    }

}

?>
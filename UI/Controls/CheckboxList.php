<?php

class CheckboxList extends FormControl
{
    public $arrayToIterate;
    public $repeatVariableName;
    public $internalContainerCssClass;
    public $itemContainerCssClass;
    public $itemLabelAngularClass;

    public $descriptionAttribute;
    public $descriptionExpression;

    public function __construct($entityName, $arrayToIterate, $repeatVariableName, $descriptionAttribute=null)
    {
        parent::__construct($entityName);

        $this->mainTag = "div";
        $this->repeatVariableName = $repeatVariableName;
        $this->arrayToIterate = $arrayToIterate;
        $this->descriptionAttribute = $descriptionAttribute;

        $this->arrAttributes["type"] = "checkbox";
    }

    public function renderLabel()
    {
        $returnString = "<label class='{$this->labelCssClass}'>{$this->label}</label>";
        return $returnString;
    }

    private function setCheckboxListAttribute($attributeKey, $attributeValue, $isAngularVariableOrFunction)
    {
        $this->arrAttributes[$attributeKey] = !$isAngularVariableOrFunction ? "{$attributeValue}" : "{$this->getAngularControllerAsDotIfExists()}{$attributeValue}";
    }

    public function setDefaultDescriptionExpression()
    {
        if(!is_null($this->descriptionAttribute))
        {
            $this->descriptionExpression = "{$this->repeatVariableName}.{$this->descriptionAttribute}";
        }
    }

    public function setDescriptionExpression($descriptionExpression)
    {
        $this->descriptionExpression = $descriptionExpression;
    }

    public function setItemLabelAngularClass($itemLabelAngularClass)
    {
        $this->itemLabelAngularClass = $itemLabelAngularClass;
    }

    public function setInternalContainerCssClass($internalContainerCssClass)
    {
        $this->internalContainerCssClass = $internalContainerCssClass;
    }

    public function setItemContainerCssClass($itemContainerCssClass)
    {
        $this->itemContainerCssClass = $itemContainerCssClass;
    }

    public function setChecklistValue($checkListValue)
    {
        $this->arrAttributes["checklist-value"] = $checkListValue;
    }

    public function setDefaultCheckListValue()
    {
        $this->arrAttributes["checklist-value"] = $this->repeatVariableName;
    }

    public function setChangeListener($angularListenerFunctionCall, $addControllerAsDotIfExists=true)
    {
        $this->setCheckboxListAttribute("ng-change", $angularListenerFunctionCall, $addControllerAsDotIfExists);
    }

    public function getAttributesString()
    {
        if($this->angularModel)
        {
            $this->addAttribute("checklist-model", $this->getAngularModel());
        }

        $attributesString = "";
        $attributesString .= parent::getAttributesString();

        return $attributesString;
    }

    public function setDefaultValues()
    {
        $this->name = "field{$this->getNormalizedEntityName(true)}";

        $this->setDefaultAngularModel();
        $this->setDefaultAngularDisabled();
        $this->setDefaultAngularRequired();

        $this->setCssClass("");
        $this->setDefaultCheckListValue();
        $this->setDefaultDescriptionExpression();
        $this->setItemContainerCssClass("col-sm-6");
        $this->setInternalContainerCssClass("checkbox-list-internal-container");
    }

    public function renderValidationBlock()
    {
        $this->validations = null;
        parent::renderValidationBlock();
    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();
        $descriptionExpressionString = !is_null($this->descriptionExpression) ? "{{ {$this->descriptionExpression} }}": "";

        $returnString .= "

            <div class=\"{$this->internalContainerCssClass}\">

                <{$this->mainTag} class=\"{$this->itemContainerCssClass}\" ng-repeat=\"{$this->repeatVariableName} in {$this->arrayToIterate}\">

                    <label ng-class=\"{$this->itemLabelAngularClass}\">
                        <input {$attributesString} />
                        {$descriptionExpressionString}
                    </label>
                
                </{$this->mainTag}>
                
            </div>";

        return $returnString;

    }

}

?>
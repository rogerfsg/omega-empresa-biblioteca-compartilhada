<?php

class CurrencyInput extends NumericInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->directive = "ui-money-mask";
    }

}

?>
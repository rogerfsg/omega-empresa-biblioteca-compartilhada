<?php

class TimeInput extends FormControl
{

    /**
     * @var PropsFilter
     */
    protected $hourStep;
    protected $minutesStep;
    protected $showMeridian;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->mainTag = "uib-timepicker";
    }

    public function setDefaultCssClass()
    {
        $this->cssClass = "uib-timepicker";
    }

    public function setDefaultLabelCssClass()
    {
        $this->labelCssClass = "col-md-2 control-label timepicker-label";
    }

    /**
     * @param PropsFilter $hourStep
     */
    public function setHourStep($hourStep)
    {
        $this->hourStep = $hourStep;
    }

    /**
     * @param mixed $minutesStep
     */
    public function setMinutesStep($minutesStep)
    {
        $this->minutesStep = $minutesStep;
    }

    /**
     * @param mixed $showMeridian
     */
    public function setShowMeridian($showMeridian)
    {
        $this->showMeridian = $showMeridian;
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();

        $this->setDefaultCssClass();
        $this->setDefaultLabelCssClass();

        $this->setHourStep("fieldsParameters.{$this->name}.timePickerParams.hourStep");
        $this->setMinutesStep("fieldsParameters.{$this->name}.timePickerParams.minuteStep");
        $this->setShowMeridian("fieldsParameters.{$this->name}.timePickerParams.showMeridian");

    }

    public function getAttributesString()
    {
        if($this->hourStep)
        {
            $this->addAttribute("hour-step", $this->hourStep);
        }

        if($this->minutesStep)
        {
            $this->addAttribute("minute-step", $this->minutesStep);
        }

        if($this->showMeridian)
        {
            $this->addAttribute("show-meridian", $this->showMeridian);
        }

        return parent::getAttributesString();

    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString}></{$this->mainTag}>";

        return $returnString;

    }

    public static function getFormattedValueForMySQL($modelValue)
    {
        $parsedDate = date_parse($modelValue);
        return "{$parsedDate['hour']}:{$parsedDate['minute']}-{$parsedDate['second']}";
    }

    public static function getFormattedValueForModel($value)
    {
        $date = date("Y-m-d", time());
        return "{$date}T{$value}.000Z";
    }

}

?>
<?php

abstract class FileUploadControl extends FormControl
{

    protected $dropAvailableVarOnScope;
    protected $keepDistinctVarOnScope;
    protected $allowMultipleFilesVarOnScope;
    protected $allowDirectoryVarOnScope;
    protected $acceptedTypesVarOnScope;
    protected $filePatternVarOnScope;
    protected $dragOverClassVarOnScope;
    protected $angularModelOptionsVarOnScope;
    protected $dropAvaiableVarOnScope;
    protected $reziseVarOnScope;
    protected $modelInvalidVarOnScope;
    protected $maxFileSizeVarOnScope;

    protected $defaultSpecialParametersPrefix;
    protected $uploadSuccessMessage;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->defaultOmegaValidationClasses = array('RequiredValidation', 'FileTypeValidation');
    }

    public function setKeepDistinctVarOnScope($keepDistinctVarOnScope)
    {
        $this->keepDistinctVarOnScope = $keepDistinctVarOnScope;
    }

    public function setAllowMultipleFilesVarOnScope($allowMultipleFilesVarOnScope)
    {
        $this->allowMultipleFilesVarOnScope = $allowMultipleFilesVarOnScope;
    }

    public function setAllowDirectoryVarOnScope($allowDirectoryVarOnScope)
    {
        $this->allowDirectoryVarOnScope = $allowDirectoryVarOnScope;
    }

    public function setAcceptedTypesVarOnScope($acceptedTypesVarOnScope)
    {
        $this->acceptedTypesVarOnScope = $acceptedTypesVarOnScope;
    }

    public function setFilePatternVarOnScope($filePatternVarOnScope)
    {
        $this->filePatternVarOnScope = $filePatternVarOnScope;
    }

    public function setDragOverClassVarOnScope($dragOverClassVarOnScope)
    {
        $this->dragOverClassVarOnScope = $dragOverClassVarOnScope;
    }

    public function setAngularModelOptionsVarOnScope($angularModelOptionsVarOnScope)
    {
        $this->angularModelOptionsVarOnScope = $angularModelOptionsVarOnScope;
    }

    public function setDropAvailableVarOnScope($dropAvailableVarOnScope)
    {
        $this->dropAvailableVarOnScope = $dropAvailableVarOnScope;
    }

    public function setReziseVarOnScope($reziseVarOnScope)
    {
        $this->reziseVarOnScope = $reziseVarOnScope;
    }

    public function setModelInvalidVarOnScope($modelInvalidVarOnScope)
    {
        $this->modelInvalidVarOnScope = $modelInvalidVarOnScope;
    }

    public function setUploadSuccessMessage($uploadSuccessMessage)
    {
        $this->uploadSuccessMessage = $uploadSuccessMessage;
    }

    public function setMaxFileSizeVarOnScope($maxUploadSiseVarOnScope)
    {
        $this->maxFileSizeVarOnScope = $maxUploadSiseVarOnScope;
    }

    protected abstract function getDefaultSpecialParametersPrefix();

    public function setDefaultKeepDistinctVarOnScope()
    {
        $this->keepDistinctVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.keepDistinct";
    }

    public function setDefaultAllowMultipleFilesVarOnScope()
    {
        $this->allowMultipleFilesVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.allowMultipleFiles";
    }

    public function setDefaultAllowDirectoryVarOnScope()
    {
        $this->allowDirectoryVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.allowDirectory";
    }

    public function setDefaultAcceptedTypesVarOnScope()
    {
        $this->acceptedTypesVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.acceptedTypes";
    }

    public function setDefaultFilePatternVarOnScope()
    {
        $this->filePatternVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.pattern";
    }

    public function setDefautDragOverClassVarOnScope()
    {
        $this->dragOverClassVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.dragOverClass";
    }

    public function setDefaultAngularModelOptionsVarOnScope()
    {
        $this->angularModelOptionsVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.modelOptionsObj";
    }

    public function setDefaultDropAvailableVarOnScope()
    {
        $this->dropAvailableVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.dropAvailable";
    }

    public function setDefaultReziseVarOnScope()
    {
        $this->reziseVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.resize";
    }

    public function setDefaultModelInvalidVarOnScope()
    {
        $this->modelInvalidVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.invalidFiles";
    }

    public function setDefaultUploadSuccessMessage()
    {
        $this->uploadSuccessMessage = I18N::getExpression("Upload realizado com sucesso.");
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();

        $this->setDefaultKeepDistinctVarOnScope();
        $this->setDefaultAllowMultipleFilesVarOnScope();
        $this->setDefaultAllowDirectoryVarOnScope();
        $this->setDefaultAcceptedTypesVarOnScope();
        $this->setDefaultFilePatternVarOnScope();
        $this->setDefaultModelInvalidVarOnScope();
        $this->setDefaultReziseVarOnScope();
        $this->setDefaultDropAvailableVarOnScope();
        $this->setDefaultAngularModelOptionsVarOnScope();
        $this->setDefaultUploadSuccessMessage();
    }

    public function getAttributesString()
    {
        $attributesString = "";
        if($this->keepDistinctVarOnScope)
        {
            $this->addAttribute("ngf-keep", "{$this->keepDistinctVarOnScope}");
        }

        if($this->allowMultipleFilesVarOnScope)
        {
            $this->addAttribute("ngf-multiple", $this->allowMultipleFilesVarOnScope);
        }

        if($this->allowDirectoryVarOnScope)
        {
            $this->addAttribute("ngf-allow-dir", $this->allowDirectoryVarOnScope);
        }

        if($this->acceptedTypesVarOnScope)
        {
            $this->addAttribute("ngf-accept", $this->acceptedTypesVarOnScope);
        }

        if($this->dragOverClassVarOnScope)
        {
            $this->addAttribute("ngf-drag-over-class", $this->dragOverClassVarOnScope);
        }

        if($this->dragOverClassVarOnScope)
        {
            $this->addAttribute("ngf-drag-over-class", $this->dragOverClassVarOnScope);
        }

        if($this->fileTypePatternVarOnScope)
        {

        }

        $attributesString .= parent::getAttributesString();
        return $attributesString;
    }

    public function renderProgressPanel()
    {
        $returnString = "<div class=\"upload-progress progress\" ng-show=\"vm.isUploading\">
                            <div class=\"progress-bar progress-bar-primary\" style=\"width:{{vm.uploadPercentage}};\">{{vm.uploadPercentage}}</div>
                        </div>";

        return $returnString;
    }

}

?>
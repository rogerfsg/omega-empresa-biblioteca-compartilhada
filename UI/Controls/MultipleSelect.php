<?php

class MultipleSelect extends Select
{

    const TYPED_QUERY = "\$select.search";
    protected $MATCH_EXPRESSION_DEFAULT_PREFIX = "\$item";

    const VALIDATION_MINCHOICES = 'minChoices';
    const VALIDATION_MAXCHOICES = 'maxChoices';

    /**
     * @var PropsFilter
     */

    public function __construct($entityName, $scopeVarWithData=null, $scopeRefreshFunctionCall=null)
    {
        parent::__construct($entityName);
        $this->addAttribute("multiple", "true");
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();
        $this->setCloseOnSelect(false);
    }

}

?>
<?php

class NumericInput extends FormControl
{

    const VALIDATION_MINVALUE = "min";
    const VALIDATION_MAXVALUE = "max";
    const DEFAULT_DECIMAL_PLACES = 2;

    /**
     * @var PropsFilter
     */
    protected $minValue;
    protected $maxValue;
    protected $allowNegative;
    protected $hideThousandsSeparator;
    protected $decimalPlaces;
    protected $directive;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->mainTag = "input";

        $this->addAttribute("type", "text");
        $this->directive = "ui-number-mask";
    }

    /**
     * @param PropsFilter $minValue
     */
    public function setMinValue($minValue)
    {
        $this->minValue = $minValue;
    }

    /**
     * @param mixed $maxValue
     */
    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;
    }

    /**
     * @param mixed $allowNegative
     */
    public function setAllowNegative($allowNegative)
    {
        $this->allowNegative = $allowNegative;
    }

    /**
     * @param mixed $hideThousandsSeparator
     */
    public function setHideThousandsSeparator($hideThousandsSeparator)
    {
        $this->hideThousandsSeparator = $hideThousandsSeparator;
    }

    /**
     * @param mixed $decimalPlaces
     */
    public function setDecimalPlaces($decimalPlaces)
    {
        $this->decimalPlaces = $decimalPlaces;
    }

    public function getAttributesString()
    {
        $returnString = "";

        if(!is_null($this->minValue))
        {
            $this->addAttribute("min", $this->minValue);
        }

        if(!is_null($this->maxValue))
        {
            $this->addAttribute("max", $this->maxValue);
        }

        if(!is_null($this->allowNegative) && $this->allowNegative)
        {
            $this->addAttribute("ui-negative-number", null);
        }

        if(!is_null($this->hideThousandsSeparator) && $this->hideThousandsSeparator)
        {
            $this->addAttribute("ui-hide-group-sep", null);
        }

        if(is_null($this->decimalPlaces) || $this->decimalPlaces == "" || !is_numeric($this->decimalPlaces))
        {
            $this->decimalPlaces = 0;
        }

        $this->addAttribute($this->directive, $this->decimalPlaces);

        $returnString .= parent::getAttributesString();
        return $returnString;

    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString} />";

        return $returnString;

    }

}

?>
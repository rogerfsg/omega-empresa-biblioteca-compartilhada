<?php

class PasswordInput extends TextInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);

        $this->mainTag = "input";
        $this->addAttribute("type", "password");

    }

}

?>
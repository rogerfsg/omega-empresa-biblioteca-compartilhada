<?php

class PercentageInput extends NumericInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->directive = "ui-percentage-mask";
    }

}

?>
<?php

class CepInput extends TextInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->arrAttributes["ui-br-cep-mask"] = null;
        $this->defaultOmegaValidationClasses[] = 'CepValidation';
    }

}

?>
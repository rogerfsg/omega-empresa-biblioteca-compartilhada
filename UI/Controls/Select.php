<?php

class Select extends FormControl
{
    const THEME_BOOTSTRAP = "bootstrap";
    const THEME_SELECT2 = "select2";
    const THEME_SELECTIZE = "selectize";

    const TYPED_QUERY = "\$select.search";
    protected $MATCH_EXPRESSION_DEFAULT_PREFIX = "\$select.selected";

    /**
     * @var PropsFilter
     */
    private $filter;
    private $matchExpression;
    private $selectChoiceTemplate;
    private $selectChoiceContent;
    protected $scopeVarWithData;
    protected $scopeRefreshFunctionCall;
    protected $refreshDelay;
    protected $closeOnSelect = false;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->mainTag = "ui-select";
    }

    public function setDefaultCssClass()
    {
        $this->cssClass = "";
    }

    /**
     * @param boolean $closeOnSelect
     */
    public function setCloseOnSelect($closeOnSelect)
    {
        $this->closeOnSelect = $closeOnSelect;
    }

    /**
     * @param PropsFilter $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param mixed $selectChoiceTemplate
     */
    public function setSelectChoiceTemplate($selectChoiceTemplate)
    {
        $this->selectChoiceTemplate = $selectChoiceTemplate;
    }

    /**
     * @param mixed $selectChoiceContent
     */
    public function setSelectChoiceContent($selectChoiceContent)
    {
        $this->selectChoiceContent = $selectChoiceContent;
    }

    /**
     * @param mixed $refreshDelay
     */
    public function setRefreshDelay($refreshDelay)
    {
        $this->refreshDelay = $refreshDelay;
    }

    /**
     * @param String $matchExpression
     */
    public function setMatchExpression($matchExpression)
    {
        $this->matchExpression = $matchExpression;
    }

    public function renderMatchExpression()
    {

        $returnString = "";
        if(!is_null($this->matchExpression))
        {
            preg_match_all('/{{(.*?)}}/', $this->matchExpression, $matches);
            $tokens = $matches[1];
            $replacementTokens = array();

            if(is_array($tokens))
            {
                foreach($tokens as $token){

                    if(strpos($token, "$") !== false){

                        $replacementTokens[] = $token;

                    }
                    else{

                        $replacementTokens[] = $this->MATCH_EXPRESSION_DEFAULT_PREFIX . ".{$token}";

                    }

                }

                $returnString = str_replace($tokens, $replacementTokens, $this->matchExpression);

            }

        }

        return $returnString;

    }

    public function setBaseDefaultValues()
    {
        parent::setDefaultValues();
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();
        $this->setDefaultCssClass();
        $this->setCloseOnSelect(true);
        $this->addAttribute("theme", static::THEME_SELECT2);

        $this->scopeVarWithData = "{$this->specialParametersVarOnScope}.field{$this->getNormalizedEntityName(true)}.remoteData";
        $this->refreshDelay = null;
        $this->scopeRefreshFunctionCall = null;
    }

    public function setDefaultValuesForRemoteDataWithRefresh()
    {
        static::setDefaultValues();

        $this->refreshDelay = 0;
        $this->scopeRefreshFunctionCall = "{$this->getAngularControllerAsDotIfExists()}getListOf{$this->getNormalizedEntityName(true)}()";
    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();
        $seletedMatchString = "<ui-select-match placeholder=\"{$this->placeHolder}\">{$this->renderMatchExpression()}</ui-select-match>";

        $selectedChoicesContent = $this->selectChoiceContent->render();

        $strPropsFilter = "";
        if(!is_null($this->filter))
        {
            $propsFilter = $this->filter->render();
            $strPropsFilter = "| {$propsFilter}";
        }

        $strRefreshDelay = !is_null($this->refreshDelay) ? "refresh-delay=\"{$this->refreshDelay}\"" : "";
        $strRefreshFunctionCall = !is_null($this->scopeRefreshFunctionCall) ? "refresh=\"{$this->scopeRefreshFunctionCall}\"" : "";

        $returnString .= "

            <{$this->mainTag} {$attributesString} append-to-body=\"false\">
                {$seletedMatchString}
                <ui-select-choices
                    repeat=\"{$this->entityName} in {$this->scopeVarWithData} {$strPropsFilter}\"
                    {$strRefreshDelay}
                    {$strRefreshFunctionCall}
                    >
                    {$selectedChoicesContent}
                </ui-select-choices>
            </{$this->mainTag}>";

        return $returnString;

    }

    public static function getSelectHighLightFilter()
    {
        return new AngularFilter(AngularFilterType::HIGHLIGHT, "\$select.search");
    }

}

?>
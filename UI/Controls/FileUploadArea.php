<?php

class FileUploadArea extends FileUploadControl
{

    protected $dropAvailableVarOnScope;
    protected $dragOverCssClassVarOnScope;


    public function __construct($entityName)
    {
        parent::__construct($entityName);

        $this->mainTag = "div";
        $this->arrAttributes['ngf-drop'] = null;
        $this->arrAttributes['ngf-select'] = null;
    }

    public function setDefaultCssClass()
    {
        $this->cssClass = "drop-box";
    }

    protected function getDefaultSpecialParametersPrefix()
    {
        if(is_null($this->defaultSpecialParametersPrefix))
        {
            $this->defaultSpecialParametersPrefix = "{$this->specialParametersVarOnScope}.field{$this->getNormalizedEntityName(true)}.uploadAreaParams";
        }

        return $this->defaultSpecialParametersPrefix;
    }

    public function setDropAvailableVarOnScope($dropAvailableVarOnScope)
    {
        $this->dropAvailableVarOnScope = $dropAvailableVarOnScope;
    }

    public function setDragOverCssClassVarOnScope($dragOverCssClassVarOnScope)
    {
        $this->dragOverCssClassVarOnScope = $dragOverCssClassVarOnScope;
    }

    public function setDefaultDropAvaiableVarOnScope()
    {
        $this->dropAvailableVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.dropAvailable";
    }

    public function setDefaultDragOverCssClassVarOnScope()
    {
        $this->dragOverCssClassVarOnScope = "{$this->getDefaultSpecialParametersPrefix()}.dragOverCssClass";
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();

        $this->setDefaultDropAvaiableVarOnScope();
        $this->setDefaultDragOverCssClassVarOnScope();
    }

    public function getAttributesString()
    {
        $attributesString = "";
        if($this->dropAvailableVarOnScope)
        {
            $this->addAttribute("ngf-drop-available", $this->dropAvailableVarOnScope);
        }

        if($this->dragOverCssClassVarOnScope)
        {
            $this->addAttribute("ngf-drag-over-class", $this->dragOverCssClassVarOnScope);
        }

        $this->addAttribute("ngf-pattern", "'image/jpeg'");

        $attributesString .= parent::getAttributesString();
        return $attributesString;
    }

    public function setPlaceHolder($placeHolder)
    {
        parent::setPlaceHolder($placeHolder, false);
    }

    public function renderFileList()
    {
        $returnString = "";
        $returnString .= "<div class='row upload-file-list' style=\"margin-top: 15px;\" ng-show=\"vm.formData.documentosUpload != null && vm.formData.documentosUpload.length > 0\">
                            <div class='col-sm-12'>
                                <div class='box bordered-box blue-border' style='margin-bottom:0;'>
                                    <div class='box-content box-no-padding'>
                                        <div class='responsive-table'>
                                            <div>
                                                <table class='table' style='margin-bottom:0;'>
                                                    <tbody>
                                                        <tr ng-repeat=\"f in vm.formData.documentosUpload\">
                                                            <td>{{f.name}}</td>
                                                            <td>{{f.fileUniqueId}}</td>
                                                            <td>
                                                                <div class='text-right'>
                                                                    <a class='btn btn-danger btn-xs' ng-click=\"vm.removeUploadedFile(f.fileUniqueId)\">
                                                                        <i class='icon-remove'></i>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>";

        return $returnString;


    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString} >

            </{$this->mainTag}>";

        $returnString .= "<div ngf-drop ngf-select ng-model=\"vm.files\" class=\"drop-box\"
                             ngf-drag-over-class=\"'dragover'\" ngf-multiple=\"true\" ngf-allow-dir=\"true\"
                             accept=\"image/*,application/pdf\"
                             ngf-pattern=\"'image/*,application/pdf'\">{$this->placeHolder}</div>
                        <div ngf-no-file-drop>Funcionalidade de arraste-e-solte n�o dispon�vel em seu navegador.</div>";

        return $returnString;

    }

}

?>
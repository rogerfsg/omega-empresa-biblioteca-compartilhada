<?php

class CnpjInput extends TextInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->arrAttributes["ui-br-cnpj-mask"] = null;
        $this->defaultOmegaValidationClasses[] = 'CnpjValidation';
    }

}

?>
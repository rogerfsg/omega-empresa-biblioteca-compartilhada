<?php

class SelectForBoolean extends Select
{
    public function __construct($entityName)
    {
        parent::__construct($entityName);
    }

    public function setDefaultValues()
    {
        parent::setBaseDefaultValues();

        $this->setDefaultCssClass();
        $this->setCloseOnSelect(true);
        $this->addAttribute("theme", static::THEME_SELECT2);

        $this->scopeVarWithData = "{$this->specialParametersVarOnScope}.field{$this->getNormalizedEntityName(true)}.options";
        $this->refreshDelay = null;
        $this->scopeRefreshFunctionCall = null;

        $selectChoiceContent = new SelectChoiceContent("{$this->entityName}");
        $selectChoiceContent->setMainString(new AngularString("displayValue", "div", Select::getSelectHighLightFilter()));
        $this->setSelectChoiceContent($selectChoiceContent);

        $this->setPlaceHolder("", false);
        $this->setMatchExpression("{{displayValue}}");
    }


    public function render()
    {
        return parent::render();

    }

}

?>
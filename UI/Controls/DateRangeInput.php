<?php

class DateRangeInput
{
    protected $dateFromField;
    protected $dateToField;

    public function __construct($dateFromField, $dateToField)
    {
        $this->dateFromField = $dateFromField;
        $this->dateToField = $dateToField;
        $this->setDefaultAngularBlur();
    }



    public function renderDateFromField()
    {
        return $this->dateFromField->render();
    }

    public function renderDateToField()
    {
        return $this->dateToField->render();
    }

    public function setFieldCommonAttributes()
    {

    }

}

?>
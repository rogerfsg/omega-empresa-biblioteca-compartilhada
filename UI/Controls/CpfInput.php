<?php

class CpfInput extends TextInput
{

    public function __construct($entityName)
    {
        parent::__construct($entityName);
        $this->arrAttributes["ui-br-cpf-mask"] = null;
        $this->defaultOmegaValidationClasses[] = 'CpfValidation';
    }

}

?>
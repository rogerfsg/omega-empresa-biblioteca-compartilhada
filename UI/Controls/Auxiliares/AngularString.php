<?php

class AngularString
{

    private $displayExpression;
    private $filterArray;
    private $prefixAlreadyApplied;
    private $tagName;
    private $tagClassName;

    public function __construct($displayExpression=null, $tagName=null)
    {
        $this->displayExpression = $displayExpression;
        $this->tagName = $tagName;

        $args = func_get_args();
        if(count($args) > 2)
        {
            for($i=2; $i < count($args); $i++)
            {
                if($args[$i] instanceof AngularFilter)
                {
                    $this->addFilter($args[$i]);
                }
            }

        }

    }

    public function setDisplayExpression($displayExpression)
    {
        $this->displayExpression = $displayExpression;
    }

    public function putPrefixOnDisplayExpressionVariables($prefix, $forcePrefixApplication=false)
    {

        if(!$this->prefixAlreadyApplied || $forcePrefixApplication)
        {
            preg_match_all('/{{(.*?)}}/', $this->matchExpression, $matches);
            $tokens = $matches[1];
            $replacementTokens = array();

            foreach ($tokens as $token)
            {
                $replacementTokens[] = "{$prefix}{$token}";
            }

            $this->displayExpression = str_replace($tokens, $replacementTokens, $this->displayExpression);
            $this->prefixAlreadyApplied = true;
        }

    }

    /**
     * @param AngularFilter $filterObj
     */
    public function addFilter($filterObj)
    {
        $this->filterArray[] = $filterObj;
    }

    /**
     * @param mixed $tagName
     */
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;
    }

    /**
     * @param mixed $tagClassName
     */
    public function setTagClassName($tagClassName)
    {
        $this->tagClassName = $tagClassName;
    }


    private function getFilterString()
    {
        $returnString = "";
        foreach($this->filterArray as $filter)
        {
            $returnString .= "| {$filter->filterName}: {$filter->filterExpression} ";
        }

        return $returnString;
    }

    public function getDisplayExpression()
    {
        return $this->displayExpression;
    }

    private function renderAsTag()
    {
        $class = $this->tagClassName ? "class=\"{$this->tagClassName}\"" : "";
        return "<{$this->tagName} {$class} ng-bind-html=\"{$this->displayExpression}\"></{$this->tagName}>";
    }

    public function render()
    {
        if($this->tagName)
        {
            return $this->renderAsTag();
        }
        else
        {
            return "{{{$this->displayExpression} {$this->getFilterString()}}}";
        }

    }

    public static function getAngularFilterString($filterArray)
    {

        $returnString = "";
        if(is_array($filterArray))
        {
            $obj = new self();
            foreach($filterArray as $filterObj)
            {
                $obj->addFilter($filterObj);
            }

            return $obj->getFilterString();

        }

        return $returnString;

    }

    public static function getStringOfPieces($arrPieces, $prefix=null)
    {

        $returnString = "";
        if(!is_array($arrPieces))
        {
            $arrPieces = array($arrPieces);
        }

        foreach($arrPieces as $piece)
        {
            if ($piece instanceof AngularString)
            {
                if(!is_null($prefix))
                    $piece->displayExpression = $prefix . $piece->displayExpression;

                $returnString .= $piece->render();
            }
            else
            {
                $returnString .= $piece;
            }

        }

        return $returnString;

    }

}
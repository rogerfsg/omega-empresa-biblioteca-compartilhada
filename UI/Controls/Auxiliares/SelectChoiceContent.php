<?php

class SelectChoiceContent
{

    private $repeatVariable;
    private $mainString;
    private $secondaryStringArray;

    public function __construct($repeatVariable)
    {
        $this->repeatVariable = $repeatVariable;
    }

    /**
     * @param String $mainString
     */
    public function setMainString()
    {
        $mainString = func_get_args();
        $this->mainString = $mainString;
    }

    /**
     * @param String[] $secondaryString
     */
    public function addSecondaryString()
    {
        $secondaryString = func_get_args();
        $this->secondaryStringArray[] = $secondaryString;
    }

    public function render()
    {
        $returnString = "";
        $returnString .= AngularString::getStringOfPieces($this->mainString, "{$this->repeatVariable}.");

        if(is_array($this->secondaryStringArray) && count($this->secondaryStringArray) > 0)
        {
            $returnString .= "<small>";

            $secondaryStringAsString = "";
            foreach($this->secondaryStringArray as $secondaryString)
            {
                $secondaryStringAsString[] = AngularString::getStringOfPieces($secondaryString, "{$this->repeatVariable}.");
            }

            $returnString .= implode("<br />", $secondaryStringAsString);
            $returnString .= "</small>";
        }

        return $returnString;
    }

}
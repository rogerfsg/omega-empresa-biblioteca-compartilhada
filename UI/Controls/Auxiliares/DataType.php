<?php

class DataType
{

    const TYPE_STRING = "string";
    const TYPE_INTEGER = "integer";
    const TYPE_FLOAT = "float";
    const TYPE_CURRENCY = "currency";
    const TYPE_DATE = "date";
    const TYPE_DATETIME = "datetime";
    const TYPE_TIME = "time";
    const TYPE_GENDER = "gender";
    const TYPE_BOOLEAN = "boolean";

    public static function getDataStruct($type, $value, $formattedValue=null)
    {
        $objRetorno = new stdClass();
        $objRetorno->t = $type;
        $objRetorno->v = $value;
        $objRetorno->fv = $formattedValue;

        return $objRetorno;
    }



}

?>
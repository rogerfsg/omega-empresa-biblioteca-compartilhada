<?php

class AngularFilter
{

    private $filterName;
    private $filterExpression;

    /**
     * @param String $filterName
     * @param String $filterExpression
     */
    public function __construct($filterName, $filterExpression)
    {
        $this->filterName = $filterName;
        $this->filterExpression = $filterExpression;
    }

    /**
     * @param String $filterName
     */
    public function setFilterName($filterName)
    {
        $this->filterName = $filterName;
    }

    /**
     * @param String $filterExpression
     */
    public function setFilterExpression($filterExpression)
    {
        $this->filterExpression = $filterExpression;
    }

}
<?php

class PropsFilter
{

    //tipos de filtro
    const FILTER_BY_TYPED_QUERY = Select::TYPED_QUERY;

    //campos a serem filtrados
    private $arrFields;
    private $defaultFilterType;

    public function __construct($changeDefaultFilterTypeTo=null)
    {
        if(is_null($changeDefaultFilterTypeTo))
            $this->defaultFilterType = self::FILTER_BY_TYPED_QUERY;
        else
            $this->defaultFilterType = $changeDefaultFilterTypeTo;

    }

    public function addFieldToFilter($fieldName, $filterType=null)
    {
        if(is_null($filterType))
            $filterType = $this->defaultFilterType;

        $this->arrFields[$fieldName] = $filterType;
    }

    public function render()
    {
        $returnString = "";
        foreach($this->arrFields as $fieldName => $filterType){

            $returnString .= "{$fieldName}: {$filterType},";

        }

        if(strlen($returnString) > 0){

            $returnString = AngularFilterType::PROPS_FILTER . ": {" . Helper::removerOsUltimosCaracteresDaString($returnString, 1) . "}";

        }

        return $returnString;

    }

}
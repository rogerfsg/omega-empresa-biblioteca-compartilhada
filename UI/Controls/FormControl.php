<?php

abstract class FormControl {

    protected $mainTag;
    protected $entityName;
    protected $arrAttributes;
    protected $angularModel;
    protected $angularDisabled;
    protected $angularRequired;
    protected $angularClass;
    protected $cssClass;
    protected $style;

    protected $name;
    protected $id;

    protected $dynamicNamePrefix;
    protected $dynamicIdPrefix;

    protected $modelVarOnScope;
    protected $disabledFieldsVarOnScope;
    protected $requiredFieldsVarOnScope;
    protected $specialParametersVarOnScope;

    protected $defaultOmegaValidationClasses;

    protected $formName;
    protected $angularControllerAs;

    protected $label;
    protected $labelCssClass;
    protected $containerCssClass;
    protected $formGroupCssClass;

    protected $validations;
    protected $placeHolder;

    private $renderPlaceHolderOnMainTag;
    private $autoAttachDefaultValidations;

    public function __construct($entityName)
    {
        $this->mainTag = "div";
        $this->entityName = $entityName;
        $this->arrAttributes = array();
        $this->defaultOmegaValidationClasses = array('RequiredValidation');
        $this->autoAttachDefaultValidations = true;
    }

    public function getNormalizedEntityName($upperFirstChar=false)
    {
        $entityName = $this->entityName;
        $entityName = str_replace("_", " ", $entityName);

        $entityName = str_replace(" ", "", ucwords($entityName));
        if(!$upperFirstChar) $entityName = lcfirst($entityName);

        return $entityName;

    }

    public function setDefaultValues()
    {
        $this->name = "field{$this->getNormalizedEntityName(true)}";
        $this->id = "field{$this->getNormalizedEntityName(true)}";

        $this->setDefaultAngularModel();
        $this->setDefaultAngularDisabled();
        $this->setDefaultAngularRequired();
    }

    public function setAutoAttachDefaultValidations($autoAttachDefaultValidations)
    {
        $this->autoAttachDefaultValidations = $autoAttachDefaultValidations;
    }

    public function addAttribute($key, $value = null)
    {
        $this->arrAttributes[$key] = $value;
    }

    public function setModelVarOnScope($modelVarOnScope)
    {
        $this->modelVarOnScope = $modelVarOnScope;
    }

    public function setDisabledFieldsVarOnScope($disabledFieldsVarOnScope)
    {
        $this->disabledFieldsVarOnScope = $disabledFieldsVarOnScope;
    }

    public function setRequiredFieldsVarOnScope($requiredFieldsVarOnScope)
    {
        $this->requiredFieldsVarOnScope = $requiredFieldsVarOnScope;
    }

    public function setSpecialParametersVarOnScope($specialParametersVarOnScope)
    {
        $this->specialParametersVarOnScope = $specialParametersVarOnScope;
    }

    public function setDefaultAngularModel()
    {
        $this->angularModel = "{$this->modelVarOnScope}.{$this->entityName}";
    }

    public function setDefaultAngularDisabled()
    {
        $this->angularDisabled = "{$this->disabledFieldsVarOnScope}.{$this->name}";
    }

    public function setDefaultAngularRequired()
    {
        $this->angularRequired = "{$this->requiredFieldsVarOnScope}.{$this->name}";
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $labelCssClass
     */
    public function setLabelCssClass($labelCssClass)
    {
        $this->labelCssClass = $labelCssClass;
    }

    public function getContainerCssClass()
    {
        return $this->containerCssClass;
    }

    public function setContainerCssClass($containerCssClass)
    {
        $this->containerCssClass = $containerCssClass;
    }

    public function getFormGroupCssClass()
    {
        return $this->formGroupCssClass;
    }

    public function setFormGroupCssClass($formGroupCssClass)
    {
        $this->formGroupCssClass = $formGroupCssClass;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDynamicNamePrefix()
    {
        return $this->dynamicNamePrefix;
    }

    public function setDynamicNamePrefix($dynamicNamePrefix)
    {
        $this->dynamicNamePrefix = $dynamicNamePrefix;
    }

    public function getDynamicIdPrefix()
    {
        return $this->dynamicIdPrefix;
    }

    public function setDynamicIdPrefix($dynamicIdPrefix)
    {
        $this->dynamicIdPrefix = $dynamicIdPrefix;
    }

    public function setDynamicNameAndIdPrefix($dynamicPrefix)
    {
        $this->dynamicNamePrefix = $dynamicPrefix;
        $this->dynamicIdPrefix = $dynamicPrefix;
    }

    /**
     * @param mixed $formName
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;
    }

    /**
     * @param mixed $angularControllerAs
     */
    public function setAngularControllerAs($angularControllerAs)
    {
        $this->angularControllerAs = $angularControllerAs;
    }


    public function getAngularControllerAsDotIfExists()
    {
        return $this->angularControllerAs ? "{$this->angularControllerAs}." : $this->angularControllerAs;
    }

    /**
     * @param Validation $placeHolder
     */
    public function addValidation($validation)
    {
        $this->validations[] = $validation;
    }

    /**
     * @param mixed $placeHolder
     */
    public function setPlaceHolder($placeHolder, $renderOnMainTag=true)
    {
        $this->placeHolder = $placeHolder;
        $this->renderPlaceHolderOnMainTag = $renderOnMainTag;
    }

    /**
     * @param mixed $angularModel
     */
    public function setAngularModel($angularModel)
    {
        $this->angularModel = $angularModel;
    }

    /**
     * @param mixed $angularDisabled
     */
    public function setAngularDisabled($angularDisabled)
    {
        $this->angularDisabled = $angularDisabled;
    }

    /**
     * @param mixed $angularRequired
     */
    public function setAngularRequired($angularRequired)
    {
        $this->angularRequired = $angularRequired;
    }

    /**
     * @param mixed $angularClass
     */
    public function setAngularClass($angularClass)
    {
        $this->angularClass = $angularClass;
    }

    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    public function setStyle($style)
    {
        $this->style = $style;
    }

    public function getAngularModel()
    {
        return $this->angularModel;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function getAttributesString()
    {
        $attributesString = "";
        if($this->angularModel)
        {
            $this->addAttribute("ng-model", $this->angularModel);
        }

        if($this->angularClass)
        {
            $this->addAttribute("ng-class", $this->angularClass);
        }

        if($this->cssClass)
        {
            $this->addAttribute("class", $this->cssClass);
        }

        if($this->angularDisabled)
        {
            $this->addAttribute("ng-disabled", $this->angularDisabled);
        }

        if($this->angularRequired)
        {
            $this->addAttribute("ng-required", $this->angularRequired);
        }

        if($this->name)
        {
            $name = $this->dynamicNamePrefix ? "{{{$this->dynamicNamePrefix}}}" . ucfirst($this->name) : $this->name;
            $this->addAttribute("name", $name);
        }

        if($this->id)
        {
            $id = $this->dynamicIdPrefix ? "{{{$this->dynamicIdPrefix}}}" . ucfirst($this->id) : $this->id;
            $this->addAttribute("id", $id);
        }

        if($this->style)
        {
            $this->addAttribute("style", $this->style);
        }

        if($this->placeHolder && $this->renderPlaceHolderOnMainTag)
        {
            $this->addAttribute("placeholder", $this->placeHolder);
        }

        $this->addAttribute("omega-control-type", get_class($this));

        if($this->autoAttachDefaultValidations)
        {
            //adiciona valida��es padr�es.
            $this->addDefaultValidations();
        }

        if(is_array($this->validations) && count($this->validations) > 0)
        {
            foreach($this->validations as $validation)
            {
                if($validation instanceof Validation)
                {
                    $arrControlAttributes = $validation->getArrControlAttributes();
                    if(is_array($arrControlAttributes) && count($arrControlAttributes) > 0)
                    {
                        foreach($arrControlAttributes as $validationAttr => $validationValue)
                        {
                            if(!isset($this->arrAttributes[$validationAttr]))
                            {
                                $this->arrAttributes[$validationAttr] = $validationValue;
                            }

                        }

                    }

                }

            }

            $arrDistinctAttributes = array();
            foreach($this->validations as $validation)
            {
                if($validation instanceof CustomValidation)
                {
                    $validationAttribute = $validation->getValidationAttribute();
                    $arrDistinctAttributes[$validationAttribute][] = $validation;
                }
            }

            foreach($arrDistinctAttributes as $attribute => $arrValidation)
            {
                $strValue = "";
                foreach($arrValidation as $validation)
                {
                    $strValue .= "{$validation->getValidationScopeFunctionCall()}, ";
                }

            }

        }

        foreach($this->arrAttributes as $attributeName => $attributeValue)
        {
            $attributesString .= is_null($attributeValue) ? "{$attributeName} " : "{$attributeName}=\"{$attributeValue}\" ";
        }

        return $attributesString;

    }

    private function addDefaultValidations()
    {
        $returnArray = array();
        foreach($this->defaultOmegaValidationClasses as $className)
        {
            for($i=0; $i < count($this->validations); $i++)
            {
                if(get_class($this->validations[$i]) == $className)
                {
                    continue 2;
                }
            }

            $arrAttributes = call_user_func(array($className, 'getArrControlAttributesCheck'));
            $returnArray[$className] = $arrAttributes;
        }

        foreach($returnArray as $validationClass => $arrAttributes)
        {
            $allAttributesArePresent = true;
            $arrAttributesValues = array();
            foreach($arrAttributes as $attribute)
            {
                if(!in_array($attribute, array_keys($this->arrAttributes)))
                {
                    $allAttributesArePresent = false;
                }
                else
                {
                    $arrAttributesValues[$attribute] = $this->arrAttributes[$attribute];
                }
            }

            if($allAttributesArePresent)
            {
                $validationInstance = call_user_func(array($validationClass, 'getDefaultInstance'), $arrAttributesValues);
                $this->addValidation($validationInstance);
            }

        }

    }

    public function getFormGroupAngularClass()
    {
        return "getFormControlCssClass({$this->formName}.{$this->name})";
    }

    public abstract function render();

    public function renderValidationBlock()
    {
        $returnString = "";
        $tempStringPieces = array();
        foreach($this->validations as $validation)
        {
            if($validation instanceof Validation)
            {
                $tempStringPieces[] = "{$validation->getValidationName()}: '{$validation->getValidationMessage()}'";
            }
        }

        if(count($this->validations))
        {
            $tempString = implode(", ", $tempStringPieces);
            $tempString = ", {{$tempString}}";

            $fieldExpression = "'{$this->name}'";
            if($this->dynamicNamePrefix)
            {
                $name = ucfirst($this->name);
                $fieldExpression = "{$this->dynamicNamePrefix} + '{$name}'";
            }

            $returnString = "\r\n<span class=\"help-block\" ng-show=\"showValidationMessage({$fieldExpression}, {$this->formName} {$tempString})\">
                                <p ng-repeat=\"messages in getValidationMessages({$fieldExpression}, {$this->formName})\">{{messages}}</p>
                             </span>";

        }

        if(Helper::isDebugModeActive())
        {
            $returnString .= "<pre class=\"debug-box\">Errors: {{{$this->formName}.{$this->name}.\$error | json}}</pre>";
            $returnString .= "<pre class=\"debug-box\">Model: {{{$this->getAngularModel()} | json}}</pre>";
        }

        return $returnString;
    }

    public function renderLabel()
    {
        $returnString = "<label class='{$this->labelCssClass}' for='{$this->name}'>{$this->label}</label>";
        return $returnString;
    }

    public function renderAddEntityButton($label, $cssClasses='btn btn-primary')
    {
        $normalizedEntityName = $this->getNormalizedEntityName(true);
        $returnString = "<button class='{$cssClasses}' onclick=\"javascript:void(0);\" ng-click=\"vm.open{$normalizedEntityName}Modal()\">{$label}</button>";

        return $returnString;
    }

    public static function getFormattedValueForMySQL($modelValue)
    {
        return $modelValue;
    }

    public static function getFormattedValueForModel($value)
    {
        return $value;
    }

}


?>
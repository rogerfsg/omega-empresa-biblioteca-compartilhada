<?php

class TextInput extends FormControl
{
    const TRANSFORMATION_DIRECTIVE_NONE = "none";
    const TRANSFORMATION_DIRECTIVE_UPPER = "upper";
    const TRANSFORMATION_DIRECTIVE_LOWER = "lower";

    protected $minLength;
    protected $maxLength;

    public function __construct($entityName, $transformationDirective = self::TRANSFORMATION_DIRECTIVE_NONE)
    {
        parent::__construct($entityName);

        $this->mainTag = "input";
        $this->addAttribute("type", "text");
        $this->defaultOmegaValidationClasses = array('RequiredValidation', 'MinLengthValidation', 'MaxLengthValidation');

        switch($transformationDirective)
        {
            case self::TRANSFORMATION_DIRECTIVE_UPPER:
                $this->addAttribute("ui-uppercase-input");
                break;

            case self::TRANSFORMATION_DIRECTIVE_LOWER:
                $this->addAttribute("ui-lowercase-input");
                break;

            case self::TRANSFORMATION_DIRECTIVE_NONE:
            default:
                break;

        }

    }

    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;
    }

    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    public function getAttributesString()
    {
        $attributesString = "";
        $attributesString .= parent::getAttributesString();

        if($this->minLength)
        {
            $attributesString .= "minlength=\"{$this->minLength}\" ";
        }

        if($this->maxLength)
        {
            $attributesString .= "maxlength=\"{$this->maxLength}\" ";
        }

        return $attributesString;

    }

    public function render()
    {
        $returnString = "";
        $attributesString = $this->getAttributesString();

        $returnString .= "

            <{$this->mainTag} {$attributesString} />";

        return $returnString;

    }

}

?>
<?php

class DateInput extends TextInput
{
    protected $currentDayText;
    protected $closeText;
    protected $clearText;
    protected $datePickerOptions;
    protected $isOpenVariable;
    protected $dateFormat;
    protected $inputGroupCssClass;
    protected $calendarButtonContainerCssClass;
    protected $calendarButtonCssClass;
    protected $calendarIconCssClass;

    public function __construct($entityName)
    {
        parent::__construct($entityName);
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();
        $this->setCurrentDayText("Hoje");
        $this->setCloseText("Fechar");
        $this->setClearText("Limpar");
        $this->setDateFormat("defaultDateFormat");
        $this->setIsOpenVariable($this->specialParametersVarOnScope . ".{$this->name}.isPopupOpened");
        $this->setDatePickerOptions($this->specialParametersVarOnScope . ".{$this->name}.datePickerParams");

        $this->setInputGroupCssClass("input-group");
        $this->setCalendarButtonContainerCssClass("input-group-btn");
        $this->setCalendarButtonCssClass("btn btn-default");
        $this->setCalendarIconCssClass("glyphicon glyphicon-calendar");
    }

    /**
     * @param mixed $currentDayText
     */
    public function setCurrentDayText($currentDayText)
    {
        $this->currentDayText = $currentDayText;
    }

    /**
     * @param mixed $closeText
     */
    public function setCloseText($closeText)
    {
        $this->closeText = $closeText;
    }

    /**
     * @param mixed $clearText
     */
    public function setClearText($clearText)
    {
        $this->clearText = $clearText;
    }

    /**
     * @param mixed $datePickerOptions
     */
    public function setDatePickerOptions($datePickerOptions)
    {
        $this->datePickerOptions = $datePickerOptions;
    }

    /**
     * @param mixed $isOpen
     */
    public function setIsOpenVariable($isOpen)
    {
        $this->isOpenVariable = $isOpen;
    }

    /**
     * @param mixed $dateFormat
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = "{{{$dateFormat}}}";
    }

    /**
     * @param mixed $inputGroupCssClass
     */
    public function setInputGroupCssClass($inputGroupCssClass)
    {
        $this->inputGroupCssClass = $inputGroupCssClass;
    }

    /**
     * @param mixed $calendarButtonContainerCssClass
     */
    public function setCalendarButtonContainerCssClass($calendarButtonContainerCssClass)
    {
        $this->calendarButtonContainerCssClass = $calendarButtonContainerCssClass;
    }

    /**
     * @param mixed $calendarButtonCssClass
     */
    public function setCalendarButtonCssClass($calendarButtonCssClass)
    {
        $this->calendarButtonCssClass = $calendarButtonCssClass;
    }

    /**
     * @param mixed $calendarIconCssClass
     */
    public function setCalendarIconCssClass($calendarIconCssClass)
    {
        $this->calendarIconCssClass = $calendarIconCssClass;
    }

    public function getAttributesString()
    {
        $attributesString = "";
        $attributesString .= parent::getAttributesString();

        if($this->isOpenVariable)
        {
            $attributesString .= "is-open=\"{$this->isOpenVariable}\" ";
        }

        if($this->datePickerOptions)
        {
            $attributesString .= "datepicker-options=\"{$this->datePickerOptions}\" ";
        }

        if($this->dateFormat)
        {
            $attributesString .= "uib-datepicker-popup=\"{$this->dateFormat}\" ";
        }

        if($this->currentDayText)
        {
            $attributesString .= "current-text=\"{$this->currentDayText}\" ";
        }

        if($this->closeText)
        {
            $attributesString .= "close-text=\"{$this->closeText}\" ";
        }

        if($this->clearText)
        {
            $attributesString .= "clear-text=\"{$this->clearText}\" ";
        }

        return $attributesString;

    }

    public function render()
    {
        $parentString = parent::render();

        $returnString = "
            <p class=\"{$this->inputGroupCssClass}\">
                {$parentString}
                <span class=\"{$this->calendarButtonContainerCssClass}\">
                    <button type=\"{$this->calendarButtonCssClass}\" class=\"btn btn-default\" ng-click=\"{$this->isOpenVariable} = true\"><i class=\"{$this->calendarIconCssClass}\"></i></button>
                </span>
            </p>";

        return $returnString;

    }

    public static function getFormattedValueForMySQL($modelValue)
    {
        $parsedDate = date_parse($modelValue);
        return "{$parsedDate['year']}-{$parsedDate['month']}-{$parsedDate['day']}";
    }

    public static function getFormattedValueForModel($value)
    {
        $formattedDate = "{$value}T00:00:00.000Z";
        return $formattedDate;
    }

}

?>
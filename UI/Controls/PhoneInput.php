<?php

class PhoneInput extends TextInput
{
    const LOCALE_GENERIC = "generic";
    const LOCALE_BRAZIL = "br";

    public function __construct($entityName, $localeMask = self::LOCALE_GENERIC)
    {
        parent::__construct($entityName);

        switch($localeMask)
        {
            case self::LOCALE_BRAZIL:
                $this->addAttribute("ui-br-phone-number");
                break;

            case self::LOCALE_GENERIC:
            default:
                $this->addAttribute("ui-generic-phone-number");
                break;
        }

    }

}

?>
<?

abstract class FlattyMenu extends InterfaceMenu
{

    const ICONE_PADRAO_MENU = 'icon-folder-open-alt';
    const ICONE_PADRAO_SUBMENU = 'icon-caret-right';
    const ICONE_PADRAO_ITEM_FORMULARIO = 'icon-edit';
    const ICONE_PADRAO_ITEM_LISTA = 'icon-table';
    const ICONE_PADRAO_ITEM_GENERICO = 'icon-list-ul';

    //Atributos
    private $arrMenu;
    private $crypt;
    private $usarCriptografia;
    public static $pathImagensMenu;

    //M�todos
    public function __construct($usarCriptografia = false)
    {
        $this->arrMenu = $this->getArrayDoMenuDoUsuarioLogado();
        $this->usarCriptografia = $usarCriptografia;
    }

    public function setMenuAdmin()
    {
        $this->arrMenu = $this->getArrayDoConteudoDoMenuCompleto();
    }

    protected function restringirMenu(&$arrayArea, &$arrayDePermissoes)
    {
        $itemCorrenteTemFilhoPermitido = false;
        foreach ($arrayArea as $chave => &$valor)
        {
            if ($chave == "config")
            {
                continue;
            }

            //se for estrutura recursiva
            if (is_array($valor))
            {
                $itemFilhoTemFilhoPermitido = $this->restringirMenu($valor, $arrayDePermissoes);
                if(!$itemFilhoTemFilhoPermitido)
                {
                    unset($arrayArea[ $chave ]);
                }
                else
                {
                    $itemCorrenteTemFilhoPermitido = true;
                }
            }
            //se for valor (objeto FlattyMenuItem)
            else
            {
                if (in_array($chave, $arrayDePermissoes))
                {
                    $itemCorrenteTemFilhoPermitido = true;
                }
                else
                {
                    unset($arrayArea[ $chave ]);
                }
            }
        }

        return $itemCorrenteTemFilhoPermitido;

    }

    public function criarMenu()
    {
        return $this->criarMenuArea(1);
    }

    public function imprimirMenu()
    {
        echo $this->criarMenu();
    }

    private function criarMenuArea($deep, $arr = null)
    {
        $strRetorno = "";
        if ($deep == 1)
        {
            $strRetorno .= "<ul class='nav nav-stacked'>
                                <li class='active'>
                                    <a href='index.php'>
                                        <i class='icon-bar-chart'></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>";

            $arr = $this->arrMenu;
        }

        foreach ($arr as $chaveArea => $paginaArea)
        {
            if ($chaveArea == "config")
            {
                $icone = $paginaArea->getIcone();
                $titulo = $paginaArea->getLabel();

                $areaLinkCssClass = $paginaArea->getForceExpand() ? "dropdown-collapse force-expand" : "dropdown-collapse";
                $areaListCssClass = $paginaArea->getForceExpand() ? "nav nav-stacked force-expand" : "nav nav-stacked";

                $strRetorno .= "<li>
                                    <a class='{$areaLinkCssClass}' href='javascript:void(0);'>
                                        <i class='{$icone}'></i>
                                        <span>{$titulo}</span>
                                        <i class='icon-angle-down angle-down'></i>
                                    </a>
                                    <ul class='{$areaListCssClass}'>";

            }
            else
            {
                if (is_array($paginaArea))
                {
                    $strRetorno .= $this->criarMenuArea($deep + 1, $paginaArea);
                }
                else
                {
                    $pageLink = $paginaArea->__toString();
                    $icone = $paginaArea->getIcone();
                    $titulo = $paginaArea->getLabel();
                    $atributosAdicionais = $paginaArea->getLinkAttributes();

                    $strRetorno .= "<li>
                                       <a class=\"menu-page\" href=\"{$pageLink}\" {$atributosAdicionais}>
                                          <i class='{$icone}'></i>
                                          <span>{$titulo}</span>
                                       </a>
                                    </li>";
                }

            }

        }

        if (isset($arr["config"]))
        {
            $strRetorno .= "</ul></li>";
        }

        if ($deep == 1)
        {
            $strRetorno .= "</ul>";
        }

        return $strRetorno;

    }

    private function returnPage($page)
    {
        if ($this->usarCriptografia)
        {
            return $this->crypt->crypt64($page);
        }
        else
        {
            return $page;
        }
    }

}

?>
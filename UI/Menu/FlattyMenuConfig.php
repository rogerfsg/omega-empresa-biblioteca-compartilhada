<?

class FlattyMenuConfig
{
    public $label;
    public $accesskey;
    public $link;
    public $icone;
    public $forceExpand;
    public static $contador;

    public function __construct($label = false, $icone = false, $forceExpand = false)
    {
        $this->label = $label;
        $this->forceExpand = $forceExpand;
        if ($icone === false)
        {
            $this->icone = FlattyMenu::ICONE_PADRAO_SUBMENU;
        }
        else
        {
            $this->icone = $icone;
        }
    }

    public function getIcone()
    {
        return $this->icone;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getForceExpand()
    {
        return $this->forceExpand;
    }
}

?>
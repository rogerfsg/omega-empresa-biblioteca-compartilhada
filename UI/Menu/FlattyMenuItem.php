<?

class FlattyMenuItem
{
    public $tipoOuURL;
    public $page;
    public $variaveis;
    public $label;
    public $icone;
    public $alt;
    public $isUrlExterna = false;

    public function __construct($label, $tipoOuURL, $page = null, $icone = false, $variaveis = null, $alt = true)
    {
        if ($alt === true)
        {
            $alt = $label;
        }

        $this->label = $label;
        $this->tipoOuURL = $tipoOuURL;
        $this->page = $page;
        $this->variaveis = $variaveis;
        $this->alt = $alt;

        if (substr_count($tipoOuURL, "://") > 0)
        {
            $this->isUrlExterna = true;
        }

        if ($icone === false)
        {
            switch ($tipoOuURL)
            {
                case "forms":
                    $this->icone = FlattyMenu::ICONE_PADRAO_ITEM_FORMULARIO;
                    break;

                case "lists":
                    $this->icone = FlattyMenu::ICONE_PADRAO_ITEM_LISTA;
                    break;

                default:
                    $this->icone = FlattyMenu::ICONE_PADRAO_ITEM_GENERICO;
                    break;
            }
        }
        else
        {
            $this->icone = $icone;
        }
    }

    public function getIcone()
    {
        return $this->icone;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function __toString()
    {
        return $this->isUrlExterna ? $this->tipoOuURL : "index.php?tipo={$this->tipoOuURL}&page={$this->page}&{$this->variaveis}";
    }

    public function getLinkAttributes()
    {
        if(!$this->isUrlExterna)
        {
            return "menu-link-page-type=\"{$this->tipoOuURL}\" menu-link-page-name=\"{$this->page}\"";
        }

    }
}

?>
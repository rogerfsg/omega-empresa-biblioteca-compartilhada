<?php

class I18N
{
    const TEMPO_EXPIRACAO_CACHE_EM_SEGUNDOS = 43200;  //12 horas
    const CHAVE_LINGUA_COOKIE_E_REDIS = 'idioma';
    const CHAVE_LINGUA_GET = 'idioma';

    public static $isoCode=false;

    static $lastIdIdioma;
    static $cacheObjLanguage;

    public static function loadLanguage($isoCode=false)
    {
        if(!static::$isoCode)
        {
            if($isoCode === false)
            {
                $isoCode = static::getSelectedLanguageIsoCode();
            }
            if(!$isoCode)
            {
                throw new Exception("ISO Code indefinido para montagem do objeto I18N");
            }

            if(!static::tryToLoadLanguageFromCache($isoCode))
            {
                //cria arquivo
                $pathToArquivo = static::getLanguageFilePath($isoCode);
                $arrExpressions = array();

                //o redis deve ter ao menos um elemento
                $arrExpressions["null"] = "null";
                if(is_file($pathToArquivo))
                {
                    $arrExpressions = array();

                    $handlerArquivo = fopen($pathToArquivo, 'r');
                    if($handlerArquivo){
                        while(!feof($handlerArquivo))
                        {
                            $linha = fgets($handlerArquivo);
                            if(preg_match('/"(.*)","(.*)"/', $linha, $matches))
                            {
                                $arrExpressions[$matches[1]] = $matches[2];
                            }
                        }
                    }


                }
                static::addLanguageToCache($isoCode, $arrExpressions);
            }
            static::$isoCode = $isoCode;
            return $isoCode;
        } else return null;

    }


    public static function getLanguageFilePath($isoCode)
    {
        return Helper::acharRaiz() . "recursos/languages/{$isoCode}.lang";
    }


    public static function addLanguageToCache($isoCode, $arrExpressions)
    {
        //seta a preferencia de language na sessao do usuario e no cookie relativo a sessao
        $s = HelperCookieRedis::getSingleton();
        $s->add(static::CHAVE_LINGUA_COOKIE_E_REDIS, $isoCode, static::TEMPO_EXPIRACAO_CACHE_EM_SEGUNDOS);

        $cache = HelperStaticAPCRedis::getSingleton();

        $cache->hmsetOnlyRedis("language_{$isoCode}",$arrExpressions, static::TEMPO_EXPIRACAO_CACHE_EM_SEGUNDOS );
    }


    public static function tryToLoadLanguageFromCache($isoCode)
    {
        $s = HelperCookieRedis::getSingleton();
        return $s->exists("language_{$isoCode}");
    }

    public static function getExpression($expressao)
    {
          //Nao podemos carregar mais no fluxo, tem que ser antes do header devido a utilizacao de setCookie no fluxo
          //if(!static::$isoCode)
          //{
          //    static::loadLanguage();
          //}
        try{

            $numArgs = func_num_args();
            $arrVariaveis = func_get_args();

            $cache = HelperStaticAPCRedis::getSingleton();
            $valor = $cache->hgetIfExists("language_".static::$isoCode, $expressao);
            $expressaoTraduzida = null;

            if($valor == null)
            {
                if(Helper::isDebugModeActive())
                {
                    $lingua = I18N::getSelectedLanguageIsoCode();
                    $pathToArquivo = static::getLanguageFilePath($lingua);
                    if(file_exists($pathToArquivo))
                    {
                        $handlerArquivo = fopen($pathToArquivo, 'a');
                        fwrite($handlerArquivo, "\"{$expressao}\",\"\"\n");
                        fclose($handlerArquivo);
                    }

                }
                $expressaoTraduzida =  $expressao;
            }
            else
            {
                $expressaoTraduzida = $valor;
            }

            if($numArgs > 1)
            {
                for ($i = 0; substr_count($expressaoTraduzida, "{{$i}}") > 0; $i++)
                {
                    if (array_key_exists($i+1, $arrVariaveis))
                    {
                        $valorVariavel = $arrVariaveis[$i+1];
                    }
                    else
                    {
                        $valorVariavel = "";
                    }

                    $expressaoTraduzida = str_replace("{{$i}}", $valorVariavel, $expressaoTraduzida);
                }

            }

            return $expressaoTraduzida;

        }catch(Exception $ex){
            HelperLog::logErro($ex, null, $expressao);
            return null;
        }
    }

    public function saveTranslatedTerms()
    {
        $arrIdiomasCopiadosComSucesso = array();
        foreach($_POST["expression"] as $codigoIso => $arrExpressions)
        {
            if(!is_null($codigoIso))
            {
                $pathArquivoIdioma = static::getLanguageFilePath($codigoIso);
                $pathArquivoTemporario = Helper::acharRaiz() . "temp/{$codigoIso}.lang." . rand(0, 1000) . "temp";
                $stringArquivo = "";

                foreach($arrExpressions as $chave => $valor)
                {
                    $stringArquivo .= "\"{$chave}\",\"{$valor}\"\n";
                }

                $tamanhoPrevistoConteudo = strlen($stringArquivo);

                $exception = false;
                try
                {
                    $numeroBytes = file_put_contents($pathArquivoTemporario, $stringArquivo, LOCK_EX);
                }
                catch(Exception $exception)
                {
                    $exception = true;
                }

                if($numeroBytes == $tamanhoPrevistoConteudo && !$exception)
                {
                    //copiar arquivo para o path definitivo
                    if(copy($pathArquivoTemporario, $pathArquivoIdioma))
                    {
                        $arrIdiomasCopiadosComSucesso[] = $codigoIso;
                    }

                }

            }

        }

        if(count($_POST["expressao"]) == count($arrIdiomasCopiadosComSucesso))
        {
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, self::getExpression("Todos os arquivos de idioma foram gravados com sucesso!"));
        }
        else
        {
            $arquivosIdiomaGravados = implode(", ", $arrIdiomasCopiadosComSucesso);
            if(!strlen($arquivosIdiomaGravados))
            {
                $arquivosIdiomaGravados = self::getExpression("Nenhum");
            }

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, self::getExpression("Os seguintes arquivos foram gravados com sucesso: {0}", $arquivosIdiomaGravados));

        }

    }

    private static function getSessionLanguageKeyForCookieAndRedis()
    {
        $identificadorSessaoPHP = session_id();
        return static::CHAVE_LINGUA_COOKIE_E_REDIS . "_" . $identificadorSessaoPHP;
    }

    public static function setLanguageToCookie($isoCode)
    {
        $expire = time() + static::TEMPO_EXPIRACAO_CACHE_EM_SEGUNDOS;
        setcookie ( static::getSessionLanguageKeyForCookieAndRedis() , $isoCode, $expire);
    }

    public function changeLanguage($codigoIdioma=null)
    {
        $codigoIdioma = !is_null($codigoIdioma) ? $codigoIdioma : Helper::GET(static::CHAVE_LINGUA_GET);
        if(!strlen($codigoIdioma))
        {
            $codigoIdioma = static::getBrowserLanguage();
        }

        if(strlen($codigoIdioma))
        {
            $helper = HelperCookieRedis::getSingleton();
            $helper->add(static::getSessionLanguageKeyForCookieAndRedis(), $codigoIdioma);
        }
    }

    public function changeLanguageAndRefreshPage($codigoIdioma=null)
    {
        self::changeLanguage($codigoIdioma);

        $urlRetorno = Helper::getUrlDeReferenciaSemVariaveisDeMensagem();
        return array("location: {$urlRetorno}");
    }

    public static function getAvaliableLanguages()
    {
        return array("pt", "en");
    }

    public static function getSelectedLanguageIsoCode()
    {
        $helper = HelperCookieRedis::getSingleton();
        if(!$helper->exists(static::getSessionLanguageKeyForCookieAndRedis()))
        {
            $i = static::getBrowserLanguage();
            $helper->add(static::getSessionLanguageKeyForCookieAndRedis(), $i);
            return $i;
        }
        else
        {
            return $helper->getIfExists(static::getSessionLanguageKeyForCookieAndRedis());
        }
    }

    public static function getSelectedLanguageId()
    {
        $isoCode = static::getSelectedLanguageIsoCode();
        if(isset(static::$cacheObjLanguage)) return static::$cacheObjLanguage;

        switch ($isoCode){

            case "en":
                $ret = new stdClass();
                $ret->nome = "English";
                $ret->codigo_iso = "en";
                $ret->imagem_ARQUIVO = "2_1.png";

                break;
            case "pt":
            default:
                $ret = new stdClass();
                $ret->nome = "Portugu&ecirc;s";
                $ret->codigo_iso = "pt";
                $ret->imagem_ARQUIVO = "1_1.png";
                break;
        }
        static::$cacheObjLanguage = $ret;
        return static::$cacheObjLanguage;
    }

    public static function getBrowserLanguage()
    {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $arrIdiomasAtivosSite = static::getAvaliableLanguages(true);

        if(in_array($lang, $arrIdiomasAtivosSite))
        {
            return $lang;
        }
        else
        {
            return 'pt';
        }
    }

    const DATA_SIMPLES = 0;
    const DATA_COMPLETA = 1;

    public static $months_pt = array(
        1 => "Janeiro",
        2 => "Fevereiro",
        3 => "Mar&accedil;o",
        4 => "Abril",
        5 => "Maio",
        6 => "Junho",
        7 => "Julho",
        8 => "Agosto",
        9 => "Setembro",
        10 => "Outubro",
        11 => "Novembro",
        12 => "Dezembro"
    );

    public static $months_en = array(
        1 => "January",
        2 => "February",
        3 => "March",
        4 => "April",
        5 => "Mai",
        6 => "June",
        7 => "July",
        8 => "August",
        9 => "September",
        10 => "October",
        11 => "Nobember",
        12 => "December"
    );

    public static function getMonthName($mesNumerico, $abreviacao=false, $isoCode=false)
    {
        if(!$isoCode)
        {
            $isoCode = static::getSelectedLanguageIsoCode();
        }

        $mesNumerico = $mesNumerico + 0;
        if($mesNumerico >= 1 || $mesNumerico <= 12)
        {
            $attrMes = "months_" . $isoCode;
            $arrMeses = static::$$attrMes;
            return $abreviacao? substr($arrMeses[$mesNumerico], 0, 3): $arrMeses[$mesNumerico];
        }

    }

    public static function getCurrentLanguageDateFormat($isoCode=false)
    {
        if(!$isoCode)
        {
            $isoCode = static::getSelectedLanguageIsoCode();
        }

        switch($isoCode)
        {
            case "en":
                $formato = "m/d/Y";
                break;
            case "pt":
                $formato = "d/m/Y";
                break;
            default:
                $formato = "d/m/Y";
                break;
        }

        return $formato;
    }

    public static function getCurrentLanguageDateTimeFormat($isoCode=false)
    {
        if(!$isoCode)
        {
            $isoCode = static::getSelectedLanguageIsoCode();
        }

        switch($isoCode)
        {
            case "en":
                $formato = "m/d/Y H:i:s";
                break;
            case "pt":
                $formato = "d/m/Y H:i:s";
                break;
            default:
                $formato = "d/m/Y H:i:s";
                break;
        }

        return $formato;
    }

    public static function getFormattedDate($timestamp, $isoCode=false)
    {
        $formato = static::getCurrentLanguageDateFormat($isoCode);
        return date($formato, $timestamp);
    }

    public static function getFormattedDateTime($timestamp, $isoCode=false)
    {
        $formato = static::getCurrentLanguageDateTimeFormat($isoCode);
        return date($formato, $timestamp);
    }

}


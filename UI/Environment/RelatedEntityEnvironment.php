<?php

class RelatedEntityEnvironment extends BaseFormEnvironment
{

    const DEFAULT_MODEL_ARRAY_VAR = "formData";
    const DEFAULT_DISABLED_FIELDS_VAR = "disabledFields";
    const DEFAULT_REQUIRED_FIELDS_VAR = "requiredFields";
    const DEFAULT_SPECIAL_PARAMETERS_VAR = "fieldsParameters";
    const DEFAULT_FORM_CONTROLS_CONTAINER_CSS_CLASS = "col-md-5";
    const DEFAULT_FORM_CONTROLS_LABEL_CSS_CLASS = "col-md-2 control-label";
    const DEFAULT_FORM_GROUP_CSS_CLASS = "form-group";
    const DEFAULT_FORM_CONTROLS_CSS_CLASS = "form-control";

    protected $baseVar;

    protected $modelArrayVar;
    protected $modelIteratorVar;

    protected $disabledFieldVar;
    protected $requiredFieldVar;
    protected $specialParameteresVar;
    protected $formControlsContainerCssClass;
    protected $formControlsLabelCssClass;
    protected $formControlsFormGroupCssClass;

    public function __construct($baseVar, $angularControllerName, $setDefaultValues=true)
    {
        $this->baseVar = $baseVar;
        parent::__construct($angularControllerName, null, $setDefaultValues);

        $this->setLayoutOneColumn();
    }

    public function getAngularRepeatExpression()
    {
        return "{$this->getModelIteratorVar()} in {$this->getModelArrayVar()}";
    }

    public function getBaseVarDotIfExists()
    {
        return $this->baseVar ? "{$this->baseVar}." : $this->baseVar;
    }

    public function setDefaultValues()
    {
        $this->modelArrayVar = $this->getBaseVarDotIfExists() . static::DEFAULT_MODEL_ARRAY_VAR;
        $this->disabledFieldVar = $this->getBaseVarDotIfExists() . static::DEFAULT_DISABLED_FIELDS_VAR;
        $this->requiredFieldVar = $this->getBaseVarDotIfExists() . static::DEFAULT_REQUIRED_FIELDS_VAR;
        $this->specialParameteresVar = $this->getBaseVarDotIfExists() . static::DEFAULT_SPECIAL_PARAMETERS_VAR;
    }

    public function setLayoutOneColumn()
    {
        $this->defaultHorizontalLineInterval = 1;
        $this->formControlsCssClass = static::DEFAULT_FORM_CONTROLS_CSS_CLASS;
        $this->formControlsContainerCssClass = static::DEFAULT_FORM_CONTROLS_CONTAINER_CSS_CLASS;
        $this->formControlsFormGroupCssClass = static::DEFAULT_FORM_GROUP_CSS_CLASS;
        $this->formControlsLabelCssClass = static::DEFAULT_FORM_CONTROLS_LABEL_CSS_CLASS;
    }

    public function setLayoutTwoColumns()
    {
        $this->defaultHorizontalLineInterval = 1000;
        $this->formControlsContainerCssClass = null;
        $this->formControlsFormGroupCssClass = "col-md-5 col-sm-offset-1";
        $this->formControlsLabelCssClass = "label-top";
        $this->formControlsCssClass = "form-control";
    }

    public function setFormName($formName)
    {
        $this->formName = $formName;
    }

    public function getBaseVar()
    {
        return $this->baseVar;
    }

    public function getModelArrayVar()
    {
        return $this->modelArrayVar;
    }

    public function setModelArrayVar($modelVarOnScope)
    {
        $this->modelArrayVar = $modelVarOnScope;
    }

    public function getModelIteratorVar()
    {
        return $this->modelIteratorVar;
    }

    public function setModelIteratorVar($modelIteratorVar)
    {
        $this->modelIteratorVar = $modelIteratorVar;
    }

    public function getDisabledFieldVar()
    {
        return $this->disabledFieldVar;
    }

    public function setDisabledFieldVar($disabledFieldVarOnScope)
    {
        $this->disabledFieldVar = $disabledFieldVarOnScope;
    }

    public function getRequiredFieldVar()
    {
        return $this->requiredFieldVar;
    }

    public function setRequiredFieldVar($requiredFieldVarOnScope)
    {
        $this->requiredFieldVarOnScope = $requiredFieldVarOnScope;
    }

    public function getSpecialParameteresVar()
    {
        return $this->specialParameteresVar;
    }

    public function setSpecialParameteresVar($specialParameteresVarOnScope)
    {
        $this->specialParameteresVar = $specialParameteresVarOnScope;
    }

    public function setFormControlsContainerCssClass($formControlsContainerCssClass)
    {
        $this->formControlsContainerCssClass = $formControlsContainerCssClass;
    }

    public function setFormGroupCssClass($formGroupCssClass)
    {
        $this->formGroupCssClass = $formGroupCssClass;
    }

    public function getFormDebugPanel()
    {
        $str = "";
        if(Helper::isDebugModeActive())
        {
            $str .= "<div class='row equal'>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Fields Types: {{ {$this->getAngularControllerAsDotIfExists()}fieldsTypes | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Disabled Fields: {{ {$this->disabledFieldVar} | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Required Fields: {{ {$this->requiredFieldVar} | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Form Model: {{ {$this->modelVar} | json }}</pre>";
            $str .= "</div>";
            $str .= "</div>";
        }

        return $str;

    }

    public function setFieldCommonAttributes(FormControl $objField)
    {
        $objField->setFormName($this->formName);
        $objField->setModelVarOnScope($this->modelIteratorVar);
        $objField->setAngularControllerAs($this->angularControllerAs);
        $objField->setDisabledFieldsVarOnScope($this->disabledFieldVar);
        $objField->setRequiredFieldsVarOnScope($this->requiredFieldVar);
        $objField->setSpecialParametersVarOnScope($this->specialParameteresVar);
        $objField->setContainerCssClass($this->formControlsContainerCssClass);
        $objField->setFormGroupCssClass($this->formControlsFormGroupCssClass);
        $objField->setLabelCssClass($this->formControlsLabelCssClass);
        $objField->setCssClass($this->formControlsCssClass);

        $objField->setDefaultAngularModel();
        $objField->setDefaultAngularDisabled();
        $objField->setDefaultAngularRequired();
        $objField->setDefaultValues();
    }

}

?>
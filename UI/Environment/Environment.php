<?php

abstract class Environment
{

    protected $angularControllerName;
    protected $angularControllerAs;

    public function __construct($angularControllerName, $angularControllerAs=null, $setDefaultValues=true)
    {
        $this->angularControllerName = $angularControllerName;
        $this->angularControllerAs = $angularControllerAs;

        if($setDefaultValues) $this->setDefaultValues();

    }

    public function setModalEnvironmentIfApplicable()
    {
        if($this->isModalView())
        {
            return "<div class=\"variable-container\" ng-init=\"{$this->getAngularControllerAsDotIfExists()}isModalView=true\"></div>";
        }
        else
        {
            return "";
        }
    }

    public function isModalView()
    {
        return Helper::getNomeDoScriptAtual() == "angularjs_modal.php";
    }

    public function renderCloseModalButtonIfExists()
    {
        if($this->isModalView())
        {
            $modalIndex = Helper::GET("modalIndex");
            return "<a class=\"btn box-remove btn-xs btn-link\" href=\"javascript:void(0);\" ng-click=\"closeLastModalInstance()\"><i class=\"icon-remove\"></i></a>";
        }
    }

    public function getModalMessagesPanel()
    {
        if($this->isModalView())
        {
            $tituloWarning = I18N::getExpression("Aten��o");
            $strWarning = "<div class=\"alert alert-warning alert-dismissable\" ng-if=\"{$this->getAngularControllerAs()}.messages.warning.length > 0\">
                            <a class=\"close\" href=\"#\" ng-click=\"{$this->getAngularControllerAs()}.messages.warning = []\">�</a>
                            <h4>
                                <i class=\"icon-exclamation-sign\"></i>
                                {$tituloWarning}
                            </h4>
                
                            <span ng-repeat=\"item in {$this->getAngularControllerAs()}.messages.warning track by \$index\">
                                {{ item }}
                            </span>
                        </div>";

            $tituloInfo = I18N::getExpression("Aviso");
            $strInfo = "<div class=\"alert alert-info alert-dismissable\" ng-if=\"{$this->getAngularControllerAs()}.messages.info.length > 0\">
                            <a class=\"close\" href=\"#\" ng-click=\"{$this->getAngularControllerAs()}.messages.info = []\">�</a>
                            <h4>
                                <i class=\"icon-info-sign\"></i>
                                {$tituloInfo}
                            </h4>
                
                            <span ng-repeat=\"item in {$this->getAngularControllerAs()}.messages.info track by \$index\">
                                {{ item }}
                            </span>
                        </div>";

            $tituloError = I18N::getExpression("Erro");
            $strError = "<div class=\"alert alert-danger alert-dismissable\" ng-if=\"{$this->getAngularControllerAs()}.messages.error.length > 0\">
                            <a class=\"close\" href=\"#\" ng-click=\"{$this->getAngularControllerAs()}.messages.error = []\">�</a>
                            <h4>
                                <i class=\"icon-remove-sign\"></i>
                                {$tituloError}
                            </h4>
                
                            <span ng-repeat=\"item in {$this->getAngularControllerAs()}.messages.error track by \$index\">
                                {{ item }}
                            </span>
                        </div>";

            $tituloSuccess = I18N::getExpression("Sucesso");
            $strSuccess = "<div class=\"alert alert-success alert-dismissable\" ng-if=\"{$this->getAngularControllerAs()}.messages.success.length > 0\">
                            <a class=\"close\" href=\"#\" ng-click=\"{$this->getAngularControllerAs()}.messages.success = []\">�</a>
                            <h4>
                                <i class=\"icon-ok-sign\"></i>
                                {$tituloSuccess}
                            </h4>
                
                            <span ng-repeat=\"item in {$this->getAngularControllerAs()}.messages.success track by \$index\">
                                {{ item }}
                            </span>
                        </div>";

            return $strWarning . "\r\n" . $strInfo . "\r\n" . $strError . "\r\n" . $strSuccess;

        }
    }

    public function getAngularControllerName()
    {
        return $this->angularControllerName;
    }

    public function getAngularControllerAs()
    {
        return $this->angularControllerAs;
    }

    public function cleanAngularControllerAs()
    {
        $this->angularControllerAs = "";
    }

    public function getAngularControllerAsDotIfExists()
    {
        return $this->angularControllerAs ? "{$this->angularControllerAs}." : $this->angularControllerAs;
    }

    public function getAngularControllerAttributeValue()
    {
        return is_null($this->angularControllerAs) ? "{$this->angularControllerName}" : "{$this->angularControllerName} as {$this->angularControllerAs}";
    }

    public abstract function setDefaultValues();

}

?>
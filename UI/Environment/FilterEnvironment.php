<?php

class FilterEnvironment extends FormEnvironment
{
    const DEFAULT_MODEL_VAR_ON_SCOPE = "filterData";
    const DEFAULT_DISABLED_FIELDS_VAR_ON_SCOPE = "disabledFilterFields";
    const DEFAULT_REQUIRED_FIELDS_VAR_ON_SCOPE = "requiredFilterFields";
    const DEFAULT_SPECIAL_PARAMETERS_VAR_ON_SCOPE = "filterFieldsParameters";
    const DEFAULT_FORM_CONTROLS_CONTAINER_CSS_CLASS = "col-sm-8 col-md-4";
    const DEFAULT_FORM_GROUP_CSS_CLASS = "col-sm-4 col-md-2 form-group";

    protected $angularControllerName;
    protected $angularControllerAs;

    public function __construct($formName, $angularControllerName, $angularControllerAs=null, $setDefaultValues=true)
    {
        parent::__construct($formName, $angularControllerName, $angularControllerAs, $setDefaultValues);
    }

    public function setDefaultValues()
    {
        parent::setDefaultValues();

        $this->submitButtonLabel = I18N::getExpression("Filtrar");
        $this->submitButtonCssClass = "btn btn-primary";
        $this->submitButtonIconCssClass = "icon-search";
        $this->submitButtonMethodCall = "filterDataSet({$this->angularControllerAs}, {$this->formName})";
    }

    public function defineAttributesForDateRangeFields($dateFromFieldInstance, $dateToFieldInstance)
    {
        $dateToFieldInstance->addAttribute("ng-blur", "setDateRangeMaxDate({$dateToFieldInstance->getAngularModel()}, {$this->specialParameteresVarOnScope}.{$dateFromFieldInstance->getName()})");
        $dateFromFieldInstance->addAttribute("ng-blur", "setDateRangeMinDate({$dateFromFieldInstance->getAngularModel()}, {$this->specialParameteresVarOnScope}.{$dateToFieldInstance->getName()})");
    }

}

?>
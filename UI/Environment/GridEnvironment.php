<?php

class GridEnvironment extends Environment
{

    private $filterEnvironment;

    protected $filterResultsText;
    protected $highlightResultsText;

    protected $recordPrimaryKeyAttribute;
    protected $recordAllowEditAttribute;
    protected $recordAllowDeleteAttribute;

    protected $paginationContainerClass;
    protected $paginationPreviousButtonDisabledVarOnScope;
    protected $paginationNextButtonDisabledVarOnScope;

    public function __construct($filterFormName, $angularControllerName, $angularControllerAs=null, $setDefaultValues=true)
    {
        parent::__construct($angularControllerName, $angularControllerAs, $setDefaultValues);

        $this->filterEnvironment = new FilterEnvironment($filterFormName, $angularControllerName, $angularControllerAs, $setDefaultValues);
    }

    public function getFilterEnvironment()
    {
        return $this->filterEnvironment;
    }

    public function setDefaultValues()
    {
        $this->filterResultsText = I18N::getExpression("Filtrar Resultados");
        $this->highlightResultsText = I18N::getExpression("Destacar Resultados");

        $this->recordAllowEditAttribute = "allowEdit";
        $this->recordAllowDeleteAttribute = "allowRemove";
        $this->recordPrimaryKeyAttribute = "primaryKey";
    }

    public function setFilterResultsText($filterResultsText)
    {
        $this->filterResultsText = $filterResultsText;
    }

    public function setHighlightResultsText($highlightResultsText)
    {
        $this->highlightResultsText = $highlightResultsText;
    }


    public function getRecordPrimaryKeyAttribute()
    {
        return $this->recordPrimaryKeyAttribute;
    }

    public function setRecordPrimaryKeyAttribute($recordPrimaryKeyAttribute)
    {
        $this->recordPrimaryKeyAttribute = $recordPrimaryKeyAttribute;
    }


    public function getRecordAllowEditAttribute()
    {
        return $this->recordAllowEditAttribute;
    }

    public function setRecordAllowEditAttribute($recordAllowEditAttribute)
    {
        $this->recordAllowEditAttribute = $recordAllowEditAttribute;
    }


    public function getRecordAllowDeleteAttribute()
    {
        return $this->recordAllowDeleteAttribute;
    }

    public function setRecordAllowDeleteAttribute($recordAllowDeleteAttribute)
    {
        $this->recordAllowDeleteAttribute = $recordAllowDeleteAttribute;
    }

    public function renderRecordCheckbox($recordVariable, $arrCheckboxAttributes=null)
    {
        $strAttributes = "";
        if(!is_null($arrCheckboxAttributes) && is_array($arrCheckboxAttributes))
        {
            foreach($arrCheckboxAttributes as $chave => $valor)
            {
                $strAttributes .= "{$chave}=\"{$valor}\" ";
            }
        }

        $strReturn = "";
        $strReturn .= "<input type=\"checkbox\" 
                           {$strAttributes}
                           checklist-value=\"{$recordVariable}.{$this->recordPrimaryKeyAttribute}\"
                           checklist-model=\"{$this->getAngularControllerAs()}.selectedRecords\" />";

        return $strReturn;

    }

    public function renderColumnContent($record, $tableName, $recordAttribute, $highlightFilteredContent=true, $highlightHighlightedContent=true)
    {
        $strReturn = "";
        $strHighlightFilteredContent = $highlightFilteredContent ? " | highlight: {$this->getAngularControllerAs()}.listFilter" : "";
        $strHighlightHighlightedContent = $highlightHighlightedContent ? " | highlight: {$this->getAngularControllerAs()}.listHighlight" : "";

        $strReturn .= "<p ng-bind-html=\"{$record}.{$tableName}__{$recordAttribute}_forHuman{$strHighlightFilteredContent}{$strHighlightHighlightedContent}\"></p>";
        return $strReturn;
    }

    public function renderEditButton($recordVariable)
    {
        $strReturn = "";
        $strReturn .= "<button
                            class=\"btn btn-success btn-xs\"
                            ng-show=\"{$recordVariable}.{$this->recordAllowEditAttribute}\"
                            ng-click=\"{$this->getAngularControllerAs()}.editRecord({$recordVariable}.{$this->recordPrimaryKeyAttribute}, {$recordVariable})\">
                            <i class='icon-edit'></i>
                       </button>";

        return $strReturn;
    }

    public function renderDeleteButton($recordVariable)
    {
        $strReturn = "";
        $strReturn .= "<button class=\"btn btn-danger btn-xs\"
                            ng-show=\"{$recordVariable}.{$this->recordAllowDeleteAttribute}\"
                            ng-click=\"{$this->getAngularControllerAs()}.showRemoveMessageDialog([{$recordVariable}.{$this->recordPrimaryKeyAttribute}])\">
                            <i class='icon-remove'></i>
                       </button>";

        return $strReturn;
    }

    public function renderListActionButtons($filterButton=true, $highlightButton=true, $collapseButton=true)
    {
        $strReturn = "";
        $printCondition = $filterButton || $highlightButton || $collapseButton;

        $strFilterButton = $filterButton ?
            "<a class=\"btn btn-xs btn-link\" href=\"javascript: void(0);\" ng-click=\"switchListSearch({$this->getAngularControllerAs()})\">
                <i class=\"icon-search\" ng-class=\"{$this->getAngularControllerAs()}.listSearchIconClass\"></i>
             </a>" :
            "";

        $strHighlightButton = $highlightButton ?
            "<a class=\"btn btn-xs btn-link\" href=\"javascript: void(0);\" ng-click=\"switchListHighlight({$this->getAngularControllerAs()})\">
                <i class=\"icon-pencil\" ng-class=\"{$this->getAngularControllerAs()}.listHighlightIconClass\"></i>
             </a>" :
            "";

        $strCollapseButton = $collapseButton ?
            "<a class=\"btn box-collapse btn-xs btn-link\" href=\"javascript: void(0);\"><i></i></a>" :
            "";

        $strReturn = "<div class=\"actions omega-list-nav-bar-actions\">
                         {$strFilterButton}
                         {$strHighlightButton}
                         {$strCollapseButton}
                      </div>";

        return $printCondition ? $strReturn : "";

    }

    public function renderHighlightAndFilterPanel()
    {
        $strReturn = "";
        $filterResultsInput = true;
        $highlightResultsInput = true;

        $strFilterResultsInput = $filterResultsInput ?
                    "<div class=\"input-group omega-in-list-filter-control\" ng-show=\"{$this->getAngularControllerAs()}.activeListSearch\">
                        <input class='form-control' placeholder='{$this->filterResultsText}' ng-model=\"{$this->getAngularControllerAs()}.listFilter\" type='text'>
                        <span class=\"input-group-addon\">
                            <span class=\"icon-search\"></span>
                        </span>
                    </div>" : "";

        $strHighLightResultsInput = $highlightResultsInput ?
                    "<div class=\"input-group omega-in-list-filter-control\" ng-show=\"{$this->getAngularControllerAs()}.activeListHighlight\">
                        <input class='form-control' placeholder='{$this->highlightResultsText}' ng-model=\"{$this->getAngularControllerAs()}.listHighlight\" type='text'>
                        <span class=\"input-group-addon\">
                            <span class=\"icon-pencil\"></span>
                        </span>
                    </div>" : "";


        $strReturn .= "<form class='navbar-form list-navbar' role='search'>
                            {$strFilterResultsInput}
                            {$strHighLightResultsInput}
                        </form>";

        return $strReturn;
    }

    public function renderMultipleRecordsActionsPanel($removeSelectedRecordsLabel=null)
    {
        if(is_null($removeSelectedRecordsLabel))
        {
            $removeSelectedRecordsLabel = I18N::getExpression("Remover itens selecionados");
        }

        $strReturn = "";
        $strReturn .= "<ul class='nav navbar-nav navbar-form navbar-right'>
                            <button class='btn btn-danger'
                                    ng-show=\"{$this->getAngularControllerAs()}.selectedRecords.length > 1\"
                                    ng-click=\"vm.showRemoveMessageDialog()\">
                                    <i class=\"icon-remove\"></i>
                                 {$removeSelectedRecordsLabel}
                            </button>
                        </ul>";

        return $strReturn;
    }

    public function renderSelectAllRecordsCheckbox()
    {
        $strReturn = "";
        $strReturn .= "<input
                            ng-model=\"{$this->getAngularControllerAs()}.selectAllRecords\"
                            ng-click=\"checkAllRecords({$this->getAngularControllerAs()})\"
                            type=\"checkbox\" />";

        return $strReturn;
    }

    public function renderColumnHeader($attributeName, $attributeDescription, $tableName, $columnName)
    {
        $strReturn = "";
        $attributeDescriptionI18N = I18N::getExpression("{$attributeDescription}");
        $columnAlias = "{$tableName}__{$columnName}";

        $strReturn .= "<th ng-class=\"{$this->getAngularControllerAs()}.sortingClass.{$attributeName}\" ng-click=\"sortColumn({$this->getAngularControllerAs()}, '{$columnName}', '{$columnAlias}')\">
                           {$attributeDescriptionI18N}
                       </th>";

        return $strReturn;
    }

    public static function renderSortIcon($columnName)
    {
        return "<div class=\"\"></div>";
    }

    public function renderTableBar()
    {
        $strReturn = "<div class=\"row datatables-bottom\">";
        $strReturn .= $this->renderPositionText();
        $strReturn .= $this->renderPaginationBar();
        $strReturn .= "</div>";

        return $strReturn;
    }

    public function renderPositionText()
    {
        $strMessage = I18N::getExpression("Mostrando registros de {0} at� {1}", "{{{$this->getAngularControllerAs()}.firstShowingRecord}}", "{{{$this->getAngularControllerAs()}.lastShowingRecord}}");
        $strReturn = "<div class=\"col-sm-6\">
                        <div class=\"dataTables_info omega-list-bottom-info\">
                            {$strMessage}
                        </div>
                    </div>";

        return $strReturn;

    }

    public function renderPaginationBar()
    {

        $strAnterior = I18N::getExpression("Anterior");
        $strProximo = I18N::getExpression("Pr�ximo");

        $strReturn = "<div class=\"col-sm-6 text-right\">
                        <div class=\"dataTables_paginate paging_bootstrap\">
                            <ul class=\"pagination pagination-sm\">
                                <li class=\"prev disabled\" ng-class=\"previousPageCssClass({$this->getAngularControllerAs()})\">
                                    <a href=\"javascript: void(0)\" ng-click=\"changeToPage({$this->getAngularControllerAs()}, 'prev')\">&larr; {$strAnterior}</a>
                                </li>
                                <li ng-class=\"page == {$this->getAngularControllerAs()}.paginationParameters.currentPage ? 'active': ''\" ng-repeat=\"page in {$this->getAngularControllerAs()}.paginationParameters.visibleButtonsArray track by \$index\">
                                    <a href=\"javascript: void(0)\" ng-click=\"changeToPage({$this->getAngularControllerAs()}, page)\">{{page}}</a>
                                </li>
                                <li class=\"next\" ng-class=\"nextPageCssClass({$this->getAngularControllerAs()})\">
                                    <a href=\"javascript: void(0)\" ng-click=\"changeToPage({$this->getAngularControllerAs()}, 'next')\">{$strProximo} &rarr;</a>
                                </li>
                            </ul>
                        </div>
                    </div>";

        return $strReturn;

    }

}
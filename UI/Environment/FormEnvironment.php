<?php

class FormEnvironment extends BaseFormEnvironment
{

    const DEFAULT_MODEL_VAR_ON_SCOPE = "formData";
    const DEFAULT_DISABLED_FIELDS_VAR_ON_SCOPE = "disabledFields";
    const DEFAULT_REQUIRED_FIELDS_VAR_ON_SCOPE = "requiredFields";
    const DEFAULT_SPECIAL_PARAMETERS_VAR_ON_SCOPE = "fieldsParameters";
    const DEFAULT_FORM_CONTROLS_CONTAINER_CSS_CLASS = "col-sm-7 col-md-5";
    const DEFAULT_FORM_CONTROLS_LABEL_CSS_CLASS = "col-sm-3 col-md-2 control-label";
    const DEFAULT_FORM_GROUP_CSS_CLASS = "form-group";
    const DEFAULT_FORM_CONTROLS_CSS_CLASS = "form-control";

    protected $modelVarOnScope;
    protected $disabledFieldVarOnScope;
    protected $requiredFieldVarOnScope;
    protected $specialParameteresVarOnScope;
    protected $formControlsContainerCssClass;
    protected $formControlsLabelCssClass;
    protected $formControlsFormGroupCssClass;

    protected $submitButtonLabel;
    protected $submitButtonCssClass;
    protected $submitButtonIconCssClass;
    protected $submitButtonMethodCall;

    protected $clearButtonLabel;
    protected $clearButtonCssClass;
    protected $clearButtonIconCssClass;
    protected $clearButtonMethodCall;

    public function __construct($formName, $angularControllerName, $angularControllerAs=null, $setDefaultValues=true)
    {
        $this->formName = $formName;
        parent::__construct($angularControllerName, $angularControllerAs, $setDefaultValues);

        $this->setLayoutOneColumn();
    }

    public function setEditionVariablesIfApplicable()
    {
        if(is_numeric(Helper::GET("id")))
        {
            $id = Helper::GET("id");
            $strRetorno = "<div class=\"variable-container\" ng-init=\"{$this->getAngularControllerAsDotIfExists()}isEdition=true\"></div>";
            $strRetorno .= "<div class=\"variable-container\" ng-init=\"{$this->getAngularControllerAsDotIfExists()}entityIdForEdition={$id}\"></div>";

            return $strRetorno;
        }
        else
        {
            return "";
        }
    }

    public function setDefaultValues()
    {
        $this->modelVarOnScope = $this->getAngularControllerAsDotIfExists() . static::DEFAULT_MODEL_VAR_ON_SCOPE;
        $this->disabledFieldVarOnScope = $this->getAngularControllerAsDotIfExists() . static::DEFAULT_DISABLED_FIELDS_VAR_ON_SCOPE;
        $this->requiredFieldVarOnScope = $this->getAngularControllerAsDotIfExists() . static::DEFAULT_REQUIRED_FIELDS_VAR_ON_SCOPE;
        $this->specialParameteresVarOnScope = $this->getAngularControllerAsDotIfExists() . static::DEFAULT_SPECIAL_PARAMETERS_VAR_ON_SCOPE;

        $this->submitButtonLabel = I18N::getExpression("Salvar Dados");
        $this->submitButtonCssClass = "btn btn-primary";
        $this->submitButtonIconCssClass = "icon-save";
        $this->submitButtonMethodCall = "{$this->getAngularControllerAsDotIfExists()}submitForm({$this->formName})";

        $this->clearButtonLabel = I18N::getExpression("Limpar");
        $this->clearButtonCssClass = "btn";
        $this->clearButtonIconCssClass = "icon-clear";
        $this->clearButtonMethodCall = "{$this->getAngularControllerAsDotIfExists()}clearForm({$this->formName})";
    }

    public function incrementNumberOfRenderedFields()
    {
        $this->numberOfRenderedFields++;
    }

    public function setLayoutOneColumn()
    {
        $this->defaultHorizontalLineInterval = 1;
        $this->formControlsCssClass = static::DEFAULT_FORM_CONTROLS_CSS_CLASS;
        $this->formControlsContainerCssClass = static::DEFAULT_FORM_CONTROLS_CONTAINER_CSS_CLASS;
        $this->formControlsFormGroupCssClass = static::DEFAULT_FORM_GROUP_CSS_CLASS;
        $this->formControlsLabelCssClass = static::DEFAULT_FORM_CONTROLS_LABEL_CSS_CLASS;
    }

    public function setLayoutTwoColumns()
    {
        $this->defaultHorizontalLineInterval = 1000;
        $this->formControlsContainerCssClass = null;
        $this->formControlsFormGroupCssClass = "col-md-5 col-sm-offset-1";
        $this->formControlsLabelCssClass = "label-top";
        $this->formControlsCssClass = "form-control";
    }

    public function setSubmitButtonParameters($submitButtonLabel, $submitButtonCssClass, $submitButtonIconCssClass, $submitButtonMethodCall)
    {
        $this->submitButtonLabel = $submitButtonLabel;
        $this->submitButtonCssClass = $submitButtonCssClass;
        $this->submitButtonIconCssClass = $submitButtonIconCssClass;
        $this->submitButtonMethodCall = $submitButtonMethodCall;
    }

    public function setSubmitButtonLabel($submitButtonLabel)
    {
        $this->submitButtonLabel = $submitButtonLabel;
    }

    public function setSubmitButtonCssClass($submitButtonCssClass)
    {
        $this->submitButtonCssClass = $submitButtonCssClass;
    }

    public function setSubmitButtonMethodCall($submitButtonMethodCall, $prependControllerAsDot=true)
    {
        $this->submitButtonMethodCall = $prependControllerAsDot ? "{$this->getAngularControllerAsDotIfExists()}{$submitButtonMethodCall}" : $submitButtonMethodCall;
    }

    public function setClearButtonParameters($clearButtonLabel, $clearButtonCssClass, $clearButtonIconCssClass, $clearButtonMethodCall)
    {
        $this->clearButtonLabel = $clearButtonLabel;
        $this->clearButtonCssClass = $clearButtonCssClass;
        $this->clearButtonIconCssClass = $clearButtonIconCssClass;
        $this->clearButtonMethodCall = $clearButtonMethodCall;
    }

    public function setClearButtonLabel($clearButtonLabel)
    {
        $this->clearButtonLabel = $clearButtonLabel;
    }

    public function setClearButtonCssClass($clearButtonCssClass)
    {
        $this->clearButtonCssClass = $clearButtonCssClass;
    }

    public function setClearButtonMethodCall($clearButtonMethodCall, $prependControllerAsDot=true)
    {
        $this->clearButtonMethodCall = $prependControllerAsDot ? "{$this->getAngularControllerAsDotIfExists()}{$clearButtonMethodCall}" : $clearButtonMethodCall;
    }

    public function getModelVarOnScope()
    {
        return $this->modelVarOnScope;
    }

    public function setModelVarOnScope($modelVarOnScope)
    {
        $this->modelVarOnScope = $modelVarOnScope;
    }

    public function getDisabledFieldVarOnScope()
    {
        return $this->disabledFieldVarOnScope;
    }

    public function setDisabledFieldVarOnScope($disabledFieldVarOnScope)
    {
        $this->disabledFieldVarOnScope = $disabledFieldVarOnScope;
    }

    public function getRequiredFieldVarOnScope()
    {
        return $this->requiredFieldVarOnScope;
    }

    public function setRequiredFieldVarOnScope($requiredFieldVarOnScope)
    {
        $this->requiredFieldVarOnScope = $requiredFieldVarOnScope;
    }

    public function getSpecialParameteresVarOnScope()
    {
        return $this->specialParameteresVarOnScope;
    }

    public function setSpecialParameteresVarOnScope($specialParameteresVarOnScope)
    {
        $this->specialParameteresVarOnScope = $specialParameteresVarOnScope;
    }

    public function setFormControlsContainerCssClass($formControlsContainerCssClass)
    {
        $this->formControlsContainerCssClass = $formControlsContainerCssClass;
    }

    public function setFormGroupCssClass($formGroupCssClass)
    {
        $this->formGroupCssClass = $formGroupCssClass;
    }

    public function getFormDebugPanel()
    {
        $str = "";
        if(Helper::isDebugModeActive())
        {
            $str .= "<div class='row equal'>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Fields Types: {{ {$this->getAngularControllerAsDotIfExists()}fieldsTypes | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Disabled Fields: {{ {$this->disabledFieldVarOnScope} | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Required Fields: {{ {$this->requiredFieldVarOnScope} | json }}</pre>";
            $str .= "</div>";
            $str .= "<div class='col-sm-3'>";
            $str .= "<pre class=\"debug-box\">Form Model: {{ {$this->modelVarOnScope} | json }}</pre>";
            $str .= "</div>";
            $str .= "</div>";
        }

        return $str;

    }

    public function renderSubmitButton()
    {
        $strButton = "<button class='{$this->submitButtonCssClass}' type='submit' ng-click=\"{$this->submitButtonMethodCall}\">
                        <i class='{$this->submitButtonIconCssClass}'></i>
                        {$this->submitButtonLabel}
                      </button>";

        return $strButton;
    }

    public function renderClearButton()
    {
        $strButton = "<button class='{$this->clearButtonCssClass}' type='clear' ng-click=\"{$this->clearButtonMethodCall}\">
                        <i class='{$this->clearButtonIconCssClass}'></i>
                        {$this->clearButtonLabel}
                      </button>";

        return $strButton;
    }

    public function setFieldCommonAttributes(FormControl $objField)
    {
        $objField->setFormName($this->formName);
        $objField->setModelVarOnScope($this->modelVarOnScope);
        $objField->setAngularControllerAs($this->angularControllerAs);
        $objField->setDisabledFieldsVarOnScope($this->disabledFieldVarOnScope);
        $objField->setRequiredFieldsVarOnScope($this->requiredFieldVarOnScope);
        $objField->setSpecialParametersVarOnScope($this->specialParameteresVarOnScope);
        $objField->setContainerCssClass($this->formControlsContainerCssClass);
        $objField->setFormGroupCssClass($this->formControlsFormGroupCssClass);
        $objField->setLabelCssClass($this->formControlsLabelCssClass);
        $objField->setCssClass($this->formControlsCssClass);

        $objField->setDefaultAngularModel();
        $objField->setDefaultAngularDisabled();
        $objField->setDefaultAngularRequired();
        $objField->setDefaultValues();
    }

}

?>
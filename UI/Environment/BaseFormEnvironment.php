<?php

abstract class BaseFormEnvironment extends Environment
{

    protected $numberOfRenderedFields;
    protected $defaultHorizontalLineInterval;
    protected $formName;

    public function __construct($angularControllerName, $angularControllerAs, $setDefaultValues)
    {
        parent::__construct($angularControllerName, $angularControllerAs, $setDefaultValues);
    }

    public function incrementNumberOfRenderedFields()
    {
        $this->numberOfRenderedFields++;
    }

    public function renderHorizontalLine($intervalBetweenFields=null, $cssClasses='hr-normal')
    {
        $returnStr = "";
        if(is_null($intervalBetweenFields))
        {
            $intervalBetweenFields = $this->defaultHorizontalLineInterval;
        }

        if($this->numberOfRenderedFields % $intervalBetweenFields == 0)
        {
            $returnStr = "<hr class='{$cssClasses}'>";
        }

        return $returnStr;

    }

    public function getFormName()
    {
        return $this->formName;
    }

    public function getCepInputInstance($entityName)
    {
        $objField = new CepInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getCnpjInputInstance($entityName)
    {
        $objField = new CnpjInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getCpfCnpjInputInstance($entityName)
    {
        $objField = new CpfCnpjInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getCpfInputInstance($entityName)
    {
        $objField = new CpfInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getCurrencyInputInstance($entityName)
    {
        $objField = new CurrencyInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getDateInputInstance($entityName)
    {
        $objField = new DateInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getDateRangeInputInstance($entityFromDateInstance, $entityToDateInstance)
    {
        $objField = new DateRangeInput($entityFromDateInstance, $entityToDateInstance);

        return $objField;
    }

    public function getMultipleSelectInstance($entityName, $scopeVarWithData=null, $scopeRefreshFunctionCall=null)
    {
        $objField = new MultipleSelect($entityName, $scopeVarWithData, $scopeRefreshFunctionCall);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getNumericInputInstance($entityName)
    {
        $objField = new NumericInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getPasswordInputInstance($entityName)
    {
        $objField = new PasswordInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getPercentageInputInstance($entityName)
    {
        $objField = new PercentageInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getPhoneInputInstance($entityName, $localeMask = PhoneInput::LOCALE_GENERIC)
    {
        $objField = new PhoneInput($entityName, $localeMask);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getSelectInstance($entityName)
    {
        $objField = new Select($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getSelectForBooleanInstance($entityName)
    {
        $objField = new SelectForBoolean($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getSwitchControlInstance($entityName)
    {
        $objField = new SwitchControl($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getCheckboxListInstance($entityName, $arrayToIterate, $repeatVariableName, $descriptionAttribute=null)
    {
        $objField = new CheckboxList($entityName, $arrayToIterate, $repeatVariableName, $descriptionAttribute);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getTextareaInstance($entityName)
    {
        $objField = new Textarea($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getTextInputInstance($entityName, $transformationDirective = TextInput::TRANSFORMATION_DIRECTIVE_NONE)
    {
        $objField = new TextInput($entityName, $transformationDirective);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getTimeInputInstance($entityName)
    {
        $objField = new TimeInput($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getTypeaheadInstance($entityName)
    {
        $objField = new Typeahead($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public function getFileUploadArea($entityName)
    {
        $objField = new FileUploadArea($entityName);
        $this->setFieldCommonAttributes($objField);
        $this->incrementNumberOfRenderedFields();

        return $objField;
    }

    public abstract function setFieldCommonAttributes(FormControl $objField);

}

?>
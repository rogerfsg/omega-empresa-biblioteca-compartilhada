var GoogleAnalyticsAppender = (function ()
{
    var gaIdentifier;
    var gaDomain;
    var commonData;
    var serviceUrl = 'http://www.google-analytics.com/collect';
    var threshold = '';

    var sessionId = null;
    var userId = null;

    var googleAnalyticsAppenderClass = function(gaIdentifier, gaDomain)
    {
        this.gaIdentifier = gaIdentifier;
        this.gaDomain = gaDomain;
        this.setCommonDataValues();
    };

    googleAnalyticsAppenderClass.prototype = {

        getLogTypeString: function(logType)
        {
            switch(logType)
            {
                case OmegaLog.LOGLEVEL.FATAL:
                    return  'Fatal';

                case OmegaLog.LOGLEVEL.ERROR:
                    return  'Error';

                case OmegaLog.LOGLEVEL.WARNING:
                    return  'Warning';

                case OmegaLog.LOGLEVEL.INFO:
                    return  'Info';

                case OmegaLog.LOGLEVEL.DEBUG:
                default:
                    return  'Debug';
            }

        },

        setSessionId: function(sessionId)
        {
            this.sessionId = sessionId;
        },

        setUserId: function()
        {
            this.userId = userId;
        },

        setThreshold: function(threshold)
        {
            this.threshold = threshold;
        },

        isLoggagle: function(logLevel)
        {
            return (this.threshold != null && typeof this.threshold != 'undefined' && logLevel >= this.threshold);
        },

        log: function(logLevel, argumentsArray)
        {
            for(var arg = 0; arg < argumentsArray.length; ++arg)
            {
                this.trackEvent(logLevel, argumentsArray[arg]);
            }
        },

        setCommonDataValues: function()
        {
            var me = this;
            var data = {
                'v': '1',
                'tid':  this.gaIdentifier,
                'sr':  screen.width + 'x' + screen.height,
                'vp':  window.outerWidth + 'x' + window.outerHeight,
                'de': 'ISO-8859-1',
                'dl': document.location.href,
                'dh': this.gaDomain,
                'ul': '', //user language
            };

            if(this.sessionId != null)
            {
                data['cid'] = this.sessionId;
            }

            if(this.userId != null)
            {
                data['uid'] = this.userId;
            }

            me.commonData = '';
            for(var chave in data){

                me.commonData += chave + '=' + data[chave] + '&';

            }

        },

        trackEvent: function(logType, value)
        {

            var data = {
                't': 'event',
                'ec': 'JavascriptLog ' + logType,
                'ea': value
            };

            this.__sendData(data);
        },

        trackPageview: function(page)
        {
            var data = {
                't': 'pageview',
                'cd': page
            };

            this.__sendData(data);
        },

        __sendData: function(specificData)
        {
            var sendData = this.config.commonData;
            for(var chave in specificData){

                if(typeof specificData[chave] !== 'undefined'){

                    sendData += chave + '=' + specificData[chave] + '&';

                }

            }

            var xhr = $.ajax({

                type: "POST",
                url: this.serviceUrl,
                data: sendData

            })
            .done(function(){

                if(specificData['t'] == 'exception') {



                }
                else if(specificData['t'] == 'appview'){



                }
                else if(specificData['t'] == 'event'){



                }

            })
            .fail(function(){



            });

        }

    };

    return googleAnalyticsAppenderClass;

})();

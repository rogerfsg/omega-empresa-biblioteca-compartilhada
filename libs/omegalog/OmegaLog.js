var OmegaLog = (function ()
{
    var sessionIdentifier;
    var loglibraryInstance;
    var googleAnalyticsAppenderInstance;

    var logClass = function(sessionId)
    {
        this.sessionIdentifier = sessionId;
        this.loglibraryInstance = log4javascript.getLogger(sessionId);
    };

    logClass.LOGLEVEL = {

        DEBUG: 3,
        INFO: 4,
        WARNING: 5,
        ERROR: 6,
        FATAL: 7

    };

    logClass.prototype = {

        addAppender: function(appender)
        {
            this.loglibraryInstance.addAppender(appender);
        },

        addGoogleAnalyticsAppender: function(gaAppender)
        {
            this.googleAnalyticsAppenderInstance = gaAppender;

            if(this.sessionIdentifier != null)
            {
                this.googleAnalyticsAppenderInstance.setSessionId(this.sessionIdentifier);
            }
        },

        getLibraryInstance: function()
        {
            return this.loglibraryInstance;
        },

        getGoogleAnalyticsAppender: function()
        {
            return this.googleAnalyticsAppenderInstance;
        },

        isLogLibraryDefined: function()
        {
            return this.loglibraryInstance instanceof Object;
        },

        getArgumentsAsString: function()
        {
            var returnString = "";
            for(var arg = 0; arg < arguments.length; ++ arg)
            {
                var arr = arguments[arg];

                for(var i = 0; i < arr.length; ++ i)
                {
                    var valor = arr[i];
                    if(typeof arr[i] == 'object')
                    {
                        valor = JSON.stringify(valor);
                    }

                    var returnString = returnString + "Argumento " + i + ": " + valor + " | ";
                }
            }

            return returnString;

        },

        isGoogleAnalyticsAppenderDefinedAndActive: function()
        {
            var isDefined = typeof GoogleAnalyticsAppender != 'undefined' && this.googleAnalyticsAppenderInstance instanceof GoogleAnalyticsAppender;
            var isActive = typeof FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO == 'undefined' &&
                                typeof FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO != 'undefined'
                                    && FORCAR_DESATIVACAO_GOOGLE_ANALYTICS_QUANTO_ATIVADO === false;

            return isDefined && isActive;
        },

        debug: function()
        {
            if(this.isLogLibraryDefined())
            {
                this.loglibraryInstance.debug.apply(this.loglibraryInstance, arguments);
            }

            if(this.isGoogleAnalyticsAppenderDefinedAndActive() &&
                this.googleAnalyticsAppenderInstance.isLoggagle(OmegaLog.LOGLEVEL.DEBUG))
            {
                this.googleAnalyticsAppenderInstance.log(OmegaLog.LOGLEVEL.DEBUG, arguments);
            }
        },

        info: function()
        {
            if(this.isLogLibraryDefined())
            {
                this.loglibraryInstance.info.apply(this.loglibraryInstance, arguments);
            }

            if(this.isGoogleAnalyticsAppenderDefinedAndActive() &&
                this.googleAnalyticsAppenderInstance.isLoggagle(OmegaLog.LOGLEVEL.INFO))
            {
                this.googleAnalyticsAppenderInstance.log(OmegaLog.LOGLEVEL.INFO, arguments);
            }
        },

        warning: function()
        {
            if(this.isLogLibraryDefined())
            {
                this.loglibraryInstance.warn.apply(this.loglibraryInstance, arguments);
            }

            if(this.isGoogleAnalyticsAppenderDefinedAndActive() &&
                this.googleAnalyticsAppenderInstance.isLoggagle(OmegaLog.LOGLEVEL.WARNING))
            {
                this.googleAnalyticsAppenderInstance.log(OmegaLog.LOGLEVEL.WARNING, arguments);
            }
        },

        error: function()
        {
            if(this.isLogLibraryDefined())
            {
                this.loglibraryInstance.error.apply(this.loglibraryInstance, arguments);
            }

            if(this.isGoogleAnalyticsAppenderDefinedAndActive() &&
                this.googleAnalyticsAppenderInstance.isLoggagle(OmegaLog.LOGLEVEL.ERROR))
            {
                this.googleAnalyticsAppenderInstance.log(OmegaLog.LOGLEVEL.ERROR, arguments);
            }
        },

        fatal: function()
        {
            if(this.isLogLibraryDefined())
            {
                this.loglibraryInstance.fatal.apply(this.loglibraryInstance, arguments);
            }

            if(this.isGoogleAnalyticsAppenderDefinedAndActive() &&
                this.googleAnalyticsAppenderInstance.isLoggagle(OmegaLog.LOGLEVEL.FATAL))
            {
                this.googleAnalyticsAppenderInstance.log(OmegaLog.LOGLEVEL.FATAL, arguments);
            }

        }

    };

    return logClass;

})();

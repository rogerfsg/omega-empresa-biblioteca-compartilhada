<?php

## 29/05/2006

######
## Verification Image
######
## This class generate an image with random text
## to be used in form verification. It has visual
## elements design to confuse OCR software preventing
## the use of BOTS.
#######
## Author: Julio Romano
## Email: julioromano@bol.com.br
##
## 29/05/2006
#######
## Usage: See attached files
#####

class ImageVerification
{

    var $stringVerificacao;
    var $codDig;
    var $codSeg;

    function ImageVerification()
    {

        $this->fonts = array('ttf/arial.ttf', 'ttf/arialbd.ttf', 'ttf/ariali.ttf', 'ttf/times.ttf', 'ttf/timesi.ttf', 'ttf/timesbd.ttf',
            'ttf/verdana.ttf', 'ttf/verdanai.ttf', 'ttf/verdanab.ttf'); #fontes usadas

        $this->img_width = 100; #largura da imagem
        $this->img_height = 30; #altura da imagem

        $this->minAngle = -10; #�ngulo m�nimo de rota��o dos caracteres
        $this->maxAngle = 10; #�ngulo m�ximo de rota��o dos caracteres

        $this->minSize = 13; #tamanho m�nimo da fonte dos caracteres
        $this->maxSize = 18; #tamanho m�ximo da fonte dos caracteres

        $this->caseSensitive = false; # compara��o case-sensitive
    }

    function geraString($tamanho = 4)
    {
        $string = array();

        for ($i = 1; $i <= $tamanho; $i++) {

            $numAleat = 3;

            switch ($numAleat) {
                case '1':
                    //letra maiuscula
                    $min = 65;
                    $max = 90;
                    break;
                case '2':
                    //letra minuscula
                    $min = 97;
                    $max = 122;
                    break;
                case '3':
                    //numeros
                    $min = 48;
                    $max = 57;
                    break;
            }

            $string[] = chr(rand($min, $max));

        }

        shuffle($string);

        return $this->stringVerificacao = implode('', $string);

    }

    function geraImagem()
    {

        $this->string = $this->stringVerificacao;

        //$this->string = "abcdef";

        header("Content-type: image/jpeg");

        $this->img = imagecreatetruecolor($this->img_width, $this->img_height);

        $grey = imagecolorallocate($this->img, 235, 235, 235);
        $white = imagecolorallocate($this->img, 255, 255, 255);
        $black = imagecolorallocate($this->img, 0, 0, 0);
        $red = imagecolorallocatealpha($this->img, 255, 0, 0, 75);
        $green = imagecolorallocatealpha($this->img, 0, 255, 0, 75);
        $blue = imagecolorallocatealpha($this->img, 0, 0, 255, 75);

        imagefilledrectangle($this->img, 0, 0, $this->img_width, $this->img_height, $grey);

        imagefilledellipse($this->img, ceil(rand(5, 145)), ceil(rand(0, 35)), 30, 30, $red);
        imagefilledellipse($this->img, ceil(rand(5, 145)), ceil(rand(0, 35)), 30, 30, $green);
        imagefilledellipse($this->img, ceil(rand(5, 145)), ceil(rand(0, 35)), 30, 30, $blue);

        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $red);
        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $green);
        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $blue);

        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $red);
        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $green);
        imageline($this->img, rand(1, 200), rand(1, 50), rand(101, 200), rand(26, 50), $blue);

        imagefilledrectangle($this->img, 0, 0, $this->img_width, 0, $black);
        imagefilledrectangle($this->img, $this->img_width - 1, 0, $this->img_width - 1, $this->img_height - 1, $black);
        imagefilledrectangle($this->img, 0, 0, 0, $this->img_height - 1, $black);
        imagefilledrectangle($this->img, 0, $this->img_height - 1, $this->img_width, $this->img_height - 1, $black);

        for ($i = 0; $i < strlen($this->string); $i++) {
            if ($i == 0)
                $y = 10;
            else
                $y += 23;

            $angle = rand($this->minAngle, $this->maxAngle);

            $fonte = rand(0, sizeof($this->fonts) - 1);

            $size = rand($this->minSize, $this->maxSize);

            imagettftext($this->img, $size, $angle, $y, 20, $black, $this->fonts[$fonte], $this->string{$i});
        }

        imagejpeg($this->img);
    }

    function leCodigos($postDigitado)
    {

        $this->codDig = md5($postDigitado);

        // J� faz a leitura do get ou do post do campo hidden do formul�rio que est� sendo postado
        ($_GET["imgCod"] != "") ? $this->codSeg = $_GET["imgCod"] : $this->codSeg = $_POST["imgCod"];
    }

    function comparacaoCaseOn()
    {
        if (strcmp($this->codDig, $this->codSes) == 0)
            return true;
        else
            return false;
    }

    function comparacaoCaseOff()
    {
        if (strcasecmp($this->codDig, $this->codSes) == 0)
            return true;
        else
            return false;
    }

    function comparaCodigos()
    {
        if ($this->caseSensitive)
            return $this->comparacaoCaseOn();
        else
            return $this->comparacaoCaseOff();
    }

    function geraHidden()
    {
        $codigo = md5($this->stringVerificacao);
        echo "<input type='hidden' name='imgCod' id='imgCod' value='$codigo'/>";
    }

}

?>
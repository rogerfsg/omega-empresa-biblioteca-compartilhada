/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//window.open();
//window.close();
function Arvore (pathImagemIcone, getIdView, raiz) {
    
    this.tree = null;
    this.tree = new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);
    this.tree.setSkin('dhx_skyblue');

    this.tree.enableTreeLines(true);
    this.tree.setImagePath(pathImagemIcone);
    this.tree.setImageArrays("plus", "plus2.gif", "plus3.gif", "plus4.gif", "plus.gif", "plus5.gif");
    this.tree.setImageArrays("minus", "minus2.gif", "minus3.gif", "minus4.gif", "minus.gif", "minus5.gif");
//    tree.setStdImages("folderClosed.gif", "folderOpen.gif", "folderClosed.gif");

    this.tree.setXMLAutoLoading("xml.php?tipo=pages&page=gerar_xml_arvore_estrutura&" + getIdView);
    this.tree.loadXML("xml.php?tipo=pages&page=gerar_xml_arvore_estrutura&id=0&"+ getIdView);    
    
    
    this.tree.attachEvent("onRightClick", this.onRightClick );
    //tree.setOnRightClickHandler(this.onRightClick);
    this.tree.attachEvent("onClick", this.onClick);
    this.tree.controlador = this;
    
    this.raiz = raiz;
};



Arvore.prototype.onRightClick = function(id){
    document.getElementById("div_pagina_arvore").innerHTML = 'Right click:' + id;
    return false;
};

Arvore.prototype.onClick = function(id) {

   var retJson = carregarValorRemoto('Arvore_view', 'onClick(' + id +  ')', this.controlador.raiz);

   if(retJson == null || retJson.length == 0){
       return false;
   }
   else{
       var objJson = JSON.parse(retJson);
       if( objJson.objPaginas.length > 1) {
       
           //Carrega estrutura TAB
          var encObjJson = encodeURIComponent(retJson); 
          carregarAjaxTabSync(
            "div_pagina_arvore", 
            "&objJson=" + encObjJson,
            "", 
            this.controlador.raiz);
          
       } else{
           var objPagina = objJson.objPaginas[0]; 
           if(objPagina.abrirNovaJanela){
               abrirNovaJanela(objPagina.tipoPagina, objPagina.pagina, objJson.get);
           } else{
                carregarListAjaxSync(
                  objPagina.tipoPagina, 
                  objPagina.pagina, 
                  "div_pagina_arvore", 
                  objJson.get,
                  "", 
                  this.controlador.raiz);
           }
               
        
       }
        
          return true;
   }
   
};



//Funcoes Estaticas

//Apple.prototype.getInfo = function() {
//    return this.color + ' ' + this.type + ' apple';
//};
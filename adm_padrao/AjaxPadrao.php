<?php

    class AjaxPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'HTML_ACTION');
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $tipo = Helper::POSTGET("tipo");
                $pagina = Helper::POSTGET("page");
                if (strlen($tipo) && strlen($pagina))
                {
                    $this->renderConteudoSeAutorizado();
                }
                else
                {
                    $class = Helper::POSTGET("class");
                    $funcao = Helper::POSTGET("funcao");
                    if (strlen($class) && strlen($funcao))
                    {
                        $classe = Helper::GET("class");

                        $obj = call_user_func_array(array($classe, "factory"), array());
                        if (!is_object($obj))
                        {
                            throw new NotImplementedException (I18N::getExpression("Factory não foi definida para a classe {0}", $classe));
                        }
                        $funcaoComParametros = Helper::GET("funcao");

                        $metodo = Helper::getNomeFuncao($funcaoComParametros);
                        $parametros = Helper::getParametrosFuncao($funcaoComParametros);

                        $retorno = call_user_func_array(array($obj, $metodo), $parametros);

                        print $retorno;
                    }
                    else
                    {
                        $classe = Helper::GET("class");
                        $funcaoComParametros = Helper::GET("metodo_estatico");
                        if (strlen($class) && strlen($funcaoComParametros))
                        {
                            $metodo = Helper::getNomeFuncao($funcaoComParametros);
                            $parametros = Helper::getParametrosFuncao($funcaoComParametros);

                            $retorno = call_user_func_array(array($classe, $metodo), $parametros);

                            print $retorno;
                        }
                        else
                        {
                            throw new InvalidArgumentException(I18N::getExpression("Parâmetros inválidos"));
                        }
                    }
                }

                Database::closeAll();
                HelperRedis::closeAll();
                HelperLog::verbose("ajax", "OK");
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $h = new HelperLog();
                $h->gravarLogEmCasoErro(new Mensagem(null, null, $exc));

                HelperLog::verbose("ajax", null, $exc);
            }
        }

        public function renderConteudoSeAutorizado()
        {
            $objSeguranca = Registry::get('Seguranca');

            if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmPaginasDoUsuarioCorrente())
            {
                Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                $autenticado = false;
            }
            else
            {
                $autenticado = true;
            }

            if ($autenticado)
            {
                Helper::includePHP(0, Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
            }
        }
    }

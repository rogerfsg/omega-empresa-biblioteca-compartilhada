<?php

    class WebhookPadrao extends InterfaceScript
    {
        public $accessToken;
        public $defaultClasse = "BO_Processo";
        public $defaultFuncao = "fluxo";
        public function __construct($accessToken, $classe, $funcao)
        {
            parent::__construct(true, 'JSON');
            $this->accessToken=$accessToken;
            $this->defaultClasse = $classe;
            $this->defaultFuncao= $funcao;
        }

        public function render()
        {
            try
            {

                if(isset($_REQUEST['hub_verify_token'])){
                    $challenge = $_REQUEST['hub_challenge'];
                    $verify_token = $_REQUEST['hub_verify_token'];
// Set this Verify Token Value on your Facebook App
                    if ($verify_token === 'testtoken') {
                        echo $challenge;
                        exit();
                    }
                }

                $strInput= file_get_contents('php://input');
                $input = json_decode($strInput, true);

                HelperLog::logWebhook("Input: ". print_r($input, true));


                $sender = $input['entry'][0]['messaging'][0]['sender']['id'];
                $message = null;
                if(isset($input['entry'][0]['messaging'][0]['message']['text']))
                    $message=$input['entry'][0]['messaging'][0]['message']['text'];

                Registry::add($sender,'senderId');
                Registry::add($input,'inputFacebook');
                Registry::add($message,'messageFacebook');

//                HelperLog::logWebhook("Input: ".$strInput);

                //TODO-EDUARDO: descomentar linha abaixo
                Helper::includePHPDoProjeto('recursos/php/cache_webservice.php');
                Helper::includePHPDoProjeto('adm/imports/instancias.php');

                $classe=Helper::POSTGET("class");
                $funcao=Helper::POSTGET("action");
                if(!strlen($classe)){
                    $classe = $this->defaultClasse;
                    $funcao = $this->defaultFuncao;
                }
                $obj = call_user_func_array(array($classe, "factory"), array());
                if (!is_object($obj))
                {
                    throw new NotImplementedException ("Factory não foi definida para a classe {$classe}");
                }
                $retorno = call_user_func(array($obj, $funcao));

//                Helper::logInfo("PASODASD: ".$retorno);
//                if ($retorno == null)
//                {
//                    $retorno = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
//                        "A funcao nao retornou nenhum valor. {$classe}::{$funcao}");
//                    $retorno = json_encode($retorno);
//                }

                if($retorno != null){
                    Helper::curlFacebook(
                        'https://graph.facebook.com/v2.6/me/messages?access_token=' . $this->accessToken,
                        $retorno);

                    $singletonCacheWebservice = SingletonCacheWebservice::getSingleton();
                    $singletonCacheWebservice->setCachingCurrentWebservice($retorno);
                }

                HelperLog::logWebhook("Retorno: ". print_r($retorno, true));


                Database::closeAll();
                HelperRedis::closeAll();
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $msg = new Mensagem(null, null, $exc);
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                $strRet = json_encode($msg);
                echo $strRet;

                HelperLog::verbose(null, $exc);

                HelperLog::webservice(print_r($exc, true));
            }
        }
    }

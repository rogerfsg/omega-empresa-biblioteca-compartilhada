<?php

    class IndexPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>
                <head>
                    <title><?= Helper::getTituloDasPaginas(); ?></title>

                    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

                    <?= Helper::getTagDoFavicon(); ?>

                    <?= Helper::carregarArquivoCss(1, "adm/css/", "padrao") ?>
                    <?= Javascript::setRaizDaEstrutura(); ?>

                    <?= Javascript::importarTodasAsBibliotecas(); ?>

                    <?= Helper::imprimirComandoJavascript("barraDeAcoesAcompanhandoRolagem();"); ?>

                </head>

                <body leftmargin="0" topmargin="0" id="body_identificator">

                <?php

                    //Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO."imports/mensagens.php");
                    $mp = new MensagensPadrao();
                    $mp->render();

                    Ajax::imprimirCorpoDaDivDeRetornoAjax();
                    Registry::add(new Menu());

                ?>

                <table width="100%" class="table_geral" cellpadding="0" cellspacing="0">
                    <tr class="fundo_menu_topo">
                        <td align="center" width="100%">
                            <table width="100%">
                                <tr>
                                    <td>

                                        <?

                                            //Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO."imports/topo_pagina.php");
                                            $tp = new TopoPaginaPadrao();
                                            $tp->render();

                                        ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td id="espacamento_menu_superior">

                                        <?

                                            $ms = new MenuSuperiorPadrao();
                                            $ms->render();

                                        ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <?php
                                            $autenticado = false;

                                            $objSeguranca = Registry::get('Seguranca');

                                            if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmPaginasDoUsuarioCorrente())
                                            {
                                                Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                                                $autenticado = false;
                                            }
                                            else
                                            {
                                                $autenticado = true;
                                            }

                                            if ($autenticado)
                                             {
                                                if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                                {
                                                    $paginaInicial = Seguranca::getPaginaInicialDoUsuario();
//                                            echo "entrou: ".PAGINA_INICIAL_PADRAO;
//                                            
//                                            exit();

                                                    if (!empty($paginaInicial))
                                                    {
                                                        Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . $paginaInicial);
                                                    }
                                                    else
                                                    {
                                                        if (defined('PAGINA_INICIAL_PADRAO') && !is_null(PAGINA_INICIAL_PADRAO))
                                                        {
                                                            $strPage = PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO;
                                                            Helper::includePHPDoProjeto($strPage);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    $pageT = Helper::GET("page");
                                                    if (!empty($pageT))
                                                    {
                                                        Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . $pageT . ".php");
                                                    }
                                                }
                                            }

                                        ?>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                </body>
                </html>
                <?
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }
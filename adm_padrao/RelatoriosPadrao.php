<?php

//Pagina Wrapper para Relatorios
    class RelatoriosPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $tipo = Helper::POSTGET("tipo");
                $pagina = Helper::POSTGET("page");
                $titulo = Helper::POSTGET("titulo");
                if (!strlen($tipo) || !strlen($pagina))
                {
                    throw new InvalidArgumentException("Parâmetro inválido.");
                }
                ?>

                <html>
                <head>
                    <title><?= $titulo; ?></title>

                    <? include_once '../recursos/php/js_constants.php'; ?>

                    <?= Helper::carregarArquivoCss(1, "adm/css/", "relatorios") ?>
                    <?= Helper::carregarArquivoCss(1, "recursos/libs/tooltip/", "tooltip") ?>

                    <?= Helper::importarBibliotecaJavascriptPadrao() ?>

                </head>

                <body leftmargin="0" topmargin="0">

                <?

                    Relatorio::imprimirCabecalhoPadraoRelatorios();

                    include_once "{$tipo}/{$pagina}.php";

                ?>

                </body>

                </html>

                <?
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

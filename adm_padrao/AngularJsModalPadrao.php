<?php

    //Pagina Wrapper para Popups
    class AngularJsModalPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                ?>

                <div class="modal-body">

                    <?

                        $arquivoExiste = Helper::includePHP(1, PATH_RELATIVO_PROJETO . "/" . Helper::GET("tipo") . "/" . Helper::GET("page") . "/view.php");
                        if(!$arquivoExiste)
                        {
                            ?>

                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='page-header'>
                                        <h1 class='pull-left'>
                                            <i class='icon-info-sign'></i>
                                            <span>
                                                <?=I18N::getExpression("Ocorreu um problema") ?>
                                            </span>
                                        </h1>
                                        <div class='pull-right close-button-container'>
                                            <a class="btn box-remove btn-xs btn-link" href="javascript:void(0);" ng-click="closeLastModalInstance()"><i class="icon-remove"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-info" id="mensagem-1" style="">
                                <?=I18N::getExpression("N�o foi poss�vel carregar o conte�do.") ?>
                            </div>

                            <?

                        }

                    ?>
                </div>

                <?
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

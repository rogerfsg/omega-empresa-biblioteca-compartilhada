<?php

    class WebservicePadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'JSON');
        }

        public function render()
        {
            try
            {
                //TODO-EDUARDO: descomentar linha abaixo
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                Helper::includePHPDoProjeto('recursos/php/cache_webservice.php');
                Helper::includePHPDoProjeto('adm/imports/instancias.php');

                if (Helper::POSTGET("class") != null)
                {
                    $classe = Helper::POSTGET("class");
                    $action = Helper::POSTGET("action");
                    $registroDaOperacao = Helper::POSTGET("id1");
                }
                else
                {
                    throw new InvalidArgumentException("Deve ser realizado parametros invalidos.");
                }

                $objSeguranca = Registry::get('Seguranca');
                if (Registry::contains($classe))
                {
                    $obj = Registry::get($classe);
                }
                else
                {
                    $obj = call_user_func_array(array($classe, "factory"), array());
                    if (!is_object($obj))
                    {
                        throw new NotImplementedException ("Factory n�o foi definida para a classe $classe");
                    }
                }
                $validade = true;
                if ($objSeguranca->isAcaoRestrita($classe, $action))
                {
                    HelperLog::logSeguranca("isAcaoRestrita - true");

                    if (!$objSeguranca->verificarPermissaoEmAcoesDoUsuarioCorrente($classe, $action))
                    {
                        $retorno = new Mensagem(
                            PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                            "Deve ser realizado o login para essa a��o. {$classe}::{$action}");
                        $validade = false;
                    }
                }

                if ($validade)
                {
                    if ($action == "loginWebservice")
                    {
                        $retorno = $obj->login(
                            array("email" => Helper::POSTGET("txtLogin")),
                            Helper::POSTGET("txtSenha"));
                    }
                    elseif ($action == "logout")
                    {
                        $retorno = $obj->__actionLogout();
                    }
                    elseif ($action == "add")
                    {
                        $retorno = $obj->__actionAdd();
                    }
                    elseif ($action == "edit")
                    {
                        $retorno = $obj->__actionEdit();
                    }
                    elseif ($action == "remove")
                    {
                        $retorno = $obj->__actionRemove();
                    }
                    else
                    {

                        $retorno = call_user_func(array($obj, $action));
                    }

                    if ($retorno == null)
                    {
                        $retorno = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "A fun��o n�o retornou nenhum valor. {$classe}::{$action}");
                    }
                }

                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($retorno);

                if(array_search("AngularJS", $_SERVER) == "HTTP_X_APPLICATION_TYPE")
                {
                    //Helper::corrigirObjetoParaJsonEncode($retorno);
                }

                $strRet = json_encode($retorno);
                echo $strRet;

                $singletonCacheWebservice = SingletonCacheWebservice::getSingleton();
                $singletonCacheWebservice->setCachingCurrentWebservice($strRet);

                HelperLog::verbose($strRet);
                HelperLog::webservice($strRet);

                Database::closeAll();
                HelperRedis::closeAll();
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $msg = new Mensagem(null, null, $exc);
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                $strRet = json_encode($msg);
                echo $strRet;

                HelperLog::verbose(null, $exc);

                HelperLog::webservice(print_r($exc, true));
            }
        }
    }

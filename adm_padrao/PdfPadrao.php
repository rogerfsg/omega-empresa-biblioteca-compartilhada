<?php

//Se ocorrer algum erro será retornado o json do objeto Mensagem com o codRetorno == ERRO
//Então sempre verifique o retorno se começa com esse valor

    class PdfPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'PDF');
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $arquivo = Helper::POSTGET("script");
                $classe = Helper::POSTGET("classe");
                $metodo = Helper::POSTGET("metodo");
                $nomeArquivoDeSaida = Helper::POSTGET("arquivoSaidaSemExtensao");

                if (strlen($arquivo))
                {
                    $strPathScript = Helper::acharRaiz() . "adm/";

                    $conteudo = Relatorio::fileGetContents($strPathScript . urldecode($arquivo));

                    if ($conteudo === false)
                    {
                        $conteudo = "";
                    }
                }
                elseif (strlen($classe) && strlen($metodo))
                {
                    $objSeguranca = Registry::get('Seguranca');
                    if (Registry::contains($classe))
                    {
                        $obj = Registry::get($classe);
                    }
                    else
                    {
                        $obj = call_user_func_array(array($classe, "factory"), array());
                    }

                    $conteudo = call_user_func(array($obj, $metodo));
                }
                else
                {
                    throw new InvalidArgumentException("Parâmetros inválidos");
                }

                if (strlen($conteudo))
                {
                    Helper::importarBibliotecaParaGerarPDF();

                    $dompdf = new DOMPDF();
                    $dompdf->load_html($conteudo);

                    $dompdf->render();

                    if (!$nomeArquivoDeSaida)
                    {
                        $nomeArquivoDeSaida = "relatorio";
                    }

                    $dompdf->stream("{$nomeArquivoDeSaida}.pdf");
                }
                else
                {
                    throw new Exception("Arquivo vazio");
                }
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);

                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

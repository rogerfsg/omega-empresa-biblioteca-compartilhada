<?php

    class ArvorePadrao extends InterfaceScript
    {
        private $aep = null;

        public function __construct(ArvoreEstruturaPadrao $aep)
        {
            parent::__construct(true);
            $this->aep = $aep;
        }

        public function render()
        {
            try
            {
                $idArvoreView = Helper::GET(Param_Get::ID_ARVORE_VIEW);
                if (!strlen($idArvoreView))
                {
                    $idArvoreView = Arvore_view::persisteNovaArvore();
                }

                ?>
                <html>
                <head>
                    <title><?= Helper::getTituloDasPaginas(); ?></title>

                    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

                    <?= Helper::getTagDoFavicon(); ?>

                    <?= Helper::carregarArquivoCss(1, "adm/css/", "padrao") ?>

                    <?= Javascript::importarTodasAsBibliotecas(); ?>

                    <?= Helper::imprimirComandoJavascript("barraDeAcoesAcompanhandoRolagem();"); ?>

                </head>
                <body leftmargin="0" topmargin="0" id="body_identificator">
                <div id="div_url_arvore"></div>
                <a href="index.php?tipo=pages&page=teste" target="_self">Pagina teste</a>
                <?

                    $m = new MensagensPadrao();
                    $m->render();

                    Ajax::imprimirCorpoDaDivDeRetornoAjax();

                ?>

                <table width="100%" class="table_geral" cellpadding="0" cellspacing="0">

                    <tr class="fundo_menu_topo">
                        <td align="center" width="100%">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">

                                        <?
                                            $tp = new TopoPaginaPadrao();
                                            $tp->render();
                                        ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td id="espacamento_menu_superior" colspan="2">

                                        <?
                                            $ms = new MenuSuperiorPadrao();
                                            $ms->render();
                                        ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" valign="top">
                                        <?
                                            $autenticado = false;

                                            $objSeguranca = Registry::get('Seguranca');

                                            if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmPaginasDoUsuarioCorrente())
                                            {
                                                Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                                                $autenticado = false;
                                            }
                                            else
                                            {
                                                $autenticado = true;
                                            }

                                            if ($autenticado)
                                            {
                                                $this->aep->render();
                                            }
                                        ?>
                                    </td>
                                    <td width="80%" valign="top">

                                        <div id="div_pagina_arvore">
                                            <?

                                                if ($autenticado)
                                                {
                                                    if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                                    {
                                                        $paginaInicial = Seguranca::getPaginaInicialDoUsuario();
                                                        //                                            echo "entrou: ".PAGINA_INICIAL_PADRAO;
                                                        //
                                                        //                                            exit();

                                                        if (strlen($paginaInicial))
                                                        {
                                                            Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . $paginaInicial);
                                                        }
                                                        else
                                                        {
                                                            Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                                    }
                                                }

                                            ?>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </body>
                </html>

                <?

            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);

                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

<?php

    class ArvoreEstruturaPadrao extends InterfaceScript
    {

        public $get;
        public $pathImagemIcone;
        public $raiz;
//$getIdView = Param_Get::ID_ARVORE_VIEW."=".$idArvoreView."&".Param_Get::ID_PROJETOS."=".Helper::GET(Param_Get::ID_PROJETOS);
//    $raiz = Helper::acharRaiz();
//        $pathImagemIcone = Helper::acharRaiz()."/imgs/icones_arvore/biblioteca_nuvem/";
        public function __construct($get, $pathImagemIcone)
        {
            parent::__construct(true);
            $this->get = $get;
            $this->pathImagemIcone = $pathImagemIcone;
        }

        public function render()
        {
            ?>

            <div id="treeboxbox_tree"
                 style="width:350px; height:577px;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div>
            <br>

            <script type="text/javascript">


                window.onload = function ()
                {
                    tree = new Arvore("<?=$this->pathImagemIcone;?>", "<?=$this->get;?>", "<?=Helper::acharRaiz();?>");
                    //bloquea o F5
                    document.onkeydown = function (e)
                    {
                        if (e.keyCode === 116)
                        {
                            return false;
                        }
                    };
                }

            </script>
            <?
        }
    }

<?php

    class MenuSuperiorPadrao extends InterfaceScript
    {
        public function render()
        {
            $objMenu = Registry::get('Menu', false);
            if ($objMenu == null)
            {
                $objMenu = new Menu();
            }

            $objMenu->criarMenus();
            $path = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "libs/spry_menubar";

            ?>

            <script type="text/javascript">

                var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {
                    imgDown: "<?=$path;?>/SpryMenuBarDownHover.gif",
                    imgRight: "<?=$path;?>/SpryMenuBarRightHover.gif"
                });
                $(".table_geral").css('margin-top', '0px');
                $("#topo_pagina").css('display', 'block');
                $("#MenuBar1").css('display', 'block');

            </script>

            <br/>
            <?
        }
    }

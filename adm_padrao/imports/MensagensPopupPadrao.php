<?php

    class MensagensPopupPadrao extends InterfaceScript
    {
        public function render()
        {
            ?>

            <center>

                <?

                    if (Helper::GET("msgSucesso"))
                    {
                        Helper::imprimirMensagem(nl2br(Helper::GET("msgSucesso")), MENSAGEM_OK);
                        Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG_ATUALIZANDO, TEMPO_PADRAO_FECHAR_DIALOG, false);
                    }
                    elseif (Helper::GET("msgErro"))
                    {
                        Helper::imprimirMensagem(nl2br(Helper::GET("msgErro")), MENSAGEM_ERRO);
                        Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, TEMPO_PADRAO_FECHAR_DIALOG, false);
                    }

                ?>

            </center>

            <?
        }
    }

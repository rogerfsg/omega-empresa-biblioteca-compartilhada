<?php

class MensagensPadrao extends InterfaceScript
{
public function render()
{
$mensagem = "";
if (Helper::GET("msgSucesso"))
{
    $mensagem = Helper::GET("msgSucesso");
}
elseif (Helper::GET("msgErro"))
{
    $mensagem = Helper::GET("msgErro");
}
if (isset($_SERVER["HTTP_REFERER"]))
{
    $ultimoReferer = $_SERVER["HTTP_REFERER"];
}
$esconder = true;
if (strlen(trim($mensagem)))
{
    if (Helper::SESSION("ultima_mensagem") == $mensagem && Helper::SESSION("ultimo_referer") == $ultimoReferer)
    {
        $esconder = true;
    }
    else
    {
        Helper::setSession("ultima_mensagem", $mensagem);
        Helper::setSession("ultimo_referer", $_SERVER["HTTP_REFERER"]);
    }
}
else
{
    Helper::clearSession("ultima_mensagem");
    $esconder = true;
}

?>

<div id="caixaDeMensagemSucesso" class="centerDivMensagem okDiv" style="display: none;">
    <table width="100%" class="okTabela" id="tabelaMensagem">
        <tr>
            <td height="70px" width="13%">
                <img src="<?= Helper::acharRaiz() ?>adm/imgs/padrao/imgSucesso.png"/>
            </td>
            <td width="87%">
                <span class="textoDivMensagem" id="textoMensagemSucesso"><?= $mensagem ?></span>
            </td>
        </tr>
        <tr>
            <td height="30px" colspan="2" align="center" valign="middle">
                <input type="button"
                       onclick="javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'"
                       id="botao_sucesso_ok" value=" OK " class="botoes_form "/>
            </td>
        </tr>
    </table>
</div>

<div id="caixaDeMensagemErro" class="centerDivMensagem erroDiv" style="display: none;">
    <table width="100%" class="erroTabela" id="tabelaMensagem">
        <tr>
            <td height="70px" width="13%">
                <img src="<?= Helper::acharRaiz() ?>adm/imgs/padrao/imgErro.png"/>
            </td>
            <td width="87%">
                <span class="textoDivMensagem" id="textoMensagemErro"><?= $mensagem ?></span>
            </td>
        </tr>
        <tr>
            <td height="30px" colspan="2" align="center" valign="middle">
                <input type="button"
                       onclick="javascript:document.getElementById('caixaDeMensagemErro').style.display='none'"
                       id="botao_erro_ok" value=" OK " class="botoes_form "/>
            </td>
        </tr>
    </table>
</div>

<div id="caixaDeMensagemDialogo" class="centerDivMensagem dialogo" style="display: none;">
    <table width="100%" class="okTabela" id="tabelaMensagem">
        <tr>
            <td height="70px" width="13%">
                <img src="<?= Helper::acharRaiz() ?>adm/imgs/padrao/imgInfo.png"/>
            </td>
            <td width="87%">
                <span class="textoDivMensagem" id="textoMensagemDialogo"><?= $mensagem ?></span>
            </td>
        </tr>
        <tr>
            <td height="30px" colspan="2" align="center" valign="middle">
                <input type="button"
                       id="botao_dialogo_sim" value=" Sim " class="botoes_form "/>
                <input type="button"
                       onclick="javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'"
                       id="botao_dialogo_nao" value=" Não " class="botoes_form "/>
            </td>
        </tr>
    </table>
</div>

<script language="javascript">

    $(function ()
    {
        $("#caixaDeMensagemSucesso").draggable();
    });
    $(function ()
    {
        $("#caixaDeMensagemErro").draggable();
    });
    $(function ()
    {
        $("#caixaDeMensagemDialogo").draggable();
    });

</script>

<? if (strlen(trim(Helper::GET("msgSucesso"))) > 0 && !strlen($esconder)) { ?>

<script language="javascript">

    document.getElementById("caixaDeMensagemSucesso").style.display = "block";
    document.getElementById("botao_sucesso_ok").focus();

</script>

<? } ?>

<? if (strlen(trim(Helper::GET("msgErro"))) > 0 && !strlen($esconder)) { ?>

<script language="javascript">

    document.getElementById("caixaDeMensagemErro").style.display = "block";
    var botaoOk = document.getElementById("botao_erro_ok");
    if (botaoOk != null)
    {
        botaoOk.focus();
    }

</script>

    <? }
    }
    }

<?php

    class TopoPaginaPadrao extends InterfaceScript
    {
        public function render()
        {
            ?>
            <table id="topo_pagina" width="100%" height="40" align="left" cellpadding="0" cellspacing="0"
                   style="display: none; background-image:url(imgs/especifico/fundo_cabecalho.jpg); background-repeat:repeat; background-position:bottom;"
                   bgcolor="#ff6600">
                <tr>
                    <td align="left" id="td_informacoes_cabecalho" valign="middle"
                        style="padding-top:5px; padding-bottom:5px;">

                        <table class="tabela_cabecalho_usuario">

                            <tr>
                                <td colspan="2">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Usuário:</b> <?= InterfaceSeguranca::getNome() ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Tipo:</b> <?= InterfaceSeguranca::getUsuarioTipo(); ?>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td align="right" id="td_botoes_cabecalho" valign="bottom" width="30%">

                        <div style="width:330px; text-align:right;">

                            <div class="botao_topo_pagina">

                                <?

                                    $objLink = new Link();
                                    $objLink->alturaGreyBox = 175;
                                    $objLink->larguraGreyBox = 450;
                                    $objLink->tituloGreyBox = "Alterar Senha";
                                    $objLink->url = "popup.php?page=alterar_senha&tipo=pages&navegacao=false";
                                    $objLink->demaisAtributos = array("onmouseover" => "tip('Clique aqui para alterar sua senha.', this);", "onmouseout" => "notip();");
                                    $objLink->cssClass = "link_topo";
                                    $objLink->label = " ALTERAR SENHA";
                                    $objLink->numeroNiveisPai = 0;

                                ?>

                                <?= $objLink->montarLink(); ?>

                            </div>

                            <div class="botao_topo_pagina">

                                <a href="index.php" target="_self" class="link_topo">PÁGINA INICIAL</a>

                            </div>

                            <div class="botao_topo_pagina" style="width:60px; height:25px;">

                                <a href="actions.php?class=Seguranca&action=logout" target="_self" class="link_topo">SAIR</a>

                            </div>

                        </div>
                    </td>
                    <td align="right" id="td_botoes_cabecalho" valign="bottom">
                        &nbsp;
                    </td>
                </tr>
            </table>

            <? Helper::imprimirComandoJavascript("document.getElementById('td_informacoes_cabecalho').width = screen.width * .7") ?>
            <? Helper::imprimirComandoJavascript("document.getElementById('td_botoes_cabecalho').width = screen.width * .3") ?>
            <?
        }
    }

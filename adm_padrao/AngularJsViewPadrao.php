<?php

//Pagina Wrapper para Popups
    class AngularJsViewPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                $viewPath = Helper::GET("path");

                if (!$viewPath)
                {
                    Helper::imprimirMensagem(I18N::getExpression("View não definida."));
                }
                else
                {
                    Helper::includePHP(1, PATH_RELATIVO_PROJETO . "/" . Helper::GET("tipo") . "/" . Helper::GET("page") . "/view.php");
                }
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

<?php

    class DownloadPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'OCTET-STREAM');
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
                Helper::includePHPDoProjeto('adm/imports/instancias.php');
                $path = Helper::GET("arquivo");
                $strRet = null;

                if (strlen($path))
                {
                    if (!file_exists($path))
                    {
                        $path = "../{$path}";
                    }

                    if (file_exists($path))
                    {
                        if (Helper::GET("arquivo_saida_sem_extensao"))
                        {
                            $nomeArquivoSaida = Helper::GET("arquivo_saida_sem_extensao") . "." . Helper::getExtensaoDoArquivo($path);
                            $objDownload = new Download($path, "application/octet-stream", "attachment", $nomeArquivoSaida);
                        }
                        else
                        {
                            $objDownload = new Download($path);
                        }

                        $objDownload->df_download();

                    }
                    else
                    {
                        //204 = No content
                        //http_response_code(204);
                        $msg = new Mensagem(
                            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                            "O arquivo requisitado não foi encontrado"
                        );
                        $log = Registry::get('HelperLog');
                        $log->gravarLogEmCasoErro($msg);
                        http_response_code(400);
                        $strRet = json_encode($msg);
                        header("Content-type: application/json; charset=iso-8859-1", true);
                        echo $strRet;
                    }
                }
                else
                {
                    if (Helper::POSTGET("class") != null)
                    {
                        $classe = Helper::POSTGET("class");
                        $action = Helper::POSTGET("action");

                        $objSeguranca = Registry::get('Seguranca');
                        if (Registry::contains($classe))
                        {
                            $obj = Registry::get($classe);
                        }
                        else
                        {
                            $obj = call_user_func_array(array($classe, "factory"), array());
                            if (!is_object($obj))
                            {
                                throw new NotImplementedException ("Factory não foi definida para a classe $classe");
                            }
                        }


                        $statusCode = 200;
                        if ($objSeguranca->isAcaoRestrita($classe, $action))
                        {
                            if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmAcoesDoUsuarioCorrente($classe, $action))
                            {
                                //403 = Forbidden

                                $retorno = new Mensagem(
                                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                    "Deve ser realizado o login para essa ação. $classe::$action");
                                $statusCode = 403;
                            }
                        }

                        if ($statusCode == 200)
                        {

                            $retorno = call_user_func(array($obj, $action));
                        }
                        if ($retorno != null && !$retorno->ok())
                        {
                            header("Content-type: application/json; charset=iso-8859-1", true);
                            $strRet = json_encode($retorno);
                            $log = Registry::get('HelperLog');
                            $log->gravarLogEmCasoErro($retorno, "Falha download!");
                            HelperLog::logErro($strRet);

                            echo $strRet;
                            $statusCode = 400;
                            http_response_code($statusCode);
                            HelperLog::verbose($strRet);
                            HelperLog::logDownload($strRet);
                        }
                        else
                        {
                            HelperLog::verbose("OK");
                            HelperLog::logDownload("OK");
                        }
                    }
                    else
                    {
                        if (Helper::GET("tipo_download") == "csv")
                        {
                            $conteudoCSV = Helper::POST("texto_recuperacao");
                            $nomeArquivo = Helper::POST("arquivo");

                            $objDownload = new Download($nomeArquivo);
                            print $objDownload->ds_download($conteudoCSV);
                        }
                    }
                }
                Database::closeAll();
                HelperRedis::closeAll();
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();
                $msg = new Mensagem(null, null, $exc);
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                http_response_code(400);
                header("Content-type: application/json; charset=iso-8859-1", true);
                $strRet = json_encode($msg);
                HelperLog::logDownload($strRet);
                HelperLog::verbose(null, $exc);
            }
        }
    }

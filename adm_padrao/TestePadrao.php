<?php

    class TestePadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $classe = Helper::POSTGET("class");
                $action = Helper::POSTGET("action");

                $obj = call_user_func_array(array($classe, "factory"), array());

                //chama a funcao de nome "$action"
                $retorno = call_user_func(array($obj, $action));
                echo $retorno;
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }



<?php

    class XmlPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'XML');
        }

        public function render()
        {
            try
            {
                $objSeguranca = Registry::get('Seguranca');

                if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmPaginasDoUsuarioCorrente())
                {
                    Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                }
                else
                {
                    if (!(!Helper::GET("tipo") || !Helper::GET("page")))
                    {
                        print("<?xml version=\"1.0\"?>");

                        Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                    }
                }
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);

                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

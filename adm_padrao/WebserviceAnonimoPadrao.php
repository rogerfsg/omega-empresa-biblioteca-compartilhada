<?php

    class WebserviceAnonimoPadrao extends InterfaceScript
    {
        private $actionsPermitidas;
        public function __construct($actionsPermitidas)
        {
            parent::__construct(true, 'JSON');
            $this->actionsPermitidas=$actionsPermitidas;
        }

        public function render()
        {
            try
            {
                Registry::add(new HelperLog());

                if (Helper::POSTGET("class") != null)
                {
                    $classe = Helper::POSTGET("class");
                    $action = Helper::POSTGET("action");
                }
                else
                {
                    throw new InvalidArgumentException("Deve ser realizado parametros invalidos.");
                }
                $sear = array_search($action, $this->actionsPermitidas);
                if( $sear===false){
                    $retorno = Mensagem::factoryAcessoNegado();
                } else {
                    $obj = call_user_func_array(array($classe, "factory"), array());
                    if (!is_object($obj))
                    {
                        throw new NotImplementedException ("Factory n�o foi definida para a classe $classe");
                    }
                    $retorno = call_user_func(array($obj, $action));
                }

                if ($retorno == null)
                {
                    $retorno = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "A fun��o n�o retornou nenhum valor. {$classe}::{$action}");
                }

                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($retorno);

                if($_SERVER["HTTP_X_APPLICATION_TYPE"] == "AngularJS")
                {
                    Helper::corrigirObjetoParaJsonEncode($retorno);
                }

                $strRet = json_encode($retorno);
                echo $strRet;

                HelperLog::verbose($strRet);
                HelperLog::webservice($strRet);


                HelperRedis::closeAll();
            }
            catch (Exception $exc)
            {
                HelperRedis::closeAll();

                $msg = new Mensagem(null, null, $exc);
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                $strRet = json_encode($msg);
                echo $strRet;

                HelperLog::verbose(null, $exc);

                HelperLog::webservice(print_r($exc, true));
            }
        }
    }

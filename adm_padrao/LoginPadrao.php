<?php

    class LoginPadrao extends InterfaceScript
    {
        private $parametros = null;

        public function __construct($parametros = null)
        {
            parent::__construct(true);
            $this->parametros = $parametros;
        }

        public function render()
        {
            try
            {
                $identificadorSessao = "";

                Helper::sessionStart();
                $singleton = SessionRedis::getSingleton();
                $singleton->loadSession();
                ?>

                <html>
                <head>
                    <title><?= Helper::getTituloDasPaginas(); ?></title>

                    <?= Helper::getTagDoFavicon(); ?>

                    <?= Helper::carregarArquivoCss(1, "adm/css/", "padrao") ?>
                    <?= Helper::carregarArquivoCss(1, "adm/css/", "login") ?>

                    <?= Javascript::importarTodasAsBibliotecas(); ?>

                </head>
                <body>


                <div class="externalcontainer">

                    <div id="content_wrapper">

                        <div class="with_arm">

                            <form action="actions.php?class=Seguranca&action=login" class="login" method="post"
                                  name="formulario_login"
                                  id="formulario_login" onsubmit="return true;">

                                <?
                                    if ($this->parametros != null && !empty($this->parametros))
                                    {
                                        foreach ($this->parametros as $nome => $valor)
                                        {
                                            ?>
                                            <input type="hidden" name="<?= $nome; ?>" value="<?= $valor; ?>"/>
                                            <?
                                        }
                                    }
                                ?>

                                <h1>DADOS <span>DE ACESSO</span> <strong><?= Helper::getTituloDasPaginas(); ?></strong>
                                </h1>
                                <h5>Área restrita, só é permitido o acesso a usuários autorizados.<br/>
                                    Entre com seus dados de acesso ao sistema:</h5>
                                <span style="color:#FF0000;"></span>
                                <label>Email: <input name="txtLogin" id="txtLogin" class="usepws" value=""
                                                     onfocus="this.value='';"
                                                     type="text"></label>

                                <label>Senha: <input name="txtSenha" id="txtSenha" class="usepws" value=""
                                                     onfocus="this.value='';"
                                                     type="password"></label>

                                <label style="text-align: right; width: 100%;">
                                    <input name="Submit" value="Submit" src="imgs/padrao/login_btn.gif" type="image">
                                </label>
                                <br class="specer">

                                <p> Se você esqueceu sua senha,

                                    <?

                                        $objLink = new Link();
                                        $objLink->alturaGreyBox = 500;
                                        $objLink->larguraGreyBox = 500;
                                        $objLink->tituloGreyBox = "Lembrar Senha";
                                        $objLink->url = "popup.php?tipo=pages&page=lembrar_senha&navegacao=false";
                                        $objLink->demaisAtributos = array("onmouseover" => "tip('Clique aqui para receber a senha em seu email.', this);", "onmouseout" => "notip();");
                                        //$objLink->cssClass = "link_padrao";
                                        $objLink->label = "clique aqui";

                                    ?>

                                    <?= $objLink->montarLink(); ?>

                                    para recuperá-la.</p>

                            </form>

                        </div>

                    </div>

                </div>


                <?

                    //Helper::includePHP(1, "adm/imports/mensagens.php");
                    $mp = new MensagensPadrao();
                    $mp->render();
                ?>

                </body>

                </html>

                <?
                Database::closeAll();
                HelperRedis::closeAll();
            }
            catch (Exception $ex)
            {
                Database::closeAll();
                HelperRedis::closeAll();
                InterfaceReportarErro::reportarExcecao($ex);
                HelperLog::verbose("login", null, $ex);
            }
        }
    }

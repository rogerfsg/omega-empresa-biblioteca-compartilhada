<?php

    class ActionsPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'HTML_ACTION');
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $classe = Helper::POSTGET("class");
                $action = Helper::POSTGET("action");

                $objSeguranca = Registry::get('Seguranca');
                if ($classe == 'Seguranca' || $classe == 'Seguranca_Pagamento')
                {
                    $obj = $objSeguranca;
                }
                else
                {
                    $obj = call_user_func_array(array($classe, "factory"), array());
                    if (!is_object($obj))
                    {
                        throw new NotImplementedException (I18N::getExpression("Factory não foi definida para a classe {0}", $classe));
                    }
                }
                if ($action == "add")
                {
                    $retorno = $obj->__actionAdd();
                }
                elseif ($action == "add_ajax")
                {
                    $retorno = $obj->__actionAddAjax();
                }
                elseif ($action == "edit")
                {
                    $retorno = $obj->__actionEdit();
                }
                elseif ($action == "edit_ajax")
                {
                    $retorno = $obj->__actionEdit();
                }
                elseif ($action == "remove")
                {
                    $retorno = $obj->__actionRemove();
                }
                elseif ($action == "login")
                {
                    $retorno = $obj->__actionLogin(
                        $obj->getParametrosLogin(),
                        Helper::POST("txtSenha"));
                }
                elseif ($action == "loginWebservice")
                {
                    $retorno = $obj->login(
                        $obj->getParametrosLogin(),
                        Helper::POST("txtSenha"));
                }
                elseif ($action == "logout")
                {
                    $retorno = $obj->__actionLogout();
                }
                else
                {
                    //chama a funcao de nome "$action"
                    $retorno = call_user_func(array($obj, $action));
                }

                if (is_array($retorno) && $retorno[0])
                {
                    HelperLog::verbose(print_r($retorno, true));
                    Helper::mudarLocation($retorno[0]);
                }
                else
                {
                    throw new Exception(I18N::getExpression("Retorno inválido da chamada da funçãoo, deve ser retornado um array"));
                }
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);

                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }



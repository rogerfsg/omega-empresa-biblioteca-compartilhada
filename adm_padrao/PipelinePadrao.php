<?php

class PipelinePadrao extends InterfaceScript
{
    private $parametros = array();

    public function __construct($parametros = null)
    {
        parent::__construct(true, 'JSON');
        if ($parametros != null)
        {
            $this->parametros = $parametros;
        }
        $p = Helper::acharRaizWorkspace() . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS;

        $this->includeAll($p);
    }

    public function includeAll($dir)
    {
        $files = scandir($dir);

        foreach ($files as $f)
        {
            if (is_dir($f) && $f != "." && $f != "..")
            {
                $this->includeAll($dir . $f . "/");
            }
            else
            {
                if (is_file($dir . $f) && Helper::endsWith($f, ".php"))
                {
                    if ($f != "sessao.php")
                    {
                        include_once $dir . $f;
                    }
                }
            }
        }
    }
    public function render()
    {

        try{
            //executavel.php [corporacao] [class] [action] [GET]
            if(php_sapi_name() != 'cli'){
                echo "Esse arquivo so pode ser chamando por linha de comando";
                exit();
            }
            $argc = $this->parametros["argc"];
            $argv = $this->parametros["argv"];

            if($argc < 4){
                echo "Chamada inválida. Ex de chamada correta: executavel.php [corporacao] [class] [action] [GET]*. Sendo GET os parametros (opcionais) separados com ','";
                exit();
            }

            $idProcesso = getmypid();

            $classe = $argv[2];
            $action = $argv[4];
            $categoria = $argv[5];

            Helper::criaArquivoComOConteudo(
                MultiRun2::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe."_".$categoria ),
                ";".$idProcesso,
                false);
            $objSeguranca = Registry::get('Seguranca');
            if ($classe == 'Seguranca' || $classe == 'Seguranca_Pagamento')
            {
                $obj = $objSeguranca;
            }
            else
            {
                $obj = call_user_func_array(array($classe, "factory"), array());
            }
            if (!is_object($obj))
            {
                throw new NotImplementedException ("Factory nÃ£o foi definida para a classe $classe");
            }

            //chama a funcao de nome "$action"
            $retorno = call_user_func(array($obj, $action));

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($retorno);

            $strRet = json_encode($retorno);

            echo $strRet;

            HelperLog::verbose($strRet);

            Database::closeAll();
            HelperRedis::closeAll();

        } catch (Exception $ex) {
            Database::closeAll();
            HelperRedis::closeAll();
            $msg = new Mensagem(null, null, $exc);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            $strRet = json_encode($msg);
            echo $strRet;
            HelperLog::verbose(null, $exc);
        }

    }
}

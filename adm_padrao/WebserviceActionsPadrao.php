<?php

    class WebserviceActionsPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true, 'JSON');
        }

        public function render()
        {
            try
            {
                ob_start();

                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                Helper::includePHPDoProjeto('recursos/php/cache_webservice.php');

                if (Helper::POSTGET("class") != null)
                {
                    $classe = Helper::POSTGET("class");
                    $action = Helper::POSTGET("action");
                    $registroDaOperacao = Helper::POSTGET("id1");
                }
                else
                {
                    throw new InvalidArgumentException("Deve ser realizado parametros invalidos.");
                }

                $objSeguranca = Registry::get('Seguranca');
                if (Registry::contains($classe))
                {
                    $obj = Registry::get($classe);
                }
                else
                {
                    $obj = call_user_func_array(array($classe, "factory"), array());
                    if (!is_object($obj))
                    {
                        throw new NotImplementedException ("Factory n�o foi definida para a classe $classe");
                    }
                }
                $validade = true;
                if ($objSeguranca->isAcaoRestrita($classe, $action))
                {
                    if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmAcoesDoUsuarioCorrente($classe, $action))
                    {
                        $retorno = new Mensagem(
                            PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                            "Deve ser realizado o login para essa a��o. $classe::$action");
                        $validade = false;
                    }
                }

                if ($validade)
                {
                    if ($action == "loginWebservice")
                    {
                        $retorno = $obj->login(
                            array("email" => Helper::POSTGET("txtLogin")),
                            Helper::POSTGET("txtSenha"));
                    }
                    elseif ($action == "logout")
                    {
                        $obj->__actionLogout();
                    }
                    else
                    {
                        call_user_func(array($obj, $action));
                    }
                }

                $out2 = ob_get_contents();
                ob_end_clean();
                echo $out2;

                $singletonCacheWebservice = SingletonCacheWebservice::getSingleton();
                $singletonCacheWebservice->setCachingCurrentWebservice($out2);

                HelperLog::verbose($out2);
                HelperLog::webservice($out2);

                Database::closeAll();
                HelperRedis::closeAll();
            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();
                $msg = new Mensagem(null, null, $exc);
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                $strRet = json_encode($msg);
                echo $strRet;

                HelperLog::verbose(null, $exc);
            }
        }
    }

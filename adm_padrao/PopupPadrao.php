<?php

//Pagina Wrapper para Popups
    class PopupPadrao extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        public function render()
        {
            try
            {
                Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');

                $tipo = Helper::GET("tipo");
                $pagina = Helper::GET("page");

                if (!strlen($tipo) || !strlen($pagina))
                {
                    throw new InvalidArgumentException("Parâmetros inválidos");
                }
                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>
            <head>
                <title><?= Helper::getTituloDasPaginas(); ?></title>

                <?= Helper::getTagDoFavicon(); ?>

                <?= Helper::carregarArquivoCss(1, "adm/css/", "padrao"); ?>

                <?= Javascript::importarTodasAsBibliotecas(); ?>

            </head>

            <body leftmargin="0" topmargin="0" id="body_identificator">

            <?

                Helper::includePHP(1, PATH_RELATIVO_PROJETO . "/imports/mensagens_popup.php");

                $objSeguranca = Registry::get('Seguranca');
                $passouSeguranca = false;

                $autenticado = false;
                if (!in_array($pagina, Seguranca::$paginasExcecao))
                {
                    $objSeguranca = Registry::get('Seguranca');

                    if (!$objSeguranca || !$objSeguranca->verificarPermissaoEmPaginasDoUsuarioCorrente())
                    {
                        Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
                        $autenticado = false;
                    }
                    else
                    {
                        $autenticado = true;
                    }
                }
                else
                {
                    $autenticado = true;
                }
                if ($autenticado)
                {
                    if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                    {
                        if (($paginaInicial = Seguranca::getPaginaInicialDoUsuario()) != null)
                        {
                            Helper::includePHP(1, PATH_RELATIVO_PROJETO . "{$paginaInicial}");
                        }
                        else
                        {
                            Helper::includePHP(1, PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO);
                        }
                    }
                    else
                    {
                        Helper::includePHP(1, PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                    }
                    if (Helper::GET("dialog"))
                    {
                        Helper::imprimirComandoJavascript(
                            "$(document).ready(function(){setTimeout(\"autoRedimensionarDialog()\", 1000);})");
                    }

                    if (Helper::GET("navegacao") != null && Helper::GET("navegacao") != false)
                    {
                        ?>
                        <fieldset class="fieldset_list">
                            <legend class="legend_list"><?= I18N::getExpression("Navegação"); ?></legend>

                            <a href="javascript:void(0);" onclick="javascript:history.go(-1);" class="link_padrao">&lt;-
                                Voltar</a>
                            &nbsp;&nbsp;
                            <a href="javascript:void(0);" onclick="javascript:history.go(+1);" class="link_padrao">Avançar
                                -&gt;</a>
                        </fieldset>
                        <?
                    }

                    Helper::includePHP(false, PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                }

            ?>

            </body>
                <?
            }
            catch (Exception $exc)
            {
                HelperLog::verbose(null, $exc);
                InterfaceReportarErro::reportarExcecao($exc);
            }
        }
    }

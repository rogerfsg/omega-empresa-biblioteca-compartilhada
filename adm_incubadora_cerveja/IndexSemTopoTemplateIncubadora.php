<?php

    class IndexSemTopoTemplateIncubadora extends InterfaceScript
    {
        public function __construct()
        {
            parent::__construct(true);
        }

        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
            I18N::loadLanguage();
        }

        public function render()
        {
            try
            {
                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html style="overflow: hidden;">
                    <head>

                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                        <?= Helper::getTagDoFavicon(); ?>

                        <?= Javascript::importarBibliotecasIncubadoraDaCervejaCabecalho(); ?>

                        <meta property="og:url" content="https://incubadoradacerveja.com.br">
                        <meta property="og:title" content="IncubadoraDaCerveja.com.br" />
                        <meta property="og:type" content="beer" />
                        <meta property="og:locale" content="pt_BR">
                        <meta property="og:site_name" content="Incubadora da Cerveja">
                        <meta property="og:image" content="https://incubadoradacerveja.com.br/arte/logo-1024.png" >
                        <meta property="og:description" content="Incube sua cerveja ou fa�a sua festa. Qual perfil � o seu? Degustador, cigano, cervejaria ou brew pub?">
                        <meta property="og:image:type" content="image/jpeg" />
                        <meta property="og:image:width" content="1024" />
                        <meta property="og:image:height" content="1024" />
                        <meta property="fb:app_id" content="173030516679427">
                    </head>

                    <body class='contrast-red without-footer incubadora-da-cerveja'>

                        <!-- Fake page loading -->
                        <div id="fakeLoader"></div>

                        <!-- Wrapper -->
                        <div class="wrapper" id="wrapper" style="overflow: hidden;">

                            <?php




                            ?>

                            <?php

                                if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                {
                                    Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . 'paginas/home.php');
                                }
                                else
                                {
                                    Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                }

                            ?>


                        </div>

                        <?= Javascript::importarBibliotecasIncubadoraDaCervejaRodape(); ?>

                    </body>

                </html>

                <?

                Database::closeAll();
                HelperRedis::closeAll();
                HelperLog::verbose("OK!");

            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $json = InterfaceReportarErro::reportarExcecao($exc);
                HelperLog::verbose(null, $json);

            }

        }

    }

?>
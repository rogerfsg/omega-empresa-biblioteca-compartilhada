<?php

    class IndexIncubadora extends InterfaceScript
    {

        public function __construct()
        {
            parent::__construct(true);
        }

        
        public function beforeHeader()
        {
            Helper::includePHPDaBibliotecaCompartilhada('imports/sessao.php');
            I18N::loadLanguage();
        }

        public function render()
        {
            try
            {
                ?>

                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
                        "http://www.w3.org/TR/html4/frameset.dtd">
                <html>
                <head>
                    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

                    <?= Helper::getTagDoFavicon(); ?>

                    <?= Javascript::importarTodasAsBibliotecasProjetoFlatty(); ?>
                    <?= Helper::carregarCssPagamentoFlatty(); ?>

                    <?= Javascript::importarBibliotecaBootstrapSwitch() ?>
                    <?= Javascript::importarBibliotecaAngularJS(); ?>
                    <?= Javascript::importarModulosDoAngularJS(Javascript::$MODULOS_ANGUJARJS_TODOS); ?>
                    <?= Javascript::importarPluginsDoAngularJS(); ?>

                    <?= Javascript::importarAngularIncubadoraApp(); ?>
                    <?= Javascript::importarBibliotecaOmegaAngularJS(Javascript::OMEGA_ANGULARJS_THEME_NEON); ?>


                    <meta property="og:url" content="https://incubadoradacerveja.com.br">
                    <meta property="og:title" content="IncubadoraDaCerveja.com.br" />
                    <meta property="og:type" content="beer" />
                    <meta property="og:locale" content="pt_BR">
                    <meta property="og:site_name" content="Incubadora da Cerveja">
                    <meta property="og:image" content="https://incubadoradacerveja.com.br/arte/logo-1024.png" >
                    <meta property="og:description" content="Incube sua cerveja ou fa�a sua festa. Qual perfil � o seu? Degustador, cigano, cervejaria ou brew pub?">
                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:width" content="1024" />
                    <meta property="og:image:height" content="1024" />
                    <meta property="fb:app_id" content="173030516679427">

                </head>

                <body class='contrast-red without-footer omega-app-body'>

                <?php

                    $mf = new  MensagensIncubadora();
                    $mf->render();

//                    $ref = new ReportarErroFlatty();
//                    $ref->render();

                    Ajax::imprimirCorpoDaDivDeRetornoAjax();

                    include("imports/topo_pagina.php");

//                    $mcf = new ModalCarregandoFlatty();
//                    $mcf->render();
                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . "imports/login_facebook.php");
                ?>
                <div id="divAlterarSenha">
                </div>

                <div id='wrapper'>


                    <? include("imports/menu_esquerdo.php"); ?>
                    <section id='content'>


                        <div class='container'>
                            <div class='row' id='content-wrapper'>
                                <div class='col-xs-12'>
                                    <?php

                                        $autenticado = false;
//                                        $objSeguranca = Registry::get('Seguranca');
//
//                                        if (!$objSeguranca || !$objSeguranca->verificaPermissao(Helper::GET("tipo"), Helper::GET("page")))
//                                        {
//                                            Helper::imprimirMensagem(MENSAGEM_DE_NAO_PERMISSAO_DE_ACESSO, MENSAGEM_ERRO);
//                                            $autenticado = false;
//                                            Helper::mudarLocation("login.php");
//                                        }
//                                        else
//                                        {
//                                            $autenticado = true;
//                                        }
//
//                                        if ($autenticado)
//                                        {
                                            if (Helper::GET("tipo") == null || Helper::GET("page") == null)
                                            {
                                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . PAGINA_INICIAL_PADRAO);
                                            }
                                            else
                                            {
                                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                                            }

//                                        }
                                    ?>
                                </div>
                            </div>

                            <?

                                Helper::includePHPDoProjeto(PATH_RELATIVO_PROJETO . "imports/rodape.php"); ?>
                        </div>
                    </section>
                </div>


                </body>
                </html>
                <?
                Database::closeAll();
                HelperRedis::closeAll();


                HelperLog::verbose("OK!");

            }
            catch (Exception $exc)
            {
                Database::closeAll();
                HelperRedis::closeAll();

                $json = InterfaceReportarErro::reportarExcecao($exc);
                HelperLog::verbose(null, $json);
            }
        }
    }
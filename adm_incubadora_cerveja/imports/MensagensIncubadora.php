<?php

class MensagensIncubadora extends InterfaceScript
{
const SUCESSO = 1;
const ERRO = 2;
const INFO = 3;
const WARNING = 4;

public function render()
{
$mensagem = null;

$tipo = null;
if (Helper::GET("msgSucesso"))
{
    $mensagem = Helper::GET("msgSucesso");
    $tipo = MensagensIncubadora::SUCESSO;
}
elseif (Helper::GET("msgErro"))
{
    $mensagem = Helper::GET("msgErro");
    $tipo = MensagensIncubadora::ERRO;
}
elseif (Helper::GET("msgWarning"))
{
    $mensagem = Helper::GET("msgWarning");
    $tipo = MensagensIncubadora::WARNING;
}
elseif (Helper::GET("msgInfo"))
{
    $mensagem = Helper::GET("msgInfo");
    $tipo = MensagensIncubadora::INFO;
}
if ($mensagem != null)
{
    $mensagem = urldecode($mensagem);
}
?>

<div class="bootbox modal fade in" id="caixaDeMensagemSucesso"
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content  ">
            <div class="modal-body">

                <div class="bootbox-body">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button"
                                class="bootbox-close-button close"
                                onclick="javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'">
                            X
                        </button>

                        <h4>
                            <i class="icon-ok-sign"></i>
                            Sucesso
                        </h4>
                        <?
                        if ($tipo == MensagensIncubadora::SUCESSO)
                        {
                            echo $mensagem;
                        }
                        ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer" style="margin: 0px;">
                <button data-bb-handler="ok"
                        type="button"
                        class="btn btn-primary botao-success"
                        onclick="javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'">
                    OK
                </button>

            </div>
        </div>
    </div>
</div>


<div class="bootbox modal fade in" id="caixaDeMensagemWarning"
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content  ">
            <div class="modal-body">

                <div class="bootbox-body">
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button"
                                class="bootbox-close-button close"
                                onclick="javascript:document.getElementById('caixaDeMensagemWarning').style.display='none'">
                            X
                        </button>

                        <h4>
                            <i class="icon-ok-sign"></i>
                            Aten��o
                        </h4>
                        <?
                        if ($tipo == MensagensIncubadora::WARNING || $tipo == MensagensIncubadora::ERRO)
                        {
                            echo $mensagem;
                        }
                        ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer" style="margin: 0px;">
                <button data-bb-handler="ok"
                        type="button"
                        class="btn btn-primary btn-warning"
                        onclick="javascript:document.getElementById('caixaDeMensagemWarning').style.display='none'">
                    OK
                </button>

            </div>
        </div>
    </div>
</div>

<div class="bootbox modal fade in" id="caixaDeMensagemDialogo"
     tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content  ">
            <div class="modal-body">

                <div class="bootbox-body">
                    <div class="alert alert-info alert-dismissable">
                        <button type="button"
                                class="bootbox-close-button close"
                                onclick="javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'">
                            X
                        </button>

                        <h4>
                            <i class="icon-info-sign"></i>
                            Informa��o
                        </h4>
                        <?
                        if ($tipo == MensagensIncubadora::INFO)
                        {
                            echo $mensagem;
                        }
                        ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer " style="margin: 0px;">
                <button data-bb-handler="ok"
                        type="button"
                        class="btn btn-primary botao-ok"
                        onclick="javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'">
                    OK
                </button>

            </div>
        </div>
    </div>
</div>

<script language="javascript">
    $(function ()
    {
        $("#caixaDeMensagemSucesso").draggable();
    });
    $(function ()
    {
        $("#caixaDeMensagemDialogo").draggable();
    });
</script>

<?
$idModal = null;
switch ($tipo)
{
    case MensagensIncubadora::WARNING:
        $idModal = "caixaDeMensagemWarning";
        break;
    case MensagensIncubadora::ERRO:
        $idModal = "caixaDeMensagemWarning";
        break;
    case MensagensIncubadora::INFO:
        $idModal = "caixaDeMensagemDialogo";
        break;
    case MensagensIncubadora::SUCESSO:
        $idModal = "caixaDeMensagemSucesso";
        break;
    default:
        break;
}
if ($idModal != null)
{
?>
<script language="javascript">
    (function ()
    {
        try
        {
            var $caixa = $('#<?=$idModal?>');
            $caixa.css('display', "block");
            var botaoOk = $caixa.find('.botao-ok');
            if (botaoOk != null)
            {
                botaoOk.focus();
            }
        }
        catch (ex)
        {
            ReportarErroExcecao(ex);
        }
    })();
</script>
    <?
    }
    }
    }
